#ifndef PXIPLCCONNECTION_H
#define PXIPLCCONNECTION_H

/*! Протокольная, сетевая часть взаимодействия с ПЛК !*/

#include <QObject>
#include <QPointer>
#include <QUdpSocket>
#include <QTimer>
#include <QNetworkDatagram>
#include <QVariantList>
#include <QFile>
#include "../../PXI-service/pxi-service/ValueTypes.h"
#include "../../PXI-service/pxi-service/config.h"
#include "../../../PXI-common/led-color.h"
#include "../../../PXI-common/zummer.h"
#include "qttricks/qqmlhelpers.h"

struct StateData
{
    Q_GADGET
//    Q_PROPERTY(int moduleId READ moduleId WRITE setModuleId) // WRITE дает возможность вписать в modelData.parameter, но это все равно не помашает
    Q_PROPERTY(QVariant variantValue READ variantValue)
    Q_PROPERTY(QString title READ title)

public:
    StateData() = default;

    StateData(const QString & title, quint32 address, QVariant value = QVariant()) :
        m_title(title), m_address(address), m_variantValue(value)
    {   }

    QString m_title;
    quint32 m_address{0};
    QVariant m_variantValue;

    QVariant variantValue() const
    { return m_variantValue; }

    QString title() const
    { return m_title; }

//    // этот способ не работает :( // видимо изменяет локальную qml копию
//    Q_INVOKABLE void setModuleId(int moduleId)

};
Q_DECLARE_METATYPE(StateData)

class PXIPlcConnection : public QObject
{
    Q_OBJECT
    SINGLETON(PXIPlcConnection)

    QML_WRITABLE_PROPERTY(QVariantList, statesPackage)
    QML_WRITABLE_PROPERTY(int, testProp)

public:
//    Q_INVOKABLE void setNewValue(int index, float value)
//    {
//        ((StateData *)(m_statesPackage[index].data()))->m_parameter = value;
//        emit statesPackageChanged(m_statesPackage); // этотоже нужно, инча между тем что в UI и в реалии будет ЮОЛЬШАЯ разница

//        for(const auto & cell : m_statesPackage) {
//            qDebug() << cell.value<StateData>().m_parameter;
//        }
//    }

public:
    PXIPlcConnection(QObject * parent = nullptr);
    Q_INVOKABLE void setVariantValue(int index, QVariant value);
    Q_INVOKABLE void logStatesPackage();

private slots:
    void socketHandler();

private:
    const QHostAddress m_hostAddress;
    QPointer<QUdpSocket> m_udpSocket;
    quint16 m_nPort;
    QFile logFile;

    void stateHandler(const QNetworkDatagram &datagram);
    void controlHandler(const QNetworkDatagram &datagram);
    void paramenterHandler(const QNetworkDatagram &datagram);
    void log(const QString &msg);
};

#endif // PXIPLCCONNECTION_H
