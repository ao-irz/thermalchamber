#include "PXIPlcConnection.h"
#include <QVector>
#include <QVariant>
#include <QRandomGenerator>
#include <QApplication>
#include <QFile>
#include <QDateTime>

PXIPlcConnection::PXIPlcConnection(QObject *parent) :
    QObject(parent),
    m_hostAddress(PLC_DESTINATION_ADDRESS),
    m_udpSocket(new QUdpSocket(this))
{
    //    for(int i=0; i<PLC_STATE_FIELDS_NUMBER; ++i)
    //        m_statesPackage.append( QVariant::fromValue(StateData(i));

    //m_statesPackage.replace(8, QVariant::fromValue(StateData(16)));

    if ( QApplication::arguments().count()==1 )
    {
        m_nPort = PLC_DESTINATION_PORT0;

        m_statesPackage.append(QVariant::fromValue(StateData("ChamberId", 28, 0)));

        m_statesPackage.append(QVariant::fromValue(StateData("Код аварии камеры", 34, 0)));

        m_statesPackage.append(QVariant::fromValue(StateData("Текущая темп.", 38, 96.6)));
        m_statesPackage.append(QVariant::fromValue(StateData("Скольз.уставка темп.", 46, 96.6)));
        m_statesPackage.append(QVariant::fromValue(StateData("Режим", 50, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. уставка", 52, 123.4)));
        m_statesPackage.append(QVariant::fromValue(StateData("Огр. скорости изм.темп.", 56, 0.0)));

        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №1", 258, 30)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №2", 262, 31)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №3", 266, 30.7)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №4", 270, 30.9)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №5", 274, 30.33)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №6", 278, 30.44)));

        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №1", 290, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №2", 292, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №3", 294, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №4", 296, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №5", 298, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №6", 300, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("Соединение с камерой", 320, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("Средн. темп. в стойке", 322, 44.1)));

        m_statesPackage.append(QVariant::fromValue(StateData("верх вент-р", 334, 10.1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 338, 2)));
        m_statesPackage.append(QVariant::fromValue(StateData("нижний вент-ра", 340, 20.2)));
        m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 344, 2)));
        m_statesPackage.append(QVariant::fromValue(StateData("Центральный вент-ра", 346, 30.3)));
        m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 350, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("UPS (ИБП)", 378, 0x1)));

        //thermocouples
        int startAddress = 400;
        int offset = 4;
        int val = 30;
        for (int i = 0; i < 20; i++)
        {
            m_statesPackage.append(QVariant::fromValue(StateData(QStringLiteral("Термопара №%1").arg(i + 1), startAddress + i * offset, 77.0)));
        }

        //thermocouples status
        startAddress = 496;
        offset = 2;
        val = 1;
        for (int i = 0; i < 20; i++)
        {
            m_statesPackage.append(QVariant::fromValue(StateData(QStringLiteral("Сост. термопара №%1").arg(i + 1), startAddress + i * offset, val)));
        }
    }
    else
    {
        m_nPort = PLC_DESTINATION_PORT1;

        m_statesPackage.append(QVariant::fromValue(StateData("ChamberId", 28, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("Код аварии камеры", 34, 0)));

        m_statesPackage.append(QVariant::fromValue(StateData("Текущая темп.", 38, 106.6)));
        m_statesPackage.append(QVariant::fromValue(StateData("Скольз.уставка темп.", 46, 106.6)));
        m_statesPackage.append(QVariant::fromValue(StateData("Режим", 50, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. уставка", 52, 100)));
        m_statesPackage.append(QVariant::fromValue(StateData("Огр. скорости изм.темп.", 56, 0.0)));

        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №7", 282, 30.55)));
        m_statesPackage.append(QVariant::fromValue(StateData("Темп. стойки №8", 286, 30.66)));

        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №7", 302, 1)));
        m_statesPackage.append(QVariant::fromValue(StateData("Сост. темп. датч. №8", 304, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("Соединение с камерой", 320, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("Средн. темп. в стойке", 322, 44.1)));

        m_statesPackage.append(QVariant::fromValue(StateData("верх вент-р", 334, 40.4)));
        m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 338, 2)));
        m_statesPackage.append(QVariant::fromValue(StateData("нижний вент-ра", 340, 50.5)));
        m_statesPackage.append(QVariant::fromValue(StateData("Авария вентилятора", 344, 1)));

        m_statesPackage.append(QVariant::fromValue(StateData("UPS (ИБП)", 378, 0x0)));

        //thermocouples
        int startAddress = 400;
        int offset = 4;
        int val = 30;
        for (int i = 0; i < 20; i++)
        {
            m_statesPackage.append(QVariant::fromValue(StateData(QStringLiteral("Термопара №%1").arg(i + 1), startAddress + i * offset, (float)val)));
        }

        //thermocouples status
        startAddress = 496;
        offset = 2;
        val = 1;
        for (int i = 0; i < 20; i++)
        {
            m_statesPackage.append(QVariant::fromValue(StateData(QStringLiteral("Сост. термопара №%1").arg(i + 1), startAddress + i * offset, val)));
        }

    }

    /*QString fileName(QStringLiteral("Emulator_")+QString::number(m_nPort)+ QString("_") + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".log1");
    fileName.prepend("D:/Projects/SparkService/");
    logFile.setFileName(fileName);
    logFile.remove();*/

    m_udpSocket->bind(QHostAddress::Any, m_nPort);
    connect(m_udpSocket, &QUdpSocket::readyRead, this, &PXIPlcConnection::socketHandler);

    log("Emulator created");
}

void PXIPlcConnection::setVariantValue(int index, QVariant value)
{
    if (value.toString().contains("."))
        ((StateData *)(m_statesPackage[index].data()))->m_variantValue = value.toDouble();
    else
        ((StateData *)(m_statesPackage[index].data()))->m_variantValue = value.toInt();
    emit statesPackageChanged(m_statesPackage);
}

void PXIPlcConnection::logStatesPackage()
{
    for(const auto & a : m_statesPackage)
        qDebug() << a.value<StateData>().m_variantValue.type();
}

void PXIPlcConnection::socketHandler()
{
    while (m_udpSocket->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = m_udpSocket->receiveDatagram();
        switch (datagram.data().size())
        {

        case PLC_STATE_REQUEST_LENGTH:
        {
            log("PLC_STATE_REQUEST_LENGTH");
            stateHandler(datagram);
            break;
        }
        case PLC_CONTROL_REQUEST_LENGTH:
        {
            log("PLC_CONTROL_REQUEST_LENGTH");
            controlHandler(datagram);
            break;
        }
        case PLC_PARAMETERS_REQUEST_LENGTH:
        {
            log("PLC_PARAMETERS_REQUEST_LENGTH");
            paramenterHandler(datagram);
            break;
        }
        default:
            log("Error: wrong pachage length ");
        }
    }
}

void PXIPlcConnection::stateHandler(const QNetworkDatagram & datagram)
{   
    log("stateHandler");
    qint16 cmd = 0;
    memcpy(&cmd, (const void *)&datagram.data().data()[0], sizeof(qint16));
    if (cmd != 1)
    {
        log("Error: wrong command");
        return;
    }

    QByteArray stateArray(PLC_STATE_RESPONSE_LENGHT, 0x00);

    for(const auto & cell : m_statesPackage)
    {
        StateData currentCell = cell.value<StateData>();

        switch (currentCell.m_variantValue.type())
        {

        case QVariant::Double:
        {
            float f = currentCell.m_variantValue.toFloat();
            memcpy(&stateArray.data()[currentCell.m_address], (const void *)&f, sizeof(float));
        } break;

        case QVariant::Int:
        {
            int ui = currentCell.m_variantValue.toUInt();
            memcpy(&stateArray.data()[currentCell.m_address], (const void *)&ui, sizeof(qint16));
        } break;
        default:
        {
            break;
        }
        }
    }

    if (m_udpSocket->writeDatagram(datagram.makeReply(stateArray)) != PLC_STATE_RESPONSE_LENGHT)
    //if (m_udpSocket->writeDatagram(QNetworkDatagram(stateArray, m_hostAddress, m_nPort)) != PLC_STATE_RESPONSE_LENGHT)
    {
        log("Error: can't send UDP state response package");
        return;
    }
    else
        log("diagram sent");
}

void PXIPlcConnection::controlHandler(const QNetworkDatagram &datagram)
{
    //! Режим loop
    // Установить температуру камеры равной температурне уставки
    float temp = 0;
    memcpy(&temp, (const void *)&datagram.data().data()[14], sizeof(float));

    quint16 mode = 0;
    memcpy(&mode, (const void *)&datagram.data().data()[0], sizeof(quint16));

    for(auto & a : m_statesPackage) {
        if (a.value<StateData>().m_address == 38) {
            a = QVariant::fromValue( StateData("Текущая темп.", 38, (double)temp));
            continue;
        }
        if (a.value<StateData>().m_address == 52) {
            a = QVariant::fromValue( StateData("Темп. уставка", 52, (double)temp) );
            continue;
        }
        if (a.value<StateData>().m_address == 50) {
            a = QVariant::fromValue( StateData("Режим", 50, (quint16)mode) );
            continue;
        }
    }

    emit statesPackageChanged(m_statesPackage);
}

void PXIPlcConnection::paramenterHandler(const QNetworkDatagram &datagram)
{
    Q_UNUSED(datagram)

}

void PXIPlcConnection::log(const QString& msg)
{
    Q_UNUSED(msg)
    /*if (logFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        logFile.write((QDateTime::currentDateTime().toString("HH:mm:ss ") + msg + "\n").toLocal8Bit());
        logFile.close();
    }*/
}
