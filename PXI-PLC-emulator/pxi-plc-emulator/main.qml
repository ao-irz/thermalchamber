import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 1160
    height: 700
    title: qsTr("PXI-PLC-emulator")

    Column {
        anchors.fill: parent

        GridView {
            id: grid
            width: parent.width
            height: parent.height - pushButton.height
            model: plcConnection.statesPackage
            cellHeight: 65
            cellWidth: 165

            delegate: Rectangle {
                color: "transparent"
                border.color: "white"
                border.width: 1
                height: 60
                width: 160

                Column {
                    Label {
                        anchors.left: parent.left
                        anchors.leftMargin: 4
                        color: "green"
                        text: modelData.title
                    }

                    TextField  {
                        horizontalAlignment: TextInput.AlignHCenter
                        color: "white"
                        selectByMouse: true
                        placeholderText: index
                        text: ((typeof modelData.variantValue === 'number') ?
                                   ((Number.isInteger(modelData.variantValue)) ?
                                        (modelData.variantValue) : (modelData.variantValue.toFixed(2)) ) :
                                   (modelData.variantValue))
                        font.pixelSize: 20
                        clip: true                        
                        onFocusChanged: {
                            //modelData = text // не получится!
                            //modelData.parameter =
                            //console.log(plcConnection.statesPackage[index].parameter)
                            //console.log(index)
                        }
                        onEditingFinished: {
                            //plcConnection.setNewValue(index, text)
                            plcConnection.setVariantValue(index, text)
                            //console.log("new value = ", text)
                            //modelData.parameter = text
                        }
                    }
                }
            }
        }

        Button {
            id: pushButton
            text: "push"
            //anchors.centerIn: parent
            onClicked: plcConnection.logStatesPackage()
        }
    }
}
