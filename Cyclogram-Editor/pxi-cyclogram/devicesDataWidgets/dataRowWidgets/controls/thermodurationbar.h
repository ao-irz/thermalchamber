#ifndef THERMODURATIONBAR_H
#define THERMODURATIONBAR_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QCursor>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QPaintEvent>

#include "../../../global_definitions.h"

class ThermoScheduleSection;

class ThermoDurationBar : public QWidget
{
    Q_OBJECT

  public:
    explicit ThermoDurationBar(QWidget* parent,ThermoScheduleSection* pParentSection);

    void select();
    void unselect();
    //void resizeBar(int newWidth);
    void setEnable(const bool bEnabled);

  signals:
    void startResizeDurationSignal();
    void resizingDurationSignal( float percent );
    void stopResizeDurationSignal();
    void changingTemperatureSignal( int delta );

  protected:
    virtual void mouseMoveEvent(QMouseEvent* e);
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
    virtual void wheelEvent(QWheelEvent* e);
    virtual void leaveEvent(QEvent* e);
    virtual void paintEvent(QPaintEvent *event);

  private:
    QWidget* m_pParent;
    QCursor m_cResizeHorCursor;
    QCursor m_cResizeVerCursor;
    QCursor m_defaultCursor;
    QCursor m_customDefaultCursor;
    ThermoScheduleSection* m_pParentSection;
    GlobalDefinitions::ThermoMode m_nThermoMode;

    QBrush m_heatingUnselectedBrush;
    QBrush m_maintainingUnselectedBrush;
    QBrush m_coolingUnselectedBrush;
    QBrush m_heatingSelectedBrush;
    QBrush m_maintainingSelectedBrush;
    QBrush m_coolingSelectedBrush;
    QBrush m_offSelectedBrush;
    QBrush m_offUnselectedBrush;
    QPen m_penSelected;
    QPen m_penNormal;

    bool m_bCanHorResize;
    bool m_bCanVerResize;
    bool m_bCanAcceptWheel;
    bool m_bHorResizing;
    bool m_bLeftSideResizing;
    bool m_bRightSideResizing;
    bool m_bVerResizing;
    bool m_bSelected;
    bool m_bOnState;

    QPolygon m_pBarPolygon;
    QPolygon m_pSubPolygon;
    int m_nWidgetWidth;
    int m_nWidgetHeight;
    int m_nInitialTemperatureBarHeight;
    int m_nFinalTemperatureBarHeight;
    int m_nPreviousWidth;

    bool isHorResizing(QMouseEvent *e);
    bool isVerResizing(QMouseEvent *e);

  private slots:
    void thermoModeChangedSlot(GlobalDefinitions::ThermoMode);
};

#endif // THERMODURATIONBAR_H
