#ifndef PXIDURATIONBAR_H
#define PXIDURATIONBAR_H

#include <QObject>
#include <QFrame>
#include <QCursor>
#include <QMouseEvent>
#include <QWheelEvent>

#include "../../../global_definitions.h"

class PxiScheduleSection;

class PxiDurationBar : public QFrame
{
    Q_OBJECT

  public:
    explicit PxiDurationBar(QWidget* parent,PxiScheduleSection* pParentSection);

    void select();
    void unselect();
    void resizeBar(int newWidth);
    void setOnState(bool bState);

  signals:
    void startResizeSignal();
    //void resizingSignal(int previousWidth, int newWidth );
    void resizingSignal( float percent );
    void stopResizeSignal();

  protected:
    virtual void mouseMoveEvent(QMouseEvent* e);
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
    virtual void wheelEvent(QWheelEvent* e);
    virtual void leaveEvent(QEvent* e);

  private:
    QWidget* m_pParent;
    QCursor m_sizeHorCursor;
    QCursor m_defaultCursor;
    PxiScheduleSection* m_pParentSection;
    int m_nPreviousWidth;

    QString m_strStyleOnSelected;
    QString m_strStyleOffSelected;
    QString m_strStyleOnUnselected;
    QString m_strStyleOffUnselected;

    bool m_bCanResize;
    bool m_bCanAcceptWheel;
    bool m_bResizing;
    bool m_bSelected;
    bool m_bStateOn;
};

#endif // PXIDURATIONBAR_H
