#ifndef PXIDIOPIN_H
#define PXIDIOPIN_H

#include <QGroupBox>
#include <QComboBox>
#include <QLabel>

#include "../../../common_controls/pxiswitcher.h"

class PxiDioPin : public QGroupBox
{
    Q_OBJECT
  public:

    explicit PxiDioPin(int nPinNumber,
                       QWidget *parent = nullptr);

    int pinNumber() const;
    QString pinMode() const;
    bool pinValue() const;
    void setMode(const QString& strMode);
    void setValue(const bool& bValue);

  signals:
    void valueChanged();

	private:
    QWidget* m_pParent;
		int m_nPinNumber;
    QComboBox* m_pModeBox;
    PxiSwitcher* m_pValueSwitcher;

    static QStringList pinModeValues;
    static QStringList pinModeTexts;

  private slots:
    void toggledSlot(bool);
};

#endif // PXIDIOPIN_H
