#ifndef PXIDEVICEDATAROWDIO_H
#define PXIDEVICEDATAROWDIO_H

#include <QObject>

#include "pxidevicedatarow.h"
#include "controls/pxidiopin.h"

class PxiDeviceDataRowDio : public PxiDeviceDataRow
{
    Q_OBJECT

public:
    PxiDeviceDataRowDio(const int nDeviceIndex,
                       const QString strLabelText,
                       const QPixmap& normalIcon,
                       const QPixmap& selectedIcon,
                       QWidget *parent = nullptr);
  double voltageLogic() const;
  void storePinsArray();

  public slots:
      virtual void clearDetailsWidget() override;


  protected:
    virtual void setupDetailsArea() override;
    virtual void updateDetailsArea() override;
    virtual void storeDataControlContents() override;
    virtual void fillDataControlContents() override;

  protected slots:
      virtual void switchStateChangedSlot(bool) override;
private:

    QComboBox* m_pVoltageLogicBox;
    QList<PxiDioPin*> m_lstPins;
};

#endif // PXIDEVICEDATAROWDIO_H
