#include "thermoschedulesection.h"
#include "../thermodataform.h"

#include <cmath>

ThermoScheduleSection::ThermoScheduleSection(ThermoDataForm* pParentForm, QWidget* pDurationWidget) : QObject(pDurationWidget)
  ,m_pParentForm(pParentForm)
  ,m_pParentFrame(pDurationWidget)
  ,m_pDataBlock(nullptr)
  ,m_bSelected(false)
  ,m_fInitialTemperature(0)
  ,m_fFinalTemperature(0)
  ,m_bEnabled(false)
{
}

ThermoScheduleSection::~ThermoScheduleSection()
{
  delete m_pDataBlock;
  m_pBar->close();
  delete m_pBar;
}

void ThermoScheduleSection::addDataBlock(ScheduleDataBlock* pDataBlock)
{
  m_pDataBlock = pDataBlock;
}

ScheduleDataBlock *ThermoScheduleSection::dataBlock() const
{
  return m_pDataBlock;
}

void ThermoScheduleSection::addBar(ThermoDurationBar* pDurationBar)
{
  m_pBar = pDurationBar;
}

ThermoDurationBar* ThermoScheduleSection::bar() const
{
  return m_pBar;
}

quint64 ThermoScheduleSection::duration() const
{
  return m_pDataBlock==nullptr ? 0 : m_pDataBlock->durationInt();
}


double ThermoScheduleSection::durationSeconds() const
{
  return m_pDataBlock==nullptr ? 0 : static_cast<double>(m_pDataBlock->durationInt())/1000;
}

void ThermoScheduleSection::setThermoMode(GlobalDefinitions::ThermoMode thermoMode)
{
  m_pDataBlock->insert(JSON_THERMO_TYPE,QJsonValue(QVariant::fromValue(thermoMode).toString().toLower()));
  m_nThermoMode = thermoMode;
  emit thermoModeChangedSignal(m_nThermoMode);
}

GlobalDefinitions::ThermoMode ThermoScheduleSection::thermoMode() const
{
  return m_nThermoMode;
}

float ThermoScheduleSection::initialTemperature()
{
  return m_fInitialTemperature;
}

float ThermoScheduleSection::finalTemperature()
{
  return m_fFinalTemperature;
}

void ThermoScheduleSection::setTemperature(float temperatureValue, float initialTemperature)
{
  m_fFinalTemperature = temperatureValue;

  if ( m_nThermoMode==GlobalDefinitions::ThermoMode::MAINTAINING )
    m_fInitialTemperature = m_fFinalTemperature;

  if ( m_nThermoMode==GlobalDefinitions::ThermoMode::HEATING &&
       m_fFinalTemperature<m_fInitialTemperature )
    m_fInitialTemperature = m_fFinalTemperature;

  if ( m_nThermoMode==GlobalDefinitions::ThermoMode::COOLING &&
       m_fFinalTemperature>m_fInitialTemperature )
    m_fInitialTemperature = m_fFinalTemperature;

  if ( static_cast<int>(roundf(initialTemperature*10))!=0 )
    m_fInitialTemperature = initialTemperature;

  dataBlock()->insert(JSON_THERMO_TEMPERATURE,QJsonValue(static_cast<double>(roundf(temperatureValue*10))/10));

  emit m_pParentForm->temperatureChangedSignal(m_nSectionIndex);
}

int ThermoScheduleSection::sectionIndex()
{
  return m_nSectionIndex;
}

void ThermoScheduleSection::setSectionIndex(int sectionIndex)
{
  m_nSectionIndex = sectionIndex;
}

bool ThermoScheduleSection::isFirstSection()
{
  return m_nSectionIndex==0;
}

bool ThermoScheduleSection::isLastSection()
{
  return m_nSectionIndex==(m_pParentForm->sectionsCount()-1);
}

void ThermoScheduleSection::temperatureChangedSlot(int nInitiatorSectionIndex)
{
  if ( m_nSectionIndex>nInitiatorSectionIndex )
  {
    float previousSectionFinalTemperature = m_pParentForm->section(m_nSectionIndex-1)->finalTemperature();
    if ( (int)roundf(m_fInitialTemperature*10) != (int)roundf(previousSectionFinalTemperature*10) )
    {
      m_fInitialTemperature = previousSectionFinalTemperature;

      if ( m_nThermoMode==GlobalDefinitions::ThermoMode::MAINTAINING )
        setTemperature(previousSectionFinalTemperature);

      if ( m_nThermoMode==GlobalDefinitions::ThermoMode::HEATING &&
           m_fFinalTemperature<m_fInitialTemperature )
        setTemperature(previousSectionFinalTemperature);

      if ( m_nThermoMode==GlobalDefinitions::ThermoMode::COOLING &&
           m_fFinalTemperature>m_fInitialTemperature )
        setTemperature(previousSectionFinalTemperature);
    }
  }
  else
  if ( m_nSectionIndex<nInitiatorSectionIndex )
  {
    float nextSectionInitialTemperature = m_pParentForm->section(m_nSectionIndex+1)->initialTemperature();
    if ( (int)roundf(m_fFinalTemperature*10) != (int)roundf(nextSectionInitialTemperature*10) )
      setTemperature(nextSectionInitialTemperature);
  }
}

void ThermoScheduleSection::select()
{
  if (! m_bSelected )
  {
    m_bSelected = true;
    m_pBar->select();
    emit selectedSignal();
  }
}

void ThermoScheduleSection::unselect()
{
  m_pBar->unselect();
  m_bSelected = false;
}

void ThermoScheduleSection::setEnable(const bool bEnabled)
{
  m_bEnabled = bEnabled;
  m_pDataBlock->setEnable(bEnabled);
  m_pDataBlock->insert(JSON_THERMO_ENABLE,QJsonValue(bEnabled));
  m_pBar->setEnable(bEnabled);
}

bool ThermoScheduleSection::isEnabled()
{
  return m_bEnabled;
}
