#ifndef THERMODURATIONWIDGET_H
#define THERMODURATIONWIDGET_H

#include <QWidget>

#include "../../global_definitions.h"

class ThermoDurationWidget : public QWidget
{
    Q_OBJECT
  public:
    ThermoDurationWidget(QWidget *parent = nullptr);

  signals:

  public slots:

  protected:
    virtual void paintEvent(QPaintEvent *event);

  private:
    void drawThemperatureGrid();
};

#endif // THERMODURATIONWIDGET_H
