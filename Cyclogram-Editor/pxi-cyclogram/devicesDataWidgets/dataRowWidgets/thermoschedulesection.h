#ifndef THERMOSHEDULESECTION_H
#define THERMOSHEDULESECTION_H

#include <QObject>
#include <QFrame>

#include "dataModel/scheduledatablock.h"
#include "controls/thermodurationbar.h"

class ThermoDataForm;

class ThermoScheduleSection : public QObject
{
    Q_OBJECT
  public:
    ThermoScheduleSection(ThermoDataForm* pParentForm, QWidget* pDurationWidget);
    ~ThermoScheduleSection();

    void addDataBlock(ScheduleDataBlock*);
    ScheduleDataBlock* dataBlock() const;
    void addBar(ThermoDurationBar*);
    ThermoDurationBar* bar() const;
		quint64 duration() const;
    double durationSeconds() const;
		void setThermoMode(GlobalDefinitions::ThermoMode mode);
		GlobalDefinitions::ThermoMode thermoMode() const;
    float initialTemperature();
    float finalTemperature();
    void setTemperature(float temperatureValue, float initialTemperature=0);
    int sectionIndex();
    void setSectionIndex(int);
    bool isFirstSection();
    bool isLastSection();

    void select();
    void unselect();
    void setEnable(const bool bEnabled);
    bool isEnabled();

  signals:

    void selectedSignal();
		void thermoModeChangedSignal(GlobalDefinitions::ThermoMode);

  public slots:

    void temperatureChangedSlot(int nSectionIndex);

  private:
    ThermoDataForm* m_pParentForm;
    QWidget* m_pParentFrame;
    ScheduleDataBlock* m_pDataBlock;
    ThermoDurationBar* m_pBar;
    bool m_bSelected;
		GlobalDefinitions::ThermoMode m_nThermoMode;
    float m_fInitialTemperature;
    float m_fFinalTemperature;
    int m_nSectionIndex;
    bool m_bEnabled;
};

#endif // THERMOSHEDULESECTION_H
