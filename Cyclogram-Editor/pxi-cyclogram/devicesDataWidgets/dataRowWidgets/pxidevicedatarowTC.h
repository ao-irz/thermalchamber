#ifndef PXIDEVICEDATAROWTC_H
#define PXIDEVICEDATAROWTC_H

#include <QObject>

#include "pxidevicedatarow.h"

class PxiDeviceDataRowThermoCouple : public PxiDeviceDataRow
{
    Q_OBJECT

public:
    PxiDeviceDataRowThermoCouple(const int nDeviceIndex,
                       const QString strLabelText,
                       const QPixmap& normalIcon,
                       const QPixmap& selectedIcon,
                       QWidget *parent = nullptr);

    double temperatureMaxValue() const;

    double temperatureMinValue() const;

  public slots:
    virtual void clearDetailsWidget() override;

protected:
    virtual void setupDetailsArea() override;
    virtual void updateDetailsArea() override;
    virtual void storeDataControlContents() override;
    virtual void fillDataControlContents() override;


private:
    QCheckBox* m_pTemperatureMaxCheck;
    QCheckBox* m_pTemperatureMinCheck;
    QDoubleSpinBox* m_pTemperatureMaxEdit;
    QDoubleSpinBox* m_pTemperatureMinEdit;
};

#endif // PXIDEVICEDATAROWTC_H
