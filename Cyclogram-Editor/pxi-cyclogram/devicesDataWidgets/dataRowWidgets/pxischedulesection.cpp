#include "pxischedulesection.h"
#include "pxidevicedatarow.h"

PxiScheduleSection::PxiScheduleSection(PxiDeviceDataRow* parent, QFrame* pDurationWidget) : QObject(parent)
  ,m_pDataRow(parent)
  ,m_pParentFrame(pDurationWidget)
  ,m_pDataBlock(nullptr)
  ,m_bSelected(false)
{
}

PxiScheduleSection::~PxiScheduleSection()
{
  delete m_pDataBlock;
  m_pBar->close();
  delete m_pBar;
}

void PxiScheduleSection::addDataBlock(ScheduleDataBlock* pDataBlock)
{
  m_pDataBlock = pDataBlock;
}

ScheduleDataBlock *PxiScheduleSection::dataBlock() const
{
  return m_pDataBlock;
}

void PxiScheduleSection::addBar(PxiDurationBar* pDurationBar)
{
  m_pBar = pDurationBar;
}

PxiDurationBar *PxiScheduleSection::bar() const
{
  return m_pBar;
}

quint64 PxiScheduleSection::duration() const
{
  return m_pDataBlock==nullptr ? 0 : m_pDataBlock->durationInt();
}

double PxiScheduleSection::durationSeconds() const
{
  return m_pDataBlock==nullptr ? 0 : static_cast<double>(m_pDataBlock->durationInt())/1000;
}

void PxiScheduleSection::select()
{
  if (! m_bSelected )
  {
    m_bSelected = true;
    m_pBar->select();
    emit selectedSignal();
  }
}

void PxiScheduleSection::unselect()
{
  m_pBar->unselect();
  m_bSelected = false;
}
