#ifndef PXISHEDULESECTION_H
#define PXISHEDULESECTION_H

#include <QObject>
#include <QFrame>

#include "dataModel/scheduledatablock.h"
#include "controls/pxidurationbar.h"

class PxiDeviceDataRow;

class PxiScheduleSection : public QObject
{
    Q_OBJECT
  public:
    explicit PxiScheduleSection(PxiDeviceDataRow* parent, QFrame* pDurationWidget);
    ~PxiScheduleSection();

    void addDataBlock(ScheduleDataBlock*);
    ScheduleDataBlock* dataBlock() const;
    void addBar(PxiDurationBar*);
    PxiDurationBar* bar() const;
		quint64 duration() const;
    double durationSeconds() const;

    void select();
    void unselect();

  signals:

    void selectedSignal();

  public slots:

  private:
    PxiDeviceDataRow* m_pDataRow;
    QFrame* m_pParentFrame;
    ScheduleDataBlock* m_pDataBlock;
    PxiDurationBar* m_pBar;
    bool m_bSelected;
};

#endif // PXISHEDULESECTION_H
