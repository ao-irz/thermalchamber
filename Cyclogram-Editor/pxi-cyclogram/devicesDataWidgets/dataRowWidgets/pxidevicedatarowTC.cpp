#include "pxidevicedatarowTC.h"
#include "../../global_definitions.h"

PxiDeviceDataRowThermoCouple::PxiDeviceDataRowThermoCouple(const int nDeviceIndex,
                                       const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget *parent):
    PxiDeviceDataRow(GlobalDefinitions::ItemType::ITEM_THERMOCOUPLE,
                     nDeviceIndex,
                     strLabelText,
                     normalIcon,
                     selectedIcon,
                     parent)
{
    setupDurationArea();
    setupDetailsArea();
}

void PxiDeviceDataRowThermoCouple::setupDetailsArea()
{
// там выставляются контролы времени, единые для всех
    PxiDeviceDataRow::setupDetailsArea();

    QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();

// блок параметров управления напряжением

    m_pTemperatureMaxEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pTemperatureMaxEdit->setProperty( JSON_FIELD_NAME ,JSON_TEMPERATURE_MAX);
    m_pTemperatureMaxEdit->setMaximum(OVEN_MAX_TEMPERATURE);
    m_pTemperatureMaxEdit->setMinimum(OVEN_MIN_TEMPERATURE);
    m_pTemperatureMaxEdit->setDecimals(2);
    m_pTemperatureMaxEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pTemperatureMaxEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowThermoCouple::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createCheckableFieldVLayout("Температура max, °C",m_pTemperatureMaxEdit,m_pTemperatureMaxCheck),
                       0,m_setupControlColumn);
    m_pTemperatureMaxCheck->setProperty( JSON_FIELD_NAME ,JSON_TEMPERATURE_MAX_ENABLED);
    connect(m_pTemperatureMaxCheck,&QCheckBox::stateChanged,
            this,&PxiDeviceDataRowThermoCouple::dataControlValueChangedSlot);

    m_pTemperatureMinEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pTemperatureMinEdit->setProperty( JSON_FIELD_NAME ,JSON_TEMPERATURE_MIN);
    m_pTemperatureMinEdit->setMaximum(OVEN_MAX_TEMPERATURE);
    m_pTemperatureMinEdit->setMinimum(OVEN_MIN_TEMPERATURE);
    m_pTemperatureMinEdit->setDecimals(2);
    m_pTemperatureMinEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pTemperatureMinEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowThermoCouple::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createCheckableFieldVLayout("Температура min, °C",m_pTemperatureMinEdit,m_pTemperatureMinCheck),
                       1,m_setupControlColumn++);
    m_pTemperatureMinCheck->setProperty( JSON_FIELD_NAME ,JSON_TEMPERATURE_MIN_ENABLED);
    connect(m_pTemperatureMinCheck,&QCheckBox::stateChanged,
            this,&PxiDeviceDataRowThermoCouple::dataControlValueChangedSlot);
}

void PxiDeviceDataRowThermoCouple::clearDetailsWidget()
{
  m_bNoSave = true;

  PxiDeviceDataRow::clearDetailsWidget();

  m_pTemperatureMaxCheck->setChecked(false);
  m_pTemperatureMinCheck->setChecked(false);
  m_pTemperatureMaxEdit->setValue(0);
  m_pTemperatureMinEdit->setValue(0);

  m_bNoSave = false;
}

void PxiDeviceDataRowThermoCouple::updateDetailsArea()
{
  PxiDeviceDataRow::updateDetailsArea();

  if ( m_pActiveSection )
  {
    m_pTemperatureMaxCheck->setEnabled(true);
    m_pTemperatureMinCheck->setEnabled(true);

    fillDataControlContents();
  }
  else
  {
    m_pTemperatureMaxCheck->setEnabled(false);
    m_pTemperatureMinCheck->setEnabled(false);
    m_pTemperatureMaxCheck->setChecked(false);
    m_pTemperatureMinCheck->setChecked(false);
    m_pTemperatureMaxEdit->setValue(0);
    m_pTemperatureMinEdit->setValue(0);
  }
}

void PxiDeviceDataRowThermoCouple::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  PxiDeviceDataRow::storeDataControlContents();

  if ( m_pTemperatureMaxCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(JSON_TEMPERATURE_MAX,QJsonValue(m_pTemperatureMaxEdit->value()));

  if ( m_pTemperatureMinCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(JSON_TEMPERATURE_MIN,QJsonValue(m_pTemperatureMinEdit->value()));
}

void PxiDeviceDataRowThermoCouple::fillDataControlContents()
{
  m_bNoSave = true;

  PxiDeviceDataRow::fillDataControlContents();

  if ( m_pActiveSection )
  {
    ScheduleDataBlock* jsonDataBlock = m_pActiveSection->dataBlock();

    if ( jsonDataBlock->value(JSON_TEMPERATURE_MAX) == QJsonValue::Undefined )
    {
      m_pTemperatureMaxCheck->setChecked(false);
      m_pTemperatureMaxEdit->setValue(0);
    }
    else
    {
      m_pTemperatureMaxCheck->setChecked(true);
      m_pTemperatureMaxEdit->setValue(jsonDataBlock->value(JSON_TEMPERATURE_MAX).toInt());
    }

    if ( jsonDataBlock->value(JSON_TEMPERATURE_MIN) == QJsonValue::Undefined )
    {
      m_pTemperatureMinCheck->setChecked(false);
      m_pTemperatureMinEdit->setValue(0);
    }
    else
    {
      m_pTemperatureMinCheck->setChecked(true);
      m_pTemperatureMinEdit->setValue(jsonDataBlock->value(JSON_TEMPERATURE_MIN).toInt());
    }
  }

  m_bNoSave = false;
}

double PxiDeviceDataRowThermoCouple::temperatureMinValue() const
{
  return m_pTemperatureMinEdit->value();
}

double PxiDeviceDataRowThermoCouple::temperatureMaxValue() const
{
  return m_pTemperatureMaxEdit->value();
}
