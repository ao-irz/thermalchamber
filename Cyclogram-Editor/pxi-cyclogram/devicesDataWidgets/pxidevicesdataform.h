#ifndef SLOTDATAWIDGET_H
#define SLOTDATAWIDGET_H

#include <QGridLayout>
#include <QPair>
#include <QMap>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QStackedLayout>

#include "dataform.h"

#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarow.h"
#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarowPS.h"
#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarowGen.h"
#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarowDio.h"
#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarowTC.h"
#include "devicesDataWidgets/dataRowWidgets/pxidevicedatarowSmu.h"

class PxiDevicesDataForm : public DataForm
{
    Q_OBJECT
public:
    explicit PxiDevicesDataForm(QWidget *parent);

    virtual bool canClose(QString msgTitle = "") override;

signals:

    void activateDataPage(QWidget*);

  protected:
    virtual void setupForm() override;

    virtual void distributeData(QString importFileName = "") override;
    virtual void collectData() override;
    virtual bool checkJson( QString& errMsg ) override;

private:

    GlobalDefinitions::ItemType m_nSelectedItemType;
    int m_nSelectedItemIndex;

    QMap<QPair<GlobalDefinitions::ItemType,int>,
         PxiDeviceDataRow*> m_mapDeviceDataRows;

    QStackedLayout* m_pDeviceParametersLayout;

    QIcon* m_pPsIcon;
    QIcon* m_pPsSelectedIcon;
    QIcon* m_pGenIcon;
    QIcon* m_pGenSelectedIcon;
    QIcon* m_pTCIcon;
    QIcon* m_pTCSelectedIcon;
    QIcon* m_pDioIcon;
    QIcon* m_pDioSelectedIcon;

    QMessageBox::StandardButton showConfirmation(const QString& title, const QString& msgText);

private slots:
    void itemLabelClickedSlot(GlobalDefinitions::ItemType nItemType,int nItemIndex);

};

#endif // SLOTDATAWIDGET_H
