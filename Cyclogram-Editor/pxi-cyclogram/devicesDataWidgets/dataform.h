#ifndef DATAFORMPAGE_H
#define DATAFORMPAGE_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QJsonDocument>
#include <QFileDialog>
#include <QFileInfo>

#include "../global_definitions.h"

class PxiCycloMainWindow;

class DataForm : public QWidget
{
    Q_OBJECT
  public:
    explicit DataForm(QWidget *parent);

    virtual bool canClose(QString msgTitle = "");

  signals:
    void resetDataSignal();

  public slots:
    void selectedDataPageSlot(GlobalDefinitions::DataFormPageType nSelectedPage);
    void showStatusBarMsgSlot(QString&);

    void loadDataSlot();
    void saveDataSlot();
    void resetDataSlot();

  protected:
    PxiCycloMainWindow* m_pParent;

    GlobalDefinitions::DataFormPageType m_nDataFormType;
    bool m_bIsActive;

    QString m_strCycloName;
    QLabel* m_pCycloNameLabel;
    QLabel* m_pChipsNameLabel;
    QLineEdit* m_pChipsNameEdit;
    QLabel* m_pChipsBatchLabel;
    QLineEdit* m_pChipsBatchEdit;

    QGridLayout* m_pDataGridLayout;

    int m_nSetupCurrentRow;
    int m_nSetupCurrentColumn;
    QFrame* m_pHSeparator;

    QJsonDocument m_jsonDataLoaded;
    QJsonDocument m_jsonDataEditing;

    virtual void setupForm();

    virtual void loadData();
    virtual bool checkJson( QString& errMsg ) = 0;
    virtual void distributeData(QString importFileName = "") = 0;
    virtual bool saveData();
    virtual void collectData() = 0;
    virtual bool clearForm(QString strCaption, bool bForcefully = false);

  private:
};

#endif // DATAFORMPAGE_H
