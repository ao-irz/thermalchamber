#ifndef PXISWITCHER_H
#define PXISWITCHER_H

#include <QSlider>

class PxiDetailsWidget;

class PxiSwitcher : public QSlider
{
    Q_OBJECT

public:

    enum SwitcherStyle {
        DIO_SWITCHER = 0x1,
        DEVICE_SWITCHER = 0x2
    };

   PxiSwitcher(PxiDetailsWidget *parent = nullptr);
   PxiSwitcher(QWidget *parent = nullptr);

   void setState(bool bState);
   bool valueBool() const;

  signals:
   void valueChanged();

protected:
   virtual void mousePressEvent(QMouseEvent *ev) override;

private:
   SwitcherStyle m_nSwitcherStyle;
   bool m_bStateOn;
   QWidget* m_pParent;
};

#endif // PXISWITCHER_H
