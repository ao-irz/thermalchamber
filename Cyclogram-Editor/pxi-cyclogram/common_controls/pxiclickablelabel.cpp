#include "pxiclickablelabel.h"

PxiClickableLabel::PxiClickableLabel(const QString strLabelText,
                                     QWidget* parent) : QLabel(parent)
{
    if ( strLabelText.length()>0 ) {
        m_bIconLabel = false;

        m_selectedFont.setItalic(true);
        m_selectedFont.setBold(true);

        m_normalFont.setItalic(false);
        m_normalFont.setBold(false);

        setText(strLabelText);
        setMinimumWidth(85);
    }
    else
        m_bIconLabel = true;
}

PxiClickableLabel::~PxiClickableLabel()
{
}

void PxiClickableLabel::setIcons(const QPixmap &normalIcon, const QPixmap &selectedIcon)
{
    m_NormalIcon = normalIcon;
    m_SelectedIcon = selectedIcon;

    unselect();
}

void PxiClickableLabel::select()
{
    if ( m_bIconLabel )
        setPixmap(m_SelectedIcon);
    else
    {
        //setStyleSheet("color: rgb(76,150,203);");
      setStyleSheet("color: rgb(29,54,97);");
      setFont(m_selectedFont);
    }
}

void PxiClickableLabel::unselect()
{
    if ( m_bIconLabel )
        setPixmap(m_NormalIcon);
    else
    {
        setStyleSheet("color: rgb(0,0,0);");
        setFont(m_normalFont);
    }
}

void PxiClickableLabel::mousePressEvent(QMouseEvent* event)
{
    if ( event->button()==Qt::LeftButton ) {
        select();
        emit clicked();
    }
}
