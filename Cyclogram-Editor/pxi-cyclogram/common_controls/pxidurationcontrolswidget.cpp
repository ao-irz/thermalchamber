#include "pxidurationcontrolswidget.h"
#include <math.h>

PxiDurationControlsWidget::PxiDurationControlsWidget(QWidget *parent) : QWidget(parent)
  ,m_strDuration("0")
  ,m_nDuration(0)
  ,m_nMinimumDuraton(0)
  ,m_bExternalSet(false)
{
    m_pLayout = new QHBoxLayout();
    m_pLayout->setMargin(1);
    m_pLayout->setSpacing(2);
    setLayout(m_pLayout);

    m_pHourBox = new QSpinBox(this);
    m_pHourBox->setMinimum(0);
    //m_pHourBox->setMaximum(999);
    m_pHourBox->setMaximum(2400);
    m_pHourBox->setValue(0);
    m_pLayout->addWidget(m_pHourBox);
    QLabel* l = new QLabel(":",this);
    l->setMargin(0);
    m_pLayout->addWidget(l);
    connect(m_pHourBox,QOverload<int>::of(&QSpinBox::valueChanged),
            this,&PxiDurationControlsWidget::hourChanged);

    m_pMinuteBox = new QSpinBox(this);
    m_pMinuteBox->setMinimum(0);
    m_pMinuteBox->setMaximum(60);
    m_pMinuteBox->setValue(0);
    m_pMinuteBox->setWrapping(true);
    m_pLayout->addWidget(m_pMinuteBox);
    l = new QLabel(":",this);
    l->setMargin(0);
    m_pLayout->addWidget(l);
    connect(m_pMinuteBox,QOverload<int>::of(&QSpinBox::valueChanged),
            this,&PxiDurationControlsWidget::minuteChanged);

    m_pSecondBox = new QDoubleSpinBox(this);
    m_pSecondBox->setMinimum(0);
    m_pSecondBox->setMaximum(60);
    m_pSecondBox->setDecimals(3);
    m_pSecondBox->setValue(0);
    m_pSecondBox->setWrapping(true);
    m_pLayout->addWidget(m_pSecondBox);
    connect(m_pSecondBox,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDurationControlsWidget::secondChanged);

    //m_pLayout->addStretch(1);
}

QString PxiDurationControlsWidget::durationStr() const
{
    return m_strDuration;
}

quint64 PxiDurationControlsWidget::durationInt() const
{
  return m_nDuration;
}

void PxiDurationControlsWidget::setDuration(QString strDuration)
{
  setDuration(strDuration.toULongLong());
}

void PxiDurationControlsWidget::setMinimumDuration(quint64 nMinimumDuration)
{
  m_nMinimumDuraton = nMinimumDuration;
}

void PxiDurationControlsWidget::setDuration(quint64 nDuration)
{
  m_bExternalSet = true;

  ushort msecs = nDuration % 1000;
  nDuration = (nDuration-msecs)/1000;
  ushort secs = static_cast<ushort>(nDuration%60);
  nDuration = (nDuration-secs)/60;
  ushort mins = static_cast<ushort>(nDuration%60);
  nDuration = (nDuration-mins)/60;

  m_pHourBox->setValue(static_cast<int>(nDuration));
  m_pMinuteBox->setValue(mins);
  m_pSecondBox->setValue(secs+static_cast<double>(msecs)/1000);

  m_strDuration = QString::number(static_cast<qulonglong>(m_nDuration));

  m_bExternalSet = false;
}

void PxiDurationControlsWidget::hourChanged(int)
{
  calculate();
}

void PxiDurationControlsWidget::minuteChanged(int newValue)
{
    static int prevValue;

    if ( newValue>59 )
    {
        if ( prevValue==0 ) // выполнен stepDown
        {
            if (m_pHourBox->value()>m_pHourBox->minimum())
            {
                m_pHourBox->stepDown();
                m_pMinuteBox->setValue(prevValue=59);
            }
            else
                m_pMinuteBox->setValue(0);
        }
        else // выполнен stepUp
        {
            if ( m_pHourBox->value()==m_pHourBox->maximum() )
                m_pMinuteBox->setValue(prevValue=59);
            else
            {
                m_pHourBox->stepUp();
                m_pMinuteBox->setValue(prevValue=0);
            }
        }
    }
    else
        prevValue = newValue;

    calculate();
}

void PxiDurationControlsWidget::secondChanged(double newValue)
{
    static double prevValue;

    if ( newValue>=59.999 )
    {
        if ( prevValue==0 ) // выполнен stepDown
        {
            if ( m_pHourBox->value()>m_pHourBox->minimum() ||
                 m_pMinuteBox->value()>m_pMinuteBox->minimum() )
            {
                m_pMinuteBox->stepDown();
                m_pSecondBox->setValue(prevValue=59);
            }
            else
                m_pSecondBox->setValue(0);
        }
        else // выполнен stepUp
        {
            if ( m_pHourBox->value()==m_pHourBox->maximum() &&
                 m_pMinuteBox->value()==m_pMinuteBox->maximum() )
                m_pSecondBox->setValue(prevValue=59);
            else
            {
                m_pMinuteBox->stepUp();
                m_pSecondBox->setValue(prevValue=0);
            }
        }
    }
    else
        prevValue = newValue;

    calculate();
}

void PxiDurationControlsWidget::calculate()
{
  m_nDuration = static_cast<quint64>(m_pHourBox->value())*60*60*1000+
                static_cast<quint64>(m_pMinuteBox->value())*60*1000+
                static_cast<quint64>(round(m_pSecondBox->value()*1000));

  m_strDuration = QString::number(static_cast<qulonglong>(m_nDuration));

  if ( m_nDuration< m_nMinimumDuraton )
    setDuration(m_nMinimumDuraton);

  if ( !m_bExternalSet )
    // сигнал выдается только если пользователь меняет значение спинбоксов
    emit durationChangedSignal();

  emit valueChanged();

}

void PxiDurationControlsWidget::clear()
{
  m_bExternalSet = true;

  m_nDuration = 0;
  m_strDuration = "0";
  m_pHourBox->setValue(0);
  m_pMinuteBox->setValue(0);
  m_pSecondBox->setValue(0);

  m_bExternalSet = false;
}
