#ifndef PXICHECKABLETOOLBUTTON_H
#define PXICHECKABLETOOLBUTTON_H
#include <QToolButton>

class PxiCheckableToolButton : public QToolButton
{
		Q_OBJECT

	public:
    PxiCheckableToolButton( QWidget* parent,
                            QString strIconResource = "",
                            QString strToolTip = "",
                            bool checkable = false );

	private:
		QIcon m_Icon;
};

#endif // PXICHECKABLETOOLBUTTON_H
