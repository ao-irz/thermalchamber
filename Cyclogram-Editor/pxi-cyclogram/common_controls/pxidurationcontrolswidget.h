#ifndef PXIDURATIONCONTROLSWIDGET_H
#define PXIDURATIONCONTROLSWIDGET_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <QDateTime>

class PxiDurationControlsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PxiDurationControlsWidget(QWidget *parent);

    QString durationStr() const;
    quint64 durationInt() const;
    void setDuration(quint64 nDuration=0);
    void setDuration(QString strDuration="");
    void setMinimumDuration(quint64 nMinimumDuration);
    void clear();

  signals:
    void durationChangedSignal();
    void valueChanged();

private:
    QString m_strDuration;
    quint64 m_nDuration;
    quint64 m_nMinimumDuraton;

    QHBoxLayout* m_pLayout;
    QSpinBox* m_pHourBox;
    QSpinBox* m_pMinuteBox;
    QDoubleSpinBox* m_pSecondBox;
    bool m_bExternalSet;

    void calculate();

private slots:
    void hourChanged(int);
    void minuteChanged(int);
    void secondChanged(double);
};

#endif // PXIDURATIONCONTROLSWIDGET_H
