#include "pxicheckabletoolbutton.h"

PxiCheckableToolButton::PxiCheckableToolButton(QWidget* parent,
                                               QString strIconResource,
                                               QString strToolTip,
                                               bool checkable):
	QToolButton ( parent )
{
	if ( strIconResource.length()>0 )
	{
		m_Icon.addFile( strIconResource, QSize(), QIcon::Normal, QIcon::Off);
		setIcon( m_Icon );
	}

	setToolTip( strToolTip.length()>0 ? strToolTip : "" );

	setCheckable( checkable );
}
