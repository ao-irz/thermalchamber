#ifndef PXISCHEDULEDATABLOCK_H
#define PXISCHEDULEDATABLOCK_H

#include <QJsonObject>

#include "../global_definitions.h"

class ScheduleDataBlock : public QJsonObject
{
  public:
    ScheduleDataBlock();
    virtual ~ScheduleDataBlock();

    QString durationStr() const;
    quint64 durationInt() const;
    void setDuration(const QString duration);
    void setDuration(const quint64 duration);
    void setEnable(const bool bEnabled);
    bool isEnabled();
    void recalcDuration(float percent);
		void setResizing(bool bResizing);

  protected:
    //GlobalDefinitions::ItemType m_enDataItemType;
    QString m_strDuration;
    quint64 m_nDuration;
		QString m_strTempDuration;
		quint64 m_nTempDuration;
    bool m_bEnable;
    bool m_bResizingFlag;
};

#endif // PXISCHEDULEDATABLOCK_H
