#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGuiApplication>
#include <QScreen>
#include <QAction>

PxiCycloMainWindow::PxiCycloMainWindow(QWidget *parent)
  : QMainWindow(parent)
,ui(new Ui::PxiCMainWindow)
{
  ui->setupUi(this);

  m_pLoadDataIcon = new QIcon(LOAD_ICON);
  m_pSaveDataIcon = new QIcon(SAVE_ICON);
  m_pClearDataIcon = new QIcon(CLEAR_ICON);

  m_pThermoCyclogram = new QIcon(THERMO_CYCLOGRAM);
  m_pDevicesCyclogram = new QIcon(DEVICES_CYCLOGRAM);

  setupUI();
  setupToolBar();

  QSize windowSize(1300,700);
  //setMaximumSize(windowSize); // если включить оба ограничения, то под Win7 перестает работать верхняя строка,
  setMinimumSize(windowSize);   //  где системное меню и кнопки сворачивания/закрытия

  int desktopWidth = QGuiApplication::screens()[0]->geometry().width();
  int desktopHeight = QGuiApplication::screens()[0]->geometry().height();
  move ( (desktopWidth - windowSize.width()) / 2, (desktopHeight - windowSize.height()) / 2 -50 );
}

PxiCycloMainWindow::~PxiCycloMainWindow()
{
  delete ui;
}

void PxiCycloMainWindow::closeEvent(QCloseEvent *event)
{
  bool bAccept = true;

  bAccept = bAccept && m_pThermoDataForm->canClose();
  bAccept = bAccept && m_pPxiDevicesDataForm->canClose();

  if (bAccept)
    event->accept();
  else
    event->ignore();

}

void PxiCycloMainWindow::setupUI()
{
  setContentsMargins(0,0,0,0);
  m_pDataFormPageTabWidget = new QTabWidget(this);
  QWidget* pOldCentralWidget = centralWidget();
  setCentralWidget(m_pDataFormPageTabWidget);
  delete pOldCentralWidget;

  m_pDataFormPageTabWidget->setTabPosition(QTabWidget::South);
  m_pDataFormPageTabWidget->setDocumentMode(true);

  m_pDataFormPageTabWidget->setContentsMargins(3,0,3,0);
  m_pDataFormPageTabWidget->setIconSize(QSize(64,16));

  m_pThermoDataForm = new ThermoDataForm(this);
  m_pDataFormPageTabWidget->addTab(m_pThermoDataForm,*m_pThermoCyclogram,"");
  m_pDataFormPageTabWidget->setTabToolTip(0,"Работа с термоциклограммами камеры");
  connect(this,&PxiCycloMainWindow::selectedDataPageSignal,
          m_pThermoDataForm,&DataForm::selectedDataPageSlot);

  m_pPxiDevicesDataForm = new PxiDevicesDataForm(this);
  m_pDataFormPageTabWidget->addTab(m_pPxiDevicesDataForm,*m_pDevicesCyclogram,"");
  m_pDataFormPageTabWidget->setTabToolTip(1,"Работа с циклограммами PXI-оснастки");
  connect(this,&PxiCycloMainWindow::selectedDataPageSignal,
          m_pPxiDevicesDataForm,&DataForm::selectedDataPageSlot);

  connect(m_pDataFormPageTabWidget,&QTabWidget::currentChanged,
          this,&PxiCycloMainWindow::selectedPageChangedSlot);

  selectedPageChangedSlot(GlobalDefinitions::DataFormPageType::THERMO);

  setStyleSheet("background-color: rgb(245, 245, 240);");
}

void PxiCycloMainWindow::selectPage(GlobalDefinitions::DataFormPageType pageIdx)
{
  m_pDataFormPageTabWidget->setCurrentIndex(pageIdx);
}

void PxiCycloMainWindow::setupToolBar()
{
  m_pToolBar = new QToolBar(this);
  m_pToolBar->setContentsMargins(0,0,0,0);
  m_pToolBar->setMovable(false);
  m_pToolBar->setFloatable(false);

  QAction* a;
  a = m_pToolBar->addAction(*m_pLoadDataIcon,LOAD_TOOLTIP,
                        m_pPxiDevicesDataForm,&DataForm::loadDataSlot);
  connect(a,&QAction::triggered,m_pThermoDataForm,&DataForm::loadDataSlot);

  a = m_pToolBar->addAction(*m_pSaveDataIcon,SAVE_TOOLTIP,
                        m_pPxiDevicesDataForm,&DataForm::saveDataSlot);
  connect(a,&QAction::triggered,m_pThermoDataForm,&DataForm::saveDataSlot);

  m_pToolBar->addSeparator();
  a= m_pToolBar->addAction(*m_pClearDataIcon,CLEAR_TOOLTIP,
                        m_pPxiDevicesDataForm,&DataForm::resetDataSlot);
  connect(a,&QAction::triggered,m_pThermoDataForm,&DataForm::resetDataSlot);

  addToolBar(m_pToolBar);
}

void PxiCycloMainWindow::selectedPageChangedSlot(int idx)
{
  m_nSelectedDataPage = static_cast<GlobalDefinitions::DataFormPageType>(idx);
  emit selectedDataPageSignal(m_nSelectedDataPage);
}
