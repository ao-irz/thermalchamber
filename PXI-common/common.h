#ifndef COMMON_H
#define COMMON_H

#define SERVICE_NAME        "spark-service"

#define DB_DATETIME_FORMAT      "yyyy-MM-dd hh:mm:ss"

#define AMOUNT_OF_SLOTS             20
#define THERMO_SLOT_NUMBER1         AMOUNT_OF_SLOTS       // номер 21-ой циклограммы
#define THERMO_SLOT_NUMBER2         AMOUNT_OF_SLOTS+1     // номер 21-ой циклограммы

#define DEFAULT_SERVER_IP           "127.0.0.1"
#define DEFAULT_SERVER_PORT         "3000"

// Кодировка состояний слота
#define NO_SLOT_STATE               0       // используется в UI, но не нужна в service
#define WAITING_START_SLOT_STATE    1
#define HARDWARE_ERROR_SLOT_STATE   2
#define NORMAL_SLOT_STATE           3
#define PAUSE_SLOT_STATE            4
//#define WARNING_SLOT_STATE          5
#define ALARM_SLOT_STATE            6
#define TEST_COMPLETED_SLOT_STATE   7

// Команды/запросы в направлении UI->service
#define NO_COMMAND                  0x00
#define STATE_REQUEST               0x01
#define RUN_CYCLOGRAM_COMMAND       0x02
#define BREAK_CYCLOGRAM_COMMAND     0x03
#define SET_RACK_TEMPERATURE_COMMAND     0x04
#define ALARM_RESET_COMMAND         0x05
#define SET_LED_COLOR_COMMAND       0x06
#define PAUSE_COMMAND               0x07
#define RESUME_COMMAND              0x08
#define REPORT_REQUEST              0x09
#define PREPARERUN_REQUEST          0x0A
#define OPEN_LOCK_COMMAND           0x0B

// Команды/ответы в направлении UI<-service
#define STATE_RESPONSE              0x01
#define REPORT_RESPONSE             0x09
#define PREPARERUN_RESPONSE         0x0A

#define HEADER_LENGTH       5               // 1byte + 4byte = "command" + "payloadLength"
#define LENGTH_OF_PAYLOAD   4               // 1byte + 4byte = "command" + "payloadLength"

#define NSLOT_LENGTH        4
#define REAL_PARAM_LENGTH   4
#define PARAMETER_LENGTH    4
#define LEDCOLOR_LENGTH     2
#define READYSTART_FLAGLENGTH 4
#define OPEN_LOCK_LENGTH    4

#define LED_NO_COLOR                0x0000
#define LED_WHITE_COLOR             0x003F // 0x0000
#define LED_BLUE_COLOR              0x0003
#define LED_GREED_COLOR             0x000C
#define LED_YELLOW_COLOR            0x003C
#define LED_RED_COLOR               0x0030
#define LED_BLINK_RED_COLOR         0x0070

#define ZUMMER_OFF                  0
#define ZUMMER_CONTINUOS            1
#define ZUMMER_SHORT_BLINK          2
#define ZUMMER_LONG_BLINK           3

#endif // COMMON_H
