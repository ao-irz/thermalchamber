#ifndef COMMONENUMERATIONS_H
#define COMMONENUMERATIONS_H

#include <QObject>

namespace PxiEnumerations
{
    Q_NAMESPACE

    enum PxiTestEnum {
        RadialStyle = 5,
        EnvelopeStyle = 6,
        FilledStyle = 7
    };

    Q_ENUMS(PxiTestEnum)
}

namespace MyNamespace
{
    Q_NAMESPACE

    enum MyEnum
    {
        Key1 = 93456,
        Key2 = 123456,
        Key3 = 12,
        Key4 = 87
    };

    Q_ENUMS(MyEnum)
}

#endif // COMMONENUMERATIONS_H
