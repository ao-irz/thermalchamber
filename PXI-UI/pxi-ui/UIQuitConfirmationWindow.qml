import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

Dialog {
    width: parent.width/3
    height: parent.height/3
    modal: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: parent
    title: qsTr("Подтверждение выхода")
    standardButtons: Dialog.Cancel | Dialog.Ok
    focus: true

    Text {
        width: parent.width
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignJustify
        wrapMode: Text.WordWrap
        font.pixelSize: 20
        color: "white"
        text: qsTr("Графическая оболочка будет закрыта. Это не повлияет на работу запущеных циклограмм.\nВы точно хотите выйти?")
    }

    onAccepted: Qt.quit()
    onRejected: close()
}
