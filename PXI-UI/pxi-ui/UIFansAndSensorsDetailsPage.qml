import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

ScrollView {
    width: parent.width
    clip: true

    Column {
        anchors.fill: parent
        spacing: 10

        Item { width: root.width-60; height: 1; }
        UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !chamber1.plcLink || !chamber2.plcLink }

        Item { width: root.width-60; height: 1; }
        UIChamberParagraph { title: qsTr("Периферийные температурные датчики"); }
        ListView {
            visible: chamber1.plcLink && chamber2.plcLink
            width: parent.width
            height: fansAndSensors.peripheralTemperature.length * 40
            spacing: 10
            model: fansAndSensors.peripheralTemperature
            delegate: Grid {
                width: parent.width
                columns: 3
                columnSpacing: 15

                UINameValueGray{ width: 350; name: qsTr("Датчик №%1 (%2):").arg(modelData.number).arg(modelData.name); value: modelData.temperature; units: "°"; pixelSize: 15}
                UINameValueGray{ name: qsTr("Состояние:"); value: (modelData.isOk) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (modelData.isOk) ? ("green") : ("red"); pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }
        }

        Item { width: root.width-60; height: 1; }
        UIChamberParagraph { title: qsTr("Вентиляторы"); }
        ListView {
            visible: chamber1.plcLink && chamber2.plcLink
            width: parent.width
            height: fansAndSensors.fans.length * 40
            spacing: 10
            model: fansAndSensors.fans
            delegate: Grid {
                width: parent.width
                columns: 3
                columnSpacing: 15

                UINameValueGray{ width: 350; name: qsTr("Вентилятор №%1 (%2):").arg(modelData.number).arg(modelData.name); value: modelData.percent; units: "%"; pixelSize: 15}
                UINameValueGray{ name: qsTr("Состояние:"); value: (modelData.isEnable) ? (qsTr("включен")) : (qsTr("выключен")); valueColor: (modelData.isEnable) ? ("green") : ("red"); pixelSize: 15 }
                Rectangle { width: 20; height: 20; color: "transparent" }
            }
        }
    }

}
