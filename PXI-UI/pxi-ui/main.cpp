#include <QDebug>
#include <QApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QDir>
#include "NetworkInterface/NetworkInterface.h"
#include "model/SlotsListModel.h"
#include "model/SideProxyModel.h"
#include "model/ChamberData.h"
#include "model/FansAndSensorsData.h"
#include "../../PXI-common/led-color.h"
#include "../../PXI-common/common-enumerations.h"

int main(int argc, char *argv[])
{
    printf("Starting program...\n");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setOrganizationName("IRZ");
    app.setOrganizationDomain("irz.ru");
    app.setApplicationName("Spark-UI");

    //! TODO: Возможно все emum вынести в отдельный Namespace, хотя будет ли это работать....
    qmlRegisterUncreatableType<NetworkInterface>("SparkUIApplication", 1, 0, "NetworkInterface", "Not creatable as it is an enum type");
    qmlRegisterUncreatableType<SlotsListModel>("SparkUIApplication", 1, 0, "SlotsListModel", "Not creatable as it is an enum type");
    qmlRegisterUncreatableType<ChamberData>("SparkUIApplication", 1, 0, "ChamberData", "Not creatable as it is an enum type");

    QQmlApplicationEngine engine;
    SideProxyModel *lPtr, *rPtr;
    QPointer<ChamberData> leftChamber, rightChamber;
    QPointer<FansAndSensorsData> fansAndSensors;
    engine.rootContext()->setContextProperty("fansAndSensors", fansAndSensors = new FansAndSensorsData(qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("chamber1", leftChamber = new ChamberData(THERMO_SLOT_NUMBER1, qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("chamber2", rightChamber = new ChamberData(THERMO_SLOT_NUMBER2, qobject_cast<QObject *>(&app)));
    SlotsListModel::getInstance(qobject_cast<QObject *>(&app));
    SlotsListModel::getInstance()->setAlienObjects(leftChamber, rightChamber, fansAndSensors);
    engine.rootContext()->setContextProperty("networkInterface", NetworkInterface::getInstance(qobject_cast<QObject *>(&app)));
    NetworkInterface::getInstance()->setChamberObjects(leftChamber, rightChamber);
    engine.rootContext()->setContextProperty("slotsListModel", SlotsListModel::getInstance());
    engine.rootContext()->setContextProperty("leftSideModel", lPtr = new SideProxyModel({0,1,2,3,4,5,6,7,8,9}, qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("rightSideModel", rPtr = new SideProxyModel({10,11,12,13,14,15,16,17,18,19}, qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("currentApplicationPath", QString(QDir::currentPath()));
    lPtr->setSourceModel(SlotsListModel::getInstance());
    rPtr->setSourceModel(SlotsListModel::getInstance());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
