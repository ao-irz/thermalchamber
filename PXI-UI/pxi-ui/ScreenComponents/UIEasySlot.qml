import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "../SimpleControls"

Component {
    Rectangle {
        id: root
        width: commonSideColumn.width
        height: (commonSideColumn.height - (commonSideListView.spacing*(slotsListModel.rowCount()-1))) / slotsListModel.rowCount()
        radius: 7
        scale: (slotsListModel.selectedSlot === index) ? (0.9) : (1.0)
        gradient: Gradient {
                orientation: Gradient.Horizontal
                GradientStop { position: 0; color: billetColor }
                GradientStop { position: 1; color: "white" }
        }

        RowLayout {
            anchors.fill: parent
            anchors.margins: 2
            anchors.leftMargin: 5
            visible: model.isActive
            spacing: 5

            Label { text: model.name; color: "black"; font.pixelSize: 14; height: 16 }
            UIProgressBar { Layout.fillWidth: true; Layout.preferredHeight: 4 }
        }
        //UINameValue { name: "Загрузка:"; value: model.dateInput; pixelSize: 12; height: 16 }

        Text {
            anchors.centerIn: parent
            opacity: 0.5
            font.pixelSize: parent.height * 0.5
            text: model.channelNumber+1
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (slotsListModel.selectedSlot !== index)
                    slotsListModel.selectedSlot = model.channelNumber
                else {
                    rootItem.state = "standard"
                }
            }
        }

        Behavior on scale {
            PropertyAnimation {
                duration: 50
            }
        }
    }
}
