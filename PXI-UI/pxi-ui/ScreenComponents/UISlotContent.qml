﻿import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import "../SimpleControls"
import "../ScreenComponents"

Item {
    anchors.fill: parent
    anchors.margins: 2
    anchors.leftMargin: 5
    anchors.rightMargin: 5

    property string baseName
    property bool slotIsActive: model.isActive

    onSlotIsActiveChanged: {
        if (!slotIsActive)
            baseName = ""
    }

    Column {
        anchors.fill: parent
        visible: model.isActive

        UINameValue { value: model.name; pixelSize: 20; }
        UIProgressBar { width: parent.width }
        Item { width: parent.width; height: 5}
        UINameValue { name: "Загрузка:"; value: model.dateInput; pixelSize: 12; height: 20 }
        UINameValue { name: "Выгрузка:"; value: model.dateOutput; pixelSize: 12; height: 20  }
    }

    FileDialog {
        id: openDialog

        nameFilters: ["Text files (*.json)"]
        //onAccepted: networkInterface.sendRunCyclogramCommand(openDialog.fileUrl, model.channelNumber)
        onAccepted: {
            networkInterface.sendPrepareRunCommand(openDialog.fileUrl, model.channelNumber)
            baseName = openDialog.fileUrl.toString().replace(/\\/g,'/').replace(/.*\//, '')
        }
    }

    Label {
        id: choosedFileLabel
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        verticalAlignment: Label.AlignVCenter
        font.bold: true
        text: baseName
    }
    Row {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        spacing: 5

        Button {
            id: openButton
            visible: (rootItem.state === "standard")
                     && (!model.isActive)
                     && chamber.isThermalChamberOk
                     && chamber.isThermalChamberOn
                     && (!chamber.pauseOn)
                     && chamber.plcLink
                     && Math.abs(chamber.mainTemperature - chamber.thermalChamberTemperatureSet) < 2
            text: (model.readyStart) ? (qsTr("Отмена")) : qsTr("Открыть<br>циклограмму")
            onClicked: {
                if ((model.readyStart)) {
                    model.readyStart = false
                    baseName = ""
                }
                else
                    openDialog.open()
            }
        }
        Button {
            height: openButton.height
            display: AbstractButton.TextBesideIcon
            topPadding: 18
            rightPadding: 18
            icon.source: "qrc:/images/play.png"
            icon.color: "green"
            visible: (rootItem.state === "standard")
                     && (!model.isActive)
                     && chamber.isThermalChamberOk
                     && chamber.isThermalChamberOn
                     && (!chamber.pauseOn)
                     && chamber.plcLink
                     && Math.abs(chamber.mainTemperature - chamber.thermalChamberTemperatureSet) < 2
                     && model.readyStart
            text: qsTr("Старт")
            onClicked: {
                networkInterface.sendRunCyclogramCommand(openDialog.fileUrl, model.channelNumber)
            }
        }
    }
}
