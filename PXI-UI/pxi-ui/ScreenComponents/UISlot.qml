import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Component {
    Item {
        width: mainGrids.cellWidth
        height: mainGrids.cellHeight

        ItemDelegate {
            checkable: true
            anchors.fill: parent
            anchors.margins: 5
            background: Rectangle {
                radius: 10
                border.color: "gray"
                gradient: Gradient {
                        orientation: Gradient.Horizontal
                        GradientStop { position: 0; color: billetColor }
                        GradientStop { position: 1; color: "white" }
                }

            }

            Text {
                anchors.centerIn: parent
                opacity: 0.5
                font.pixelSize: parent.height * 0.5
                text: model.channelNumber+1
            }

            UISlotContent { }

            onClicked: {
                slotsListModel.selectedSlot = model.channelNumber
                rootItem.state = "detailed"
            }
        }
    }
}
