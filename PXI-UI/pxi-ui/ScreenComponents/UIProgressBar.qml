import QtQuick 2.0
import QtQuick.Controls 2.12
import SparkUIApplication 1.0

ProgressBar {
    id: progressBar

    height: 5
    from: Math.floor(Date.fromLocaleString(Qt.locale(), model.dateInput, "yyyy-MM-dd hh:mm:ss"))
    to: Math.floor(Date.fromLocaleString(Qt.locale(), model.dateOutput, "yyyy-MM-dd hh:mm:ss"))
//    value: ((model.stateRole === SlotsListModel.NormalSlotState) || (model.stateRole === SlotsListModel.PauseSlotState)) ?
//                (Math.floor(mainWindow.currentDateTime)):(Math.floor(Date.fromLocaleString(Qt.locale(), model.emergencyStopTime, "yyyy-MM-dd hh:mm:ss")))
    value: if (model.stateRole === SlotsListModel.NormalSlotState)
               (Math.floor(mainWindow.currentDateTime))
           else if(model.stateRole === SlotsListModel.PauseSlotState)
               (Math.floor(mainWindow.pauseDateTime))
           else
               (Math.floor(Date.fromLocaleString(Qt.locale(), model.emergencyStopTime, "yyyy-MM-dd hh:mm:ss")))

    background: Rectangle {
        implicitWidth: parent.width
        implicitHeight: 16
        color: "white"
        radius: 3
    }

    contentItem: Item {
        implicitHeight: 4

        Rectangle {
            width: progressBar.visualPosition * parent.width
            height: parent.height
            radius: 2
            color: "#17a81a"
        }
    }
}
