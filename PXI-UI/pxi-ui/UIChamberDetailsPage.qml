import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

ScrollView {
    property var chamber
    property date upsStartDate
    width: parent.width
    clip: true

    Item{
        Timer{
            id: timeoutTimer
            running: true
            interval: 500
            repeat: true
            onTriggered:
            {
                upsValue.value = chamber.getElapsedTime()
            }
        }
    }

    Column {
        anchors.fill: parent
        spacing: 10

        Item { width: root.width-60; height: 1; }
        UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !chamber.plcLink}
        UIChamberParagraph { title: qsTr("Системная информация"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("ID:"); value: chamber.systemInfoId; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Версия модуля:"); value: chamber.systemInfoVersion; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Модель устройства:"); value: chamber.systemInfoDeviceVersion; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Версия аппартного обеспечения:"); value: chamber.systemInfoHWVersion; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Версия программого обеспечения:"); value: chamber.systemInfoSWVersion; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Серийный номер устройства:"); value: chamber.systemInfoSerialNumber; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }
        }

        UIChamberParagraph { title: qsTr("Информация о термокамере"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("ID:"); value: chamber.thermalChamberId; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Режим:"); value: chamber.thermalChamberMode; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Состояние:"); value: chamber.thermalChamberErrorCode; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Температура:"); value: chamber.mainTemperature; units: "°"; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Температурная установка:"); value: chamber.thermalChamberTemperatureSet; units: "°"; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Ограничение скорости изменения температуры:"); value: chamber.thermalChamberTemperatureSpeed; units: "об/мин"; pixelSize: 15}
            Rectangle { width: 20; height: 20; color: "transparent" }
        }

        UIChamberParagraph { title: qsTr("Прочие модули"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("Accessories ID:"); value: chamber.accessoriesChamberId; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Options ID:"); value: chamber.optionsChamberId; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Alarm ID:"); value: chamber.alarmChamberId; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }
        }

        UIChamberParagraph { title: qsTr("Дискретные входы/выходы"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("Выход:"); value: chamber.extDI; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }

            UINameValueGray{ name: qsTr("Вход:"); value: chamber.extDO; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }
        }

        /*UIChamberParagraph { title: qsTr("Информация о стойке"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("Усредненная температура стойки:"); value: chamber.calculatedRackTemerature; units: "°"; pixelSize: 15 }
            Rectangle { width: 20; height: 20; color: "transparent" }
        }*/

        /*UIChamberParagraph { title: qsTr("Вентилятор"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("Обороты:"); value: chamber.fanFrequency; units: "об/мин"; pixelSize: 15}
            UINameValueGray{ name: qsTr("Состояние:"); value: (chamber.isFanOk) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (chamber.isFanOk) ? ("green") : ("red"); pixelSize: 15 }
        }*/


        UIChamberParagraph { title: qsTr("Питание"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ name: qsTr("Питание стойки:"); value: chamber.ups220 ? chamber.getElapsedTime() : chamber.getElapsedTime(); valueColor: (chamber.ups220) ? ("green") : ("red"); pixelSize: 15}
        }

        UIChamberParagraph { title: qsTr("Связь"); }
        Grid {
            visible: chamber.plcLink
            width: parent.width
            columns: 2
            columnSpacing: 15

            UINameValueGray{ id:upsValue; name: qsTr("Связь с ИБП:"); value: chamber.upsLink ? ("есть") : ("нет"); valueColor: (chamber.upsLink) ? ("green") : ("red"); pixelSize: 15}
            UINameValueGray{ name: qsTr("Связь с камерой:"); value: chamber.cameraLink ? ("есть") : ("нет"); valueColor: (chamber.cameraLink) ? ("green") : ("red"); pixelSize: 15}
        }

        //! TODO: Перенести, возможно, в отдельное окно. Сделать расшифровку имен модулей
        UIChamberParagraph { title: qsTr("Аварии модулей"); }
        ListView {
            visible: chamber.plcLink
            width: parent.width
            height: chamber.alarmDataList.length * 40
            spacing: 10
            model: chamber.alarmDataList
            delegate: Grid {
                width: parent.width
                columns: 3
                columnSpacing: 15
                UINameValueGray{ name: qsTr("Модуль:"); value: modelData.moduleId; pixelSize: 15}
                UINameValueGray{ name: qsTr("Код:"); value: modelData.errorCode; pixelSize: 15}
                UINameValueGray{ name: qsTr("Параметер:"); value: modelData.parameter; pixelSize: 15}
            }
        }
    }
}
