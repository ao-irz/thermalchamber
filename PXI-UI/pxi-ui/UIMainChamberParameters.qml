import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import SparkUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

Grid {
    property var chamberObject: value

    visible: chamberObject.plcLink
    width: parent.width/2
    anchors.verticalCenter: parent.verticalCenter
    columns: 3
    columnSpacing: 10
    Item{
        Timer{
            id: timeoutTimer
            running: true
            interval: 250
            repeat: true
            onTriggered:
            {
                upsValue.value = chamberObject.getElapsedTime()
            }
        }
    }

    UINameValueGray { name: qsTr("Температура камеры(t°):"); value: chamberObject.mainTemperature; units: "°"; visible: chamberObject.plcLink; pixelSize: 35; height: 45 }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }

    UINameValueGray{
        name: qsTr("Режим камеры: ");
        value: (chamberObject.thermalChamberMode === ChamberData.ChamberManualMode) ?
                   (qsTr("Включена")) : ((chamberObject.thermalChamberMode === ChamberData.ChamberStandByMode) ? (qsTr("В ожидании")) : (qsTr("Аварийная остановка")));
        valueColor: (chamberObject.thermalChamberMode === ChamberData.ChamberManualMode) ?
                        ("green") : ((chamberObject.thermalChamberMode === ChamberData.ChamberStandByMode) ? ("lightblue") : ("red"));
    }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }

    UINameValueGray{ name: qsTr("Целевая температура: "); value: chamberObject.thermalChamberTemperatureSet; units: "°" }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }

    UINameValueGray{ name: qsTr("Ограничение: "); value: chamberObject.thermalChamberTemperatureSpeed; units: "°/мин" }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }

    /*UINameValueGray{ name: qsTr("Темп. стойки (сред.): "); value: chamberObject.calculatedRackTemerature; }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }*/

//    UINameValueGray{ name: qsTr("Обороты вент.: "); value: chamberObject.fanFrequency; units: "об/мин" }
//    Rectangle { width: 20; height: 20; color: "transparent" }//UINameValueGray{ name: qsTr("Состояние: "); value: (/*chamberObject.isFanOk*/true) ? (qsTr("исправен")) : (qsTr("не исправен")); valueColor: (/*chamberObject.isFanOk*/true) ? ("green") : ("red") }
//    Rectangle { width: 20; height: 20; color: "transparent" }

    UINameValueGray{ id:upsValue; name: qsTr("Питание стойки:"); value: chamberObject.ups220 ? chamberObject.getElapsedTime() : chamberObject.getElapsedTime(); valueColor: (chamberObject.ups220) ? ("green") : ("red") }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }

    UINameValueGray { name: qsTr("Состояние камеры:"); value: (chamberObject.isThermalChamberOk) ? (qsTr("исправна")) : (qsTr("не исправна")); valueColor: (chamberObject.isThermalChamberOk) ? ("green") : ("red"); visible: chamberObject.plcLink }
    Rectangle { width: 20; height: 20; color: "transparent" }
    Rectangle { width: 20; height: 20; color: "transparent" }
}
