import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import SparkUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

ApplicationWindow {
    id: mainWindow

    visible: true
    //width: 1280
    //height: 768
    title: qsTr("Spark User Interface")
    //visibility: "FullScreen"
    visibility: "Maximized"
    flags: Qt.Dialog | Qt.FramelessWindowHint

    property Item detailedSlot
    property date currentDateTime: new Date()
    property date pauseDateTime
    property bool firstStratup: true

    onClosing: {
        close.accepted = false
        quitConfirmationWindow.open()
    }

    Component.onCompleted: {
        authWindow.open()
    }

    ListModel {
        id: loginList
        ListElement {text: qsTr("Пользователь")}
        ListElement {text: qsTr("Администратор")}
    }

    Settings {
        id: settings
        fileName: currentApplicationPath + "/spark-ui-settings.ini"

        property string currentLogin: loginList.get(0).text
        property string adminPassword: Qt.md5("admin")
        property string userPassword: Qt.md5("user")
        property string serverIP: "127.0.0.1" // устанавливается еще и в networkInterface, здесь устанавливать не обязательно
        property string serverPort: "3000"    // устанавливается еще и в networkInterface, здесь устанавливать не обязательно
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        onTriggered: currentDateTime = new Date()
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        onTriggered: networkInterface.sendStateRequest()
    }

    Timer {
        id: noServiceWatchdog
        interval: 4000
        repeat: true
        running: true
        onTriggered: {
            if (!noServiceWindow.opened)
            {
                if (mainWindow.firstStratup)
                {
                    noServiceWindow.open()
                }
            }
        }
    }

    Connections {
        target: networkInterface
        onServiceAlive: {
            noServiceWatchdog.restart()
            if (noServiceWindow.opened)
            {
                if (noServiceWindow.firstStratup)
                    noServiceWindow.firstStratup = false
                noServiceWindow.close()
            }
        }
    }

    Connections {
        target: slotsListModel
        onNotReadyForStart: {
            console.log("ОТКЫВАЕТСЯ ОКНО С ПРЕДУПРЕЖДЕНИЕМ", nSlot, listFaultyDevices)
            equipmentNotReadyWindow.openDialog(nSlot, listFaultyDevices)
        }
    }

    UIImageButton {
        id: menuButton
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Настройки")
        imageSource: "qrc:/images/menu-ui-pngrepo-com.png"
        buttonText: "menu"
        onClicked: settingsWindow.open()
    }

    UIImageButton {
        id: openCyclogramEditorButton
        anchors.right: menuButton.left
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Разрегистрироваться")
        imageSource: "qrc:/images/logout.png"
        onClicked: authWindow.open()
    }

    UIImageButton {
        imageSource: "qrc:/images/unnamed.png"
        anchors.right: openCyclogramEditorButton.left
        anchors.top: parent.top
        anchors.margins: 10
        toolTip: qsTr("Подробности")
        onClicked: { chamberDetailsWindow.open() }
    }

    UIImageButton {
        anchors.rightMargin: 10
        anchors.right: parent.right
        anchors.top: menuButton.bottom
        toolTip: qsTr("Технологические операции")
        imageSource: "qrc:/images/Renewal_Tracking.png"
        onClicked: operationWindow.open()
    }

    UISettingsWindow {
        id: settingsWindow
    }

    UIChamberDetailsWindow {
        id: chamberDetailsWindow
    }

    UINoServiceWindow {
        id: noServiceWindow
    }

    UIOperationsWindow {
        id: operationWindow
    }

    UIQuitConfirmationWindow {
        id: quitConfirmationWindow
    }

    UIAuthWindow {
        id: authWindow
    }

    UIEquipmentNotReadyWindow {
        id: equipmentNotReadyWindow
    }

    Column {
        id: rootItem

        property int rightListWidth: 350

        anchors.fill: parent
        focus: true
        state: "standard"

        Item {
            id: upperItem
            width: parent.width;
            height: parent.height/3

            Column {
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10

                UINameValueGray { name: qsTr("Текущее время:"); value: Qt.formatDateTime(currentDateTime, "yyyy-MM-dd hh:mm:ss") }
                UINameValueGray { name: qsTr("Текущий пользователь:"); value: authWindow.currentLogin }
                UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК левой камеры"); valueColor: "red"; visible: !chamber1.plcLink }
                UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК правой камеры"); valueColor: "red"; visible: !chamber2.plcLink }
            }
            Row {
                anchors.fill: parent
                anchors.leftMargin: 5
                spacing: 20

                UIMainChamberParameters {
                    chamberObject: chamber1
                }

                UIMainChamberParameters {
                    chamberObject: chamber2
                }
            }
        }

        Row {
            id: lowerItem
            width: parent.width
            height: parent.height/3*2

            ListView {
                id: detailList
                width: parent.width - rootItem.rightListWidth
                height: parent.height
                currentIndex: slotsListModel.selectedSlot
                interactive: true
                clip: true
                focus: true
                snapMode: ListView.SnapOneItem
                highlightRangeMode: ListView.StrictlyEnforceRange
                boundsBehavior: Flickable.StopAtBounds
                highlightMoveDuration: 200
                model: slotsListModel
                delegate:
                    UIDetailedScreen { }
                onCurrentIndexChanged: {
                    // обработка вращения мышкой
                    slotsListModel.selectedSlot = currentIndex
                }
            }

            Item {
                id: commonSideColumn
                width: rootItem.rightListWidth
                height: parent.height

                ListView {
                    id: commonSideListView
                    anchors.fill: parent
                    spacing: 5
                    interactive: false
                    clip: true
                    model: slotsListModel
                    delegate: UIEasySlot {}
                }
            }

            Row {
                id: mainGrids

                readonly property int splittingWidth: 10
                property real cellHeight: height/5
                property real cellWidth: width/(slotsListModel.amountOfSlots/5) - splittingWidth/2

                width: parent.width
                height: parent.height

                Rectangle {
                    width: parent.width/2
                    height: parent.height
                    border.color: "transparent"
                    border.width: 2
                    color: "transparent"

                    GridView {
                        id: leftSlotsGrid

                        anchors.fill: parent
                        anchors.rightMargin: mainGrids.splittingWidth
                        cellHeight: mainGrids.cellHeight
                        cellWidth: mainGrids.cellWidth
                        boundsBehavior: Flickable.StopAtBounds
                        model: leftSideModel
                        delegate: UISlot {
                            property var chamber: chamber1
                        }
                    }
                }

                Rectangle {
                    width: 4
                    height: parent.height
                    color: "white"
                }

                Rectangle {
                    width: parent.width/2
                    height: parent.height
                    border.color: "transparent"
                    border.width: 2
                    color: "transparent"

                    GridView {
                        id: rightSlotsGrid

                        anchors.fill: parent
                        anchors.leftMargin: mainGrids.splittingWidth
                        cellHeight: mainGrids.cellHeight
                        cellWidth: mainGrids.cellWidth
                        boundsBehavior: Flickable.StopAtBounds
                        model: rightSideModel
                        delegate: UISlot {
                            property var chamber: chamber2
                        }
                    }
                }
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Escape) {
                rootItem.state = "standard"
                event.accepted = true;
            }
        }

        states: [
            State {
                name: "detailed"
                PropertyChanges { target: upperItem; height: rootItem.height/4 }
                PropertyChanges { target: mainGrids
                    //height: 0
                    //width: 0
                    visible: false
                }
                PropertyChanges { target: commonSideColumn
                    ////height: rootItem.height
                    //width: rootItem.rightListWidth
                    visible: true
                }
                PropertyChanges { target: detailList
                    width: rootItem.width - rootItem.rightListWidth
                    visible: true
                }
                PropertyChanges {
                    target: lowerItem
                    width: rootItem.width - rootItem.rightListWidth
                    height: rootItem.height/4*3
                }
            },
            State {
                name: "standard"
                PropertyChanges { target: upperItem; height: rootItem.height/3 }
                PropertyChanges { target: mainGrids
                    //height: rootItem.height/3*2
                    //width: rootItem.width
                    visible: true
                }
                PropertyChanges { target: commonSideColumn
                    ////height: 0
                    //width: 0
                    visible: false
                }
                PropertyChanges { target: detailList
                    width: 0
                    visible: false
                }
                PropertyChanges {
                    target: lowerItem
                    width: rootItem.width
                    height: rootItem.height/3*2
                }
            }
        ]
        transitions: Transition {
            NumberAnimation { properties: "width,height" }
        }
    }
}
