import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

Dialog {
    property int m_slotNumber
    property string m_faultyDevices

    function openDialog(nSlot, faultyDevices) {
        m_slotNumber = nSlot
        m_faultyDevices = faultyDevices
        open()
    }

    width: parent.width/3
    height: parent.height/3
    modal: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: parent
    title: qsTr("Невозможно начать тест")
    standardButtons: Dialog.Ok
    focus: true

    Column {
        anchors.fill: parent
        spacing: 10

        Text {
            width: parent.width
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignJustify
            font.pixelSize: 20
            color: "white"
            text: qsTr("Тест не может быть начат в это слоте №%1 по причине неработоспособности следующего перечня оборудования:").arg(m_slotNumber+1)
        }

        Rectangle {
            color: "white"
            width: parent.width
            height: 2
        }

        Text {
            width: parent.width
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignJustify
            font.pixelSize: 20
            color: "red"
            text: m_faultyDevices
        }
    }
    //onAccepted: Qt.quit()
}
