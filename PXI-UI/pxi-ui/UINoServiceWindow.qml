import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "SimpleControls"
import "ScreenComponents"

Dialog {
    property bool firstStratup: true
    width: parent.width/3*2
    height: parent.height/3*2
    modal: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: parent

    Text {
        anchors.centerIn: parent
        font.pixelSize: 50
        color: "red"
        text: firstStratup == true ? ("Инициализация оборудования..."):"Управляющий сервис не доступен"
    }
}
