import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.0

Button {
    id: menuButton

    property string buttonText
    property string imageSource
    property string toolTip

    width: 70
    height: 80
    text: text
    ToolTip.visible: (toolTip === "") ? false : hovered
    ToolTip.text: toolTip

    Image {
        width: 50
        height: 50
        anchors.centerIn: parent
        source: imageSource
    }
}
