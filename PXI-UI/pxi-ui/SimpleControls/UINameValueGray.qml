import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
    id: root

    height: 30
    width: nameContent.width + valueContent.width//parent.width
    clip: true

    property int pixelSize: 20
    property string name
    property var value
    property string units
    property string valueColor: "lightgray"

    Row {
        id: textContent

        height: nameContent.height + valueContent.height
        anchors.fill: parent

        Text {
            id: nameContent
            text: root.name + " "
            font.bold: true
            color: "white"
            font.pixelSize: pixelSize
        }

        Text {
            id: valueContent
            // для string вот так округлять parseFloat(value).toFixed(2)
            text: ((typeof value === 'number') ? ((Number.isInteger(value)) ? (value) : (value.toFixed(2)) ) : (value)) + " " + units
            color: valueColor
            font.pixelSize: pixelSize
        }
    }
}
