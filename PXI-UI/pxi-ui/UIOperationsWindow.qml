import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12 as QtQuickControl
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import SparkUIApplication 1.0
import "SimpleControls"
import "ScreenComponents"

QtQuickControl.Dialog {
    id: root    
    width: parent.width/2
    height: parent.height/4*3
    title: qsTr("Технологические операции")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Close

    property var thermoprofileStates: (new Map([
        [ ChamberData.ThermoprofileNoState,      qsTr("Не загружена")],
        [ ChamberData.ThermoprofileNormalState,  qsTr("Работает")],
        [ ChamberData.ThermoprofilePauseState,   qsTr("На паузе")],
        [ ChamberData.ThermoprofileProblemState, qsTr("Ошибка")],
    ]))
    property var thermoprofileStatesColor: (new Map([
        [ ChamberData.ThermoprofileNoState,      qsTr("gray")],
        [ ChamberData.ThermoprofileNormalState,  qsTr("green")],
        [ ChamberData.ThermoprofilePauseState,   qsTr("gray")],
        [ ChamberData.ThermoprofileProblemState, qsTr("red")],
    ]))

    function getStateString(stateId) {
        if (thermoprofileStates.has(stateId))
            return thermoprofileStates.get(stateId)
        return "неизвестное состояние"
    }
    function getStateColorString(stateId) {
        if (thermoprofileStatesColor.has(stateId))
            return thermoprofileStatesColor.get(stateId)
        return qsTr("gray")
    }

    FileDialog {
        id: openDialog1
        nameFilters: ["Text files (*.json)"]
        onAccepted: networkInterface.sendRunCyclogramCommand(openDialog1.fileUrl, chamber1.termoSlotChannelNumber)
    }
    FileDialog {
        id: openDialog2
        nameFilters: ["Text files (*.json)"]
        onAccepted: networkInterface.sendRunCyclogramCommand(openDialog2.fileUrl, chamber2.termoSlotChannelNumber)
    }

    QtQuickControl.ScrollView {
        anchors.fill: parent
        clip: true

        Column {
            anchors.fill: parent
            spacing: 10

            Row {
                width: parent.width
                spacing: 10

                Column {
                    width: (parent.width-parent.spacing)/2
                    spacing: 10

                    Item { width: root.width-60; height: 1; }
                    UIChamberParagraph { title: qsTr("Изъятие, замена, установка в левой камере"); }
                    UINameValueGray { name: qsTr("Состояние тех. паузы в левой камере:"); value: (chamber1.pauseOn) ? (qsTr("установлена пауза")) : (qsTr("нет")); valueColor: (chamber1.pauseOn) ? ("red") : ("green"); pixelSize: 15 }
                    Column {
                        spacing: 10
                        width: parent.width
                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (!chamber1.pauseOn)
                            text: qsTr("Собираюсь открыть крышку левой термокамеры")
                            onClicked: {
                                networkInterface.sendPauseCommand(chamber1.termoSlotChannelNumber)
                                mainWindow.pauseDateTime = new Date()
                            }
                        }

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: chamber1.pauseOn
                            text: qsTr("Можно продолжить тесты в левой камере")
                            onClicked: {
                                networkInterface.sendResumeCommand(chamber1.termoSlotChannelNumber)
                            }
                        }
                    }

                    Item { width: root.width-60; height: 30; }
                    UIChamberParagraph { title: qsTr("Загрузка термо-циклограммы левой камеры"); }
                    UINameValueGray{ name: qsTr("Состояние термопрофиля левой камеры: "); value: getStateString(chamber1.thermoprofileState); valueColor: getStateColorString(chamber1.thermoprofileState); pixelSize: 15 }
                    Column {
                        width: parent.width
                        spacing: 10

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (!chamber1.pauseOn) && (chamber1.thermalChamberMode === ChamberData.ChamberStandByMode)
                            text: qsTr("Загрузить термо-циклограмму в левой камере")
                            onClicked: {
                                openDialog1.open()
                            }
                        }

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (chamber1.thermoprofileState != ChamberData.ThermoprofileNoState)
                            text: qsTr("Завершить")
                            onClicked: {
                                networkInterface.sendBreakCyclogramCommand(chamber1.termoSlotChannelNumber)
                            }
                        }
                    }
                }
                Column {
                    width: (parent.width-parent.spacing)/2
                    spacing: 10

                    Item { width: root.width-60; height: 1; }
                    UIChamberParagraph { title: qsTr("Изъятие, замена, установка в правой камере"); }
                    UINameValueGray { name: qsTr("Состояние тех. паузы в правой камере:"); value: (chamber2.pauseOn) ? (qsTr("установлена пауза")) : (qsTr("нет")); valueColor: (chamber2.pauseOn) ? ("red") : ("green"); pixelSize: 15 }
                    Column {
                        spacing: 10
                        width: parent.width
                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (!chamber2.pauseOn)
                            text: qsTr("Собираюсь открыть крышку правой термокамеры ")
                            onClicked: {
                                networkInterface.sendPauseCommand(chamber2.termoSlotChannelNumber)
                            }
                        }

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: chamber2.pauseOn
                            text: qsTr("Можно продолжить тесты в правой камере")
                            onClicked: {
                                networkInterface.sendResumeCommand(chamber2.termoSlotChannelNumber)
                            }
                        }
                    }

                    Item { width: root.width-60; height: 30; }
                    UIChamberParagraph { title: qsTr("Загрузка термо-циклограммы правой камеры"); }
                    UINameValueGray{ name: qsTr("Состояние термопрофиля правой камеры: "); value: getStateString(chamber2.thermoprofileState); valueColor: getStateColorString(chamber2.thermoprofileState); pixelSize: 15 }
                    Column {
                        width: parent.width
                        spacing: 10

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (!chamber2.pauseOn) && (chamber2.thermalChamberMode === ChamberData.ChamberStandByMode)
                            text: qsTr("Загрузить термо-циклограмму в правой камере")
                            onClicked: {
                                openDialog2.open()
                            }
                        }

                        QtQuickControl.Button {
                            width: (parent.width)
                            enabled: (chamber2.thermoprofileState != ChamberData.ThermoprofileNoState)
                            text: qsTr("Завершить")
                            onClicked: {
                                networkInterface.sendBreakCyclogramCommand(chamber2.termoSlotChannelNumber)
                            }
                        }
                    }
                }
            }

            Item { width: root.width-60; height: 30; }
            UIChamberParagraph { title: qsTr("Управление электрозамками"); }
            Grid {

                width: parent.width
                columnSpacing: 10
                columns: 2
                rows: 2
                Repeater {
                    model: 4
                    QtQuickControl.Button {
                        function buttonText(i) {
                            switch (i) {
                            case 0: return qsTr("Открыть левый верхний")
                            case 1: return qsTr("Открыть правый верхний")
                            case 2: return qsTr("Открыть левый нижний")
                            case 3: return qsTr("Открыть правый нижний")
                            }
                        }
                        width: (parent.width-parent.columnSpacing) / 2
                        text: buttonText(index)
                        icon.source: "qrc:/images/unlock.png"
                        onClicked: {
                            networkInterface.sendOpenElectricalLock(index)
                        }
                    }
                }
            }
        }
    }
}
