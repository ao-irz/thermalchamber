#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QSettings>
#include <QPointer>
#include "../model/ChamberData.h"
#include "../../PXI-common/common.h"
#include "../qttricks/qqmlhelpers.h"
#include "../../PXI-common/common-enumerations.h"

class NetworkInterface : public QObject
{
    Q_OBJECT
    SINGLETON(NetworkInterface)

    enum SocketCommandType
    {
        NoCommand = NO_COMMAND,
        StateResponse = STATE_RESPONSE,
        ReportResponse = REPORT_RESPONSE,
        PrepareRunResponse = PREPARERUN_RESPONSE,
    };

public:
    explicit NetworkInterface(QObject *parent = nullptr);

    void setChamberObjects(QPointer<ChamberData> ptr1, QPointer<ChamberData> ptr2)
    { m_chamber1 = ptr1; m_chamber2 = ptr2; }

    enum LedColor
    {
        NoLed = LED_NO_COLOR,
        WhiteLed = LED_WHITE_COLOR,
        BlueLed = LED_BLUE_COLOR,
        GreenLed = LED_GREED_COLOR,
        YellowLed = LED_YELLOW_COLOR,
        RedLed = LED_RED_COLOR,
        BlinkRedLed = LED_BLINK_RED_COLOR
    };
    Q_ENUM(LedColor)

signals:
    void serviceAlive();

public slots:
    bool sendStateRequest();
    bool sendRunCyclogramCommand(const QUrl &fileUrl, const quint32 nSlot);
    bool sendBreakCyclogramCommand(const quint32 nSlot);
    bool sendRackTemperatureCommand(const quint32 nChamberSlot);
    bool sendAlarmResetCommand(const quint32 nChamberSlot);
    bool sendLedColorCommand(LedColor color);
    bool sendPauseCommand(const quint32 nChamberSlot);
    bool sendResumeCommand(const quint32 nChamberSlot);
    bool sendReportRequest(const quint32 nSlot);
    bool sendPrepareRunCommand(const QUrl & fileUrl, const quint32 nSlot);
    bool sendOpenElectricalLock(qint32 index);

private:
    QPointer<ChamberData> m_chamber1{nullptr};
    QPointer<ChamberData> m_chamber2{nullptr};

    QScopedPointer<QSettings> m_settings;
    QScopedPointer<QTcpSocket> m_socket;
    QScopedPointer<QTimer> m_disconnectTimer;

    bool m_socketFlag{false};
    quint8 m_socketCommand{0};
    quint32 m_socketCommandLength{0};

    void socketHandler();
    void stateResponseHandler(const QByteArray &meat);
    void reportResponseHandler(const QByteArray &meat);
    void prepareRunHandler(const QByteArray &meat);
    void cleanSocketCommand();

};

#endif // NETWORKINTERFACE_H
