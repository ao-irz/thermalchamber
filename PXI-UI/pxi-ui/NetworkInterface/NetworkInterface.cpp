#include <QSslError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QUrl>
#include <QDir>
#include "NetworkInterface.h"
#include "model/SlotsListModel.h"
#include "model/ChamberMainParametersData.h"

struct ChamberMainParametersData;

NetworkInterface::NetworkInterface(QObject *parent) :
    QObject(parent),
    m_settings(new QSettings(QDir::currentPath() + "/spark-ui-settings.ini", QSettings::IniFormat)),
    m_socket(new QTcpSocket()),
    m_disconnectTimer(new QTimer())
{
    if (!m_settings->contains("serverIP"))
        m_settings->setValue("serverIP", DEFAULT_SERVER_IP);
    if (!m_settings->contains("serverPort"))
        m_settings->setValue("serverPort", DEFAULT_SERVER_PORT);

    connect(m_socket.data(), &QTcpSocket::connected, [this]() {
        qDebug() << "connect to service";
        connect(m_socket.data(), &QTcpSocket::readyRead, [this]() {
            //qDebug() << "socket readyRead";
            //qDebug() << m_socket->readAll();
            socketHandler();
        });

        connect(m_socket.data(), &QTcpSocket::disconnected, []() {
            qDebug() << "socket disconnected";
        });
    });

    connect(m_disconnectTimer.data(), &QTimer::timeout, [this]() {
        if (m_socket->state() == QTcpSocket::ConnectedState)
            return;

        m_socket->connectToHost(m_settings->value("serverIP").toString(), m_settings->value("serverPort").toUInt());
    });
    m_disconnectTimer->start(1000);
}

bool NetworkInterface::sendStateRequest()
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QByteArray request;
    request.append(static_cast<char>(STATE_REQUEST));
    request.append("\0\0\0\0", LENGTH_OF_PAYLOAD);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendRunCyclogramCommand(const QUrl & fileUrl, const quint32 nSlot)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QFile file( fileUrl.toLocalFile() );
    if (!file.open(QIODevice::ReadOnly))
        return false;

    QByteArray command;
    QByteArray jsonCyclogram(file.readAll());
    quint32 length = jsonCyclogram.size() + NSLOT_LENGTH;
    file.close();

    command.append(static_cast<char>(RUN_CYCLOGRAM_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH);
    command.append(jsonCyclogram);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendBreakCyclogramCommand(const quint32 nSlot)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QByteArray command;
    quint32 length = NSLOT_LENGTH;
    command.append(static_cast<char>(BREAK_CYCLOGRAM_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendRackTemperatureCommand(const quint32 nChamberSlot)
{
    if ((m_socket->state() != QTcpSocket::ConnectedState) || ((nChamberSlot != THERMO_SLOT_NUMBER1) && (nChamberSlot != THERMO_SLOT_NUMBER2)))
        return false;

    auto chamber = (nChamberSlot == THERMO_SLOT_NUMBER1) ? (m_chamber1) : (m_chamber2);
    if (chamber->get_chamberMainParameters().size() < 11)
        return false;

    float chamberParamTempHH = chamber->get_chamberMainParameters().at(0).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamTempLL = chamber->get_chamberMainParameters().at(1).value<ChamberMainParametersData>().value().toFloat();
    quint32 chamberParamAutostop = chamber->get_chamberMainParameters().at(2).value<ChamberMainParametersData>().value().toBool();
    float chamberParamTempAutostop = chamber->get_chamberMainParameters().at(3).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackSP = chamber->get_chamberMainParameters().at(4).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackHH = chamber->get_chamberMainParameters().at(5).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackH = chamber->get_chamberMainParameters().at(6).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackL = chamber->get_chamberMainParameters().at(7).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackLL = chamber->get_chamberMainParameters().at(8).value<ChamberMainParametersData>().value().toFloat();
    float chamberParamRackOverHeat = chamber->get_chamberMainParameters().at(9).value<ChamberMainParametersData>().value().toFloat();
    int upsTimerTimeout =  chamber->get_chamberMainParameters().at(10).value<ChamberMainParametersData>().value().toInt();
    QByteArray command;
    quint32 length = NSLOT_LENGTH + 11 * REAL_PARAM_LENGTH;
    command.append(static_cast<char>(SET_RACK_TEMPERATURE_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nChamberSlot), NSLOT_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamTempHH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamTempLL), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamAutostop), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamTempAutostop), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackSP), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackHH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackH), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackL), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackLL), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&chamberParamRackOverHeat), REAL_PARAM_LENGTH);
    command.append(reinterpret_cast<const char *>(&upsTimerTimeout), REAL_PARAM_LENGTH);
    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendAlarmResetCommand(const quint32 nChamberSlot)
{
    if ((m_socket->state() != QTcpSocket::ConnectedState) || ((nChamberSlot != THERMO_SLOT_NUMBER1) && (nChamberSlot != THERMO_SLOT_NUMBER2)))
        return false;

    QByteArray request;
    quint32 length = NSLOT_LENGTH;
    request.append(static_cast<char>(ALARM_RESET_COMMAND));
    request.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    request.append(reinterpret_cast<const char *>(&nChamberSlot), NSLOT_LENGTH);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendLedColorCommand(LedColor color)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QByteArray command;
    quint32 length = LEDCOLOR_LENGTH;
    command.append(static_cast<char>(SET_LED_COLOR_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&color), LEDCOLOR_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendPauseCommand(const quint32 nChamberSlot)
{
    if ((m_socket->state() != QTcpSocket::ConnectedState) || ((nChamberSlot != THERMO_SLOT_NUMBER1) && (nChamberSlot != THERMO_SLOT_NUMBER2)))
        return false;

    QByteArray request;
    quint32 length = NSLOT_LENGTH;
    request.append(static_cast<char>(PAUSE_COMMAND));
    request.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    request.append(reinterpret_cast<const char *>(&nChamberSlot), NSLOT_LENGTH);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendResumeCommand(const quint32 nChamberSlot)
{
    if ((m_socket->state() != QTcpSocket::ConnectedState) || ((nChamberSlot != THERMO_SLOT_NUMBER1) && (nChamberSlot != THERMO_SLOT_NUMBER2)))
        return false;

    quint32 parameter = 0;

    QByteArray command;
    quint32 length = NSLOT_LENGTH + PARAMETER_LENGTH;
    command.append(static_cast<char>(RESUME_COMMAND));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nChamberSlot), NSLOT_LENGTH);
    command.append(reinterpret_cast<const char *>(&parameter), PARAMETER_LENGTH);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendReportRequest(const quint32 nSlot)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QByteArray command;
    quint32 length = NSLOT_LENGTH;
    command.append(static_cast<char>(REPORT_REQUEST));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH); // payload

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendPrepareRunCommand(const QUrl & fileUrl, const quint32 nSlot)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QFile file( fileUrl.toLocalFile() );
    if (!file.open(QIODevice::ReadOnly))
        return false;

    QByteArray command;
    QByteArray jsonCyclogram(file.readAll());
    quint32 length = jsonCyclogram.size() + NSLOT_LENGTH;
    file.close();

    command.append(static_cast<char>(PREPARERUN_REQUEST));
    command.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    command.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH);
    command.append(jsonCyclogram);

    m_socket->write(command);
    m_socket->waitForBytesWritten();
    return true;
}

bool NetworkInterface::sendOpenElectricalLock(qint32 index)
{
    if (m_socket->state() != QTcpSocket::ConnectedState)
        return false;

    QByteArray request;
    quint32 length = NSLOT_LENGTH;
    request.append(static_cast<char>(OPEN_LOCK_COMMAND));
    request.append(reinterpret_cast<char *>(&length), LENGTH_OF_PAYLOAD);
    request.append(reinterpret_cast<const char *>(&index), PARAMETER_LENGTH);
    m_socket->write(request);
    m_socket->waitForBytesWritten();
    return true;
}

void NetworkInterface::socketHandler()
{
    //! Format specification: 1byte(command) + 4byte(length of payload) + Nbyte(payload)
    qint64 availabeBytes = m_socket->bytesAvailable();
    while (availabeBytes >= HEADER_LENGTH) {
        if (!m_socketFlag) {
            m_socket->getChar(reinterpret_cast<char *>(&m_socketCommand));
            QByteArray lenBA = m_socket->read(LENGTH_OF_PAYLOAD);
            m_socketCommandLength = *(reinterpret_cast<quint32 *>(lenBA.data()));
            m_socketFlag = true;
            availabeBytes -= HEADER_LENGTH;
        }

        if (availabeBytes < m_socketCommandLength)
            return;

        QByteArray meat;
        switch(m_socketCommand)
        {
        case StateResponse:
            meat = m_socket->read(m_socketCommandLength);
            stateResponseHandler(meat);
            cleanSocketCommand();
            break;
        case ReportResponse:
            meat = m_socket->read(m_socketCommandLength);
            reportResponseHandler(meat);
            cleanSocketCommand();
            break;
        case PrepareRunResponse:
            meat = m_socket->read(m_socketCommandLength);
            prepareRunHandler(meat);
            cleanSocketCommand();
            break;
        default:
            meat = m_socket->read(m_socketCommandLength);
            cleanSocketCommand();
        }

        availabeBytes = m_socket->bytesAvailable();
    }
}

void NetworkInterface::stateResponseHandler(const QByteArray &meat)
{
    QJsonParseError error;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(meat, &error);
    if (error.error != QJsonParseError::NoError)
        qDebug() << "config error: " << error.errorString() << " at " << error.offset;

    QJsonObject rootObj = jsonDoc.object();

    SlotsListModel::getInstance()->updateModel(rootObj);

    emit serviceAlive();
}

void NetworkInterface::reportResponseHandler(const QByteArray &meat)
{
    //TODO: сохранение pdf файла (или что-то еще)

    if (meat.size() < 4) {
        qDebug() << "report response error";
        return;
    }

    const quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
    const QByteArray pdfData(meat.mid(NSLOT_LENGTH));

    QFile file("report_" + QString::number(nSlot) + "_" + QDateTime::currentDateTime().toString("yyyyMMdd-hhmmss") + ".pdf");
    if (!file.open(QIODevice::WriteOnly))
        return;
    file.write(pdfData);
    file.close();
}

void NetworkInterface::prepareRunHandler(const QByteArray &meat)
{
    //! TODO: принять true/false и в случае, если false, то в виде строки списко проблемных модулей оборудования

    if (meat.size() < 8) {
        qDebug() << "prepare run response error";
        return;
    }

    const quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
    const quint32 readyStart = *reinterpret_cast<quint32 *>(meat.mid(NSLOT_LENGTH, READYSTART_FLAGLENGTH).data());
    const QByteArray listFaultyDevice = meat.mid(NSLOT_LENGTH + READYSTART_FLAGLENGTH);

    SlotsListModel::getInstance()->updateReadyStart(nSlot, readyStart, listFaultyDevice);
}

void NetworkInterface::cleanSocketCommand()
{
    m_socketFlag = false;
    m_socketCommandLength = 0;
    m_socketCommand = 0;
}
