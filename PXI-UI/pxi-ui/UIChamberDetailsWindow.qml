import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: root
    width: parent.width/2
    height: parent.height/3*2
    title: qsTr("Подробности")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Close

    ColumnLayout {
        anchors.fill: parent
        TabBar {
            id: tabBar
            Layout.fillWidth: true
            currentIndex: 0
            TabButton {
                down: checked
                text: qsTr("Левая камера")
            }
            TabButton {
                down: checked
                text: qsTr("Правая камера")
            }
            TabButton {
                down: checked
                text: qsTr("Вентиляторы и датчики")
            }
        }

        StackLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            currentIndex: tabBar.currentIndex

            UIChamberDetailsPage {
                chamber: chamber1
            }
            UIChamberDetailsPage {
                chamber: chamber2
            }
            UIFansAndSensorsDetailsPage {

            }
        }
    }
}
