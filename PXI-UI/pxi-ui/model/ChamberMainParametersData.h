#ifndef CHAMBERMAINPARAMETERSDATA_H
#define CHAMBERMAINPARAMETERSDATA_H

#include <QDebug>
#include <QVariant>

// Фактически, это параметры, которые передаются в камеру отдельноый специальной командой "parameters"
struct ChamberMainParametersData
{
    Q_GADGET
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QVariant value READ value)

public:
    ChamberMainParametersData() = default;
    ChamberMainParametersData(const QString & name, const QVariant & value) :
        m_name(name),
        m_value(value)
    {   }

    const QString & name() const
    { return m_name; }
    QVariant value() const
    { return m_value; }

    QString m_name;
    QVariant m_value;

};
Q_DECLARE_METATYPE(ChamberMainParametersData)

#endif // CHAMBERMAINPARAMETERSDATA_H
