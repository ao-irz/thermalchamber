#ifndef FANSDATA_H
#define FANSDATA_H

#include <QObject>

class FansData
{
    Q_GADGET
    Q_PROPERTY(int number READ number)
    Q_PROPERTY(float percent READ percent)
    Q_PROPERTY(bool isEnable READ isEnable)
    Q_PROPERTY(QString name READ name)

public:
    FansData() = default;
    FansData(int number, float percent, bool enable) :
        m_number(number),
        m_percent(percent),
        m_isEnable(enable),
        m_name(fanName(m_number))
    {   }

    int number() const
    { return m_number; }
    float percent() const
    { return m_percent; }
    bool isEnable() const
    { return m_isEnable; }
    const QString & name() const
    { return m_name; }

private:
    const QString & fanName(qint32 number) const
    { return (number < m_fanNames.size()) ? m_fanNames.at(number) : m_emptyFansName; }

    static const QStringList m_fanNames;
    static const QString m_emptyFansName;

    int m_number;
    float m_percent;
    bool m_isEnable;
    QString m_name;
};
Q_DECLARE_METATYPE(FansData)

#endif // FANSDATA_H
