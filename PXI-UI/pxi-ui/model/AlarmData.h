#ifndef ALARMDATA_H
#define ALARMDATA_H

#include <QDebug>

struct AlarmData
{
    Q_GADGET
    Q_PROPERTY(int moduleId READ moduleId)
    Q_PROPERTY(int instanceId READ instanceId)
    Q_PROPERTY(int errorCode READ errorCode)
    Q_PROPERTY(float parameter READ parameter)

public:
    AlarmData() = default;

    AlarmData(qint16 id, qint16 instanceNumber, qint16 errorCode, float param) :
        m_moduleId(id), m_instanceId(instanceNumber), m_errorCode(errorCode), m_parameter(param)
    {   }

    int moduleId() const
    { return m_moduleId; }
    int instanceId() const
    { return m_instanceId; }
    int errorCode() const
    { return m_errorCode; }
    float parameter() const
    { return m_parameter; }

    int m_moduleId;
    int m_instanceId;
    int m_errorCode;
    float m_parameter;
};
Q_DECLARE_METATYPE(AlarmData)

#endif // ALARMDATA_H
