#ifndef SIDEPROXYMODEL_H
#define SIDEPROXYMODEL_H

#include <QSet>
#include <QSortFilterProxyModel>

class SideProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    SideProxyModel(QObject *parent = nullptr);
    SideProxyModel(const QSet<qint32> & indexes, QObject *parent = nullptr);

private:
    QSet<qint32> m_acceptedIndexes;

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};

#endif // SIDEPROXYMODEL_H
