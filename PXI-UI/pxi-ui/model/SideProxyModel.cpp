#include "SideProxyModel.h"

SideProxyModel::SideProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

SideProxyModel::SideProxyModel(const QSet<qint32> & indexes, QObject * parent) :
    QSortFilterProxyModel(parent),
    m_acceptedIndexes(indexes)
{

}

bool SideProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    Q_UNUSED(sourceParent)
    return m_acceptedIndexes.contains(sourceRow);
}
