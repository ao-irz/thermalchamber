#ifndef FANSANDSENSORSDATA_H
#define FANSANDSENSORSDATA_H

#include <QObject>
#include <QVariant>
#include <QJsonArray>
#include <QJsonObject>
#include "../../PXI-service/pxi-service/config.h"
#include "../../PXI-common/common.h"
#include "qttricks/qqmlhelpers.h"


class FansAndSensorsData : public QObject
{
    Q_OBJECT
    QML_READONLY_PROPERTY(QVariantList, fans)
    QML_READONLY_PROPERTY(QVariantList, peripheralTemperature)
public:
    explicit FansAndSensorsData(QObject *parent = nullptr);

    void updateAll(const QJsonObject &object);
private:
    void setPeripheralTemperature(const QJsonArray &peripheralArray);
    void setFans(const QJsonArray &fansArray);

signals:

};

#endif // FANSANDSENSORSDATA_H
