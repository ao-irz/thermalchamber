#ifndef SLOTSLISTMODEL_H
#define SLOTSLISTMODEL_H

#include <QDebug>
#include <QAbstractListModel>
#include <QDateTime>
#include <QSharedPointer>
#include <QQmlListProperty>
#include <QVariantList>
#include <QPointer>
#include "qttricks/qqmlhelpers.h"
#include "ChamberData.h"
#include "FansAndSensorsData.h"
#include "../../PXI-common/common.h"
#include "../../PXI-service/pxi-service/config.h"

class SlotsListModel : public QAbstractListModel
{
    Q_OBJECT
    SINGLETON(SlotsListModel)

    QML_WRITABLE_PROPERTY(int, selectedSlot)
    QML_CONSTANT_PROPERTY(int, amountOfSlots)

public:
    SlotsListModel(QObject *parent = nullptr);
    virtual ~SlotsListModel();

    void setAlienObjects(QPointer<ChamberData> ptr1, QPointer<ChamberData> ptr2, QPointer<FansAndSensorsData> ptr3)
    { m_chamber1 = ptr1; m_chamber2 = ptr2; m_fansAndSensors = ptr3; }

    // Обязаны быть переопределены
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QHash<int, QByteArray> roleNames() const override;
    //int columnCount(const QModelIndex &parent) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    void updateModel(const QJsonObject &rootObject);
    void updateReadyStart(quint32 nSlot, quint32 readyStart, const QByteArray &listFaultyDevice);

    Q_INVOKABLE QString faultyDevices(int index) const;

    enum SlotState {
        NoSlotState = NO_SLOT_STATE,
        WaitingStartSlotState = WAITING_START_SLOT_STATE,
        HardwareErrorSlotState = HARDWARE_ERROR_SLOT_STATE,
        NormalSlotState = NORMAL_SLOT_STATE,
        PauseSlotState = PAUSE_SLOT_STATE,
        //WarningSlotState = WARNING_SLOT_STATE,
        AlarmSlotState = ALARM_SLOT_STATE,
        TestCompletedSlotState = TEST_COMPLETED_SLOT_STATE
    };
    Q_ENUM(SlotState)

    enum Column {
        SlotNumberRole = Qt::UserRole + 1,
        NameRole,
        BatchRole,
        DateInputRole,
        DateOutputRole,
        EmergencyStopTimeRole,
        EmergencySlotDescription,

        PowerSupplyCount, // простое количество активных источников
        PowerSupplyNumber,  // массив номеров активных источниклв

        VoltageArrayRole,
        VoltageMinArrayRole,
        VoltageMaxArrayRole,
        CurrentArrayRole,
        CurrentMinArrayRole,
        CurrentMaxArrayRole,

        GeneratorAmplitudeRole,
        GeneratorFrequencyRole,
        GeneratorDutycycleRole,

        DIOOkRole,
        DIOVoltageLogicRole,

        SMUCurrentRole,
        SMUVoltageRole,

        ChipTemperatureRole,                // приходит с pxi-thermocouple
        ChipTemperatureMaxRole,
        ChipTemperatureSensorOkRole,        //! TODO: пока не известно, будет это существовать или нет

        ColorRole,
        StateRole,

        IsActiveRole,

        ReadyStartRole,

        EndOfColumnRole
    };
    Q_ENUM(Column)

    struct SlotData
    {
        SlotData(const qint32 slotNumber,
                 const QString &name,
                 const QString &batch,
                 const QDateTime &dateInput,
                 const QDateTime &dateOutput,
                 const QString &emergencyDescription,
                 SlotState state) :
            slotNumber(slotNumber),
            name(name),
            batch(batch),
            dateInput(dateInput),
            dateOutput(dateOutput),
            emergencyStopTime(dateOutput), // ну как-то проиниц-ть надо
            emergencySlotDescription(emergencyDescription),
            state(state)
        {   }

        void setChipTemperature(double v, bool ok, double max)
        { isChipTemperatureValid = true; chipTemperature = v; chipTemperatureSensorOk = ok; chipTemperatureMax = max;}

        void setGeneratorInfo(double a, double f, double d)
        { isGeneratorValid = true; generatorAmplitude = a; generatorFrequency = f; generatorDutycycle = d; }

        void setReadyStart(quint32 readyStart, const QByteArray & listFaultyDevice)
        { m_readyStart = readyStart; m_listFaultyDevices = listFaultyDevice; }

        bool m_selectedByUser{false};

        qint32 slotNumber;
        QString name;
        QString batch;
        QDateTime dateInput;
        QDateTime dateOutput;
        QDateTime emergencyStopTime;
        QString emergencySlotDescription;
        SlotState state;

        QList<int> powerSupplyNumberArray;

        QList<double> voltageArray;
        QList<double> voltageMinArray;
        QList<double> voltageMaxArray;
        QList<double> currentArray;
        QList<double> currentMinArray;
        QList<double> currentMaxArray;

        bool isGeneratorValid{false};
        double generatorAmplitude;
        double generatorFrequency;
        double generatorDutycycle;

        bool isDioValid{false};
        bool dioOk;
        double dioVoltageLogic;

        bool isSmuValid{false};
        double smuCurrent;
        double smuVoltage;

        bool isChipTemperatureValid{false};
        double chipTemperature{0.0};
        double chipTemperatureMax{0.0};
        bool chipTemperatureSensorOk{true}; // не факт, что будет

        bool m_readyStart{false};
        QString m_listFaultyDevices;
    };

signals:
    void notReadyForStart(qint32 nSlot, const QString &listFaultyDevices); // для открытия окна c предупреждением

private:
    QPointer<ChamberData> m_chamber1{nullptr};
    QPointer<ChamberData> m_chamber2{nullptr};
    QPointer<FansAndSensorsData> m_fansAndSensors{nullptr};
    //QList<QSharedPointer<SlotData> > m_rows;
    QHash<quint32, QSharedPointer<SlotData> > m_rows;

    const QHash<SlotState, QString> m_colorTable;
};

#endif // SLOTSLISTMODEL_H
