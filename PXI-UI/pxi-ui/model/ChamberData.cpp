#include <QJsonObject>
#include <QJsonArray>
#include "../model/AlarmData.h"
#include "ChamberData.h"
//#include "PeripheralTemperatureData.h"

ChamberData::ChamberData(quint32 nThermoSlot, QObject *parent) :
    QObject(parent),
    m_termoSlotChannelNumber(nThermoSlot),
    m_plcLink(false)
{

}

void ChamberData::updateAll(const QJsonObject &rootObject, int chamberNumber)
{
    update_pauseOn( rootObject.value("pauseOn").toBool() );
    update_plcLink( rootObject.value("plcLink").toBool() );
    update_thermoprofileState( rootObject.value("thermoprofileState").toInt() );

    setChamberMainParameters(rootObject, chamberNumber);

    if (m_plcLink) {
        update_plcLink(true);

        setAlarms(rootObject.value("modulesAlarms").toArray());

        setExt(rootObject.value("extDI").toInt(), rootObject.value("extDO").toInt());
        setCalculatedRackTemerature(rootObject.value("calculatedRackTemerature").toDouble());
        //setFan(rootObject.value("fanErrorCode").toInt(), rootObject.value("fanFrequency").toDouble());

        //! Остальная куча параметров UPS сервисом пробрасывается, но здесь за ненадобностью не парсится
        bool upsValue = (bool)!(rootObject.value("bitsStatusUPS").toInt() & 0x0012);
        if (get_ups220() != upsValue)
        {
            upsValueChanged = true;
        }
        else
            upsValueChanged = false;
        update_ups220( !(rootObject.value("bitsStatusUPS").toInt() & 0x0012) );
        if (!upsValue && upsValueChanged)
            setUpsInitialTime();

        update_upsLink( (bool)rootObject.value("upsLink").toInt() );
        update_cameraLink( (bool)rootObject.value("cameraLink").toInt() );


        setThermalChamberParameters(
                    rootObject.value("thermalChamberErrorCode").toInt(),
                    rootObject.value("mainTemperature").toDouble(),
                    rootObject.value("thermalChamberSlidedTemperatureSet").toDouble(),
                    rootObject.value("thermalChamberMode").toInt(),
                    rootObject.value("thermalChamberTemperatureSet").toDouble(),
                    rootObject.value("thermalChamberTemperatureSpeed").toDouble());


        //setPeripheralTemperature(rootObject.value("peripheralTemperature").toArray());
    }
}

void ChamberData::setThermalChamberParameters(qint16 cameraOk, double mainTemp, double slideTempSet, qint16 mode, double tempSet, double tempSpeed)
{
    update_thermalChamberErrorCode(cameraOk);
    update_isThermalChamberOk(cameraOk == PLC_THERMAL_CHAMBER_ERROR_CODE_OK);
    update_mainTemperature(mainTemp);
    update_thermalChamberSlidedTemperatureSet(slideTempSet);
    update_thermalChamberMode(mode);
    update_isThermalChamberOn(mode == PLC_CHAMBER_MANUAL_MODE);
    update_thermalChamberTemperatureSet(tempSet);
    update_thermalChamberTemperatureSpeed(tempSpeed);
}

void ChamberData::setAlarms(const QJsonArray & alarmArray)
{
    m_alarmDataList.clear();

    for (auto i = alarmArray.constBegin(); i != alarmArray.constEnd(); ++i) {
        const QJsonObject & objectRef = (*i).toObject();
        m_alarmDataList.append(
                    QVariant::fromValue(AlarmData( objectRef.value("moduleId").toInt(),
                                                   objectRef.value("instanceNumber").toInt(),
                                                   objectRef.value("errorCode").toInt(),
                                                   objectRef.value("parameter").toDouble())));
    }

    emit alarmDataListChanged(m_alarmDataList);
}

void ChamberData::setExt(int extDO, int extDI)
{
    update_extDO(extDO);
    update_extDI(extDI);
}

void ChamberData::setCalculatedRackTemerature(double temp)
{
    update_calculatedRackTemerature(temp);
}

//void ChamberData::setFan(qint16 fanOk, double freq)
//{
//    update_fanFrequency(freq);
//    update_isFanOk(fanOk == PLC_FAN_STATUS_OK);
//}

void ChamberData::setChamberMainParameters(const QJsonObject &rootObject, int chamberNumber)
{
    m_chamberMainParameters.clear();
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Уставка защиты по перегреву камеры", rootObject.value("chamberParamTempHH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Уставка защиты по переохлаждению камеры", rootObject.value("chamberParamTempLL").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Разрешение останова холодильника", rootObject.value("chamberParamAutostop").toBool())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Температура отключения холодильника", rootObject.value("chamberParamTempAutostop").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Температура стойки (для вентилятора)", rootObject.value("chamberParamRackSP").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога HH температуры стойки", rootObject.value("chamberParamRackHH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога H температуры стойки", rootObject.value("chamberParamRackH").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога L температуры стойки", rootObject.value("chamberParamRackL").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Задание порога LL температуры стойки", rootObject.value("chamberParamRackLL").toDouble())));
    //! TODO: добавить параметр температуры перегрева стойки

    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Уставка защиты по перегреву стоек", rootObject.value("chamberParamRackOverHeat").toDouble())));
    m_chamberMainParameters.append(QVariant::fromValue(ChamberMainParametersData("Время работы от ИБП", rootObject.value("upsTimerTimeout").toInt())));

    // не посылакть сигнал обновления, потому что тогда окно Settings постоянно будет возвращаться к показу текущего значения
    //emit chamberMainParametersChanged(m_chamberMainParameters);
}

void ChamberData::setChamberParameterValue(int index, QVariant value)
{
    ((ChamberMainParametersData *)(m_chamberMainParameters[index].data()))->m_value = value;

    // тоже нельзя здесь посылать сигнал, потому что обновлять в цикле в qml будет много параметров
    //emit chamberMainParametersChanged(m_chamberMainParameters);
}

QString ChamberData::getName(int index)
{
     return ((ChamberMainParametersData *)(m_chamberMainParameters[index].data()))->m_name;
}

QVariant ChamberData::getValue(int index)
{
    return ((ChamberMainParametersData *)(m_chamberMainParameters[index].data()))->m_value;
}

void ChamberData::setUpsInitialTime()
{
    startTime = QDateTime::currentDateTime();
    endTime = startTime.addSecs(getValue(m_chamberMainParameters.length() - 1).toInt() * 60);
}

QString ChamberData::getElapsedTime()
{
    QString result;
    if (!get_ups220())
    {
        QTime elapsedTime(0, 0, 0);
        elapsedTime = elapsedTime.addMSecs(QDateTime::currentDateTime().msecsTo(endTime));
        result = (startTime.msecsTo(endTime)) == 0? "ИБП":"ИБП " + elapsedTime.toString("mm:ss");
    }
    else
        result = "220";
    return result;
}
