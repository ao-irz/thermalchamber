#include "FansAndSensorsData.h"
#include "PeripheralTemperatureData.h"
#include "FansData.h"

FansAndSensorsData::FansAndSensorsData(QObject *parent) :
    QObject(parent)
{

}

void FansAndSensorsData::updateAll(const QJsonObject & object)
{
    setPeripheralTemperature(object.value("peripheralTemperature").toArray());
    setFans(object.value("fans").toArray());
}

void FansAndSensorsData::setPeripheralTemperature(const QJsonArray &peripheralArray)
{
    m_peripheralTemperature.clear();

    for (auto i = peripheralArray.constBegin(); i != peripheralArray.constEnd(); ++i) {
        const QJsonObject & objectRef = (*i).toObject();
        m_peripheralTemperature.append(
                    QVariant::fromValue(PeripheralTemperatureData(
                                            objectRef.value("number").toInt(),
                                            objectRef.value("temperature").toDouble(),
                                            objectRef.value("sensorOk").toInt())));
    }

    emit peripheralTemperatureChanged(m_peripheralTemperature);
}

void FansAndSensorsData::setFans(const QJsonArray &fansArray)
{
    m_fans.clear();

    for (auto i = fansArray.constBegin(); i != fansArray.constEnd(); ++i) {
        const QJsonObject & objectRef = (*i).toObject();
        m_fans.append(
                    QVariant::fromValue(FansData(
                                            objectRef.value("number").toInt(),
                                            objectRef.value("percent").toDouble(),
                                            objectRef.value("enable").toBool())));
    }

    emit fansChanged(m_fans);
}
