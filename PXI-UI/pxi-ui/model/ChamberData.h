#ifndef CHAMBERDATA_H
#define CHAMBERDATA_H

#include <QObject>
#include <QVariant>
#include "../../PXI-service/pxi-service/config.h"
#include "../../PXI-common/common.h"
#include "qttricks/qqmlhelpers.h"
#include "ChamberMainParametersData.h"
#include <QDateTime>

/*
 * Класс - простое хранилище всех properies для одной (любой, левой или правой) термокамеры
 */

class ChamberData : public QObject
{
    Q_OBJECT

public:
    enum ChamberMode {
        ChamberStandByMode = PLC_STANDBY_MODE,
        ChamberManualMode = PLC_CHAMBER_MANUAL_MODE,
        ChamberAlarmMode = PLC_CHAMBER_ALARM_MODE
    };
    Q_ENUM(ChamberMode)

    enum ThermoprofileState { // нет смысла вводить новые define
        ThermoprofileNoState = NO_SLOT_STATE,
        ThermoprofileNormalState = NORMAL_SLOT_STATE,
        ThermoprofilePauseState = PAUSE_SLOT_STATE,
        ThermoprofileProblemState = ALARM_SLOT_STATE
    };
    Q_ENUM(ThermoprofileState)

    QML_CONSTANT_PROPERTY(int, termoSlotChannelNumber)
    QML_READONLY_PROPERTY(int, pauseOn)
    QML_READONLY_PROPERTY(bool, plcLink)
    QML_READONLY_PROPERTY(int, thermoprofileState)
    QML_READONLY_PROPERTY(int, systemInfoId)
    QML_READONLY_PROPERTY(int, systemInfoVersion)
    QML_READONLY_PROPERTY(int, systemInfoErrorCode)
    QML_READONLY_PROPERTY(int, systemInfoDeviceVersion)
    QML_READONLY_PROPERTY(int, systemInfoHWVersion)
    QML_READONLY_PROPERTY(int, systemInfoSWVersion)
    QML_READONLY_PROPERTY(int, systemInfoSerialNumber)

    QML_READONLY_PROPERTY(int, thermalChamberId)
    QML_READONLY_PROPERTY(int, thermalChamberErrorCode)
    QML_READONLY_PROPERTY(bool, isThermalChamberOk)                 // производый от thermalChamberErrorCode
    QML_READONLY_PROPERTY(double, mainTemperature)                  //! TEMPERATURE
    QML_READONLY_PROPERTY(double, thermalChamberSlidedTemperatureSet) // (по видимому речь идет об установленном параметре)
    QML_READONLY_PROPERTY(int, thermalChamberMode)
    QML_READONLY_PROPERTY(bool, isThermalChamberOn)                 // производый от thermalChamberMode
    QML_READONLY_PROPERTY(double, thermalChamberTemperatureSet)     // цеелвая температура
    QML_READONLY_PROPERTY(double, thermalChamberTemperatureSpeed)   // (по видимому речь идет об установленном ограничительном параметре)

    QML_READONLY_PROPERTY(QVariantList, chamberMainParameters)

    QML_READONLY_PROPERTY(int, accessoriesChamberId)
    QML_READONLY_PROPERTY(int, optionsChamberId)
    QML_READONLY_PROPERTY(int, alarmChamberId)

    QML_READONLY_PROPERTY(QVariantList, alarmDataList)
    //QML_READONLY_PROPERTY(QList<AlarmData *>, alarmDataList) // так и не смог запустить :(

    QML_READONLY_PROPERTY(int, extDI)
    QML_READONLY_PROPERTY(int, extDO)

    //QML_READONLY_PROPERTY(QVariantList, peripheralTemperature)

    QML_READONLY_PROPERTY(double, calculatedRackTemerature) //! TEMPERATURE
    //QML_READONLY_PROPERTY(double, fanFrequency)
    //QML_READONLY_PROPERTY(bool, isFanOk)                            // не производный тип, просто сразу интерпретированный в bool

    QML_READONLY_PROPERTY(bool, ups220)                             // питание от 220В/питание от батарей UPS

    QML_READONLY_PROPERTY(bool, upsLink)
    QML_READONLY_PROPERTY(bool, cameraLink)

public:
    explicit ChamberData(quint32 nThermoSlot, QObject *parent = nullptr);

    void updateAll(const QJsonObject &rootObject, int chamberNumber);

    Q_INVOKABLE void setChamberParameterValue(int index, QVariant value);

    Q_INVOKABLE QString getName(int index);
    Q_INVOKABLE QVariant getValue(int index);

    Q_INVOKABLE void setUpsInitialTime();

    Q_INVOKABLE QString getElapsedTime();

private:
    //void setFan(qint16 fanOk, double freq);
    void setCalculatedRackTemerature(double temp);
    void setExt(int extDO, int extDI);
    void setAlarms(const QJsonArray &alarmArray);
    void setThermalChamberParameters(qint16 cameraOk, double mainTemp, double slideTempSet, qint16 mode, double tempSet, double tempSpeed);
    void setChamberMainParameters(const QJsonObject &rootObject, int chamberNumber);

    QDateTime startTime = QDateTime::currentDateTime();
    QDateTime endTime;
    bool upsValueChanged{false};
};

#endif // CHAMBERDATA_H
