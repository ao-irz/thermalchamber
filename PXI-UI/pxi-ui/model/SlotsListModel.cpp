#include <math.h>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QModelIndex>
#include <QRandomGenerator>
#include "SlotsListModel.h"
#include "ChamberMainParametersData.h"
#include "AlarmData.h"
//#include "PeripheralTemperatureData.h"
#include "../../PXI-service/pxi-service/config.h"

// Количество комплектов

SlotsListModel::SlotsListModel(QObject * parent) :
    QAbstractListModel(parent),
    m_selectedSlot(-1),
    m_amountOfSlots(AMOUNT_OF_SLOTS),
    m_colorTable({
                 {NoSlotState, "#778899"},
                 {WaitingStartSlotState, "#90ee90" /*"#fff0f5"*/ /*"#ffd700"*/},
                 {HardwareErrorSlotState, "#ff0000"/*"#f08080"*/},
                 {NormalSlotState, "#32cd32"},
                 {PauseSlotState, "#d3d3d3"},
                 //{WarningSlotState, "#ff7f00"},
                 {AlarmSlotState, "#ff7f00"},
                 {TestCompletedSlotState, "#008b8b"}})
{
    // Создать все AMOUNT_OF_SLOTS слотов
    for (qint32 i=0; i < AMOUNT_OF_SLOTS; i++) {
        QSharedPointer<SlotData> emptySlot( new SlotData(i, QString(), QString(), QDateTime::currentDateTime(),  QDateTime::currentDateTime(), QString(), NoSlotState) );
        m_rows.insert(i, emptySlot);
    }
}

SlotsListModel::~SlotsListModel()
{
    m_rows.clear();
}

int SlotsListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return AMOUNT_OF_SLOTS;
}

QVariant SlotsListModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid()) || (index.row() >= AMOUNT_OF_SLOTS))
        return QVariant();

    if (!m_rows.contains(index.row())) {
        return QVariant();
    }

    switch(role)
    {
    case SlotNumberRole:
        return m_rows.value(index.row())->slotNumber;
    case NameRole:
        return m_rows.value(index.row())->name;
    case BatchRole:
        return m_rows.value(index.row())->batch;
    case DateInputRole:
        return m_rows.value(index.row())->dateInput.toString(DB_DATETIME_FORMAT);
    case DateOutputRole:
        return m_rows.value(index.row())->dateOutput.toString(DB_DATETIME_FORMAT);
    case EmergencyStopTimeRole:
        return m_rows.value(index.row())->emergencyStopTime.toString(DB_DATETIME_FORMAT);
    case EmergencySlotDescription:
        return m_rows.value(index.row())->emergencySlotDescription;

    case PowerSupplyCount:
        return m_rows.value(index.row())->powerSupplyNumberArray.size();
    case PowerSupplyNumber:
        return QVariant::fromValue(  m_rows.value(index.row())->powerSupplyNumberArray  );

    case VoltageArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->voltageArray  );
    case VoltageMinArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->voltageMinArray  );
    case VoltageMaxArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->voltageMaxArray  );

    case CurrentArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->currentArray  );
    case CurrentMinArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->currentMinArray  );
    case CurrentMaxArrayRole:
        return QVariant::fromValue(  m_rows.value(index.row())->currentMaxArray  );

    case GeneratorAmplitudeRole:
        return (m_rows.value(index.row())->isGeneratorValid) ? (m_rows.value(index.row())->generatorAmplitude) : (QVariant(QStringLiteral("n/a")));
    case GeneratorFrequencyRole:
        return (m_rows.value(index.row())->isGeneratorValid) ? (m_rows.value(index.row())->generatorFrequency) : (QVariant(QStringLiteral("n/a")));
    case GeneratorDutycycleRole:
        return (m_rows.value(index.row())->isGeneratorValid) ? (m_rows.value(index.row())->generatorDutycycle) : (QVariant(QStringLiteral("n/a")));

    case DIOOkRole:
        return (m_rows.value(index.row())->isDioValid) ? (m_rows.value(index.row())->dioOk) : (QVariant(QStringLiteral("n/a")));
    case DIOVoltageLogicRole:
        return (m_rows.value(index.row())->isDioValid) ? (m_rows.value(index.row())->dioVoltageLogic) : (QVariant(QStringLiteral("n/a")));

    case SMUCurrentRole:
        return (m_rows.value(index.row())->isSmuValid) ? (m_rows.value(index.row())->smuCurrent) : (QVariant(QStringLiteral("n/a")));
    case SMUVoltageRole:
        return (m_rows.value(index.row())->isSmuValid) ? (m_rows.value(index.row())->smuVoltage) : (QVariant(QStringLiteral("n/a")));

    case ChipTemperatureRole:
        return (m_rows.value(index.row())->isChipTemperatureValid) ? (m_rows.value(index.row())->chipTemperature) : (QVariant(QStringLiteral("n/a")));
    case ChipTemperatureMaxRole:
        return (m_rows.value(index.row())->isChipTemperatureValid) ? (m_rows.value(index.row())->chipTemperatureMax) : (QVariant(QStringLiteral("n/a")));
    case ChipTemperatureSensorOkRole:
        return (m_rows.value(index.row())->isChipTemperatureValid) ?
                    ((m_rows.value(index.row())->chipTemperatureSensorOk) ? (QVariant(QStringLiteral("исправен"))) : (QVariant(QStringLiteral("поврежден")))) :
                    (QVariant(QStringLiteral("n/a")));

    case ColorRole:
        return m_colorTable.value(m_rows.value(index.row())->state);
    case StateRole:
        return m_rows.value(index.row())->state;
    case IsActiveRole:
        return m_rows.value(index.row())->state != NoSlotState;
    case ReadyStartRole:
        return m_rows.value(index.row())->m_readyStart;

    default:
        return QVariant();
    }

    return QVariant();
}

bool SlotsListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ((!index.isValid()) || (index.row() >= AMOUNT_OF_SLOTS) || data(index, role) == value)
        return false;

    if (!m_rows.contains(index.row())) {
        return false;
    }

    if (role == ReadyStartRole) {
        m_rows.value(index.row())->m_readyStart = value.toBool();
        emit dataChanged(index, index, {ReadyStartRole});
        return true;
    }

    return false;
}

Qt::ItemFlags SlotsListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> SlotsListModel::roleNames() const
{
    static const QHash<int, QByteArray> roles
            ({
                 {SlotNumberRole, "channelNumber"},
                 {NameRole, "name"},
                 {BatchRole, "batch"},
                 {DateInputRole, "dateInput"},
                 {DateOutputRole, "dateOutput"},
                 {EmergencyStopTimeRole, "emergencyStopTime"},
                 {EmergencySlotDescription, "emergencySlotDescription"},

                 {PowerSupplyCount, "powerSupplyCount"}, // простое количество активных источников
                 {PowerSupplyNumber, "powerSupplyNumber"}, // массив номеров активных источниклв

                 {VoltageArrayRole, "voltageArray"},
                 {VoltageMinArrayRole, "voltageMinArray"},
                 {VoltageMaxArrayRole, "voltageMaxArray"},

                 {CurrentArrayRole, "currentArray"},
                 {CurrentMinArrayRole, "currentMinArray"},
                 {CurrentMaxArrayRole, "currentMaxArray"},

                 {GeneratorAmplitudeRole, "generatorAmplitude"},
                 {GeneratorFrequencyRole, "generatorFrequency"},
                 {GeneratorDutycycleRole, "generatorDutycycle"},

                 {DIOOkRole, "dioOk"},
                 {DIOVoltageLogicRole, "dioVoltageLogic"},

                 {SMUCurrentRole, "smuCurrent"},
                 {SMUVoltageRole, "smuVoltage"},

                 {ChipTemperatureRole, "chipTemperature"},
                 {ChipTemperatureMaxRole, "chipTemperatureMax"},
                 {ChipTemperatureSensorOkRole, "chipTemperatureSensorOk"},
                 {ColorRole, "billetColor"},
                 {StateRole, "stateRole"},
                 {IsActiveRole, "isActive"},
                 {ReadyStartRole, "readyStart"}
             });
    return roles;
}

// Для того что бы Charts отображали правильную "легенду"
QVariant SlotsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)
    Q_UNUSED(role)
    return data( createIndex(section, 0), SlotNumberRole );
}

void SlotsListModel::updateModel(const QJsonObject &rootObject)
{
    //m_rows.clear();

    QSet<qint32> availableSlots;
    for (auto i = rootObject.value("Slots").toArray().constBegin(); i != rootObject.value("Slots").toArray().constEnd(); ++i) {
        const qint32 slotNumber = (*i).toObject().value("slotNumber").toInt();
        if (!m_rows.contains(slotNumber))
            continue;

        QSharedPointer<SlotData> insertedData(new SlotData(
                slotNumber,
                (*i).toObject().value("name").toString(),
                (*i).toObject().value("batch").toString(),
                QDateTime::fromString( (*i).toObject().value("dateInput").toString(), DB_DATETIME_FORMAT),
                QDateTime::fromString( (*i).toObject().value("dateOutput").toString(), DB_DATETIME_FORMAT),
                (*i).toObject().value("emergencySlotDescription").toString(),
                (SlotState)(*i).toObject().value("state").toInt()));

        if ( !(*i).toObject().value("emergencyStopTime").isUndefined() )
            insertedData->emergencyStopTime = QDateTime::fromString( (*i).toObject().value("emergencyStopTime").toString(), DB_DATETIME_FORMAT);

        for (auto ps : (*i).toObject().value("powerSupplies").toArray()) {
            if (!ps.toObject().value("powerSupplyNumber").isUndefined())
                insertedData->powerSupplyNumberArray.append(ps.toObject().value("powerSupplyNumber").toInt());

            if (!ps.toObject().value("voltage").isUndefined()) {
                insertedData->voltageArray.append(ps.toObject().value("voltage").toDouble());
                insertedData->voltageMinArray.append(ps.toObject().value("voltageMin").toDouble());
                insertedData->voltageMaxArray.append(ps.toObject().value("voltageMax").toDouble());
            }

            if (!ps.toObject().value("current").isUndefined()) {
                insertedData->currentArray.append(ps.toObject().value("current").toDouble());
                insertedData->currentMinArray.append(ps.toObject().value("currentMin").toDouble());
                insertedData->currentMaxArray.append(ps.toObject().value("currentMax").toDouble());
            }
        }

        if (!(*i).toObject().value("generatorAmplitude").isUndefined()) {
            insertedData->setGeneratorInfo((*i).toObject().value("generatorAmplitude").toDouble(),
                                           (*i).toObject().value("generatorFrequency").toDouble(),
                                           (*i).toObject().value("generatorDutycycle").toDouble());
        } else
            insertedData->isGeneratorValid = false;

        if (!(*i).toObject().value("dioOk").isUndefined()) {
            insertedData->dioOk = (*i).toObject().value("dioOk").toBool();
            insertedData->dioVoltageLogic = (*i).toObject().value("dioVoltageLogic").toBool();
        } else
            insertedData->isDioValid = false;

        if (!(*i).toObject().value("smuCurrent").isUndefined()) {
            insertedData->smuCurrent = (*i).toObject().value("smuCurrent").toDouble();
            insertedData->smuVoltage = (*i).toObject().value("smuVoltage").toDouble(); // "0 Вольт" будет означать, что это режим без подачи напряжения
            insertedData->isSmuValid = true;
        } else
            insertedData->isSmuValid = false;

        if (!(*i).toObject().value("chipTemperature").isUndefined()) {
            insertedData->setChipTemperature((*i).toObject().value("chipTemperature").toDouble(),
                                             (*i).toObject().value("chipTemperatureSensorOk").toBool(),
                                             (*i).toObject().value("chipTemperatureMax").toDouble());
        } else
            insertedData->isChipTemperatureValid = false;

        insertedData->m_selectedByUser = m_rows.value(slotNumber)->m_selectedByUser;
        m_rows[slotNumber].swap(insertedData);
        availableSlots.insert(slotNumber);
        emit dataChanged(index(slotNumber, 0), index(slotNumber, 0));
    }

    // удаление слотов
    auto i = m_rows.begin();
    while(i != m_rows.end()) {
        //(!availableSlots.contains( i.value()->slotNumber )) ? (i = m_rows.erase(i)) : (++i);
        if (!availableSlots.contains( i.key()) && (i.value()->state != NoSlotState)) {
            i->reset(new SlotData(i.key(), QString(), QString(), QDateTime::currentDateTime(),  QDateTime::currentDateTime(), QString(), NoSlotState));
            emit dataChanged(index(i.key(), 0), index(i.key(), 0));
        } else
            ++i;
    }

//    QByteArray tmp("{\"fansAndSensors\": {\"fans\": [{\"number\": 0,\"enable\":  true,\"percent\": 32.34},{\"number\": 1,\"enable\": true,\"percent\": 36.34},{\"number\": 2,\"enable\": true,\"percent\": 78.34},{\"number\": 3,\"enable\": true,\"percent\": 37.34},{\"number\": 4,\"enable\": true,\"percent\": 99.34}],\"peripheralTemperature\": [{\"number\": 0,\"temperature\": 100.7,\"sensorOk\": 1},{\"number\": 1,\"temperature\": 100.7,\"sensorOk\": 1},{\"number\": 2,\"temperature\": 100.7,\"sensorOk\": 2},{\"number\": 3,\"temperature\": 100.7,\"sensorOk\": 1},{\"number\": 4,\"temperature\": 100.7,\"sensorOk\": 1},{\"number\": 5,\"temperature\": 100.7,\"sensorOk\": 1},{\"number\": 6,\"temperature\": 100.7,\"sensorOk\": 2},{\"number\": 7,\"temperature\": 100.7,\"sensorOk\": 1}]}}");
//    QJsonParseError error;

//    QJsonDocument doc = QJsonDocument::fromJson(tmp, &error);
//    if (error.error != QJsonParseError::NoError) {
//        qDebug() << error.errorString() << " at " << QString::number(error.offset);
//    }

//    QJsonObject obj = doc.object();
    m_fansAndSensors->updateAll(rootObject/*obj*/.value("fansAndSensors").toObject());

    const QJsonArray & chambersArray = rootObject.value("chambers").toArray();
    if (chambersArray.size() > 0)
        m_chamber1->updateAll(chambersArray.at(0).toObject(), 0);
    if (chambersArray.size() > 1)
        m_chamber2->updateAll(chambersArray.at(1).toObject(), 1);
}

void SlotsListModel::updateReadyStart(quint32 nSlot, quint32 readyStart, const QByteArray & listFaultyDevice)
{
    m_rows[nSlot]->setReadyStart(readyStart, listFaultyDevice);
    emit dataChanged(index(nSlot, 0), index(nSlot, 0), {SlotsListModel::ReadyStartRole});

    if (!readyStart)
        emit notReadyForStart(nSlot, listFaultyDevice); // для открытия окна c предупреждением
}

QString SlotsListModel::faultyDevices(int index) const
{
    if (index >= AMOUNT_OF_SLOTS)
        return QString();

    return m_rows[index]->m_listFaultyDevices;
}

