#ifndef PERIPHERALTEMPERATUREDATA_H
#define PERIPHERALTEMPERATUREDATA_H

#include <QDebug>
#include "../../PXI-service/pxi-service/config.h"

struct PeripheralTemperatureData
{
    Q_GADGET
    Q_PROPERTY(int number READ number)
    Q_PROPERTY(float temperature READ temperature)
    Q_PROPERTY(int errorCode READ errorCode)
    Q_PROPERTY(bool isOk READ isOk)
    Q_PROPERTY(QString name READ name)
public:
    PeripheralTemperatureData() = default;

    PeripheralTemperatureData(qint16 number, float temp, int errorCode) :
        m_number(number), m_temperature(temp), m_errorCode(errorCode), m_isOk(errorCode == PLC_PERIPHERAL_SENSOR_STATUS_OK),
        m_name(sensorName(m_number))
    {   }

    int number() const
    { return m_number; }
    float temperature() const
    { return m_temperature; }
    int errorCode() const
    { return m_errorCode; }
    bool isOk() const
    { return m_isOk; }
    const QString & name() const
    { return m_name; }

private:
    const QString & sensorName(qint32 number) const
    { return (number < m_sensorNames.size()) ? m_sensorNames.at(number) : m_emptySensorName; }

    static const QStringList m_sensorNames;
    static const QString m_emptySensorName;

    int m_number;
    float m_temperature;
    int m_errorCode;
    bool m_isOk;
    QString m_name;

};
Q_DECLARE_METATYPE(PeripheralTemperatureData)

#endif // PERIPHERALTEMPERATUREDATA_H
