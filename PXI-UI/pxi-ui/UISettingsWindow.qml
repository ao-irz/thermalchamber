import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import SparkUIApplication 1.0
//import Qt.labs.settings 1.1
import "SimpleControls"
import "ScreenComponents"

Dialog {
    id: root
    width: parent.width/3*2
    height: parent.height/3*2
    title: qsTr("Настройки")
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Close //Dialog.Cancel | Dialog.Ok

    onVisibleChanged: {
        if (visible) {
            // Обновить модель, послать сигнал
            chamber1.chamberMainParametersChanged(chamber1.chamberMainParameters)
            chamber2.chamberMainParametersChanged(chamber2.chamberMainParameters)
            infoText.color = "transparent"
            infoText.text = " "
            passTextField.clear()
            confirmPassTextField.clear()
        }
    }

    ScrollView {
        anchors.fill: parent
        clip: true

        Column {
            anchors.fill: parent
            spacing: 10
            Item { width: root.width-60; height: 1; }

            UINameValueGray { name: ""; value: qsTr("Нет связи с ПЛК камеры"); valueColor: "red"; visible: !chamber1.plcLink || !chamber2.plcLink }
            UIChamberParagraph { title: qsTr("Аварии левой камеры"); }
            ListView {
                visible: chamber1.plcLink
                width: parent.width
                height: chamber1.alarmDataList.length * 40
                spacing: 10
                model: chamber1.alarmDataList
                delegate: Grid {
                    width: parent.width
                    columns: 3
                    columnSpacing: 15
                    UINameValueGray{ name: qsTr("Модуль:"); value: modelData.moduleId; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Код:"); value: modelData.errorCode; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Параметер:"); value: modelData.parameter; pixelSize: 15}
                }
            }

            Button {
                text: qsTr("Сбросить аварии левой камеры")
                onClicked: networkInterface.sendAlarmResetCommand(chamber1.termoSlotChannelNumber)
            }

            UIChamberParagraph { title: qsTr("Аварии правой камеры"); }
            ListView {
                visible: chamber2.plcLink
                width: parent.width
                height: chamber2.alarmDataList.length * 40
                spacing: 10
                model: chamber2.alarmDataList
                delegate: Grid {
                    width: parent.width
                    columns: 3
                    columnSpacing: 15
                    UINameValueGray{ name: qsTr("Модуль:"); value: modelData.moduleId; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Код:"); value: modelData.errorCode; pixelSize: 15}
                    UINameValueGray{ name: qsTr("Параметер:"); value: modelData.parameter; pixelSize: 15}
                }
            }

            Button {
                text: qsTr("Сбросить аварии правой камеры")
                onClicked: networkInterface.sendAlarmResetCommand(chamber2.termoSlotChannelNumber)
            }

            UIChamberParagraph { title: qsTr("Подстветка"); }
            Grid {
                visible: chamber1.plcLink
                width: parent.width
                columns: 3
                columnSpacing: 15
                Button {
                    text: qsTr("Нет")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.NoLed)
                }
                Button {
                    text: qsTr("Белый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.WhiteLed)
                }
                Button {
                    text: qsTr("Синий")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.BlueLed)
                }

                Button {
                    text: qsTr("Зеленый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.GreenLed)
                }

                Button {
                    text: qsTr("Желтый")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.YellowLed)
                }
                Button {
                    text: qsTr("Красный")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.RedLed)
                }
                Button {
                    text: qsTr("Моргающий красный")
                    onClicked: networkInterface.sendLedColorCommand(NetworkInterface.BlinkRedLed)
                }

            }

            UIChamberParagraph { title: qsTr("Параметры термокамер"); }
            Item { width: root.width-60; height: 10;
            }
            Row {
                property alias valueText: valueField.text
                id: rackOverheat
                width: parent.width
                height: 40
                spacing: 2
                Label { font.pixelSize: 16; anchors.top: parent.top; anchors.topMargin: 5; text: (chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 2] !== undefined
                                                                                                  ?chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 2].name:""); width: 300; height: 40; }

                TextField {
                    id: valueField
                    height: 40
                    anchors.top: parent.top
                    color: "white"
                    selectByMouse: true
                    text: (chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 2] !== undefined
                           ? chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 2].value.toFixed(2) : 45.00)
                }
            }

            Row {
                property alias valueText: valueFieldUPS.text
                id: upsTimeout
                width: parent.width
                height: 40
                spacing: 2
                Label { font.pixelSize: 16; anchors.top: parent.top; anchors.topMargin: 5; text: (chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 1] !== undefined
                                                                                                  ?chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 1].name:""); width: 300; height: 40; }

                TextField {
                    id: valueFieldUPS
                    height: 40
                    anchors.top: parent.top
                    color: "white"
                    selectByMouse: true
                    text: (chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 1] !== undefined
                           ? chamber1.chamberMainParameters[chamber1.chamberMainParameters.length - 1].value : 15)
                }
            }

            Row {
                width: parent.width
                height: (chamber1.chamberMainParameters.length - 4) * 40
                spacing: 2

                Column {
                    width: parent.width/2
                    height: parent.height
                    UIChamberParagraph { title: qsTr("Левая камера"); height: 40 }
                    Repeater {
                        id: parametersRepeater1
                        model: chamber1.chamberMainParameters
                        width: parent.width

                        delegate: Row {
                            property alias valueText: valueField1.text
                            width: parametersRepeater1.width
                            visible: ((!modelData.name.includes("переохлаждению") & !modelData.name.includes("холодильника")) & !modelData.name.includes("стоек") & !modelData.name.includes("ИБП"))
                            spacing: 0
                            Label { font.pixelSize: 16; anchors.top: parent.top; anchors.topMargin: 5; text: modelData.name; width: 300; height: 40; }
                            TextField {
                                id: valueField1
                                height: 40
                                anchors.top: parent.top
                                color: "white"
                                selectByMouse: true
                                text: (typeof modelData.value === 'number') ?
                                           (modelData.value.toFixed(2)) :
                                           (modelData.value)
                            }
                        }
                    }
//                    Button {
//                        text: qsTr("Сохранить")
//                        onClicked: {
//                            for (let i=0; i<chamber1.chamberMainParameters.length; ++i)
//                                chamber1.setChamberParameterValue(i, parametersRepeater1.itemAt(i).valueText)

//                            networkInterface.sendRackTemperatureCommand(chamber1.termoSlotChannelNumber)
//                        }
//                    }
                }

                Rectangle { width: 2; height: parent.height; border.color: "white" }

                Column {
                    width: parent.width/2
                    height: parent.height
                    UIChamberParagraph { title: qsTr("Правая камера"); height: 40 }
                    Repeater {
                        id: parametersRepeater2
                        model: chamber2.chamberMainParameters
                        width: parent.width

                        delegate: Row {
                            property alias valueText: valueField2.text
                            width: parametersRepeater2.width
                            visible: (!modelData.name.includes("переохлаждению") & !modelData.name.includes("холодильника") & !modelData.name.includes("стоек"))
                            spacing: 0
                            Label { font.pixelSize: 16; anchors.top: parent.top; anchors.topMargin: 5; text: modelData.name; width: 300; height: 40; }
                            TextField {
                                id: valueField2
                                height: 40
                                anchors.top: parent.top
                                color: "white"
                                selectByMouse: true
                                text: (typeof modelData.value === 'number') ?
                                           (modelData.value.toFixed(2)) :
                                           (modelData.value)
                            }
                        }
                    }
//                    Button {
//                        text: qsTr("Сохранить")
//                        onClicked: {
//                            for (let i=0; i<chamber2.chamberMainParameters.length; ++i)
//                                chamber2.setChamberParameterValue(i, parametersRepeater2.itemAt(i).valueText)

//                            networkInterface.sendRackTemperatureCommand(chamber2.termoSlotChannelNumber)
//                        }
//                    }
                }

            }
            Button {
                text: qsTr("Сохранить")
                onClicked: {
                    for (let i=0; i<chamber1.chamberMainParameters.length - 2; ++i)
                        chamber1.setChamberParameterValue(i, parametersRepeater1.itemAt(i).valueText)
                    chamber1.setChamberParameterValue(chamber1.chamberMainParameters.length - 2,rackOverheat.valueText)
                    chamber1.setChamberParameterValue(chamber1.chamberMainParameters.length - 1,upsTimeout.valueText)

                    networkInterface.sendRackTemperatureCommand(chamber1.termoSlotChannelNumber)

                    for (let i=0; i<chamber2.chamberMainParameters.length - 2; ++i)
                        chamber2.setChamberParameterValue(i, parametersRepeater2.itemAt(i).valueText)
                    chamber2.setChamberParameterValue(chamber2.chamberMainParameters.length - 2,rackOverheat.valueText)
                    chamber2.setChamberParameterValue(chamber2.chamberMainParameters.length - 1,upsTimeout.valueText)

                    networkInterface.sendRackTemperatureCommand(chamber2.termoSlotChannelNumber)

                }
            }

            UIChamberParagraph { title: qsTr("Смена пароля пользователя"); visible: authWindow.isAdminActive}

            Grid {
                visible: authWindow.isAdminActive
                verticalItemAlignment: Grid.AlignVCenter
                width: parent.width
                columns: 2
                columnSpacing: 15

                Text {
                    font.pixelSize: 15
                    color: "white"
                    text: qsTr("Пароль")
                }

                TextField {
                    id: passTextField
                    echoMode: TextInput.Password
                    selectByMouse: true
                }

                Text {
                    font.pixelSize: 15
                    color: "white"
                    text: qsTr("Подтверждение")
                }

                TextField {
                    id: confirmPassTextField
                    echoMode: TextInput.Password
                    selectByMouse: true
                }
            }

            Text {
                id: infoText
                color: "transparent"
                text: " "
                font.pixelSize: 15
            }

            Button {
                visible: authWindow.isAdminActive
                text: qsTr("Сохранить пароль")
                onClicked: {
                    if (passTextField.text === confirmPassTextField.text) {
                        infoText.color = "green"; infoText.text = qsTr("Новый пароль пользователя сохранен")
                        settings.userPassword = Qt.md5(passTextField.text)
                        settings.sync()
                    } else {
                        infoText.color = "red"; infoText.text = qsTr("Пароли не совпадают")
                    }
                }
            }
        }
    }
}
