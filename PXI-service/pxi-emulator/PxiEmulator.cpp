#include "../../PXI-common/common.h"
#include "PxiEmulator.h"

PxiEmulator::PxiEmulator(QObject *parent, const QString &selfPath) :
    QObject(parent),
    m_selfPath(selfPath),
    m_settings(new QSettings(selfPath + "/" + "pxi-emulator-data.ini", QSettings::IniFormat, this))
{
}

Bugs PxiEmulator::init(const int slot_power_count_, const int connect_timeout_ms, const int operation_timeout_ms, const QStringList power_cfg, const QStringList fgen_cfg)
{
    Q_UNUSED(slot_power_count_)
    Q_UNUSED(connect_timeout_ms)
    Q_UNUSED(operation_timeout_ms)
    Q_UNUSED(power_cfg)
    Q_UNUSED(fgen_cfg)
    for (int i = 0; i < AMOUNT_OF_SLOTS; i++) {
        auto s = new SampleEmulator(i, m_settings);
        samples.append(s);
    }

    for (int i = 0; i < AMOUNT_OF_SLOTS; i++) {
        m_settings->beginGroup(QString("Slot%1").arg(i));

        m_settings->beginGroup("Thermocouple");
        m_settings->setValue("temperature", 113.4);
        m_settings->endGroup();

//        m_settings->beginGroup("PowerSupply0");
//        m_settings->setValue("voltage", 5.1);
//        m_settings->setValue("current", 0.5);
//        m_settings->endGroup();

//        m_settings->beginGroup("PowerSupply1");
//        m_settings->setValue("voltage", 3.1);
//        m_settings->setValue("current", 0.39);
//        m_settings->endGroup();

//        m_settings->beginGroup("PowerSupply2");
//        m_settings->setValue("voltage", 2.1);
//        m_settings->setValue("current", 0.2);
//        m_settings->endGroup();

//        m_settings->beginGroup("PowerSupply3");
//        m_settings->setValue("voltage", 3.1);
//        m_settings->setValue("current", 0.3);
//        m_settings->endGroup();

//        m_settings->beginGroup("PowerSupply4");
//        m_settings->setValue("voltage", 4.1);
//        m_settings->setValue("current", 0.4);
//        m_settings->endGroup();

        m_settings->endGroup();
    }

    Bugs bugs;
    return bugs;
}

void PxiEmulator::close()
{
}

SampleEmulator *PxiEmulator::sample(int slot)
{
    if ((slot < 0) || (slot >= AMOUNT_OF_SLOTS)) {
        throw HWException(QString("bad sample slot = %1").arg(slot));
    }
    return samples[slot];
}

SampleEmulator::SampleEmulator(int nSlot, const QPointer<QSettings> sett) :
    m_settings(sett),
    m_nSlot(nSlot)
{
    auto s = QString::number(m_nSlot);
    QString sample = "d" + s;

    power_chanels.append(new PowerEmulator(m_nSlot, 0, m_settings)); // 0 V to +6 V
    power_chanels.append(new PowerEmulator(m_nSlot, 1, m_settings)); // 0 V to +20 V
    power_chanels.append(new PowerEmulator(m_nSlot, 2, m_settings)); // 0 V to -20 V
    power_chanels.append(new PowerEmulator(m_nSlot, 3, m_settings)); // 0 V to 6 V    
    smu = new SMUEmulator(m_nSlot, 4, m_settings); // здесь как будто бы smu ложится за место 5-го источника
    fgen = new FGenEmulator(m_nSlot, m_settings);
    hsdio = new HsdioEmulator(m_nSlot, m_settings);
    tcouple = new ThermocoupleEmulator(m_nSlot, m_settings);
    relay = new RelayEmulator(m_nSlot, m_settings);
}

Bugs SampleEmulator::neutral()
{
    Bugs rv;
    for(auto p : power_chanels) {
        rv += p->off();
    }
    rv += fgen->off();
    for (int idx = 0; idx < hsdio->pin_count; ++idx) {
        rv += hsdio->initPin(idx, Hsdio::none);
    }
    for (int idx = 0; idx < relay->count; ++idx) {
        rv += relay->toDIO(idx);
    }
    return rv;
}

PowerEmulator *SampleEmulator::power(int ch)
{
    if ((ch < 0) || (ch > 3)) {
        throw HWException("Sample::power: bad channel index");
    }
    return power_chanels[ch];
}

Bugs PowerEmulator::put(double u, double i) //задаёт выходное напряжение и оганичение по току
{
    QString settingsVKey;
    QString settingsCKey;
    if (m_n == 4) {
        settingsVKey = QStringLiteral("Slot%1/SMU%2/voltage");
        settingsCKey = QStringLiteral("Slot%1/SMU%2/current");
    } else {
        settingsVKey = QStringLiteral("Slot%1/PowerSupply%2/voltage");
        settingsCKey = QStringLiteral("Slot%1/PowerSupply%2/current");
    }

    m_u = u;
    m_i = i;
    m_settings->setValue(settingsVKey.arg(m_nSlot).arg(m_n), u);
    m_settings->setValue(settingsCKey.arg(m_nSlot).arg(m_n), i);
    m_settings->sync();
    return Bugs();
}

Bugs PowerEmulator::voltage(double *result)
{
    QString settingsVKey;
    if (m_n == 4) {
        settingsVKey = QStringLiteral("Slot%1/SMU%2/voltage");
    } else {
        settingsVKey = QStringLiteral("Slot%1/PowerSupply%2/voltage");
    }

    m_settings->sync();
    bool ok = false;
    *result = m_settings->value(settingsVKey.arg(m_nSlot).arg(m_n)).toDouble(&ok);
    if (ok)
        return Bugs();

    Bugs bugs;
    bugs.append(Bug("PowerSupply", 13, 0, "Emulator's voltage can't be readed"));
    return bugs;
}

Bugs PowerEmulator::current(double *result)
{
    QString settingsCKey;
    if (m_n == 4) {
        settingsCKey = QStringLiteral("Slot%1/SMU%2/current");
    } else {
        settingsCKey = QStringLiteral("Slot%1/PowerSupply%2/current");
    }

    m_settings->sync();
    bool ok = false;
    *result = m_settings->value(settingsCKey.arg(m_nSlot).arg(m_n)).toDouble(&ok);
    if (ok)
        return Bugs();

    //! вот в порядке эксперимента исключение
    //!     Bugs rv;
    //!     go(rv, status, notes);
    //!     if (rv.count()) {
    //!         //qDebug() << "try_" << 1 << rv[0].message;
    //!         throw HWException(rv[0].message);
    //!     }

    Bugs bugs;
    bugs.append(Bug("PowerSupply", 999, 999, "Emulator's current can't be readed"));
    return bugs;
}

Bugs ThermocoupleEmulator::get(double *result)
{
    m_settings->sync();
    bool ok = false;
    *result = m_settings->value(QString("Slot%1/Thermocouple/temperature").arg(m_nSlot)).toDouble(&ok);
    if (ok)
        return Bugs();

    Bugs bugs;
    bugs.append(Bug("TermocoupleEmulator", 999, 999, "Emulator's termocouple temperature can't be readed"));
    return bugs;
}

Bugs HsdioEmulator::initPin(int pin_idx, Hsdio::PinState ps)
{
    Q_UNUSED(pin_idx)
    Q_UNUSED(ps)
    return Bugs();
}

Bugs HsdioEmulator::init4Slots(Hsdio::LogicVoltage lv)
{
    Q_UNUSED(lv)
    return Bugs();
}

Bugs HsdioEmulator::put(int pin_idx, bool up)
{
    m_settings->setValue(QString("Slot%1/Hsdio%2/pins").arg(m_nSlot).arg(pin_idx), up);
    m_settings->sync();
    return Bugs();
}

Bugs HsdioEmulator::get(int pin_idx, bool *up)
{
    m_settings->sync();
    if (m_settings->value(QString("Slot%1/Hsdio%2/pins").arg(m_nSlot).arg(pin_idx)).canConvert(QMetaType::Bool)) {
        *up = m_settings->value(QString("Slot%1/Hsdio%2/pins").arg(m_nSlot).arg(pin_idx)).toBool();
        return Bugs();
    } else {
        *up = true;
        return Bugs();
    }

    //! Мы физически не знаем здесь, какой должен возвращен уровень пина для того чтобы он был правильным
    //! так что подноценного процесса эмуляции здесь не будет
    //! Поэтому пока будем считать, что возвращаемый уровень читаемого пина должен быть "1"

    Bugs bugs;
    bugs.append(Bug("Hsdio", 999, 999, "Emulator's hsdio can't be readed"));
    return bugs;
}

Bugs RelayEmulator::toDIO(int local_idx)
{
    Q_UNUSED(local_idx)
    Bugs rv;
    return rv;
}

Bugs RelayEmulator::toSMU(int local_idx)
{
    Q_UNUSED(local_idx)
    Bugs rv;
    return rv;
}

Bugs SMUEmulator::init()
{
    Bugs rv;
    return rv;
}
