#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "SparkService.h"
#include "schedule-components/SparkSchedulePlc.h"
#include "../../PXI-common/common.h"


Bugs SparkService::parseCyclogramThermo(const QJsonObject & cyclogramObject,const quint32& nSlot)
{
    const QDateTime currentTime(QDateTime::currentDateTime());
    QDateTime stopTime(currentTime);

    Bugs errors;
    if (cyclogramObject.value("thermoProfile").toArray().count() == 0)
    {
        errors += Bug("термоциклограмма", nSlot + 1, Bug::Thermocyclogram, "Некорректная термоциклограмма!");
    }


    for (const auto & generatorObject : cyclogramObject.value("thermoProfile").toArray())
    {
        QJsonObject scheduleObject = generatorObject.toObject();

        bool isInfinite = false;
        qint64 duration = 0;
        if (scheduleObject.value("duration").isUndefined()) {
            isInfinite = true;
            duration = INT64_MAX/2;
        } else
            duration = scheduleObject.value("duration").toString().toULongLong();
        if ( scheduleObject.value("enable").isUndefined() ) {
            logMessageString("Warning: (Thermoprofile) enable attribute not defined");
            continue;
        }
        if ( scheduleObject.value("duration").isUndefined() && scheduleObject.value("type").toString() != "maintaining" ) {
            logMessageString("Warning: (Thermoprofile) infinite block couldn't be heating or cooling");
            continue;
        }
        if ( (duration==0) && scheduleObject.value("enable").toBool()) {
            logMessageString("Warning: (Thermoprofile) duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ? // для нулевого элемента это значит "начать прямо сейчас", для остальных "начать с конца предыдущего"
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);

        m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] :m_plcConnectionList[1];
				m_logger->log(QStringLiteral("beginThermoprofile for camera ")+QString::number(m_plcConnection->plcSlot()));

        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_sparkSchedules[nSlot].insert(startTime,
                                         QSharedPointer<SparkSchedulePlc>(
                                             new SparkSchedulePlc(
                                                 m_plcConnection, m_logger,
                                                 startTime, stopTime,
                                                 scheduleObject.value("temperature").toDouble(),
                                                 scheduleObject.value("maxTemperatureChangeRate").toDouble(),
                                                 scheduleObject.value("type").toString(),
                                                 isInfinite )));
        else
            m_sparkSchedules[nSlot].insert(startTime,
                                         QSharedPointer<SparkSchedulePlc>(
                                             new SparkSchedulePlc(
                                                 m_plcConnection, m_logger,
                                                 startTime, stopTime,
                                                 SparkScheduleBase::ScheduleActionOff,
                                                 isInfinite )));

        // После блока с бесконечной длительностью уже не должно быть других блков
        if (isInfinite)
            break;
    }
    return errors;
}
