#include <QDir>
#include <QDateTime>
#include <QDir>
#include "Logger.h"
#include "qtservice.h"

#define DBG_LOG_FILENAME    QStringLiteral("dbg-service.log")
#define CURRENT_TIME        QDateTime::currentDateTime().date().toString(Qt::SystemLocaleShortDate) + " " + QDateTime::currentDateTime().time().toString(Qt::SystemLocaleLongDate) +  " - "

// TODO: For LogMaxDays
// ======================
// TODO: Deleting old log files for LogDaily=True
// TODO: Renaming old log file for LogDaily=False (to *.bak)

Logger::Logger(const Settings * settings, QObject * parent, void * serviceObject) :
    QObject(parent),
    m_settings(settings),
    m_serviceObject(serviceObject),
		fileNameByCreation(QStringLiteral("SparkService_") + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".log")
{
    QString fileName = fileNameByCreation;

    fileName.prepend(logPath() + "/");

    logFile.setFileName(fileName);
    if (logFile.open(QIODevice::WriteOnly | QIODevice::Append))
        logFile.close();
    else
        static_cast<QtServiceBase *>(m_serviceObject)->logMessage("Log file can not be created", QtServiceBase::Error);

}

void Logger::log(const QString &msg)
{
    QString fileName(QStringLiteral("SparkService_") + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".log");

    fileName.prepend(logPath() + "/");

    logFile.setFileName(fileName);
    if (logFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        logFile.write((CURRENT_TIME + msg + "\n").toLocal8Bit());
        logFile.close();
    } else
        static_cast<QtServiceBase *>(m_serviceObject)->logMessage("Log file not writeble", QtServiceBase::Error);
}

void Logger::dbgLog(const QString &msg)
{
    QString fileName(DBG_LOG_FILENAME);
    fileName.prepend(logPath() + "/");

    dbgLogFile.setFileName(fileName);
    if (dbgLogFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        dbgLogFile.write((CURRENT_TIME + msg + "\n").toLocal8Bit());
        dbgLogFile.close();
    } else
        log("Debug log file can not be created");
}

void Logger::logChamberTemperature(float temp, int t)
{
    if (t == 1)
        log("Chamber should maintaining temperature " + QString::number(temp));
    else
        log("Chamber should leads temperature to " + QString::number(temp));
}

void Logger::logTemperature(float peakTemp, float avgTemp, float tempSet)
{
		log("Peak chamber temperature: " + QString::number(peakTemp) + "; Average chamber temperature: " + QString::number(avgTemp) + "; chamber set: " + QString::number(tempSet));
}

void Logger::logPowerSupply(quint32 nSlot, quint32 instance, double v, double c)
{
    log("Slot=" + QString::number(nSlot) + " Power supply " + QString::number(instance) + " changes mode: Voltage: " + QString::number(v) + " Current limit: " + QString::number(c));
}

void Logger::logPowerSupplySwitch(quint32 nSlot, quint32 instance, bool b)
{
    QString a = (b) ? ("on") : ("off");
    log("Slot=" + QString::number(nSlot) + " Power supply " + QString::number(instance) + " switches " + a);
}

void Logger::logGenerator(quint32 nSlot, QString signalForm, double a, double o, double f, double d)
{
		log("Slot=" + QString::number(nSlot) +
				" Generator mode set to: "+
				" SignalForm: " + signalForm +
				" Amplitude: " + QString::number(a) +
				" Offset: " + QString::number(o) +
				" Frequency: " + QString::number(f) +
				" Duty cycle: " + QString::number(d) );
}

void Logger::logGeneratorSwitch(quint32 nSlot, bool b)
{
    QString a = (b) ? ("on") : ("off");
    log("Slot=" + QString::number(nSlot) + " Generator switches " + a);
}

void Logger::logSmu(quint32 nSlot, double v)
{
    log("Slot=" + QString::number(nSlot) + " SMU changes mode: Voltage: " + QString::number(v));
}

void Logger::logSmuSwitch(quint32 nSlot, bool b)
{
    QString a = (b) ? ("on") : ("off");
    log("Slot=" + QString::number(nSlot) + " SMU switches " + a);
}

QString Logger::logPath()
{
    if (m_settings->logPath().length() == 0)
        return m_settings->selfPath();
    QDir dir;
    if (!dir.exists(m_settings->logPath())) {
        if (!dir.mkpath(m_settings->logPath())) {
            //TODO: MANDATORY logging about this situation to anywhere
            return m_settings->selfPath();
        }
    }

    return m_settings->logPath();
}
