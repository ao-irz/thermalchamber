#ifndef SPARKSWITCHCONNECTION_H
#define SPARKSWITCHCONNECTION_H

#include <QObject>
#include <QSerialPort>
#include <QPointer>

class Logger;

class SparkSwitchConnection : public QObject
{
    Q_OBJECT
public:
    explicit SparkSwitchConnection(const QPointer<Logger> &logger, QObject *parent = nullptr);
    ~SparkSwitchConnection();

public:
    void switchOn(quint32 n);
    void switchOff(quint32 n);
    void switchOff();


private slots:
    void portHandler();

private:
    QPointer<Logger> m_logger;

    QByteArray m_buffer;
    QPointer<QSerialPort> m_serialPort;
    bool m_endFlag;

    void responseHandler();

};

#endif // SPARKSWITCHCONNECTION_H
