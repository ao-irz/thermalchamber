#include <QSerialPortInfo>
#include "SparkSwitchConnection.h"
#include "Logger.h"


SparkSwitchConnection::SparkSwitchConnection(const QPointer<Logger> &logger, QObject *parent) :
    QObject(parent),
    m_logger(logger),
    m_serialPort(new QSerialPort("COM1", this))
{

//    // Возвращает возможные варианты: COM1 COM3 COM4 COM5sd
//    foreach (const QSerialPortInfo & info, QSerialPortInfo::availablePorts())
//        m_logger->log(info.portName());

    m_serialPort->setBaudRate(QSerialPort::Baud9600);
    if (!m_serialPort->open(QIODevice::ReadWrite)) {
        m_logger->log(QString("rs232: can't open port, error code = %1, %2").arg(m_serialPort->error()).arg(m_serialPort->errorString()));
    }

    connect(m_serialPort.data(), &QSerialPort::readyRead, this, &SparkSwitchConnection::portHandler);
}

SparkSwitchConnection::~SparkSwitchConnection()
{
    m_serialPort->close();
}

void SparkSwitchConnection::switchOn(quint32 n)
{
    if (!m_serialPort->isOpen()) {
        //m_logger->log(QString("rs232: port closed"));
        return;
    }

    m_serialPort->write(":SWITCH ON " + QByteArray::number(n) + QByteArray(1, 0x0A));
    m_serialPort->waitForBytesWritten();
}

void SparkSwitchConnection::switchOff(quint32 n)
{
    if (!m_serialPort->isOpen()) {
        //m_logger->log(QString("rs232: port closed"));
        return;
    }

    m_serialPort->write(":SWITCH OFF " + QByteArray::number(n) + QByteArray(1, 0x0A));
    m_serialPort->waitForBytesWritten();
}

void SparkSwitchConnection::switchOff()
{
    if (!m_serialPort->isOpen()) {
        //m_logger->log(QString("rs232: port closed"));
        return;
    }

    m_serialPort->write(":SWITCH_ALL_OFF" + QByteArray(1, 0x0A));
    m_serialPort->waitForBytesWritten();
}

void SparkSwitchConnection::portHandler()
{
    while(m_serialPort->bytesAvailable()) {
        quint8 oneByte{0};
        m_serialPort->read((char *)&oneByte, 1);

        if (m_buffer.size() >= 100) {
            m_buffer.clear();
            m_endFlag = false;
        }

        m_buffer.append(oneByte);

        if (oneByte == 0xFF)
            m_endFlag = true;
        else {
            if ((m_endFlag) && (oneByte == 0x0A)) {
                responseHandler();
                m_buffer.clear();
                m_endFlag = false;
            } else
                m_endFlag = false;
        }
    }
}

void SparkSwitchConnection::responseHandler()
{
    if (m_buffer.isEmpty())
        return;

    m_buffer.chop(1);
    if (m_buffer == ":OK")
        // пока не знаю, что делать тут дальше
        return;

    if (m_buffer == ":ERROR") {
        m_logger->log("rs232: got ERROR response");
        return;
    }
}
