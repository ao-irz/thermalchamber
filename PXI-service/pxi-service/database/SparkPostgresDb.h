#ifndef DB_PostgresH
#define DB_PostgresH

#include <QObject>
#include "Logger.h"
#include <QSqlDatabase>
#include <QDateTime>
#include "config.h"
#include "Settings.h"
#include <QPointer>
#include "../adapter/hardware/bugs.h"

class SparkPostgresDb : public QObject
{
    Q_OBJECT

public:
		static SparkPostgresDb* getInstance(const QPointer<Logger> &logger = nullptr, const QPointer<Settings> settings = nullptr, QObject *parent = nullptr)
    {
        static bool first=true;
        if ((parent == 0) && (first == true))
            qCritical("Incorrect Initialisation - no parent and first");

        if ((parent != 0) && (first == false))
            qCritical("Incorrect Initialisation - parent and not first");

        first = false;
				static SparkPostgresDb *instance = new SparkPostgresDb(logger, settings, parent);
        return instance;
    }

		virtual ~SparkPostgresDb();

    void powerSet(const quint8 nPs, const double v, const double vP, const double vM, const double c, const double cP, const double cM);
    void powerGet(const quint8 nPs, const double v, const double c);
    void powerOff(const quint8 nPs);
    void powerLog(const quint8 nPs, const QString &comment);

    void smuSet(const double v, const double c);
    void smuGet(const double v, const double c);
    void smuOff();
    void smuLog(const QString & comment);

    void logBugs(const Bugs &bugs);
    void powerLog(const QString & comment);
private:
		SparkPostgresDb(SparkPostgresDb const &);
		SparkPostgresDb(const QPointer<Logger> &logger, const QPointer<Settings> settings, QObject *parent);
		void operator=(SparkPostgresDb const&);

    enum Devices {
        PowerSupply0Device = 1,
        PowerSupply1Device = 2,
        PowerSupply2Device = 3,
        PowerSupply3Device = 4,
        GeneratorDevice = 2,
        ThermoCoupleDevice = 3,
        PlcDevice = 4,
        DIODevice = 5,
        SMUDevice = 6,
        Log = 7
    };

    enum Operations {
        SetOperation,
        OffOperation,
        GetOperation,
        LogOperation
    };

    QPointer<Logger> m_logger;
    QPointer<Settings> m_settings;

		bool m_bDbOk;
		QSqlDatabase m_db;

    const QString insertQuery;
    const QString createQuery;

    bool connectToSqlite();
    void doInsert(const QString &insertString);
};

#endif // DB_PostgresH
