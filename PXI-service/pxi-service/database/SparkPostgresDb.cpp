#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>
#include "SparkPostgresDb.h"
#include "config.h"
//#include "C:\Qt\Qt5.13.1\5.13.1\Src\qtbase\src\plugins\sqldrivers\psql"


//! Создать таблицу operation (set/get/log/что-то еще)
//! Создать таблицу sourceDevice (PS/G/TC/DIO/Chamber)
//! DIO пока не добавлял, потому что хрен знает как его добавлять

//! Таблица sourceDevices
//! ps0 = 1
//! ps1 = 2
//! ps2 = 3
//! ps3 = 4
//! gen = 2
//! couple = 3
//! plc = 4
//! dio = 5
//! smu = 6
//! log = 7

//! Таблица operation
//! set = 1 автоматически подразумевает включение устройства
//! off = 2 выключение устройства
//! get = 2 запрос параметров устройства
//! log = 3 просто для логирования

SparkPostgresDb::SparkPostgresDb(const QPointer<Logger> &logger, const QPointer<Settings> settings, QObject *parent) :
    QObject(parent),
    m_logger(logger),
    m_settings(settings),
    insertQuery(QStringLiteral(
            "INSERT INTO rack_log "
						"(sourceDevice, operation %1) "
						"VALUES (%2, %3 %4)")),
    createQuery(QStringLiteral(
          "CREATE TABLE IF NOT EXISTS rack_log\n"
          "(\n\t"
						"id bigserial PRIMARY KEY NOT NULL,\n\t"
						"dataTime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,\n\t"
            "sourceDevice INTEGER NOT NULL,\n\t"
            "operation INTEGER NOT NULL,\n\t"
            "psVoltage REAL,\n\t"
            "psVPlus   REAL,\n\t"
            "psVMinus  REAL,\n\t"
            "psCurrent REAL,\n\t"
            "psCPlus   REAL,\n\t"
            "psCMinus  REAL,\n\t"
            "psV       REAL,\n\t"
            "psC       REAL,\n\t"
            "psAmplitude REAL,\n\t"
            "generatorOffset  REAL,\n\t"
            "generatorFrequency REAL,\n\t"
            "generatorCycle REAL,\n\t"
            "thermocoupleMin REAL,\n\t"
            "thermocoupleMax REAL,\n\t"
            "thermocoupleTemperature REAL,\n\t"
            "plcAction TEXT,\n\t"
            "plcPreferedtemperature REAL,\n\t"
            "plcTemperatureTolerance REAL,\n\t"
            "plcTemperatureChangeRate REAL,\n\t"
            "plcTemperature REAL,\n\t"
            "smuV REAL,\n\t"
            "smuC REAL,\n\t"
            "comment TEXT\n"
          ");"))
{
    connectToSqlite();
}

SparkPostgresDb::~SparkPostgresDb()
{
		if (m_db.isOpen())
				m_db.close();
}

bool SparkPostgresDb::connectToSqlite()
{
    /* Будем исходить из той идеи, что соединение с sqlite не может быть разорвано ни при каких обстоятельствах */

		//QSqlDatabase::removeDatabase(SQLITE);
		m_db = QSqlDatabase::addDatabase("QPSQL");
        m_db.setHostName(m_settings->groupValueContents(INI_DB_GROUP,INI_DB_ADDRESS));
        m_db.setPort(m_settings->groupValueContents(INI_DB_GROUP,INI_DB_PORT).toInt());
        m_db.setDatabaseName(m_settings->groupValueContents(INI_DB_GROUP,INI_DB_DBNAME));
        m_db.setUserName(m_settings->groupValueContents(INI_DB_GROUP,INI_DB_USER));
        m_db.setPassword(m_settings->groupValueContents(INI_DB_GROUP,INI_DB_PASSWORD));
        /*m_db.setHostName("10.0.2.2");
		m_db.setPort(5432);
		m_db.setDatabaseName("SparkDB");
		m_db.setUserName("postgres");
        m_db.setPassword("gbnfy6");*/
		if (!m_db.open())
		{
				m_logger->log("Cannot open DB due to this error:");
				m_logger->log(m_db.lastError().text());
        return false;
    }
		else
			m_logger->log("SparkDB connected, opened");

    // create запрос с проверкой существования, поэтому проверку можно не делать
		QSqlQuery sqlCreate(m_db);
		if (!sqlCreate.exec(createQuery))
		{
				m_logger->log("Cannot create rack_log table:");
				m_logger->log(m_db.lastError().text());
        return false;
    }

    return true;
}

void SparkPostgresDb::powerSet(const quint8 nPs, const double v, const double vP, const double vM, const double c, const double cP, const double cM)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", psVoltage, psVPlus, psVMinus, psCurrent, psCPlus, psCMinus"));
    const QString otherValue(QStringLiteral(", %1, %2, %3, %4, %5, %6").arg(v).arg(vP).arg(vM).arg(c).arg(cP).arg(cM));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(SetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::powerGet(const quint8 nPs, const double v, const double c)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", psV, psC"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(GetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::powerOff(const quint8 nPs)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds;
    const QString otherValue;
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(OffOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::powerLog(const quint8 nPs, const QString & comment)
{
    Devices psN;
    switch (nPs) {
    case 0: psN = PowerSupply0Device; break;
    case 1: psN = PowerSupply1Device; break;
    case 2: psN = PowerSupply2Device; break;
    case 3: psN = PowerSupply3Device; break;
    default: { m_logger->log("Used wrong power supply number"); return; }
    }
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", '%1'").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::smuSet(const double v, const double c)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", psVoltage, psCurrent"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(SetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::smuGet(const double v, const double c)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", smuV, smuC"));
    const QString otherValue(QStringLiteral(", %1, %2").arg(v).arg(c));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(GetOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::smuOff()
{
    Devices psN = SMUDevice;
    const QString otherFileds;
    const QString otherValue;
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(OffOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::smuLog(const QString & comment)
{
    Devices psN = SMUDevice;
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", '%1'").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(psN)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::powerLog(const QString & comment)
{
    const QString otherFileds(QStringLiteral(", comment"));
    const QString otherValue(QStringLiteral(", %1").arg(comment));
    const QString valuesString(insertQuery
                         .arg(otherFileds)
												 .arg(Log)
                         .arg(LogOperation)
                         .arg(otherValue));

    doInsert(valuesString);
}

void SparkPostgresDb::logBugs(const Bugs &bugs)
{
    QString concatString;
    for (auto bug : bugs) {
        concatString.append( bug.message + " type = " + QString::number(bug.type) + " slot = " + QString::number(bug.slot) + "; ");
    }
    powerLog(concatString);
}

void SparkPostgresDb::doInsert(const QString &insertString)
{
		QSqlQuery sqlInsert(m_db);
    if (!sqlInsert.exec(insertString)) {
        m_logger->log("Postgres database can't insert rows");
        return;
    }
}

