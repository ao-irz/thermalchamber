#ifndef SPARKSERVICE_H
#define SPARKSERVICE_H
#include <QDateTime>
#include <QCoreApplication>
#include <QTimer>
#include <QPointer>
#include <QProcess>
#include "QTcpServer"
#include "QTcpSocket"
#include "qtservice.h"
#include <SparkSlot.h>
#include "Logger.h"
#include "Settings.h"
#include "ValueTypes.h"
#include "config.h"
#include "schedule-components/SparkScheduleBase.h"
#include "plc-connection/SparkPlcConnection.h"
#include "switch-connection/SparkSwitchConnection.h"
//#ifdef SQLITE
//#include "database/SparkDb.h"
//#else
#include "database/SparkPostgresDb.h"
//#endif
#include "../../PXI-common/common.h"
#include "../../PXI-common/led-color.h"

#ifdef PXI_EMULATOR_ENABLE
#include "../pxi-emulator/PxiEmulator.h"
#else
#include "../adapter/hardware/hardware.h"
#endif

class SparkService : public QtService<QCoreApplication>
{
public:
    SparkService(int argc, char **argv, const QString & m_selfPath);
    virtual ~SparkService();

protected:
    void start() override;
    void stop() override;
    void pause() override;
    void resume() override;
    void processCommand(int code) override;

private:
    enum SocketCommandType
    {
        NoCommand = NO_COMMAND,
        StateRequest = STATE_REQUEST,
        RunCyclogramCommand = RUN_CYCLOGRAM_COMMAND,
        BreakCyclogramCommand =	BREAK_CYCLOGRAM_COMMAND,
        SetRackTemperatureCommand =	SET_RACK_TEMPERATURE_COMMAND,
        AlarmResetCommand = ALARM_RESET_COMMAND,
        SetLedColorCommand = SET_LED_COLOR_COMMAND,
        PauseCommand = PAUSE_COMMAND,
        ResumeCommand = RESUME_COMMAND,
        ReportRequest = REPORT_REQUEST,
        PrepareRunRequest = PREPARERUN_REQUEST,
        OpenLockCommand = OPEN_LOCK_COMMAND
    };

    QString m_selfPath;
    QPointer<Settings> m_settings{nullptr};
    QPointer<Logger> m_logger{nullptr};
    QPointer<SparkPostgresDb> m_db{nullptr};
//    QPointer<SparkDb> m_db{nullptr};
    QPointer<QTcpServer> m_localServer{nullptr};
    QPointer<SparkPlcConnection> m_plcConnection{nullptr};
    QList<QPointer<SparkPlcConnection>> m_plcConnectionList;
    QPointer<SparkSwitchConnection> m_switchConnection{nullptr};
    QPointer<QTimer> m_scheduleTimer{nullptr};              // основной таймер планировщика
    QPointer<QTimer> m_tryResumeTimer{nullptr};             // таймер включается только для контроля завершения паузы
    QCoreApplication * m_serviceApp{nullptr};
    //bool m_isPauseOn{false};
    QDateTime m_beginPauseTime0;                             // время начала паузы в левой камере
    QDateTime m_beginPauseTime1;                             // время начала паузы в правой камере
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Hardware> m_hardware;
#endif

    bool m_socketFlag{false};
    quint8 m_socketCommand{0};
    quint32 m_socketCommandLength{0};

    QHash<quint32, QSharedPointer<SparkSlot> > m_slots;  // хранит работающие слоты
    QHash<quint32, QMultiMap<QDateTime, QSharedPointer<SparkScheduleBase> > > m_sparkSchedules; // основной объект расписания

    LedColor m_currentLedColor{NoColor};

    void socketHandler(QTcpSocket *socket);
    void cleanSocketCommand();

    void stateRequestHandler(QTcpSocket * socket);
    void runCyclogramCommandHandler(const QByteArray &meat, QTcpSocket * socket = nullptr);
    void breakCyclogramCommandHandler(const QByteArray &meat);
    void breakCyclogramCommandHandler(const quint32 &nSlot);
    void setRackTemperatureCommandHandler(const QByteArray &meat);
    void alarmResetCommandHandler(const QByteArray &meat);
    void setLedColorCommandHandler(const QByteArray &meat);
    void pauseCommandHandler(const QByteArray &meat);
    void pauseByTemperatureReason(quint32 nSlot);
    void resumeCommandHandler(const QByteArray &meat);
    void reportRequestHandler(QTcpSocket *socket, const QByteArray &meat);
    void prepareRunRequestHandler(QTcpSocket *socket, const QByteArray &meat);
    void openLockCommandHandler(QTcpSocket *socket, const QByteArray &meat);
    void tryResumeTests();

    void emergencyBreakHardwareModules(quint32 nSlot);
    void emergencyBreakThermoprofile(quint32 nSlot);

    void disableTests();

    void interruptTest(quint32 nSlot, SparkSlot::SlotState state);

    void changeLedColor(LedColor color = LedColor::NoColor);

    Bugs parseCyclogram(quint32 nSlot, const QJsonObject &cyclogramObject);
    //! void parseDio(const QDateTime &currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> &currentPXISlot, const QJsonObject &cyclogramObject);
    //! void parseSmu(const QDateTime &currentTime, const quint32 nSlot, const QSharedPointer<PXISlot> &currentPXISlot, const QJsonObject &cyclogramObject);
//    void parseCyclogramThermo(const QJsonObject & cyclogramObject,const quint32& nSlot);
    Bugs parseCyclogramThermo(const QJsonObject & cyclogramObject,const quint32& nSlot);
    bool postParsing();

    void sendResponseState(QTcpSocket * socket);
    void sendReport(QTcpSocket * socket);
    void sendPrepareRunResponse(QTcpSocket * socket, qint32 nSlot);

    void logMessageString(const QString &head);
    void logBugs(const Bugs & bugs);

    void schedulerHandler();

    void swapScheduler(qint64 timeLength, qint32 nCameraSlot);

    void sendErrorsMessage(QTcpSocket *socket, Bugs bugs, quint32 nSlot);

    void saveChamberTempSettings(quint32 nSlot, float overHeatTemp, float overFreezeTemp, quint16 refrigeratorAutoStop, float refrigeratorStopTemp,
                                 float rackTemp, float rackTempHH, float  rackTempH, float rackTempL, float rackTempLL, float rackOverHeat, int upsTimerTimeout);

    QList<float> loadChamberTempSettings(quint32 nSlot);

    Bugs checkPowerDevices(Sample* pSample, quint32 slot);

    Bugs checkGenerators(Sample* sample, quint32 slot);
};

#endif // SPARKSERVICE_H
