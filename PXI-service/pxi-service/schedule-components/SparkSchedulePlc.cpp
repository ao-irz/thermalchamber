#include "SparkSchedulePlc.h"

//! TODO: Пока не понятно, где и когда делать первичную установку параметров камеры m_hardware->sendParametersRequest()

SparkSchedulePlc::SparkSchedulePlc(const QPointer<SparkPlcConnection> &hardware,
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        double temperature, double chanegeRate,
        const QString & thermoAction,
        const bool isInfinite) :
    SparkScheduleBase(logger, PLC_POLL_INTERVAL*2, start, stop, SparkScheduleBase::ScheduleActionOn, isInfinite),
    m_hardware(hardware),
    m_preferedtemperature(temperature),
    m_temperatureChangeRate(chanegeRate)
{
    if (thermoAction == "heating")
        m_thermoAction = HeatingType;
    else if (thermoAction == "cooling")
        m_thermoAction = CoolingType;
    else
        m_thermoAction = MaintainingType;
}

SparkSchedulePlc::SparkSchedulePlc(
        const QPointer<SparkPlcConnection> &hardware,
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        SparkScheduleBase::ScheduleAction action,
        const bool isInfinite) :
    SparkScheduleBase(logger, PLC_POLL_INTERVAL*2, start, stop, action, isInfinite),
    m_hardware(hardware)
{

}

void SparkSchedulePlc::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
    {
        if (m_hardware->sendControlRequest(m_preferedtemperature, m_temperatureChangeRate, (int)m_thermoAction))
        {
            //!m_hardware->changeCheckInterval(m_pollInterval); //! отключил, потому что юудет сложно сделать усреднение темп., если потребуется его делать равно на 10 сек.
            m_logger->logChamberTemperature(m_preferedtemperature, m_thermoAction);
            startAliveTimer();
            startPollTimer();
            m_stage = ScheduleRun;
        } else
            m_stage = ScheduleError;
    } else {
        if (m_hardware->sendStandByRequest(true)) {
            m_logger->log("Chamber should be switch off");
            if (m_startTime == m_stopTime) {
                m_stage = ScheduleCompleted;
                m_logger->log("Schedule's task completed at once");
            } else {
                startAliveTimer();
                //startPollTimer(); // поопрашивать камеру (ПЛК) для контроля температуры. Юра сказал, что темп. будет возвращаться все равно. Оно и логично, ибо внешний датчик - это не plc
                //! если цикл.предписывыет выключить термкам. то и незачем следить за ee ошибками и темп.
                m_stage = ScheduleRun;
            }
        } else
            m_stage = ScheduleError;
    }
}

void SparkSchedulePlc::pollHardware()
{
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    if (!m_hardware->mainTemperature().isValid)
        return;

		//m_logger->logTemperature(m_hardware->mainTemperature().value, m_hardware->averageMainTemperature(), m_hardware->thermalChamberTemperatureSet().value);

//    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
        m_hardware->setTemperatureToWindow(m_preferedtemperature, m_temperatureTolerance, (m_thermoAction == MaintainingType), true);
//    else
//        m_hardware->setTemperatureToWindow(m_preferedtemperature, m_temperatureTolerance, false, true);
}

void SparkSchedulePlc::latestPollHardware()
{
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    if (!m_hardware->mainTemperature().isValid)
        return;

//    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
        m_hardware->setTemperatureToWindow(m_preferedtemperature, m_temperatureTolerance, (m_thermoAction != MaintainingType), false);
//    else
//        m_hardware->setTemperatureToWindow(m_preferedtemperature, m_temperatureTolerance, false, false);
}
