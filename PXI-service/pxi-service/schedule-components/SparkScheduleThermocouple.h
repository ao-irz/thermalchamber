#ifndef SPARKSCHEDULETHERMOCOUPLE_H
#define SPARKSCHEDULETHERMOCOUPLE_H

#include "SparkScheduleEquipment.h"
#include "../plc-connection/SparkPlcConnection.h"

class SparkScheduleThermocouple : public SparkScheduleEquipment
{
public:
    SparkScheduleThermocouple() = delete;
    SparkScheduleThermocouple(const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Hardware> &hardware,
#endif
            const QPointer<SparkPlcConnection> &plcConnection,
            const QPointer<Logger> &logger, const qint64 pollIntervall,
            const QDateTime &start, const QDateTime &stop,
            const float tempMax, const float tempMin);

    SparkScheduleThermocouple(const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Hardware> &hardware,
#endif
            const QPointer<SparkPlcConnection> &plcConnection,
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop,
            ScheduleAction action = ScheduleActionOff);

    void execSchedule() override;

private:
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Hardware> m_hardware;

    QPointer<SparkPlcConnection> m_plcConnection;

    // устанавливаемые параметры thermocouple
    const float m_temperatureMax{0.0};
    const float m_temperatureMin{0.0};

    void pollHardware() override;

#endif
};

#endif // SPARKSCHEDULETHERMOCOUPLE_H
