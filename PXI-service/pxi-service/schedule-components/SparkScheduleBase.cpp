#include "SparkScheduleBase.h"

SparkScheduleBase::SparkScheduleBase(//const QSharedPointer<PXISlot> &pxiSlot,
        const QPointer<Logger> & logger,
        const qint64 pollIntervall,
        const QDateTime &start,
        const QDateTime &stop,
        const ScheduleAction action,
        const bool isInfinite) :
    m_logger(logger),
    m_startTime(start),
    m_stopTime(stop),
    m_action(action),
    m_stage(ScheduleWaitingStart),
    m_pollInterval(pollIntervall),
    m_isInfinite(isInfinite)
    //m_remainingTime(0)
{

}

SparkScheduleBase::~SparkScheduleBase()
{

}

void SparkScheduleBase::logBugs(const Bugs &bugs)
{
    for (auto bug : bugs) {
				m_logger->log(bug.message + " type=" + QString(bug.type==0?"PS":"Generator") + " slot = " + QString::number(bug.slot));
    }
}

quint32 SparkScheduleBase::instanceNumber() const
{
    return 0;
}

void SparkScheduleBase::latestPollHardware()
{

}

void SparkScheduleBase::setStartStopTime(const QDateTime &time1, const QDateTime &time2)
{
    m_startTime = time1; m_stopTime = time2;
}

void SparkScheduleBase::startAliveTimer()
{
    m_aliveTimer.reset(new QTimer());
    m_aliveTimer->setSingleShot(true);

    if (m_isInfinite)
        return;

    QObject::connect(m_aliveTimer.data(), &QTimer::timeout, [this](){
        //! Костыль на случай очень большого числа для интервала таймера (более 2^31)
        qint64 interval = QDateTime::currentDateTime().msecsTo(m_stopTime);
        if (interval > 3000) {
            m_aliveTimer->start( interval );
            m_logger->log("Schedule's task prolonged. New interval = " + QString::number(interval) + " ms" );
            return;
        }

        if (m_stage == ScheduleError)
            return;

        latestPollHardware();
        m_stage = ScheduleCompleted;
        m_logger->log("Schedule's task completed");
    });

    qint64 interval = m_startTime.msecsTo(m_stopTime);
    m_aliveTimer->start( interval );
}

void SparkScheduleBase::startPollTimer()
{
    if (!m_pollInterval)
        return;

    m_pollTimer.reset(new QTimer());
    QObject::connect(m_pollTimer.data(), &QTimer::timeout, [this](){

        if ( (QDateTime::currentDateTime().msecsTo(m_stopTime) <= m_pollInterval)  && (m_stage != SchedulePause)) {
            m_pollTimer->stop();
            return ;
        }

        pollHardware();
    });

    if (m_pollInterval < 2000)
        m_pollInterval = 2000;
    m_pollTimer->start(m_pollInterval);
}

void SparkScheduleBase::pauseSchedule()
{
    if (m_stage != ScheduleRun) {
        m_logger->log("Warning: Schedule item try to make a pause but item isn't in 'run' state");
        return;
    }

    if (m_aliveTimer.isNull()) {
        m_logger->log("Warning: Schedule item try to pause but aliveTimer wasn't created yet");
        return;
    }

    //m_remainingTime = m_aliveTimer->remainingTime();
    m_aliveTimer->stop();
    m_stage = SchedulePause;
    m_logger->log("Schedule item pause");

    //! pollTimer будет оставаться включенным на время паузы, что бы
    //! V,I,T,t были доступны в UI
    //! В то же время syncState() работать не будет, потому что слот будет в PauseSlotState
}

void SparkScheduleBase::resumeSchedule()
{
    if (m_stage != SparkScheduleBase::SchedulePause)
        return;

    m_stage = ScheduleRun;
    //m_aliveTimer->setInterval(m_remainingTime);
    //! изменение m_stopTime производятся в PXIService::swapScheduler через ::setStartStopTime()
    m_logger->log("Schedule item resume");

    if (m_isInfinite)
        return;

    m_aliveTimer->start(QDateTime::currentDateTime().msecsTo(m_stopTime));
}
