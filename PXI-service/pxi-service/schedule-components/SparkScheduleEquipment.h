#ifndef SPARKSCHEDULEEQUIPMENT_H
#define SPARKSCHEDULEEQUIPMENT_H

/*! Класс предок для всего оборудования PXI !*/

#include "SparkScheduleBase.h"

class SparkScheduleEquipment : public SparkScheduleBase
{
public:
    SparkScheduleEquipment() = delete;
    SparkScheduleEquipment(
        const QSharedPointer<SparkSlot> &pxiSlot,
        const QPointer<Logger> & logger,
        const qint64 pollIntervall,
        const QDateTime &start,
        const QDateTime &stop,
        const ScheduleAction action);

protected:
    QSharedPointer<SparkSlot> m_slot;
    quint32 m_slotNumber;
};

#endif // SPARKSCHEDULEEQUIPMENT_H
