#include "SparkScheduleEquipment.h"

SparkScheduleEquipment::SparkScheduleEquipment(const QSharedPointer<SparkSlot> &pxiSlot,
        const QPointer<Logger> &logger,
        const qint64 pollIntervall,
        const QDateTime &start,
        const QDateTime &stop,
        const SparkScheduleBase::ScheduleAction action) :
    SparkScheduleBase(logger, pollIntervall, start, stop, action),
    m_slot(pxiSlot),
    m_slotNumber(pxiSlot->number())
{

}
