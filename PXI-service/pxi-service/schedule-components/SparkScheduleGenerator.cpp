#include "SparkScheduleGenerator.h"


SparkScheduleGenerator::SparkScheduleGenerator(
        const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Hardware> &hardware,
#endif
        const QPointer<Logger> &logger,
        const quint32 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        const QString &signalForm, double a, double o, double f, double d) :
    SparkScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, SparkScheduleBase::ScheduleActionOn),
    m_hardware(hardware),
		m_signalForm( toSignalForm(signalForm) ),
    m_amplitude(a),
    m_offset(o),
    m_frequency(f),
    m_dutycycle(d)
{   }

SparkScheduleGenerator::SparkScheduleGenerator(
        const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
#else
        const QSharedPointer<Hardware> &hardware,
#endif
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        SparkScheduleBase::ScheduleAction action) :
    SparkScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_hardware(hardware)
{   }

SparkScheduleGenerator::SignalForm SparkScheduleGenerator::toSignalForm(const QString &string)
{
    if (string == "sinus")
        return SinusForm;
    if (string == "square")
        return SquareForm;
    if (string == "triangle" || string == "ramp")
        return TriangleForm;

    return SinusForm;
}

GenForm::GenForm SparkScheduleGenerator::toSignalForm(SparkScheduleGenerator::SignalForm signalForm)
{
    switch (signalForm) {
    case SinusForm:
        return GenForm::sinus;
    case SquareForm:
        return GenForm::square;
    case TriangleForm:
        return GenForm::triangle;
    default:
        return GenForm::sinus;
		}
}

QString SparkScheduleGenerator::signalFormToString() const
{
	QString retVal;

	switch(m_signalForm)
	{
		case SparkScheduleGenerator::SignalForm::SinusForm:
			retVal = "sinus";
			break;
		case SparkScheduleGenerator::SignalForm::SquareForm:
			retVal = "square";
			break;
		case SparkScheduleGenerator::SignalForm::TriangleForm:
			retVal = "triangle";
	}

	return retVal;
}

void SparkScheduleGenerator::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Bugs bugs;
    try {
#ifdef PXI_EMULATOR_ENABLE
        SampleEmulator * sample = m_hardware->sample(m_slotNumber);
#else
        Sample * sample = m_hardware->sample(m_slotNumber);
#endif
        if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
        {
            //TODO: Нужна ли проверка состояния подключения
            if (!sample->fgen->isConnected())
                bugs += sample->fgen->reconnect(m_slotNumber, sample->fgen->getNumber() + 1);
            if (bugs.empty())
            {
                m_logger->logGenerator(m_slotNumber+1, signalFormToString(), m_amplitude, m_offset, m_frequency, m_dutycycle);
                bugs += sample->fgen->put(toSignalForm(m_signalForm), m_amplitude, m_offset, m_frequency, m_dutycycle);

                if (!sample->fgen->isOn())
                {
                    m_logger->logGeneratorSwitch(m_slotNumber+1, true);
                    bugs += sample->fgen->on();
                }
            }
        }
        else
        {
            if (!sample->fgen->isConnected())
                bugs += sample->fgen->reconnect(m_slotNumber, sample->fgen->getNumber() + 1);
            if (bugs.empty())
            {
                m_logger->logGeneratorSwitch(m_slotNumber+1, false);
                bugs += sample->fgen->off();
            }
        }
    } catch (HWException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest(e.msg);
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        QString error;
        for (auto bug : bugs)
        {
            error += (!error.isEmpty()? "\n":"") + bug.device + ":" + bug.message;
        }
        m_slot->harwareErrorTest(error);
        logBugs(bugs);
        return;
    }

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        m_slot->setGeneratorInfo(m_amplitude, m_frequency, m_dutycycle);
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableGenerator(); // на время выключения PS syncState не должен играть роли да и отображение в UI должно пропасть
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

void SparkScheduleGenerator::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

		Bugs bugs;
		try
		{
				auto generator = m_hardware->sample(m_slotNumber)->fgen;
                if (!generator->isConnected())
                    bugs += generator->reconnect(m_slotNumber, generator->getNumber() + 1);
                if (bugs.empty())
                {
                    bugs += generator->getOn();
                    m_slot->setGeneratorOk(true);
                }
                // TODO: забить паузу во все задачи
//                else if (bugs[0].message == "Socket operation timed out")
//                {
//                    m_stage = SchedulePause;
//                    m_slot->pauseTest();
//                    m_slot->setGeneratorOk(false);
//                    m_logger->log("pause");
//                    logBugs(bugs);
//                    bugs.clear();
//                }

		} catch (HWException & e)
		{
				m_stage = ScheduleError;
                m_slot->harwareErrorTest(e.msg);
				m_logger->log(e.msg);
				m_slot->setGeneratorOk(false);
				return;
		}

		if (!bugs.isEmpty()) {
				m_stage = ScheduleError;
                QString error;
                for (auto bug : bugs)
                {
                    error += (!error.isEmpty()? "\n":"") + bug.device + ":" + bug.message;
                }
                m_slot->harwareErrorTest(error);
				m_slot->setGeneratorOk(false);
				logBugs(bugs);
		}


}
