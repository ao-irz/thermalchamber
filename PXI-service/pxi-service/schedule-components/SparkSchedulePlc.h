#ifndef SPARKSCHEDULEPLC_H
#define SPARKSCHEDULEPLC_H

#include "config.h"
#include "../plc-connection/SparkPlcConnection.h"
#include "SparkScheduleBase.h"

class SparkSchedulePlc : public SparkScheduleBase
{
public:
    enum ThermoAction {
        HeatingType,
        MaintainingType,
        CoolingType
    };

    SparkSchedulePlc() = delete;
    SparkSchedulePlc(const QPointer<SparkPlcConnection> &hardware,
            const QPointer<Logger> &logger,
            const QDateTime &start, const QDateTime &stop,
            double temperature, double chanegeRate, const QString &thermoAction, const bool isInfinite);

    SparkSchedulePlc(const QPointer<SparkPlcConnection> &hardware,
            const QPointer<Logger> &logger,
            const QDateTime &start, const QDateTime &stop,
            SparkScheduleBase::ScheduleAction action, const bool isInfinite);


    void execSchedule() override;

private:
    QPointer<SparkPlcConnection> m_hardware;

    ThermoAction m_thermoAction{MaintainingType};
    const float m_preferedtemperature{0.0};
    const float m_temperatureTolerance{TEMPERATURE_TOLERANCE};
    const float m_temperatureChangeRate{0.0};

    void pollHardware() override;
    void latestPollHardware() override;
};

#endif // SPARKSCHEDULEPLC_H
