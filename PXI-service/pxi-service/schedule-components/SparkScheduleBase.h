#ifndef SPARKSCHEDULEBASE_H
#define SPARKSCHEDULEBASE_H

#include <qglobal.h>
#include <QTimer>
#include <QDateTime>
#include <QSharedPointer>
#include <QPointer>
#include "Logger.h"
#include "SparkSlot.h"

#ifdef PXI_EMULATOR_ENABLE
#include "../pxi-emulator/PxiEmulator.h"
#else
#include "../adapter/hardware/hardware.h"
#endif

class SparkScheduleBase
{
public:
    enum ScheduleAction {
        ScheduleActionNoNeed,
        ScheduleActionOn,
        ScheduleActionOff,
    };

    enum ScheduleStage {
        ScheduleWaitingStart,
        ScheduleError,
        ScheduleRun,
        SchedulePause,
        ScheduleCompleted
    };

protected:
    SparkScheduleBase(const QPointer<Logger> &logger,
            const qint64 pollIntervall,
            const QDateTime & start,
            const QDateTime & stop,
            const ScheduleAction action = ScheduleActionNoNeed,
            const bool isInfinite = false);

    virtual ~SparkScheduleBase();

    QPointer<Logger> m_logger;

    // В какой момент времени нужно сделать
    QDateTime m_startTime;
    // Когда нужно закончить
    QDateTime m_stopTime;
    // No/On/Off для устройства
    ScheduleAction m_action;
    // Complete/Incomplete
    ScheduleStage m_stage;
    // Период опроса параметро (мс)
    qint64 m_pollInterval;
    // Таймер опроса параметров, впрочем, возможно, что не все наследики им воспользуются
    QScopedPointer<QTimer> m_pollTimer;
    QScopedPointer<QTimer> m_aliveTimer;
    qint32 m_remainingTime;
    bool m_isInfinite{false};               // специальный флаг исключительно для бесконечных блоков для 9-ой циклограммы

    void logBugs(const Bugs &bugs);

public:
    virtual void execSchedule() = 0;
    virtual quint32 instanceNumber() const; // только ради PowerSupply, потому что их может быть 5
    virtual void pollHardware() = 0;
    virtual void latestPollHardware(); // будет вызываться перед самым окончанием блока пока будет использован только для 9-ой цикл.

    void setAction(const ScheduleAction action)
    { m_action = action; }

    QDateTime startTime() const
    { return m_startTime; }

    QDateTime stopTime() const
    { return m_stopTime; }

    void setStartStopTime(const QDateTime & time1, const QDateTime & time2);

    quint64 duration() const
    { return m_startTime.msecsTo(m_stopTime); }

    ScheduleStage stage() const
    { return m_stage; }

    quint32 poolInterval() const
    { return m_pollInterval; }

    void startAliveTimer();
    void startPollTimer();
    void pauseSchedule();
    void resumeSchedule();
};

#endif // SPARKSCHEDULEBASE_H
