#ifndef SPARKSCHEDULEGENERATOR_H
#define SPARKSCHEDULEGENERATOR_H

#include "SparkScheduleEquipment.h"

class SparkScheduleGenerator : public SparkScheduleEquipment
{
public:
    enum SignalForm {
        SinusForm,
        SquareForm,
        TriangleForm
    };

    SparkScheduleGenerator() = delete;
    SparkScheduleGenerator(const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Hardware> &hardware,
#endif
            const QPointer<Logger> &logger,
            const quint32 pollIntervall,
            const QDateTime & start, const QDateTime &stop,
            const QString & signalForm, double a, double o, double f, double d);

    SparkScheduleGenerator(const QSharedPointer<SparkSlot> &pxiSlot,
#ifdef PXI_EMULATOR_ENABLE
            const QSharedPointer<PxiEmulator> &hardware,
#else
            const QSharedPointer<Hardware> &hardware,
#endif
            const QPointer<Logger> &logger,
            const QDateTime & start, const QDateTime &stop,
            ScheduleAction action = ScheduleActionOff);

		SignalForm toSignalForm(const QString & string);
		GenForm::GenForm toSignalForm(SignalForm signalForm);
		QString signalFormToString() const;

    void execSchedule() override;

private:
#ifdef PXI_EMULATOR_ENABLE
    QSharedPointer<PxiEmulator> m_hardware;
#else
    QSharedPointer<Hardware> m_hardware;
#endif


    // устанавливаемые параметры generator
    SignalForm m_signalForm;
    const double m_amplitude{0.0};
    const double m_offset{0.0};
    const double m_frequency{0.0};
    const double m_dutycycle{0.0};

    void pollHardware() override;
};

#endif // SPARKSCHEDULEGENERATOR_H
