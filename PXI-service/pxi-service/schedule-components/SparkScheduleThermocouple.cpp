#include "SparkScheduleThermocouple.h"

SparkScheduleThermocouple::SparkScheduleThermocouple(
        const QSharedPointer<SparkSlot> &pxiSlot,
        #ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
        #else
        const QSharedPointer<Hardware> &hardware,
        #endif
        const QPointer<SparkPlcConnection> &plcConnection,
        const QPointer<Logger> &logger, const qint64 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        const float tempMax, const float tempMin):
    SparkScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, SparkScheduleBase::ScheduleActionOn),
    m_hardware(hardware), m_plcConnection(plcConnection), m_temperatureMax(tempMax), m_temperatureMin(tempMin)
{  }

SparkScheduleThermocouple::SparkScheduleThermocouple(
        const QSharedPointer<SparkSlot> &pxiSlot,
        #ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
        #else
        const QSharedPointer<Hardware> &hardware,
        #endif
        const QPointer<SparkPlcConnection> &plcConnection,
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        SparkScheduleBase::ScheduleAction action) :
    SparkScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_hardware(hardware), m_plcConnection(plcConnection)
{  }

//инициализация задачи
void SparkScheduleThermocouple::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;

    Sample* sample = m_hardware->sample(m_slotNumber);
    QList<Thermocouple*> thermocouples = sample->thermocouples();
    for (int i = 0; i < thermocouples.count(); i++)
    {
        int thermocoupleIndex = m_slotNumber * 2 + i;
        thermocouples[i]->set_temperatureMax(m_temperatureMax);
        thermocouples[i]->set_temperatureMin(m_temperatureMin);
        thermocouples[i]->set_currentTemperature(0.0);
    }
    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
    {
        m_stage = ScheduleRun;
        startAliveTimer();
        startPollTimer();
    }
    else
    {
        if (m_startTime == m_stopTime)
        {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        }
        else
        {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

//опрос термопар и сравнение их значений с допустимыми
void SparkScheduleThermocouple::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;
    Sample* sample = m_hardware->sample(m_slotNumber);
    const QList<QPair<FloatValue,ShortValue>> thermocouples = m_plcConnection->slotTemperatures();
    QList<Thermocouple *> slotThermocouples = sample->thermocouples();
    double thermocoupleAverage{0.0};
    short thermocouplesStatus{0};
    for (int i = 0; i < slotThermocouples.count(); i++)
    {
        int thermocoupleIndex = (m_slotNumber <= 9 ? m_slotNumber: m_slotNumber - 10) * 2 + i;
        float thermocoupleValue = thermocouples[thermocoupleIndex].first.value;
        short thermocoupleStatus = thermocouples[thermocoupleIndex].second.value;
        slotThermocouples[i]->set_currentTemperature(thermocoupleValue);
        slotThermocouples[i]->set_thermocoupleState(thermocoupleStatus);
        thermocouplesStatus += thermocoupleStatus;
        thermocoupleAverage += thermocoupleValue;
    }
    m_slot->setChipTemperature(thermocoupleAverage / 2, m_temperatureMax, m_temperatureMin, thermocouplesStatus);
}
