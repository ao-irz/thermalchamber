
#include "config.h"
#include "SparkSchedulePowerSupply.h"
//#include "database/SparkDb.h"
#include "database/SparkPostgresDb.h"

SparkSchedulePowerSupply::SparkSchedulePowerSupply(
        const QSharedPointer<SparkSlot> &pxiSlot,
        #ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
        #else
        const QSharedPointer<Hardware> &hardware,
        #endif
        const QPointer<Logger> &logger,
        const qint64 pollIntervall,
        const QDateTime &start, const QDateTime &stop,
        quint32 number,
        double v, double ptv, double mtv, double c, double ptc, double mtc) :
    SparkScheduleEquipment(pxiSlot, logger, pollIntervall, start, stop, SparkScheduleBase::ScheduleActionOn),
    m_number(number),
    m_hardware(hardware),
    //!m_voltage(((m_number == NEGATIVE_POWER_SUPPLY_NUMBER) && (v > 0.0)) ? (-1 * v) : (v)), //! временно удалено. Работа с СМУ временно отключена. СМУ становится снова обычным источником
    m_voltage(v),
    m_plusToleranceVoltage(ptv),
    m_minusToleranceVoltage(mtv),
    m_voltageMin(m_voltage - m_minusToleranceVoltage),
    m_voltageMax(m_voltage + m_plusToleranceVoltage),
    m_current(c),
    m_plusToleranceCurrent(ptc),
    m_minusToleranceCurrent(mtc),
    m_currentMin(m_current - m_minusToleranceCurrent),
    m_currentMax(m_current + m_plusToleranceCurrent)
{
}

SparkSchedulePowerSupply::SparkSchedulePowerSupply(
        const QSharedPointer<SparkSlot> &pxiSlot,
        #ifdef PXI_EMULATOR_ENABLE
        const QSharedPointer<PxiEmulator> &hardware,
        #else
        const QSharedPointer<Hardware> &hardware,
        #endif
        const QPointer<Logger> &logger,
        const QDateTime &start, const QDateTime &stop,
        quint8 number,
        SparkScheduleBase::ScheduleAction action) :
    SparkScheduleEquipment(pxiSlot, logger, 0, start, stop, action),
    m_number(number),
    m_hardware(hardware)
{
}

void SparkSchedulePowerSupply::execSchedule()
{
    if (m_stage != ScheduleWaitingStart)
        return;
    Bugs bugs;
    try {
#ifndef PXI_EMULATOR_ENABLE
        Sample * sample = m_hardware->sample(m_slotNumber);
        if ( !sample )
            throw HWException(QString("Слот %1 не готов к работе").arg(m_slotNumber+1));
        auto powerSupply = sample->power(m_number);
#else
        //auto powerSupply = m_hardware->sample(m_slotNumber);
        auto powerSupply = m_hardware->sample(m_slotNumber)->power(m_number);
#endif
        //auto powerSupply = m_hardware->sample(m_slotNumber)->power(m_number);
        if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed))
        {
            if (!powerSupply->isConnected())
              bugs += powerSupply->reconnect(m_slotNumber, powerSupply->getNumber() + 1);
            if (bugs.empty())
            {
              m_logger->logPowerSupply(m_slotNumber+1, m_number, m_voltage, m_current);
              bugs += powerSupply->put(m_voltage, m_current);
              if (bugs.empty())
              {
                  SparkPostgresDb::getInstance()->powerSet(m_number, m_voltage, m_plusToleranceVoltage, m_minusToleranceVoltage, m_current, m_plusToleranceCurrent, m_minusToleranceCurrent);

                  if (!powerSupply->isOn())
                  {
                      m_logger->logPowerSupplySwitch(m_slotNumber+1, m_number, true);
                      bugs += powerSupply->on();
                  }
              }
            }
            else
            {
                //TODO: поставить испытания на паузу
            }
        }
        else
        {
          if (!powerSupply->isConnected())
            bugs += powerSupply->reconnect(m_slotNumber, powerSupply->getNumber() + 1);
          if (bugs.empty())
          {
            m_logger->logPowerSupplySwitch(m_slotNumber+1, m_number, false);
            bugs += powerSupply->off();
            SparkPostgresDb::getInstance()->powerOff(m_number);
          }
        }
    } catch (HWException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest(e.msg);
        m_logger->log(e.msg);
        SparkPostgresDb::getInstance()->powerLog(m_number, e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        QString error;
        for (auto bug : bugs)
        {
            error += (!error.isEmpty()? "\n":"") + bug.device + ":" + bug.message;
        }
        m_slot->harwareErrorTest(error);
        logBugs(bugs);
        SparkPostgresDb::getInstance()->logBugs(bugs);
        return;
    }

    if ((m_action == ScheduleActionOn) || (m_action == ScheduleActionNoNeed)) {
        m_stage = ScheduleRun;
        startAliveTimer();
        startPollTimer();
    } else {
        m_slot->disableVoltageCurrent(m_number); // на время выключения PS syncState не должен играть роли да и отображение в UI должно пропасть
        if (m_startTime == m_stopTime) {
            m_stage = ScheduleCompleted;
            m_logger->log("Schedule's task completed at once");
        } else {
            m_stage = ScheduleRun;
            startAliveTimer();
        }
    }
}

quint32 SparkSchedulePowerSupply::instanceNumber() const
{
    return m_number;
}

void SparkSchedulePowerSupply::pollHardware()
{
    // Эта проверка под сомнением. Т.е. нужно ли продолжать запросы состояния в данном таске, если произошла hardware ошибка
    if ((m_stage != ScheduleRun) && (m_stage != SchedulePause))
        return;

    Bugs bugs;
    try {
        auto powerSupply = m_hardware->sample(m_slotNumber)->power(m_number);
        if (!powerSupply->isConnected())
            bugs += powerSupply->reconnect(m_slotNumber, powerSupply->getNumber() + 1);
        if (bugs.empty())
        {
            double v;
            double c;
            bugs += powerSupply->voltage(&v);
            bugs += powerSupply->current(&c);
            m_logger->log(QStringLiteral("%1 Measure V = %2 I = %3").arg(m_number).arg(v).arg(c));
            SparkPostgresDb::getInstance()->powerGet(m_number, v, c);

            m_slot->setChipVoltage(m_number, v, m_voltageMin, m_voltageMax);
            m_slot->setChipCurrent(m_number, c, m_currentMin, m_currentMax);
        }
    } catch (HWException & e) {
        m_stage = ScheduleError;
        m_slot->harwareErrorTest(e.msg);
        m_logger->log(e.msg);
        return;
    }

    if (!bugs.isEmpty()) {
        m_stage = ScheduleError;
        QString error;
//        for (auto bug : bugs)
//        {
//            error += (!error.isEmpty()? "\n":"") + bug.device + ":" + bug.message;
//        }
        error += (!error.isEmpty()? "\n":"") + bugs[instanceNumber()].device + ":" + bugs[instanceNumber()].message;
        m_slot->harwareErrorTest(error);
        logBugs(bugs);
    }
}
