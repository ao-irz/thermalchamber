QT += network sql serialport
QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

#DESTDIR = C:/SparkService
TARGET = sparkservice

include(qtservice/src/qtservice.pri)

QMAKE_LFLAGS += -static

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../adapter/hardware/thermocouple.cpp \
    ../pxi-emulator/PxiEmulator.cpp \
    ../adapter/hardware/bugs.cpp \
    ../adapter/hardware/fgen.cpp \
    ../adapter/hardware/power.cpp \
    ../adapter/hardware/scpi.cpp \
    ../adapter/hardware/hardware.cpp \
    Logger.cpp \
    Settings.cpp \
    SparkService.cpp \
    SparkServiceParsingEssential.cpp \
    SparkServiceParsingThermo.cpp \
    SparkSlot.cpp \
    database/SparkDb.cpp \
    database/SparkPostgresDb.cpp \
    main.cpp \
    plc-connection/SparkPlcConnection.cpp \
    schedule-components/SparkScheduleBase.cpp \
    schedule-components/SparkScheduleEquipment.cpp \
    schedule-components/SparkScheduleGenerator.cpp \
    schedule-components/SparkSchedulePlc.cpp \
    schedule-components/SparkSchedulePowerSupply.cpp \
    schedule-components/SparkScheduleThermocouple.cpp \
    switch-connection/SparkSwitchConnection.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ../../PXI-common/common.h \
    ../../PXI-common/led-color.h \
    ../../PXI-common/zummer.h \
    ../adapter/hardware/thermocouple.h \
    ../pxi-emulator/PxiEmulator.h \
    ../adapter/hardware/bugs.h \
    ../adapter/hardware/fgen.h \
    ../adapter/hardware/power.h \
    ../adapter/hardware/scpi.h \
    ../adapter/hardware/hardware.h \
    Logger.h \
    Settings.h \
    SparkService.h \
    SparkSlot.h \
    ValueTypes.h \
    config.h \
    database/SparkDb.h \
    database/SparkPostgresDb.h \
    plc-connection/SparkPlcConnection.h \
    schedule-components/SparkScheduleBase.h \
    schedule-components/SparkScheduleEquipment.h \
    schedule-components/SparkScheduleGenerator.h \
    schedule-components/SparkSchedulePlc.h \
    schedule-components/SparkSchedulePowerSupply.h \
    schedule-components/SparkScheduleThermocouple.h \
    switch-connection/SparkSwitchConnection.h

LIBS += -lws2_32
#INCLUDEPATH+=D:/PostgreSQL/10/include
#LIBS+=D:/PostgreSQL/10/lib/libpq.lib
#LIBS += -lpq
