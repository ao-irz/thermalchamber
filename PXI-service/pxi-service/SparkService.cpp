#include <QCoreApplication>
#include <QThread>
#include "QTcpServer"
#include <QTcpSocket>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <QSettings>
#include <QDir>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "SparkService.h"
#include "schedule-components/SparkSchedulePowerSupply.h"
#include "schedule-components/SparkScheduleGenerator.h"

SparkService::SparkService(int argc, char **argv, const QString & selfPath) :
  QtService<QCoreApplication>(argc, argv, SERVICE_NAME),
  m_selfPath(selfPath)
{    
  //! application() here is not valid

  setServiceDescription("Service for modules managing");
  setStartupType(QtServiceController::AutoStartup);

  //    Settings dbgSettings(m_selfPath, nullptr);
  //    qDebug() << dbgSettings.logPath();
  //    qDebug() << dbgSettings.value("LogPath");
  //    //dbgSettings.sync();
}

SparkService::~SparkService()
{
  //! do not clear variables here, because destructor called
}

void SparkService::start()
{
  logMessage("Service started");

  m_serviceApp = application();
  m_settings = new Settings(m_selfPath, m_serviceApp);
  m_logger = new Logger(m_settings, m_serviceApp, this);

#ifndef PSU_EMULATOR
  QThread::sleep(21);
#endif
  m_db = SparkPostgresDb::getInstance(m_logger, m_settings, m_serviceApp);
//  m_db = SparkDb::getInstance(m_logger, m_settings, m_serviceApp);

    // ================ Starting logging  ================
  logMessageString(QStringLiteral("Service started"));

  // ================ Local socket for UI ================
  m_localServer = new QTcpServer(m_serviceApp);
  //m_localServer->setSocketOptions(QLocalServer::WorldAccessOption); // it is very imortant, because service live under administrator privelegie level
  QObject::connect(m_localServer.data(), &QTcpServer::newConnection, [this]() {
    QTcpSocket * socket = m_localServer->nextPendingConnection();
    //m_logger->dbgLog("New connection detected");
    QObject::connect(socket, &QTcpSocket::readyRead, [=]() {
      socketHandler(socket);
    });

    QObject::connect(socket, &QTcpSocket::disconnected, socket, &QTcpSocket::deleteLater);

    //TODO: maybe add timer for every connection
  });

  // ================ Unknown socket for PLC connection ================
  // загружаем настройки из файла для 1 камеры
  QList<float> loadedChamber1Settings = loadChamberTempSettings(1);
  m_plcConnection = new SparkPlcConnection(m_logger,
                                           std::bind(&SparkService::pauseByTemperatureReason,this,THERMO_SLOT_NUMBER1),
                                           THERMO_SLOT_NUMBER1,
                                           m_settings->groupValueContents(INI_PLC_GROUP,INI_LEFT_CAMERA_PORT).toUShort(),
                                           loadedChamber1Settings,
                                           m_settings->groupValueContents(INI_PLC_GROUP,INI_PLC_IP),
                                           m_serviceApp);
  QObject::connect(m_plcConnection, &SparkPlcConnection::changeLeds, [=]() {changeLedColor();});
  QObject::connect(m_plcConnection, &SparkPlcConnection::saveChamberTempSettings, [=](quint32 nSlot, float overHeatTemp, float overFreezeTemp, quint16 refrigeratorAutoStop, float refrigeratorStopTemp, float rackTemp, float rackTempHH, float  rackTempH, float rackTempL, float rackTempLL, float rackOverHeat, int upsTimerTimeout)
                    {saveChamberTempSettings(nSlot, overHeatTemp, overFreezeTemp, refrigeratorAutoStop, refrigeratorStopTemp, rackTemp, rackTempHH, rackTempH, rackTempL, rackTempLL, rackOverHeat, upsTimerTimeout);});
  m_plcConnectionList.append(m_plcConnection);
  m_plcConnection->sendParametersRequest();

  // загружаем настройки из файла для 2 камеры
  QList<float> loadedChamber2Settings = loadChamberTempSettings(2);
  m_plcConnection = new SparkPlcConnection(m_logger,
                                           std::bind(&SparkService::pauseByTemperatureReason,this,THERMO_SLOT_NUMBER2),
                                           THERMO_SLOT_NUMBER2,
                                           m_settings->groupValueContents(INI_PLC_GROUP,INI_RIGHT_CAMERA_PORT).toUShort(),
                                           loadedChamber2Settings,
                                           m_settings->groupValueContents(INI_PLC_GROUP,INI_PLC_IP),
                                           m_serviceApp);
  QObject::connect(m_plcConnection, &SparkPlcConnection::saveChamberTempSettings, [=](quint32 nSlot, float overHeatTemp, float overFreezeTemp, quint16 refrigeratorAutoStop, float refrigeratorStopTemp, float rackTemp, float rackTempHH, float  rackTempH, float rackTempL, float rackTempLL, float rackOverHeat, int upsTimerTimeout)
                    {saveChamberTempSettings(nSlot, overHeatTemp, overFreezeTemp, refrigeratorAutoStop, refrigeratorStopTemp, rackTemp, rackTempHH, rackTempH, rackTempL, rackTempLL, rackOverHeat, upsTimerTimeout);});
  m_plcConnectionList.append(m_plcConnection);
  m_plcConnection->sendParametersRequest();

  m_plcConnection = m_plcConnectionList[0];
  changeLedColor();


  // ================ RS-232 ================
  //m_switchConnection = new SparkSwitchConnection(m_logger, m_serviceApp);
  // ================ Hardware ================
  Bugs bugs;
  m_logger->log(QStringLiteral("----- Hardware init ------"));
  try
  {
#ifdef PXI_EMULATOR_ENABLE
    m_hardware.reset(new PxiEmulator(m_serviceApp, m_selfPath));
#else
//    m_hardware.reset(new Hardware());
    m_hardware.reset(new Hardware(m_logger));
#endif
    QStringList psList = m_settings->groupContents(INI_PSS_GROUP);
		/*for(auto & slot : psList)
			m_logger->log(slot);*/
    QStringList genList = m_settings->groupContents(INI_GENERATORS_GROUP);
    QStringList power_slots_list = m_settings->groupContents(INI_SLOTS_GROUP);
    QList<int> power_channel_map;
    for(auto & slot : power_slots_list)
      power_channel_map.append(slot.toInt());
    //TODO: добавить инициализацию термопар
    const int thermocouples_onslot = m_settings->groupValueContents(INI_TCOUPLE_GROUP, INI_SLOT_TCOUPLE_COUNT).toInt();
    bugs = m_hardware->init(POWER_SUPPLY_NUMBER, 21000, 55,psList,genList, power_channel_map, thermocouples_onslot);
//    bugs = m_hardware->init(POWER_SUPPLY_NUMBER, 21000, 105,psList,genList, power_channel_map);
  }
  catch (HWException & e)
  {
    m_logger->log(e.msg);
    disableTests();
  }

  if (!bugs.isEmpty())
  {
    logBugs(bugs);
    disableTests();
  }
  m_logger->log( QStringLiteral("----- Hardware init complete ------") );

  // ================ Schedule timer ================
  m_scheduleTimer = new QTimer(m_serviceApp);
  QObject::connect(m_scheduleTimer.data(), &QTimer::timeout, [this](){
    schedulerHandler();
  });
  m_scheduleTimer->setInterval(SCHEDULER_PERIOD);
  m_scheduleTimer->start();

  // ==================== Pause timer ====================
  m_tryResumeTimer = new QTimer(m_serviceApp);
  QObject::connect(m_tryResumeTimer.data(), &QTimer::timeout, [this](){
    tryResumeTests();
  });
  m_tryResumeTimer->setInterval(CHECK_END_PAUSE_PERIOD);

  // ================ Listen ================
  if (m_localServer->listen(QHostAddress::Any, QString(DEFAULT_SERVER_PORT).toUInt()))
    m_logger->dbgLog("Service TCP server is now listening");
  else
    m_logger->dbgLog("Service TCP server is not listening");

}

void SparkService::stop()
{
  //m_switchConnection->switchOff(); // !не уверен, что это правильно
  m_plcConnectionList[0]->sendBuzzerRequest(Zummer::ZummerOff);

  m_hardware->close();
	m_hardware.clear();
	emergencyBreakThermoprofile(THERMO_SLOT_NUMBER1); // не уверен, что команда успеет отправиться в порт
  emergencyBreakThermoprofile(THERMO_SLOT_NUMBER2); // не уверен, что команда успеет отправиться в порт
	delete m_localServer;
	delete m_plcConnectionList[0];
	delete m_plcConnectionList[1];
	m_plcConnectionList.clear();
	//delete m_switchConnection;
	delete m_db;
	if (m_scheduleTimer && m_scheduleTimer->isActive())
		m_scheduleTimer->stop();
  delete m_scheduleTimer;
	if (m_tryResumeTimer && m_tryResumeTimer->isActive())
		m_tryResumeTimer->stop();
	delete m_tryResumeTimer;
	m_socketFlag = false;
  m_socketCommand = 0;
  m_socketCommandLength = 0;

	//delete m_settings; не жолжно удаляться раньше чем m_logger
	// иначе -terminate сервиса просто не происходит корректно и он остается в висячем состоянии

	m_logger->log("Service  stopped");
  logMessage("Service  stopped");

	delete m_logger;

}

void SparkService::pause()
{

}

void SparkService::resume()
{

}

void SparkService::processCommand(int code)
{
  if (code == 1) {
    m_logger->log("Service stopping");
  }
}

void SparkService::socketHandler(QTcpSocket *socket)
{
  //! Format specification: 1byte(command) + 4byte(length of payload) + Nbyte(payload)
  qint64 availabeBytes = socket->bytesAvailable();
  while (availabeBytes >= HEADER_LENGTH)
  {
    if (!m_socketFlag)
    {
      socket->getChar(reinterpret_cast<char *>(&m_socketCommand));
      QByteArray lenBA = socket->read(LENGTH_OF_PAYLOAD);
      m_socketCommandLength = *(reinterpret_cast<quint32 *>(lenBA.data()));
      m_socketFlag = true;
      availabeBytes -= HEADER_LENGTH;
    }

    if (availabeBytes < m_socketCommandLength)
      return;

    QByteArray meat;
    switch(m_socketCommand)
    {
      case StateRequest:
        stateRequestHandler(socket);
        cleanSocketCommand();
        break;
      case RunCyclogramCommand:
        meat = socket->read(m_socketCommandLength);
        runCyclogramCommandHandler(meat, socket);
        cleanSocketCommand();
        break;
      case BreakCyclogramCommand:
        meat = socket->read(m_socketCommandLength);
        breakCyclogramCommandHandler(meat);
        cleanSocketCommand();
        break;
      case SetRackTemperatureCommand:
        meat = socket->read(m_socketCommandLength);
        setRackTemperatureCommandHandler(meat);
        cleanSocketCommand();
        break;
      case AlarmResetCommand:
        meat = socket->read(m_socketCommandLength);
        alarmResetCommandHandler(meat);
        cleanSocketCommand();
        break;
      case SetLedColorCommand:
        meat = socket->read(m_socketCommandLength);
        setLedColorCommandHandler(meat);
        cleanSocketCommand();
        break;
      case PauseCommand:
        meat = socket->read(m_socketCommandLength);
        pauseCommandHandler(meat);
        cleanSocketCommand();
        break;
      case ResumeCommand:
        meat = socket->read(m_socketCommandLength);
        resumeCommandHandler(meat);
        cleanSocketCommand();
        break;
      case ReportRequest:
        meat = socket->read(m_socketCommandLength);
        reportRequestHandler(socket, meat);
        cleanSocketCommand();
        break;
      case PrepareRunRequest:
        meat = socket->read(m_socketCommandLength);
        prepareRunRequestHandler(socket, meat);
        cleanSocketCommand();
        break;
      case OpenLockCommand:
        meat = socket->read(m_socketCommandLength);
        openLockCommandHandler(socket, meat);
        cleanSocketCommand();
        break;
      default:
        meat = socket->read(m_socketCommandLength);
        cleanSocketCommand();
    }

    availabeBytes = socket->bytesAvailable();
  }
}

void SparkService::cleanSocketCommand()
{
  m_socketFlag = false;
  m_socketCommandLength = 0;
  m_socketCommand = 0;
}

void SparkService::stateRequestHandler(QTcpSocket *socket)
{
  //m_logger->log("received state request");
  sendResponseState(socket);
}

void SparkService::runCyclogramCommandHandler(const QByteArray &meat, QTcpSocket *socket)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  QByteArray jsonCyclogram = meat.right(meat.size() - NSLOT_LENGTH);

  if (m_slots.contains(nSlot)) {
    m_logger->log("Error: Attempt to start a cyclogram on busy slot");
    return;
  }

  /*if (nSlot>=m_hardware->sampleCount())
    {
        m_logger->log("Slot " + QString::number(nSlot+1) + " is not ready to perform schedule step");
        return;
    }*/

	m_logger->log("---- begin cyclogram ---- slot = " + QString::number(nSlot));
  m_logger->log(jsonCyclogram);
  m_logger->log("---- end cyclogram ---- ");

  QJsonParseError error;
  QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonCyclogram, &error);
  if (error.error != QJsonParseError::NoError) {
    m_logger->log(QStringLiteral("Error: Cyclogram json error: ") + error.errorString() + " at " + QString::number(error.offset));
    return;
  }
  Bugs thermocyclogramErrors;

  if (nSlot == THERMO_SLOT_NUMBER1) {
    thermocyclogramErrors += parseCyclogramThermo(jsonDoc.object(),nSlot);
    //! TODO: сделать валидацию на подобие postParsing()
    if (thermocyclogramErrors.empty())
        m_plcConnectionList[0]->beginThermoprofile();
//    else
//        sendErrorsMessage(socket, thermocyclogramErrors, nSlot);
    return;
  }

  if (nSlot == THERMO_SLOT_NUMBER2) {
    thermocyclogramErrors += parseCyclogramThermo(jsonDoc.object(),nSlot);
    //! TODO: сделать валидацию наподобие postParsing()

    if (thermocyclogramErrors.empty())
        m_plcConnectionList[1]->beginThermoprofile();
//    else
//        sendErrorsMessage(socket, thermocyclogramErrors, nSlot);
    return;
  }

  //
  //Сюда вставить проверку работоспособности приборов
  //
  Bugs hardwareErrors = parseCyclogram(nSlot, jsonDoc.object());
  if (hardwareErrors.empty())
    changeLedColor();
  else
      sendErrorsMessage(socket, hardwareErrors, nSlot);
}

void SparkService::breakCyclogramCommandHandler(const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  breakCyclogramCommandHandler( nSlot );
}

void SparkService::breakCyclogramCommandHandler(const quint32 &nSlot)
{
  if ((nSlot == THERMO_SLOT_NUMBER1) || (nSlot == THERMO_SLOT_NUMBER2))
  {
    if (!m_sparkSchedules.contains(nSlot)) {
      m_logger->log(QString("Service: (Error) Attempt to stop empty thermoprofile"));
      return;
    }
    emergencyBreakThermoprofile(nSlot);
    //! Здесь по хорошему бы завершать все тесты...
    m_logger->log(QString("Service: Received break command for thermoprofile"));
    return;
  }

  m_logger->log(QString("Service: Received break command for slot %1").arg(nSlot+1));

  if (!m_slots.contains(nSlot)) {
    m_logger->log(QString("Service: (Error) Attempt to stop empty slot %1").arg(nSlot+1));
    return;
  }

  if (m_slots.value(nSlot)->state() == SparkSlot::TestCompletedState)
  {
    logMessageString(QString("Service: Slot %1 removed normally").arg(nSlot+1));
  }
  else
  if ((m_slots.value(nSlot)->state() == SparkSlot::AlarmSlotState) || (m_slots.value(nSlot)->state() == SparkSlot::HardwareErrorSlotState))
  {
    logMessageString(QString("Service: Slot %1 removed (slot had alarm or hardware error)").arg(nSlot+1));
  }
  else
  {
    logMessageString(QString("Service: Slot %1 removed (test forsed stopped)").arg(nSlot+1));
    // Загасить всё вне планировщика
		emergencyBreakHardwareModules(nSlot);
  }

  m_slots.remove(nSlot);
  changeLedColor();
  m_sparkSchedules[nSlot].clear();
  m_sparkSchedules.remove(nSlot);
}

void SparkService::setRackTemperatureCommandHandler(const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  if ((nSlot != THERMO_SLOT_NUMBER1) && (nSlot != THERMO_SLOT_NUMBER2))
    return;

  float tempHH = *reinterpret_cast<float *>(meat.mid(NSLOT_LENGTH, REAL_PARAM_LENGTH).data());
  float tempLL = *reinterpret_cast<float *>(meat.mid(2*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  quint16 autostop = *reinterpret_cast<quint32 *>(meat.mid(3*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data()); // а вообще тут бул, который сокарщается до uint16
  float tempAutostop = *reinterpret_cast<float *>(meat.mid(4*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());

  float rackSP = *reinterpret_cast<float *>(meat.mid(5*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  float rackHH = *reinterpret_cast<float *>(meat.mid(6*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  float rackH = *reinterpret_cast<float *>(meat.mid(7*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  float rackL = *reinterpret_cast<float *>(meat.mid(8*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  float rackLL = *reinterpret_cast<float *>(meat.mid(9*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  // температура перегрева стоек
  float rackOverHeat = *reinterpret_cast<float *>(meat.mid(10*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
  int upsTimerTimeout{0};
//  if (nSlot == THERMO_SLOT_NUMBER1)
//  {
  upsTimerTimeout = *reinterpret_cast<int *>(meat.mid(11*REAL_PARAM_LENGTH, REAL_PARAM_LENGTH).data());
//      m_logger->log(QString("upsTimerTimeout:%1").arg(upsTimerTimeout));
//  }
  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];

  saveChamberTempSettings(nSlot==THERMO_SLOT_NUMBER1? 1:2, tempHH, tempLL, autostop, tempAutostop, rackSP, rackHH, rackH, rackL, rackL, rackOverHeat, upsTimerTimeout);

  m_plcConnection->sendParametersRequest(tempHH, tempLL, autostop, tempAutostop, rackSP, rackHH, rackH, rackL, rackLL, rackOverHeat, upsTimerTimeout);

  m_logger->log(QString("Set chamber parameters: tempHH = %1,tempLL = %2,autostop = %3,tempAutostop = %4,rackSP = %5,rackHH = %6,rackH = %7,rackL = %8,rackLL = %9, rackOverHeat = %10, upsTimerTimeout = %11")
                .arg(tempHH).arg(tempLL).arg(autostop).arg(tempAutostop).arg(rackSP).arg(rackHH).arg(rackH).arg(rackL).arg(rackLL).arg(rackOverHeat).arg(upsTimerTimeout));
}

void SparkService::alarmResetCommandHandler(const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  if ((nSlot != THERMO_SLOT_NUMBER1) && (nSlot != THERMO_SLOT_NUMBER2))
    return;

  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];
  m_plcConnection->sendAlarmReset();

  m_logger->log("Chamber alarm reset");
}

void SparkService::setLedColorCommandHandler(const QByteArray &meat)
{
  quint16 colorInt = *reinterpret_cast<quint32 *>(meat.left(LEDCOLOR_LENGTH).data());
  LedColor color = (LedColor)colorInt;

  //! отключил для отладки эту проверку
  //!if ( (color != LedColor::NoColor) && (color != LedColor::WhiteLed) && (color != LedColor::BlueLed) ) {
  //!    m_logger->log(QStringLiteral("Error: trying to set the unacceptable led's color"));
  //!    return;
  //!}

  m_plcConnectionList[0]->sendRGBRequest(color);
  m_logger->log("Set led color");
}

void SparkService::pauseCommandHandler(const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  if ((nSlot != THERMO_SLOT_NUMBER1) && (nSlot != THERMO_SLOT_NUMBER2))
    return;

  if (!m_slots.size()) {
    m_logger->log("Warning: There are no active slots for pause");
    return;
  }

  // Забить паузу во все слоты, которые активны. Думаю это должно быть сделано раньше чем в пауза пойдет в планировщик
  for (auto & pxiSlot : m_slots)
  {
    if ( (nSlot==THERMO_SLOT_NUMBER1 && pxiSlot->number()<10) ||
         (nSlot==THERMO_SLOT_NUMBER2 && pxiSlot->number()>=10) )
      pxiSlot->pauseTest();
  }

  // Забить паузу во текущее задания
  for (const auto & slot : m_sparkSchedules.keys())
  {
    if ( (nSlot==THERMO_SLOT_NUMBER1 && slot<10) ||
         (nSlot==THERMO_SLOT_NUMBER2 && slot>=10))
      for (const auto & sch : m_sparkSchedules.value(slot))
      {
        if (sch->stage() == SparkScheduleBase::ScheduleRun)
          sch->pauseSchedule();
      }
  }


  // Забить паузу в камеру
  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];
  m_plcConnection->pauseChamber();

  // Планировщик тоже должен быть приостановлен, но на самом деле внутри начнет выполняться другая ветка
  if ( nSlot==THERMO_SLOT_NUMBER1 )
    m_beginPauseTime0 = QDateTime::currentDateTime();
  else
  if ( nSlot==THERMO_SLOT_NUMBER2 )
    m_beginPauseTime1 = QDateTime::currentDateTime();


  m_logger->log(QString("Tests was paused, camera ")+(nSlot==THERMO_SLOT_NUMBER1?QString("left"):QString("right")));
}

void SparkService::pauseByTemperatureReason(quint32 nSlot)
{
  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];

  if ( m_plcConnection->isPaused() )
    return;

  if ( !m_slots.size() )
    return;

  for ( auto & pxiSlot : m_slots )
  {
    if ( (nSlot==THERMO_SLOT_NUMBER1 && pxiSlot->number()<10) ||
         (nSlot==THERMO_SLOT_NUMBER2 && pxiSlot->number()>=10) )
      pxiSlot->pauseTest();
  }

  for (const auto & slot : qAsConst(m_sparkSchedules))
  {
    for (const auto & sch : slot)
    {
      if (sch->stage() == SparkScheduleBase::ScheduleRun)
        sch->pauseSchedule();
    }
  }

  //m_plcConnection->pauseChamber(); // не знаю, нужно ли стопорить камеру и потом сразу ее запускать ?

  if ( nSlot==THERMO_SLOT_NUMBER1 )
    m_beginPauseTime0 = QDateTime::currentDateTime();
  else
  if ( nSlot==THERMO_SLOT_NUMBER2 )
    m_beginPauseTime1 = QDateTime::currentDateTime();

  m_plcConnection->setPause(true);

  m_logger->log("All tests are paused by temperature reason");

  //! ----  второе деййствие ----

  //m_plcConnection->resumeChamber();
  m_tryResumeTimer->start();

  m_logger->log("Now waiting for required temperature");
}

void SparkService::resumeCommandHandler(const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  if ((nSlot != THERMO_SLOT_NUMBER1) && (nSlot != THERMO_SLOT_NUMBER2))
    return;

  quint32 parameter = *reinterpret_cast<quint32 *>(meat.mid(NSLOT_LENGTH, PARAMETER_LENGTH).data());
  Q_UNUSED(parameter)

  if (!m_slots.size()) {
    m_logger->log("Warning: There are no active slots for resume tests");
    // return;
  }

  // Прием этой команды должен всего лишь разрешать ожидание слотами достижения камерой нужной температуры продолжения теста
  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];
  m_plcConnection->resumeChamber();
  m_tryResumeTimer->start();

  m_logger->log("Try to resume tests. Waiting for required temperature");
}

void SparkService::reportRequestHandler(QTcpSocket *socket, const QByteArray &meat)
{
    Q_UNUSED(socket);
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());

  if (!m_slots.contains(nSlot)) {
    m_logger->log("Error: Attempt to take a report. This slot is busy");
    return;
  }

  if (m_slots.value(nSlot)->state() == SparkSlot::TestCompletedState) {
    //! TODO: генерация отчета об удачном завершении теста
    //! sendReport()
  } else if ((m_slots.value(nSlot)->state() == SparkSlot::AlarmSlotState) || (m_slots.value(nSlot)->state() == SparkSlot::HardwareErrorSlotState)) {
    //! TODO: генерация отчета о неуспешном тесте
    //! sendReport(socket)
  } else {
    //! TODO: видимо рано еще генерировать отчет, потому что тест еще не закончен
  }
}

void SparkService::prepareRunRequestHandler(QTcpSocket *socket, const QByteArray &meat)
{
  quint32 nSlot = *reinterpret_cast<quint32 *>(meat.left(NSLOT_LENGTH).data());
  QByteArray jsonCyclogram = meat.right(meat.size() - NSLOT_LENGTH);

  if (m_slots.contains(nSlot)) {
    m_logger->log("Error: Attempt to prepare run. This slot is busy");
    return;
  }

  QJsonParseError error;
  QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonCyclogram, &error);
  if (error.error != QJsonParseError::NoError)
  {
    m_logger->log(QStringLiteral("Error: Cyclogram json error: ") + error.errorString() + " at " + QString::number(error.offset));
    return;
  }

  sendPrepareRunResponse(socket, nSlot);
}

void SparkService::openLockCommandHandler(QTcpSocket *socket, const QByteArray &meat)
{
    Q_UNUSED(socket);
  quint32 nLock = *reinterpret_cast<quint32 *>(meat.left(OPEN_LOCK_LENGTH).data());

  if ( nLock==0 || nLock==2 )
    m_plcConnectionList[0]->sendCommandRequest(nLock==0?PLC_OPEN_UPPER_LOCK:PLC_OPEN_LOWER_LOCK);
  else
    if ( nLock==1 || nLock==3 )
      m_plcConnectionList[1]->sendCommandRequest(nLock==1?PLC_OPEN_UPPER_LOCK:PLC_OPEN_LOWER_LOCK);
    else
    {
      m_logger->log(QStringLiteral("Invalid lock number"));
      return;
    }
}

void SparkService::tryResumeTests()
{
  for ( auto& plcConnection : m_plcConnectionList )
  {
    if ( !plcConnection->isPaused() )
      continue;

    float averageTemp = m_plcConnection->averageMainTemperature();
    if (!m_plcConnection->mainTemperature().isValid || !m_plcConnection->thermalChamberTemperatureSet().isValid
        || (abs(averageTemp - m_plcConnection->thermalChamberTemperatureSet().value) > TEMPERATURE_TOLERANCE/2) ) {

      return;
    }

    m_plcConnection->resumeThermoprofileState();

    //! Увеличить все ключи-временные-метки в m_pxiSchedules на CHECK_END_PAUSE_PERIOD * X
    //! TODO: оттестировать
		qint64 timeLength=0;
		switch(plcConnection->plcSlot())
		{
			case THERMO_SLOT_NUMBER1:
			{
				timeLength = m_beginPauseTime0.msecsTo(QDateTime::currentDateTime());
				break;
			}
			case THERMO_SLOT_NUMBER2:
			{
				timeLength = m_beginPauseTime1.msecsTo(QDateTime::currentDateTime());
				break;
			}
		}
		swapScheduler(timeLength,plcConnection->plcSlot());

		plcConnection->setPause(false);

    // Снять c паузы в слотых
    //! Проверка состояния слота должна быть!!! Везде!!!
    // В слотах время финиша изменяется постоянно, но дискретно поэтому стоит привести все "к общему знаменателю"
    for (auto & pxiSlot : m_slots) {
      pxiSlot->extendOutputTime( timeLength );
      pxiSlot->resumeTest();
    }

    // Снять паузу со всех приостановленных текущих заданий
    for (auto & slot : m_sparkSchedules)
    {
      for (auto & sch : slot)
        sch->resumeSchedule();
    }

    m_logger->log("All tests resume");
  }
  m_tryResumeTimer->stop();
}

void SparkService::emergencyBreakHardwareModules(quint32 nSlot)
{
  //! TODO: Любое добавление нового оборудования в перечень PXI (того которое умеет выключаться), должно попадать и сюда

#ifndef PXI_EMULATOR_ENABLE
  if (nSlot>=m_hardware->sampleCount())
    return;
#endif

	Bugs bugss = m_hardware->sample(nSlot)->neutral();
  if (!bugss.isEmpty())
    logBugs(bugss);

	// Вернуть в исх. состояние все пины (на всякий случай)
  //for (quint32 i=0; i<HSDIO_PINS_NUMBER; ++i)
  //m_switchConnection->switchOff(nSlot*HSDIO_PINS_NUMBER + i + 1);
}

void SparkService::emergencyBreakThermoprofile(quint32 nSlot)
{
  m_sparkSchedules[nSlot].clear();
  m_sparkSchedules.remove(nSlot);

  m_plcConnection = nSlot==THERMO_SLOT_NUMBER1 ? m_plcConnectionList[0] : m_plcConnectionList[1];
  m_plcConnection->endThermoprofile();
}

void SparkService::disableTests()
{
  //! TODO: ЗДЕСЬ необходимо предусмотреть какое-то действие блокирующее выполнение работу всех тестов
  //! И это же действие нужно сделать в ::start()
}

void SparkService::interruptTest(quint32 nSlot, SparkSlot::SlotState state)
{
	emergencyBreakHardwareModules(nSlot);
  changeLedColor();
  if (state == SparkSlot::HardwareErrorSlotState)
  {
    m_logger->log("HardwareErrorSlotState");
//    m_plcConnectionList[0]->sendBuzzerRequest(Zummer::ZummerContinuos);
  }
  else
  if (state == SparkSlot::AlarmSlotState)
  {
    m_logger->log("AlarmSlotState");
    //m_plcConnectionList[0]->sendBuzzerRequest(Zummer::ZummerShortBlink);
  }
}

void SparkService::changeLedColor(LedColor color)
{
  std::function<void(LedColor)> changing = [this](LedColor newColor){
    if (m_currentLedColor != newColor) {
      m_currentLedColor = newColor;
      m_plcConnectionList[0]->sendRGBRequest(m_currentLedColor);
    }
  };
  if (color != LedColor::NoColor)
  {
      changing(color);
      m_logger->dbgLog(QString("Change led color to %1").arg(color));
      return;
  }


  if (m_plcConnectionList[0]->upsState() == SparkPlcConnection::UpsBatteryState)
  {
      changing(LedColor::BlinkRedLed);
      m_logger->dbgLog("Change led color to blink red");
      return;
  }

  if (m_slots.isEmpty()) {
    changing(LedColor::WhiteLed);
    m_logger->dbgLog("Change led color to white");
    return;
  }

  bool hardwareError{false};
  bool alarm{false};
  bool testCompleted{false};
  for (const auto & sparkSlot : qAsConst(m_slots)) {
    if (sparkSlot->state() == SparkSlot::HardwareErrorSlotState) {
      hardwareError = true;
      break;
    }
    if (sparkSlot->state() == SparkSlot::AlarmSlotState)
      alarm = true;
    if (sparkSlot->state() == SparkSlot::TestCompletedState)
      testCompleted = true;
  }
  if ((m_plcConnectionList[0]->thermoprofileState() == SparkPlcConnection::ThermoprofileProblemState) ||
          (m_plcConnectionList[1]->thermoprofileState() == SparkPlcConnection::ThermoprofileProblemState))
  {
      hardwareError = true;
  }

  if (hardwareError) {
    changing(LedColor::RedLed);
    m_logger->dbgLog("Change led color to red");
    return;
  }
  if (alarm) {
    changing(LedColor::YellowLed);
    m_logger->dbgLog("Change led color to yellow");
    return;
  }
  if (testCompleted) {
    changing(LedColor::BlueLed);
    m_logger->dbgLog("Change led color to blue");
    return;
  }

  changing(LedColor::GreenLed);
  m_logger->dbgLog("Change led color to green");
}

void SparkService::sendResponseState(QTcpSocket *socket)
{
  QJsonDocument jsonDoc;

  QJsonObject rootObject;
  QJsonArray chamberArray;

  QJsonObject fanAddSensorsObject;
  QJsonArray fanArray;
  QJsonArray peripheralArray;

  for (auto& tmpPlcConnection :m_plcConnectionList)
  {
    QJsonObject chamberObject;
    // --------- Блок температур от ПЛК ---------   (все атрибуты переносятся в одном пакете состояния, поэтому мтожно проверить isValid только для одного поля)
    chamberObject.insert("pauseOn", tmpPlcConnection->isPaused());
    bool bLinked = tmpPlcConnection->linkState();
    chamberObject.insert("plcLink", bLinked );
    //m_logger->dbgLog("Connection via "+QString::number(tmpPlcConnection->plcPort())+" is "+QString(bLinked?"true":"false"));
    chamberObject.insert("thermoprofileState", tmpPlcConnection->thermoprofileState());

    // эти параметры не получаются сервисом из пакета состояния, а существуют по умолчанию и вводятся пользователем
    chamberObject.insert("chamberParamTempHH", tmpPlcConnection->chamberParamTempHH());
    chamberObject.insert("chamberParamTempLL", tmpPlcConnection->chamberParamTempLL());
    chamberObject.insert("chamberParamAutostop", tmpPlcConnection->chamberParamCoolAutostop());
    chamberObject.insert("chamberParamTempAutostop", tmpPlcConnection->chamberParamTempAutostop());
    chamberObject.insert("chamberParamRackSP", tmpPlcConnection->chamberParamRackSP());
    chamberObject.insert("chamberParamRackHH", tmpPlcConnection->chamberParamRackHH());
    chamberObject.insert("chamberParamRackH", tmpPlcConnection->chamberParamRackH());
    chamberObject.insert("chamberParamRackL", tmpPlcConnection->chamberParamRackL());
    chamberObject.insert("chamberParamRackLL", tmpPlcConnection->chamberParamRackLL());
    chamberObject.insert("chamberParamRackOverHeat", tmpPlcConnection->chamberParamRackOverHeat());
    chamberObject.insert("upsTimerTimeout", tmpPlcConnection->chamberParamUpsTimeout());

    if (bLinked)
    {
      chamberObject.insert("systemInfoId", tmpPlcConnection->systemInfoId().value);
      chamberObject.insert("systemInfoVersion", tmpPlcConnection->systemInfoVersion().value);
      chamberObject.insert("systemInfoDeviceVersion", tmpPlcConnection->systemInfoDeviceVersion().value);
      chamberObject.insert("systemInfoHWVersion", tmpPlcConnection->systemInfoHWVersion().value);
      chamberObject.insert("systemInfoSWVersion", tmpPlcConnection->systemInfoSWVersion().value);
      chamberObject.insert("systemInfoSerialNumber", tmpPlcConnection->systemInfoSerialNumber().value);
      chamberObject.insert("thermalChamberId", tmpPlcConnection->thermalChamberId().value);
      chamberObject.insert("thermalChamberErrorCode", tmpPlcConnection->thermalChamberErrorCode().value);
      chamberObject.insert("mainTemperature", tmpPlcConnection->mainTemperature().value); // фактически в UI отправляется не устредненное значение, возможно стоит поменять на ->averageMainTemperature()
      chamberObject.insert("thermalChamberSlidedTemperatureSet", tmpPlcConnection->thermalChamberSlidedTemperatureSet().value);
      chamberObject.insert("thermalChamberMode", tmpPlcConnection->thermalChamberMode().value);
      chamberObject.insert("thermalChamberTemperatureSet", tmpPlcConnection->thermalChamberTemperatureSet().value);
      chamberObject.insert("thermalChamberTemperatureSpeed", tmpPlcConnection->thermalChamberTemperatureSpeed().value);

      chamberObject.insert("accessoriesChamberId", tmpPlcConnection->accessoriesChamberId().value);
      chamberObject.insert("optionsChamberId", tmpPlcConnection->optionsChamberId().value);
      chamberObject.insert("alarmChamberId", tmpPlcConnection->alarmChamberId().value);

      QJsonArray alarmArray;
      for (const auto &a : qAsConst(tmpPlcConnection->alarmDataList()))
      { // qAsConst not needed but for it's for remaind
        QJsonObject alarmObject;
        alarmObject.insert("moduleId", a.moduleId.value);
        alarmObject.insert("instanceNumber", a.instanceNumber.value);
        alarmObject.insert("errorCode", a.errorCode.value);
        alarmObject.insert("parameter", a.parameter.value);
        alarmArray.append(alarmObject);
      }
      chamberObject.insert("modulesAlarms", alarmArray);

      chamberObject.insert("extDI", tmpPlcConnection->extDI().value);
      chamberObject.insert("extDO", tmpPlcConnection->extDO().value);

      /*for (const auto &t : qAsConst(tmpPlcConnection->peripheralTemperatures())) { // qAsConst not needed but for it's for remaind
                 QJsonObject peripheralObject;

                 peripheralObject.insert("number", peripheralArray.size());
                 peripheralObject.insert("temperature", t.first.value);
                 peripheralObject.insert("sensorOk", t.second.value);
                 peripheralArray.append(peripheralObject);
             }
             chamberObject.insert("peripheralTemperature", peripheralArray);*/

      chamberObject.insert("calculatedRackTemerature", tmpPlcConnection->calculatedRackTemerature().value);
      //chamberObject.insert("fanFrequency", tmpPlcConnection->fanFrequency().value);
      //chamberObject.insert("fanErrorCode", tmpPlcConnection->fanErrorCode().value);

//      chamberObject.insert("currentInputUPS", tmpPlcConnection->currentInputUPS().value);
//      chamberObject.insert("currentOutputUPS", tmpPlcConnection->currentOutputUPS().value);
//      chamberObject.insert("ratingPowerUPS", tmpPlcConnection->ratingPowerUPS().value);
//      chamberObject.insert("outputVoltageUPS", tmpPlcConnection->outputVoltageUPS().value);
//      chamberObject.insert("batteryVoltageUPS", tmpPlcConnection->batteryVoltageUPS().value);
//      chamberObject.insert("loadPercentUPS", tmpPlcConnection->loadPercentUPS().value);
//      chamberObject.insert("inputVoltageFrequencyUPS", tmpPlcConnection->inputVoltageFrequencyUPS().value);
//      chamberObject.insert("outputVoltageFrequencyUPS", tmpPlcConnection->outputVoltageFrequencyUPS().value);
//      chamberObject.insert("workingTimeUPS", tmpPlcConnection->workingTimeUPS().value);
//      chamberObject.insert("chargeUPS", tmpPlcConnection->chargeUPS().value);
//      chamberObject.insert("inputVoltageUPS", tmpPlcConnection->inputVoltageUPS().value);

      chamberObject.insert("bitsStatusUPS", m_plcConnectionList[0]->bitsStatusUPS().value);

      chamberObject.insert("upsLink", tmpPlcConnection->upsLink().value);
      chamberObject.insert("cameraLink", tmpPlcConnection->cameraLink().value);

      switch (tmpPlcConnection->plcSlot())
      {
        case THERMO_SLOT_NUMBER1:
        {
          QJsonObject fanObject;

          fanObject.insert("number",0);
          fanObject.insert("enable",tmpPlcConnection->fanData()[0].second.value==2);
          fanObject.insert("percent",tmpPlcConnection->fanData()[0].first.value);
          fanArray.append(fanObject);
          fanObject.insert("number",1);
          fanObject.insert("enable",tmpPlcConnection->fanData()[1].second.value==2);
          fanObject.insert("percent",tmpPlcConnection->fanData()[1].first.value);
          fanArray.append(fanObject);
          fanObject.insert("number",2);
          fanObject.insert("enable",tmpPlcConnection->fanData()[2].second.value==2);
          fanObject.insert("percent",tmpPlcConnection->fanData()[2].first.value);
          fanArray.append(fanObject);

          QJsonObject peripheralObject;

          peripheralObject.insert("number", 0);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[0].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[0].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 1);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[1].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[1].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 2);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[2].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[2].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 3);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[3].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[3].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 4);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[4].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[4].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 5);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[5].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[5].second.value);
          peripheralArray.append(peripheralObject);
          break;
        }
        case THERMO_SLOT_NUMBER2:
        {
          QJsonObject fanObject;

          fanObject.insert("number",3);
          fanObject.insert("enable",tmpPlcConnection->fanData()[0].second.value==2);
          fanObject.insert("percent",tmpPlcConnection->fanData()[0].first.value);
          fanArray.append(fanObject);

          fanObject.insert("number",4);
          fanObject.insert("enable",tmpPlcConnection->fanData()[1].second.value==2);
          fanObject.insert("percent",tmpPlcConnection->fanData()[1].first.value);
          fanArray.append(fanObject);

          QJsonObject peripheralObject;

          peripheralObject.insert("number", 6);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[6].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[6].second.value);
          peripheralArray.append(peripheralObject);
          peripheralObject.insert("number", 7);
          peripheralObject.insert("temperature", tmpPlcConnection->peripheralTemperatures()[7].first.value);
          peripheralObject.insert("sensorOk", tmpPlcConnection->peripheralTemperatures()[7].second.value);
          peripheralArray.append(peripheralObject);


          break;
        }
        default:;
      }
    }
    // Добавляем набранный объект chamber в массив
    chamberArray.append(chamberObject);
  }

  fanAddSensorsObject.insert("fans",fanArray);
  fanAddSensorsObject.insert("peripheralTemperature",peripheralArray);
  rootObject.insert("fansAndSensors", fanAddSensorsObject);

  // Добавляем массив chamber-ов в root объект
  rootObject.insert("chambers", chamberArray);

  // --------- Блок сосотяний слотов ---------
  QJsonArray slotsArray;
  for (const auto & sparkSlot : qAsConst(m_slots))
    slotsArray.append(sparkSlot->stateToJson());

  rootObject.insert("Slots", slotsArray);
  jsonDoc.setObject(rootObject);

  QByteArray payload = jsonDoc.toJson();
  quint32 len = payload.size();
  QByteArray response;
  response.append(STATE_RESPONSE);
  response.append(reinterpret_cast<char *>(&len), LENGTH_OF_PAYLOAD);
  response.append(payload);

  socket->write(response);
}

void SparkService::sendReport(QTcpSocket *socket)
{
    Q_UNUSED(socket);
  //! TODO: делать
}

void SparkService::sendErrorsMessage(QTcpSocket *socket, Bugs bugs, quint32 nSlot)
{
    qint32 readyStartFlag = 0x00; // 1 - слот готов к запуску теста; 0 - не готов
    QByteArray payload; // пока пусктой список неработающих устройств

    logBugs(bugs);

    QString errMsg = "Обнаружены ошибки!";

    for (Bug bug : bugs)
    {
        QString deviceName;
        switch (bug.type) {
        case Bug::Power:
            deviceName = bug.device;
            bug.message += " слот:" + QString::number(bug.slot);
            break;
        case Bug::FGen:
            deviceName = bug.device;
            bug.message += " слот:" + QString::number(bug.slot);
            break;
        case Bug::Thermocouple:
            deviceName = "Термопары:";
            bug.message += " слот:" + QString::number(bug.slot);
            break;
        case Bug::Cyclogram:
            deviceName = bug.device;
            bug.message += " слот:" + QString::number(bug.slot);
            break;
        case Bug::Thermocyclogram:
            deviceName = nSlot == 20? "Левая ": "Правая " + bug.device;
            break;
        default:
            deviceName = "Undefined:";
            break;
        }
        errMsg += "\n" + deviceName + " ошибка:" + bug.message;
    }
    payload = QByteArray(errMsg.toUtf8());
    //qint32 readyStartFlag = 0x00; // 1 - слот готов к запуску теста; 0 - не готов
    //QByteArray payload("Термопара, Источник питания №10, Генератор"); // пока пусктой список неработающих устройств
    quint32 len = payload.size() + NSLOT_LENGTH + READYSTART_FLAGLENGTH;
    QByteArray response;
    response.append(PREPARERUN_RESPONSE);
    response.append(reinterpret_cast<char *>(&len), LENGTH_OF_PAYLOAD);
    response.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH); // payload
    response.append(reinterpret_cast<const char *>(&readyStartFlag), READYSTART_FLAGLENGTH); // payload
    response.append(payload);

    socket->write(response);
}

void SparkService::sendPrepareRunResponse(QTcpSocket *socket, qint32 nSlot)
{
  qint32 readyStartFlag = 0x01; // 1 - слот готов к запуску теста; 0 - не готов
  QByteArray payload; // пока пусктой список неработающих устройств

#ifndef PXI_EMULATOR_ENABLE
  if ( (nSlot>=m_hardware->sampleCount())) // не все аппараты подключены
  {
    QString errMsg = "Slot " + QString::number(nSlot+1) + " не все оборудование подключено";
    m_logger->log(errMsg);
    readyStartFlag = 0x00;
    payload = QByteArray(errMsg.toUtf8());
  }
#endif
  //qint32 readyStartFlag = 0x00; // 1 - слот готов к запуску теста; 0 - не готов
  //QByteArray payload("Термопара, Источник питания №10, Генератор"); // пока пусктой список неработающих устройств
  quint32 len = payload.size() + NSLOT_LENGTH + READYSTART_FLAGLENGTH;
  QByteArray response;
  response.append(PREPARERUN_RESPONSE);
  response.append(reinterpret_cast<char *>(&len), LENGTH_OF_PAYLOAD);
  response.append(reinterpret_cast<const char *>(&nSlot), NSLOT_LENGTH); // payload
  response.append(reinterpret_cast<const char *>(&readyStartFlag), READYSTART_FLAGLENGTH); // payload
  response.append(payload);

  socket->write(response);
}

void SparkService::logMessageString(const QString & head)
{
  m_logger->log(head);
}

void SparkService::logBugs(const Bugs &bugs)
{
  for (auto bug : bugs) {
        m_logger->log(bug.message + " type=" + QString(bug.type==0?"PS":"Gen") + " slot = " + QString::number(bug.slot));
  }
}

void SparkService::schedulerHandler()
{
    bool conn0Paused = false;
    bool conn1Paused = false;
    if (m_plcConnectionList[0]->isPaused())
    {
        conn0Paused = true;
        // Во время паузы время завершения слота должно ползти вперед (а progressBar оставаться на месте)
        for (auto & sparkSlot : m_slots)
            //! TODO: увеличивать время окончания текущей задачи циклограммы
        {
            if ( sparkSlot->number()<10 )
                sparkSlot->extendOutputTime( m_beginPauseTime0.msecsTo(QDateTime::currentDateTime()) );
        }
    }
    if (m_plcConnectionList[1]->isPaused())
    {
        conn1Paused = true;
        // Во время паузы время завершения слота должно ползти вперед (а progressBar оставаться на месте)
        for (auto & sparkSlot : m_slots)
            //! TODO: увеличивать время окончания текущей задачи циклограммы
        {
            if ( sparkSlot->number()>=10 )
                sparkSlot->extendOutputTime( m_beginPauseTime1.msecsTo(QDateTime::currentDateTime()) );

        }
    }

    // если в обоих камерах пауза - делать дальше нечего
    if ( conn0Paused && conn1Paused )
        return;

    for (auto slot = m_sparkSchedules.begin(); slot != m_sparkSchedules.end(); ++slot)
    {
        if ( (slot.key() != THERMO_SLOT_NUMBER1) && (slot.key() != THERMO_SLOT_NUMBER2) )
        {
            if ( (conn0Paused && slot.key()<10) ||
                 (conn1Paused && slot.key()>=10)
                 )
                continue;

            if ( (int)slot.key()>=m_hardware->sampleCount() )
            {
                m_logger->log("Слот " + QString::number(slot.key()+1) + " не проинициализирован");
                continue;
            }
        }

        QMultiMap<QDateTime, QSharedPointer<SparkScheduleBase> > & slotSchedule = slot.value();

        // Условие завершения теста
        if (!slotSchedule.size())
        {
            if (m_slots.contains(slot.key()))
            {
                if (m_slots.value(slot.key())->state() != SparkSlot::HardwareErrorSlotState && m_slots.value(slot.key())->state() != SparkSlot::AlarmSlotState && m_slots.value(slot.key())->state() != SparkSlot::TestCompletedState)
                {
//                    emergencyBreakHardwareModules(slot.key()); // добавлен на тот случай, если циклограмма не будет  содержать концевых блоков
                    m_slots.value(slot.key())->completeTest();
                    changeLedColor();
                }
            }
            else
                if (slot.key() == THERMO_SLOT_NUMBER1)
                {
                    emergencyBreakThermoprofile(THERMO_SLOT_NUMBER1);
                    m_logger->log(QString("Service: Thermoprofile successfully completed in left camera"));
                    //m_pxiSchedules.remove(THERMO_SLOT_NUMBER);
                }
                else
                    if (slot.key() == THERMO_SLOT_NUMBER2)
                    {
                        emergencyBreakThermoprofile(THERMO_SLOT_NUMBER2);
                        m_logger->log(QString("Service: Thermoprofile successfully completed in right camera"));
                        //m_pxiSchedules.remove(THERMO_SLOT_NUMBER);
                    }
            continue;
        }

        auto i = slotSchedule.begin();
        while (i != slotSchedule.end())
        {
            // Специально для темпоциклограммы
            if ((slot.key() == THERMO_SLOT_NUMBER1) &&
                    ((i.value()->stage() == SparkScheduleBase::ScheduleError) ||
                     (m_plcConnectionList[0]->thermoprofileState() == SparkPlcConnection::ThermoprofileProblemState)))
            {                  
                for (auto & sparkSlot : m_slots)
                {
                    if (sparkSlot->number()<10)
                    {
                        if (m_plcConnectionList[0]->centralRackOverheating())
                        {
//                            sparkSlot->chamberErrorTest("Перегрев центральной стойки");
                            m_plcConnectionList[1]->setCentralRackOverheating(m_plcConnectionList[0]->centralRackOverheating());
                        }

//                        else
//                        {
//                            sparkSlot->chamberErrorTest("Перегрев левой стойки");
//                        }
                        sparkSlot->chamberErrorTest(m_plcConnectionList[0]->getErrorMessage());
                    }
                }
                emergencyBreakThermoprofile(THERMO_SLOT_NUMBER1);
                i = slotSchedule.end();
                changeLedColor();
                logMessageString(QString("Проблемы с левой камерой, удалены все задания слотов"));
                continue;
            }
            if ((slot.key() == THERMO_SLOT_NUMBER2) &&
                    ((i.value()->stage() == SparkScheduleBase::ScheduleError) ||
                     (m_plcConnectionList[1]->thermoprofileState() == SparkPlcConnection::ThermoprofileProblemState) || m_plcConnectionList[1]->centralRackOverheating()
                      || (m_plcConnectionList[0]->upsState() == SparkPlcConnection::UpsAlarmState)))
            {
                for (auto & sparkSlot : m_slots)
                {
                    if (sparkSlot->number()>=10)
                    {
                        if (m_plcConnectionList[1]->centralRackOverheating() || (m_plcConnectionList[0]->upsState() == SparkPlcConnection::UpsAlarmState))
                            sparkSlot->chamberErrorTest(m_plcConnectionList[0]->getErrorMessage());
                        else
                            sparkSlot->chamberErrorTest(m_plcConnectionList[1]->getErrorMessage());
//                        if (m_plcConnectionList[1]->centralRackOverheating())
//                            sparkSlot->chamberErrorTest("Перегрев центральной стойки");
//                        else
//                            sparkSlot->chamberErrorTest("Перегрев правой стойки");
                    }
                }
                emergencyBreakThermoprofile(THERMO_SLOT_NUMBER2);
                i = slotSchedule.end();
                changeLedColor();
                logMessageString(QString("Проблемы с правой камерой, удалены все задания слотов"));
                continue;
            }
            // По моим расчетам следующий код выполнится в случае чего только 1 раз
            if ((slot.key() != THERMO_SLOT_NUMBER1) && (slot.key() != THERMO_SLOT_NUMBER2)
                    && m_slots.contains(slot.key()) &&
                    ((i.value()->stage() == SparkScheduleBase::ScheduleError) ||
                     (m_slots.value(slot.key())->state() == SparkSlot::AlarmSlotState) ||
                     (m_slots.value(slot.key())->state() == SparkSlot::HardwareErrorSlotState))) {
                // стереть список заданий, но саму запись оставить, она удалиться в обработчике команды break.
                //а алг. еще пройдет через "Условие завершения теста", но это в данном случае не важно
                m_sparkSchedules[slot.key()].clear();
                i = slotSchedule.end();
                interruptTest(slot.key(), m_slots.value(slot.key())->state());
                logMessageString(QString("Service: Erasing all tasks for slot %1 by reason %2").arg(slot.key()+1).arg(m_slots.value(slot.key())->state()));
                i = slotSchedule.erase(i);
                continue;
            }

            if (i.value()->stage() == SparkScheduleBase::ScheduleCompleted)
            {
                i = slotSchedule.erase(i);
                continue;
            }

            if (i.value()->stage() == SparkScheduleBase::ScheduleRun)
            {
                ++i;
                continue;
            }

            if ((i.key() > QDateTime::currentDateTime()))
            {
                break;
            }

            //! А не нужно ли в этой проверке проверять еще и на i.value()->stage() == PXIScheduleBase::ScheduleWaitingStart ???
            if (i.key().secsTo(QDateTime::currentDateTime()) > 3)
            {
                logMessageString(QString("Warning: Detected outdated schedule's task in slot %1").arg(slot.key()+1) );
                logMessageString(QString("Warning: SecsTo = %1").arg( i.key().secsTo(QDateTime::currentDateTime()) ));
                logMessageString(QString("Warning: startTime = %1").arg( i.key().toString(DB_DATETIME_FORMAT) ));
                logMessageString(QString("Warning: stage = %1").arg( i.value()->stage() ));
                i = slotSchedule.erase(i);
                continue;
            }

            // Выполнение задания
            if (i.value()->stage() == SparkScheduleBase::ScheduleWaitingStart)
                i.value()->execSchedule();

            ++i;
        }
    }
}

void SparkService::swapScheduler(qint64 timeLength, qint32 nCameraSlot)
{
    Q_UNUSED(nCameraSlot);
  m_logger->log("---Old schedule---");
  for (auto slot = m_sparkSchedules.constBegin(); slot != m_sparkSchedules.constEnd(); ++slot) {
    for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
      m_logger->log(QStringLiteral("startTime = ") + sch.key().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(sch.value()->stage()));
    }
  }
  m_logger->log("-----------------");

  QHash<quint32, QMultiMap<QDateTime, QSharedPointer<SparkScheduleBase> > > newSparkSchedules;

  m_logger->log("offset length time = " + QString::number(timeLength));
  for (auto slot = m_sparkSchedules.constBegin(); slot != m_sparkSchedules.constEnd(); ++slot) {

    QMultiMap<QDateTime, QSharedPointer<SparkScheduleBase> > newSlotSchedules;

    for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
      // Для тех, кто еще не нечался, необходимо перенести время старта и финиша "вперед"
      switch(sch.value()->stage()) {

        case SparkScheduleBase::ScheduleWaitingStart: {
          QDateTime newTime1 = sch.key().addMSecs(timeLength);
          QDateTime newTime2 = sch.value()->stopTime().addMSecs(timeLength);
          sch.value()->setStartStopTime(newTime1, newTime2);
          newSlotSchedules.insert(newTime1, sch.value());
          m_logger->log(QStringLiteral("Service: SwapSchedulre: Waiting schedule. Inserted startTime = ") + newSlotSchedules.last()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key " + newSlotSchedules.lastKey().toString(DB_DATETIME_FORMAT)  + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
        } break;

        case SparkScheduleBase::SchedulePause: {
          QDateTime newTime1 = sch.key();
          QDateTime newTime2 = sch.value()->stopTime().addMSecs(timeLength);
          sch.value()->setStartStopTime(newTime1, newTime2);
          newSlotSchedules.insert( newTime1, sch.value() );
          m_logger->log(QStringLiteral("Service: SwapSchedulre: Paused schedule. Inserted startTime = ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
        } break;

        case SparkScheduleBase::ScheduleRun:
          newSlotSchedules.insert(sch.key(), sch.value());
          m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: Running schedule appears. It's impossible"));
          break;
        case SparkScheduleBase::ScheduleError:
          newSlotSchedules.insert(sch.key(), sch.value());
          m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: ErrorSchedule appears. It's possible, but strange...."));
          break;
        case SparkScheduleBase::ScheduleCompleted: {
          newSlotSchedules.insert(sch.key(), sch.value());
          m_logger->log(QStringLiteral("Service: SwapSchedulre: Completed schedule appears. ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
        } break;

        default:
          newSlotSchedules.insert(sch.key(), sch.value());
          m_logger->log(QStringLiteral("Service: Warning: SwapSchedulre: DefaultSchedule. It means, that we have the new undefined ScheduleState") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(newSlotSchedules.last()->stage()) + " key = " + sch.key().toString(DB_DATETIME_FORMAT) + " stopTime = " + sch.value()->stopTime().toString(DB_DATETIME_FORMAT));
      }
    }

    newSparkSchedules.insert(slot.key(), newSlotSchedules);
  }
  m_sparkSchedules.swap(newSparkSchedules);

  m_logger->log("---New schedule---");
  for (auto slot = m_sparkSchedules.constBegin(); slot != m_sparkSchedules.constEnd(); ++slot) {
    for (auto sch = slot.value().constBegin(); sch != slot.value().constEnd(); ++sch) {
      m_logger->log(QStringLiteral("startTime = ") + sch.value()->startTime().toString(DB_DATETIME_FORMAT) + " stage = " + QString::number(sch.value()->stage()) + "key = " + sch.key().toString(DB_DATETIME_FORMAT));
    }
  }
  m_logger->log("-----------------");
}


void SparkService::saveChamberTempSettings(quint32 nSlot, float overHeatTemp, float overFreezeTemp, quint16 refrigeratorAutoStop, float refrigeratorStopTemp,
                                           float rackTemp, float rackTempHH, float  rackTempH, float rackTempL, float rackTempLL, float rackOverHeat, int upsTimerTimeout)
{
    if (nSlot == 1)
        m_settings->beginGroup(INI_CHAMBER1_GROUP);
    else if (nSlot == 2)
        m_settings->beginGroup(INI_CHAMBER2_GROUP);
    m_settings->setValue(INI_CHAMBER_OVERHEAT_TEMP, (double)overHeatTemp);
    m_settings->setValue(INI_CHAMBER_OVERFREEZE_TEMP, (double)overFreezeTemp);
    m_settings->setValue(INI_CHAMBER_REFRIGERATOR_AUTOSTOP, (double)refrigeratorAutoStop);
    m_settings->setValue(INI_CHAMBER_REFRIGERATOR_STOP_TEMP, (double)refrigeratorStopTemp);
    m_settings->endGroup();
    if (nSlot == 1)
        m_settings->beginGroup(INI_RACK1_GROUP);
    else if (nSlot == 2)
        m_settings->beginGroup(INI_RACK2_GROUP);
    m_settings->setValue(INI_RACK_TEMP, (double)rackTemp);
    m_settings->setValue(INI_RACK_TEMP_HH, (double)rackTempHH);
    m_settings->setValue(INI_RACK_TEMP_H, (double)rackTempH);
    m_settings->setValue(INI_RACK_TEMP_L, (double)rackTempL);
    m_settings->setValue(INI_RACK_TEMP_LL, (double)rackTempLL);
    m_settings->setValue(INI_RACK_OVERHEAT_TEMP, (double)rackOverHeat);
    m_settings->endGroup();
    if (nSlot == 1)
    {
        m_settings->beginGroup(INI_UPS_GROUP);
        m_settings->setValue(INI_UPS_BATTERY_STATE_TIMEOUT, upsTimerTimeout);
        m_settings->endGroup();
    }
    m_settings->syncSettings();
}

QList<float> SparkService::loadChamberTempSettings(quint32 nSlot)
{
    QList<float> loadedValues;
    if (nSlot == 1)
        m_settings->beginGroup(INI_CHAMBER1_GROUP);
    else
        m_settings->beginGroup(INI_CHAMBER2_GROUP);
    loadedValues.append(m_settings->value(INI_CHAMBER_OVERHEAT_TEMP).toFloat());
    loadedValues.append(m_settings->value(INI_CHAMBER_OVERFREEZE_TEMP).toFloat());
    if (m_settings->childKeys().contains(INI_CHAMBER_REFRIGERATOR_AUTOSTOP))
    {
        loadedValues.append(m_settings->value(INI_CHAMBER_REFRIGERATOR_AUTOSTOP).toFloat());
    }
    else
        loadedValues.append(-1);
    loadedValues.append(m_settings->value(INI_CHAMBER_REFRIGERATOR_STOP_TEMP).toFloat());
    m_settings->endGroup();
    if (nSlot == 1)
        m_settings->beginGroup(INI_RACK1_GROUP);
    else
        m_settings->beginGroup(INI_RACK2_GROUP);
    loadedValues.append(m_settings->value(INI_RACK_TEMP).toFloat());
    loadedValues.append(m_settings->value(INI_RACK_TEMP_HH).toFloat());
    loadedValues.append(m_settings->value(INI_RACK_TEMP_H).toFloat());
    loadedValues.append(m_settings->value(INI_RACK_TEMP_L).toFloat());
    loadedValues.append(m_settings->value(INI_RACK_TEMP_LL).toFloat());
    loadedValues.append(m_settings->value(INI_RACK_OVERHEAT_TEMP).toFloat());
    m_settings->endGroup();
    m_settings->beginGroup(INI_UPS_GROUP);
    loadedValues.append(m_settings->value(INI_UPS_BATTERY_STATE_TIMEOUT).toFloat());
    m_settings->endGroup();
    return loadedValues;
}
