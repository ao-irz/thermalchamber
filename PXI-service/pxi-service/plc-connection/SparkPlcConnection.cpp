#include "config.h"
#include "SparkPlcConnection.h"
#include <QVector>
#include "Logger.h"
#include <QRandomGenerator>

/// Иметь ввиду, поля в пакетах ДВУХбайтные и ЧЕТЫРЕХбайтные (и Бог его знает какие еще могут потом добавиться)
/// Но подготовленные пакеты будут лежать в классе как массивы байт.
/// Порядок байт в полях выбран согласно intel-формату. (хотя для float это утверждение почти теряет смысл)
/// В общем как в памяти переменная лежит, так он в массив и будет класться, без переворачивания порядка следования байт

SparkPlcConnection::SparkPlcConnection(const QPointer<Logger> &logger,
                                       std::function<void(quint32)> pauseFunction,
                                       quint32 nCameraSlot,
                                       quint16 nPort,
                                       QList<float> chamberTempSettings,
                                       QString address,
                                       QObject *parent) :
    QObject(parent),
    m_pauseFunction(pauseFunction),
    m_logger(logger),
    m_hostAddress(address=="" ? PLC_DESTINATION_ADDRESS : address),
    m_stateRequest(PLC_STATE_REQUEST_LENGTH, 0x00),
    m_controlRequest(PLC_CONTROL_REQUEST_LENGTH, 0x00),
    m_parametersRequest(PLC_PARAMETERS_REQUEST_LENGTH, 0x00),
    m_udpSocket(new QUdpSocket(this)),
    m_checkTimer(new QTimer(this)),
    m_watchDogTimer(new QTimer(this)),
    m_nPort(nPort),
    m_nSlot(nCameraSlot),
    m_upsBatteryTimer(new QTimer(this))
{
    const quint16 manualMode = PLC_STANDBY_MODE;
    memcpy(&m_controlRequest.data()[0], (const void *)(&manualMode), sizeof(qint16));

    m_tempHH = chamberTempSettings[0] == 0?m_tempHH:chamberTempSettings[0];
    m_tempLL = chamberTempSettings[1] == 0?m_tempLL:chamberTempSettings[1];
    m_coolAutostopEnable = chamberTempSettings[2] == -1?m_coolAutostopEnable:(quint16)chamberTempSettings[2];
    m_tempCoolAutostop = chamberTempSettings[3] == 0?m_tempCoolAutostop:chamberTempSettings[3];
    m_rackSP = chamberTempSettings[4] == 0?m_rackSP:chamberTempSettings[4];
    m_rackHH = chamberTempSettings[5] == 0?m_rackHH:chamberTempSettings[5];
    m_rackH = chamberTempSettings[6] == 0?m_rackH:chamberTempSettings[6];
    m_rackL = chamberTempSettings[7] == 0?m_rackL:chamberTempSettings[7];
    m_rackLL = chamberTempSettings[8] == 0?m_rackLL:chamberTempSettings[8];
    m_rackOverHeat = chamberTempSettings[9] <= 0?m_rackOverHeat:chamberTempSettings[9];
    m_upsBatteryTimeout = chamberTempSettings[10] == 0?m_upsBatteryTimeout:int(chamberTempSettings[10]);

    //qint16 cmd = PLC_STATE_REQUEST_CMD;
    //memcpy(&m_stateRequest.data()[0], (const void *)(&cmd), sizeof(qint16));

    //!TODO: Предварительная конфигурация пакета параметров. Пока константы не выданы заказчиком. Это лишь неподтвержденная догадка, что нижеследующие парамерты будут const
    memcpy(&m_parametersRequest.data()[0], (const void *)(&m_tempHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[4], (const void *)(&m_tempLL), sizeof(float));
    memcpy(&m_parametersRequest.data()[8], (const void *)(&m_coolAutostopEnable), sizeof(quint16));
    memcpy(&m_parametersRequest.data()[10], (const void *)(&m_tempCoolAutostop), sizeof(float));
    memcpy(&m_parametersRequest.data()[74], (const void *)(&m_rackSP), sizeof(float));
    memcpy(&m_parametersRequest.data()[78], (const void *)(&m_rackHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[82], (const void *)(&m_rackH), sizeof(float));
    memcpy(&m_parametersRequest.data()[86], (const void *)(&m_rackL), sizeof(float));
    memcpy(&m_parametersRequest.data()[90], (const void *)(&m_rackLL), sizeof(float));

    connect(m_checkTimer.data(), &QTimer::timeout, [this](){
        sendStateRequest();
        /*static bool selector = false;
        if (selector)
        {
            sendStateRequest();
            selector = false;
        }
        else
        {
            sendControlPackage();
            selector = true;
        }*/
    });

    connect(m_watchDogTimer, &QTimer::timeout, [this](){
        m_linkState = false;
        m_logger->log("Error: no link to PLC, port "+QString::number(m_nPort));
    });

    changeCheckInterval(PLC_POLL_INTERVAL);

    //m_logger->log("try to bind port "+QString::number(m_nPort));
    m_udpSocket->bind(QHostAddress::Any, m_nPort);

    connect(m_udpSocket, &QUdpSocket::readyRead, this, &SparkPlcConnection::socketHandler);
}

bool SparkPlcConnection::sendStateRequest()
{
    qint16 cmd = PLC_STATE_REQUEST_CMD;
    memcpy(&m_stateRequest.data()[0], (const void *)(&cmd), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_stateRequest, m_hostAddress, m_nPort)) != PLC_STATE_REQUEST_LENGTH)
    {
        m_logger->log(QStringLiteral("Error: can't send UDP state request"));
        return false;
    }
    //else
      //m_logger->log(QStringLiteral("State request"));

    return true;
}

//  по соглашению с Юрой отправляется тот же запрос, как в sendStateRequest,
// только 1-е 2 байта - конкретный код команды (см. Протокол ПК-ПЛК версия Искра.xls)
bool SparkPlcConnection::sendCommandRequest(qint16 cmd)
{
    memcpy(&m_stateRequest.data()[0], (const void *)(&cmd), sizeof(qint16));

    m_checkTimer->stop();

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_stateRequest, m_hostAddress, m_nPort)) != PLC_STATE_REQUEST_LENGTH)
    {
        m_logger->log(QStringLiteral("Error: can't send UDP command request"));
        return false;
    }
    //else
      //m_logger->log(QStringLiteral("Command request sent ")+QString::number(cmd));

    m_checkTimer->start();

    return true;
}

bool SparkPlcConnection::sendParametersRequest()
{
    // все значения по умолчанию и константы уже внесены в массив пакета в конструкторе
    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_parametersRequest, m_hostAddress, m_nPort)) != PLC_PARAMETERS_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP parameter request"));
        return false;
    }

    return true;
}

bool SparkPlcConnection::sendParametersRequest(const float tempHH, const float tempLL, const quint16 autostop, const float tempAutostop,
                                             const float rackSP, const float rackHH, const float rackH, const float rackL, const float rackLL, const float rackOverHeat, const int upsTimerTimeout)
{
    m_tempHH = tempHH;
    m_tempLL = tempLL;
    m_coolAutostopEnable = autostop;
    m_tempCoolAutostop = tempAutostop;

    m_rackSP = rackSP;
    m_rackHH = rackHH;
    m_rackH = rackH;
    m_rackL = rackL;
    m_rackLL = rackLL;
    m_rackOverHeat = rackOverHeat;
    m_upsBatteryTimeout = upsTimerTimeout;

    memcpy(&m_parametersRequest.data()[0], (const void *)(&m_tempHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[4], (const void *)(&m_tempLL), sizeof(float));
    memcpy(&m_parametersRequest.data()[8], (const void *)(&m_coolAutostopEnable), sizeof(quint16));
    memcpy(&m_parametersRequest.data()[10], (const void *)(&m_tempCoolAutostop), sizeof(float));

    memcpy(&m_parametersRequest.data()[74], (const void *)(&m_rackSP), sizeof(float));
    memcpy(&m_parametersRequest.data()[78], (const void *)(&m_rackHH), sizeof(float));
    memcpy(&m_parametersRequest.data()[82], (const void *)(&m_rackH), sizeof(float));
    memcpy(&m_parametersRequest.data()[86], (const void *)(&m_rackL), sizeof(float));
    memcpy(&m_parametersRequest.data()[90], (const void *)(&m_rackLL), sizeof(float));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_parametersRequest, m_hostAddress, m_nPort)) != PLC_PARAMETERS_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP parameter request"));
        return false;
    }

    return true;
}

bool SparkPlcConnection::sendControlRequest(float temperature, float maxTemperatureChangeRate = 0.0, int actionType = 2 )
{
    const quint16 manualMode = PLC_CHAMBER_MANUAL_MODE;

    // если камера не охлаждается, то меняем значение перегрева
    if (actionType != 2)
    {
        // устанавливаем температуру перегрева на 5 больше, чем вциклограмме
        m_tempHH = temperature + 5;
        // сохранение в файл
        emit saveChamberTempSettings(m_nSlot==THERMO_SLOT_NUMBER1? 1:2, m_tempHH, m_tempLL, m_coolAutostopEnable, m_tempCoolAutostop, m_rackSP, m_rackHH,
                                     m_rackH, m_rackL, m_rackLL, m_rackOverHeat, m_upsBatteryTimeout);
        // обновляем значение в пакете
        memcpy(&m_parametersRequest.data()[0], (const void *)(&m_tempHH), sizeof(float));
        // отправляем параметры в плк
        sendParametersRequest();
    }
    //! TODO: Входные параметры нигде не сохраняются (только в массиве самого пакета). И на данном этапе я не знаю надо ли вообще их сохранять.
    memcpy(&m_controlRequest.data()[0], (const void *)(&manualMode), sizeof(qint16));
    memcpy(&m_controlRequest.data()[14], (const void *)(&temperature), sizeof(float));
    memcpy(&m_controlRequest.data()[18], (const void *)(&maxTemperatureChangeRate), sizeof(float));



    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP control request"));
        return false;
    }

		 return true;
}

bool SparkPlcConnection::sendStandByRequest(bool standByEnable)
{
    quint16 standByMode;
    (standByEnable) ? (standByMode = PLC_STANDBY_MODE) : (standByMode = PLC_CHAMBER_MANUAL_MODE);

    memcpy(&m_controlRequest.data()[0], (const void *)(&standByMode), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP standby request"));
        return false;
    }
    //m_udpSocket->waitForBytesWritten(); //не уверен что это нужно и чем-то поможет

    return true;
}

bool SparkPlcConnection::sendRGBRequest(LedColor color) // передалать, навреное, на общий тип надо ?
{
    const quint16 rgb = color;
    memcpy(&m_controlRequest.data()[48], (const void *)(&rgb), sizeof(qint16));

    m_logger->log(QStringLiteral("Changing RGB color to 0x") + QString::number(rgb, 16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP RGB led request"));
        return false;
    }

    return true;
}

bool SparkPlcConnection::sendBuzzerRequest(Zummer zummer)
{
    qint16 buzzerOn = zummer==Zummer::ZummerOff ? 0:1;
    memcpy(&m_controlRequest.data()[60], (const void *)(&buzzerOn), sizeof(qint16));

    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send UDP RGB led request"));
        return false;
		}

    return true;
}

bool SparkPlcConnection::sendControlPackage()
{
    if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH) {
        m_logger->log(QStringLiteral("Error: can't send controll request"));
        return false;
    }

    return true;
}

bool SparkPlcConnection::sendAlarmReset()
{
    quint16 alarmResetCommand = PLC_ALARM_RESET;
    //! TODO: Входные параметры нигде не сохраняются (только в массиве самого пакета). И на данном этапе я не знаю надо ли вообще их сохранять.
    memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));

		if (m_udpSocket->writeDatagram(QNetworkDatagram(m_controlRequest, m_hostAddress, m_nPort)) != PLC_CONTROL_REQUEST_LENGTH)
		{
        m_logger->log(QStringLiteral("Error: can't send UDP control request"));

        alarmResetCommand = PLC_ALARM_NORESET;
        memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));
        return false;
    }

    alarmResetCommand = PLC_ALARM_NORESET;
    memcpy(&m_controlRequest.data()[2], (const void *)(&alarmResetCommand), sizeof(qint16));
    return true;
}

void SparkPlcConnection::changeCheckInterval(qint64 newInterval)
{
    m_checkTimer->start(newInterval);
    m_watchDogTimer->start(newInterval + 3000); // интервал срабатывания watchDog должен всегда быть больше интевала посылки команд
}

void SparkPlcConnection::pauseChamber()
{
    if (m_thermoprofileState != ThermoprofileNormalState)
        return;

    if (sendStandByRequest(true))
        m_thermoprofileState = ThermoprofilePauseState;
    else
    {
        m_thermoprofileState = ThermoprofileProblemState;
        m_currentErrorMessage = "Ошибка при постановке камеры на паузу!";
    }

    m_isPauseOn = true;
}

void SparkPlcConnection::resumeChamber()
{
    /// Не знаю точно, как, но буду возобновлять работу камеры через перевод ее режима в manual_mode
    /// Хотя возможно, что надо просто делать уставку температуры пониже

    if (m_thermoprofileState != ThermoprofilePauseState)
        return;

    if ( !sendStandByRequest(false) )
    {
        m_thermoprofileState = ThermoprofileProblemState;
        m_currentErrorMessage = "Ошибка при возобновлении работы термокамеры!";
    }
    // в ThermoprofileNormalState она вернется только когда температура устаканится (см. ниже)

    m_isPauseOn = true;
}

void SparkPlcConnection::resumeThermoprofileState()
{
    if (m_thermoprofileState == ThermoprofilePauseState)
        m_thermoprofileState = ThermoprofileNormalState;
}

void SparkPlcConnection::beginThermoprofile()
{
    if (sendAlarmReset())
		{
			//m_logger->log(QStringLiteral("beginThermoprofile set to ThermoprofileNormalState"));
			m_thermoprofileState = ThermoprofileNormalState;
		}
    else
		{
        m_thermoprofileState = ThermoprofileProblemState;
        m_currentErrorMessage = "Ошибка при попытке сброса аварий!";
				//m_logger->log(QStringLiteral("beginThermoprofile set to ThermoprofileProblemState"));
		}
}

void SparkPlcConnection::endThermoprofile()
{
    sendStandByRequest(true);
    m_thermoprofileState = ThermoprofileNoState;
    m_centralRackOverheating = false;
}

float SparkPlcConnection::averageMainTemperature()
{
    float averageTemperature = 0;
    for(const auto & t : m_measuredTemperatures)
        averageTemperature += t;
    return averageTemperature / m_measuredTemperatures.size();
}

void SparkPlcConnection::setTemperatureToWindow(float preferedTemperature, float temperatureTolerance, bool doTemperatureCheck, bool append)
{
    if (!mainTemperature().isValid)
        return;

    if (append) {
        m_measuredTemperatures.enqueue(mainTemperature().value);
        if (m_measuredTemperatures.size() > 5)
            m_measuredTemperatures.dequeue();
    }

    m_preferedTemperature = preferedTemperature;
    m_temperatureTolerance = temperatureTolerance;

    syncState(doTemperatureCheck);
    checkOverheating();
    checkUpsState();
}

void SparkPlcConnection::syncState(bool doTemperatureChack)
{
    if ((m_thermoprofileState != ThermoprofileNormalState) && (m_thermoprofileState != ThermoprofileProblemState))
        return;

    bool temperatureProblemCondition = false;
    if (doTemperatureChack && (m_measuredTemperatures.size() >= 5)) {

        float averageTemperature = averageMainTemperature();

        // Реализация опции не учета времени при понижении температуры ниже уставки
        if ((m_thermalChamberTemperatureSet.isValid) && (averageTemperature < (m_thermalChamberTemperatureSet.value - m_temperatureTolerance/2))) {
            m_pauseFunction(m_nSlot);
        }

        if (m_thermalChamberTemperatureSet.isValid && (averageTemperature > (m_thermalChamberTemperatureSet.value + m_temperatureTolerance/2)) /*|| (averageTemperature < (m_preferedTemperature - m_temperatureTolerance/2))*/){
            m_logger->log(QString("Error: Chamber temperature (%1) out of range %2").arg(averageTemperature).arg(m_thermalChamberTemperatureSet.value));
            temperatureProblemCondition = true;
        }
    }

    // для любого блока (нагрев/не нагрев)
    bool alarmChamberCondition = false;
    if (alarmDataList().size()) {
        m_logger->log("Error: One of the plc module has alarm");
        for (auto a : alarmDataList()) {
            m_logger->log("module = "  + QString::number(a.moduleId.value) + " errorCode = " + QString::number(a.errorCode.value) + " param = " + QString::number(a.parameter.value));
        }
        alarmChamberCondition = true;
    }

    if (m_upsLink.isValid && (!m_upsLink.value)) {
				//m_logger->log("Warning: PLC miss link to UPS");
    }
    if (m_cameraLink.isValid && (!m_cameraLink.value)) {
        m_logger->log("Warning: PLC miss link to camera");
    }

    if (temperatureProblemCondition || alarmChamberCondition)
        m_thermoprofileState = ThermoprofileProblemState;
    else
        m_thermoprofileState = ThermoprofileNormalState;
}

void SparkPlcConnection::socketHandler()
{
    while (m_udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = m_udpSocket->receiveDatagram();
        //m_logger->log(QStringLiteral("UDP state datagram length = ") + QString::number(datagram.data().size()));
        stateHandler(datagram);
        // datagram.makeReply(QByteArray("answer")); // очень удобная функция
    }
}

void SparkPlcConnection::stateHandler(const QNetworkDatagram & datagram)
{
    if (datagram.data().size() != PLC_STATE_RESPONSE_LENGHT) {
        m_logger->log(QStringLiteral("Error: wrong UDP state datagram length = ") + QString::number(datagram.data().size()));
        return;
    }

    if (!m_linkState)
        m_logger->log("Link to PLC Ok, port "+QString::number(m_nPort));

    m_linkState = true;
    m_watchDogTimer->start();

		//m_logger->log("Camera port "+QString::number(m_nPort));

    memcpy(&m_systemInfoId.value, (const void *)&datagram.data().data()[0], sizeof(qint16));
    memcpy(&m_systemInfoVersion.value, (const void *)&datagram.data().data()[2], sizeof(qint16));
    memcpy(&m_systemInfoErrorCode.value, (const void *)&datagram.data().data()[6], sizeof(qint16));
    memcpy(&m_systemInfoDeviceVersion.value, (const void *)&datagram.data().data()[8], sizeof(qint16));
    memcpy(&m_systemInfoHWVersion.value, (const void *)&datagram.data().data()[12], sizeof(qint16));
    memcpy(&m_systemInfoSWVersion.value, (const void *)&datagram.data().data()[14], sizeof(qint16));
    memcpy(&m_systemInfoSerialNumber.value, (const void *)&datagram.data().data()[16], sizeof(qint16));
   // m_logger->log(QStringLiteral("m_systemInfoSerialNumber = ") + QString::number(m_systemInfoSerialNumber.value));

    memcpy(&m_thermalChamberId.value, (const void *)&datagram.data().data()[28], sizeof(qint16));
		//m_logger->log(QStringLiteral("m_thermalChamberId = ") + QString::number(m_thermalChamberId.value));
    memcpy(&m_thermalChamberErrorCode.value, (const void *)&datagram.data().data()[34], sizeof(qint16));
    memcpy(&m_thermalChamberTemperature.value, (const void *)&datagram.data().data()[38], sizeof(float));
		//m_logger->log(QStringLiteral("m_thermalChamberTemperature = ") + QString::number(m_thermalChamberTemperature.value,'f',2));
    memcpy(&m_thermalChamberSlidedTemperatureSet.value, (const void *)&datagram.data().data()[46], sizeof(float));
		//m_logger->log(QStringLiteral("m_thermalChamberSlidedTemperatureSet = ") + QString::number(m_thermalChamberSlidedTemperatureSet.value,'f',2));
    memcpy(&m_thermalChamberMode.value, (const void *)&datagram.data().data()[50], sizeof(qint16));
		//m_logger->log(QStringLiteral("m_thermalChamberMode = ") + QString::number(m_thermalChamberMode.value));
    memcpy(&m_thermalChamberTemperatureSet.value, (const void *)&datagram.data().data()[52], sizeof(float));
		//m_logger->log(QStringLiteral("m_thermalChamberTemperatureSet = ") + QString::number(m_thermalChamberTemperatureSet.value,'f',2));
    memcpy(&m_thermalChamberTemperatureSpeed.value, (const void *)&datagram.data().data()[56], sizeof(float));
    //m_logger->log(QStringLiteral("m_thermalChamberTemperatureSpeed = ") + QString::number(m_thermalChamberTemperatureSpeed.value,'f',2));

    memcpy(&m_accessoriesChamberId.value, (const void *)&datagram.data().data()[114], sizeof(qint16));
    memcpy(&m_accessoriesErrorCode.value, (const void *)&datagram.data().data()[120], sizeof(qint16));

    memcpy(&m_optionsChamberId.value, (const void *)&datagram.data().data()[132], sizeof(qint16));
    memcpy(&m_optionsErrorCode.value, (const void *)&datagram.data().data()[138], sizeof(qint16));

    memcpy(&m_alarmChamberId.value, (const void *)&datagram.data().data()[150], sizeof(qint16));
    memcpy(&m_alarmErrorCode.value, (const void *)&datagram.data().data()[156], sizeof(qint16));

    memcpy(&m_alarmErrorsNumber.value, (const void *)&datagram.data().data()[158], sizeof(qint16));

    m_alarmDataList.clear();
    for (qint32 i=0; ((i<m_alarmErrorsNumber.value) && (i<8)); ++i) {
        qint16 id, instance, errorCode;
        float param;
        memcpy(&id,         (const void *)&datagram.data().data()[160+i*10], sizeof(qint16));
        memcpy(&instance,   (const void *)&datagram.data().data()[162+i*10], sizeof(qint16));
        memcpy(&errorCode,  (const void *)&datagram.data().data()[164+i*10], sizeof(qint16));
        memcpy(&param,      (const void *)&datagram.data().data()[166+i*10], sizeof(float));
        m_alarmDataList.append(AlarmData(ShortValue(id), ShortValue(instance), ShortValue(errorCode), FloatValue(param)));
    }

    memcpy(&m_extDI.value, (const void *)&datagram.data().data()[250], sizeof(qint16));
    memcpy(&m_extDO.value, (const void *)&datagram.data().data()[252], sizeof(qint16));

    float sensorTemp0{0},sensorTemp1{0},sensorTemp2{0},sensorTemp3{0},
          sensorTemp4{0},sensorTemp5{0},sensorTemp6{0},sensorTemp7{0};
    qint16 sensorOk0{0},sensorOk1{0},sensorOk2{0},sensorOk3{0},
            sensorOk4{0},sensorOk5{0},sensorOk6{0},sensorOk7{0};
    memcpy(&sensorTemp0, (const void *)&datagram.data().data()[258], sizeof(float));
		//m_logger->log(QStringLiteral("sensorTemp0 = ") + QString::number(sensorTemp0,'f',2));
    memcpy(&sensorTemp1, (const void *)&datagram.data().data()[262], sizeof(float));
		//m_logger->log(QStringLiteral("sensorTemp1 = ") + QString::number(sensorTemp1,'g'));
    memcpy(&sensorTemp2, (const void *)&datagram.data().data()[266], sizeof(float));
    memcpy(&sensorTemp3, (const void *)&datagram.data().data()[270], sizeof(float));
    memcpy(&sensorTemp4, (const void *)&datagram.data().data()[274], sizeof(float));
    memcpy(&sensorTemp5, (const void *)&datagram.data().data()[278], sizeof(float));
    memcpy(&sensorTemp6, (const void *)&datagram.data().data()[282], sizeof(float));
    memcpy(&sensorTemp7, (const void *)&datagram.data().data()[286], sizeof(float));

    memcpy(&sensorOk0, (const void *)&datagram.data().data()[290], sizeof(qint16));
		//m_logger->log(QStringLiteral("sensorOk0 = ") + QString::number(sensorOk0));
    memcpy(&sensorOk1, (const void *)&datagram.data().data()[292], sizeof(qint16));
		//m_logger->log(QStringLiteral("sensorOk1 = ") + QString::number(sensorOk1));
    memcpy(&sensorOk2, (const void *)&datagram.data().data()[294], sizeof(qint16));
    memcpy(&sensorOk3, (const void *)&datagram.data().data()[296], sizeof(qint16));
    memcpy(&sensorOk4, (const void *)&datagram.data().data()[298], sizeof(qint16));
    memcpy(&sensorOk5, (const void *)&datagram.data().data()[300], sizeof(qint16));
    memcpy(&sensorOk6, (const void *)&datagram.data().data()[302], sizeof(qint16));
    memcpy(&sensorOk7, (const void *)&datagram.data().data()[304], sizeof(qint16));

    m_sensorTemperatures[0].first.setValue(sensorTemp0); m_sensorTemperatures[0].second.setValue(sensorOk0);
    m_sensorTemperatures[1].first.setValue(sensorTemp1); m_sensorTemperatures[1].second.setValue(sensorOk1);
    m_sensorTemperatures[2].first.setValue(sensorTemp2); m_sensorTemperatures[2].second.setValue(sensorOk2);
    m_sensorTemperatures[3].first.setValue(sensorTemp3); m_sensorTemperatures[3].second.setValue(sensorOk3);
    m_sensorTemperatures[4].first.setValue(sensorTemp4); m_sensorTemperatures[4].second.setValue(sensorOk4);
    m_sensorTemperatures[5].first.setValue(sensorTemp5); m_sensorTemperatures[5].second.setValue(sensorOk5);
    m_sensorTemperatures[6].first.setValue(sensorTemp6); m_sensorTemperatures[6].second.setValue(sensorOk6);
    m_sensorTemperatures[7].first.setValue(sensorTemp7); m_sensorTemperatures[7].second.setValue(sensorOk7);

    memcpy(&m_upsLink.value, (const void *)&datagram.data().data()[318], sizeof(qint16));
    memcpy(&m_cameraLink.value, (const void *)&datagram.data().data()[320], sizeof(qint16));

    memcpy(&m_calculatedRackTemerature.value, (const void *)&datagram.data().data()[322], sizeof(float));

    float fanFrequency;
    qint16 fanErrCode;
    memcpy(&fanFrequency, (const void *)&datagram.data().data()[334], sizeof(float));
    memcpy(&fanErrCode, (const void *)&datagram.data().data()[338], sizeof(qint16));
    m_fanData[0].first.setValue(fanFrequency); m_fanData[0].second.setValue(fanErrCode);
    //m_logger->log(QStringLiteral("Верхний вентилятор, скорость = ") + QString::number(fanFrequency,'f',2));
    //m_logger->log(QStringLiteral("Верхний вентилятор, статус = ") + QString::number(fanErrCode));
    fanFrequency = 0.0f;
    fanErrCode = 1;
    memcpy(&fanFrequency, (const void *)&datagram.data().data()[340], sizeof(float));
    memcpy(&fanErrCode, (const void *)&datagram.data().data()[344], sizeof(qint16));
    m_fanData[1].first.setValue(fanFrequency); m_fanData[1].second.setValue(fanErrCode);
    //m_logger->log(QStringLiteral("Нижний вентилятор, скорость = ") + QString::number(fanFrequency,'f',2));
    //m_logger->log(QStringLiteral("Нижний вентилятор, статус = ") + QString::number(fanErrCode));
    fanFrequency = 0.0f;
    fanErrCode = 1;
    memcpy(&fanFrequency, (const void *)&datagram.data().data()[346], sizeof(float));
    memcpy(&fanErrCode, (const void *)&datagram.data().data()[350], sizeof(qint16));
    m_fanData[2].first.setValue(fanFrequency); m_fanData[2].second.setValue(fanErrCode);
    //m_logger->log(QStringLiteral("Центральный вентилятор, скорость = ") + QString::number(fanFrequency,'f',2));
    //m_logger->log(QStringLiteral("Центральный вентилятор, статус = ") + QString::number(fanErrCode));

    memcpy(&m_upperDoorState.value, (const void *)&datagram.data().data()[380], sizeof(qint16));
    memcpy(&m_lowerDoorState.value, (const void *)&datagram.data().data()[382], sizeof(qint16));

    /*memcpy(&m_currentInputUPS.value, (const void *)&datagram.data().data()[344], sizeof(qint16));
    memcpy(&m_currentOutputUPS.value, (const void *)&datagram.data().data()[346], sizeof(qint16));
    memcpy(&m_ratingPowerUPS.value, (const void *)&datagram.data().data()[348], sizeof(qint16));
    memcpy(&m_outputVoltageUPS.value, (const void *)&datagram.data().data()[350], sizeof(qint16));
    memcpy(&m_batteryVoltageUPS.value, (const void *)&datagram.data().data()[352], sizeof(qint16));
    memcpy(&m_loadPercentUPS.value, (const void *)&datagram.data().data()[362], sizeof(qint16));
    memcpy(&m_inputVoltageFrequencyUPS.value, (const void *)&datagram.data().data()[368], sizeof(qint16));
    memcpy(&m_outputVoltageFrequencyUPS.value, (const void *)&datagram.data().data()[370], sizeof(qint16));
    memcpy(&m_workingTimeUPS.value, (const void *)&datagram.data().data()[372], sizeof(qint16));
    memcpy(&m_chargeUPS.value, (const void *)&datagram.data().data()[374], sizeof(qint16));
    memcpy(&m_inputVoltageUPS.value, (const void *)&datagram.data().data()[376], sizeof(qint16));*/

    memcpy(&m_bitsStatusUPS.value, (const void *)&datagram.data().data()[378], sizeof(qint16));

    //m_logger->log(QStringLiteral("UPS status bits = ") + QString::number(m_bitsStatusUPS.value)); //! только для отладки порядка бит
    //! Видимо в это же месте нужно ставить посылку команды redBlink,
    //! Хотя можно и в syncState), но я считаю, что авария питания может произойти и без термоциклограммы


    // получаем температуру с датчиков микросхем
    QList<int> sensorSlotTempDataArrayIndices = {400,404,408,412,416,420,424,428,432,436,
                                                440,444,448,452,456,460,464,468,472,476};
    QList<int> sensorSlotStateDataArrayIndices = {496,498,500,502,504,506,508,510,512,514,
                                                  516,518,520,522,524,526,528,530,532,534};

    for (int i=0; i<sensorSlotTempDataArrayIndices.size();i++ )
    {
      float sensorTemp{0};qint16 sensorOk{0};

      memcpy(&sensorTemp, (const void *)&datagram.data().data()[sensorSlotTempDataArrayIndices[i]], sizeof(float));
      memcpy(&sensorOk, (const void *)&datagram.data().data()[sensorSlotStateDataArrayIndices[i]], sizeof(qint16));
      if (!sensorOk)
      {
          changeLeds(LedColor::YellowLed);
          //! TODO: поставить на паузу как в SparkService::pauseCommandHandler
      }

      m_sensorSlotTemperatures[i].first.setValue(sensorTemp); m_sensorSlotTemperatures[i].second.setValue(sensorOk);
    }
}

void SparkPlcConnection::checkOverheating()
{
    bool bOverheatingDetected = false;
    const QList<QPair<FloatValue,ShortValue>> peripheralTemps = qAsConst(peripheralTemperatures());
    float temperatureValue {0.0};
    for (int i = 0; i < peripheralTemps.size(); i++)
    {
        temperatureValue = peripheralTemps[i].first.value;
        if ( ( m_nSlot==THERMO_SLOT_NUMBER1 && i>5 ) ||
             ( m_nSlot==THERMO_SLOT_NUMBER2 && i<=5 )
             )
        {
            continue;
        }
        //! TODO: сравнивается не с тем значением, нужно добавить новую переменную для контроля температуры в стойках
        if ( temperatureValue > m_rackOverHeat )
        {
            bOverheatingDetected = true;
            if (i < 4)
            {
                m_centralRackOverheating = bOverheatingDetected;
                m_currentErrorMessage = QString("Перегрев центральной стойки! Температура = %1").arg(temperatureValue);
                m_logger->log("Central rack overheating!");
            }
            break;
        }
    }
    if ( bOverheatingDetected )
    {
        if (!m_centralRackOverheating)
        {
            QString rackPosition = (m_nSlot == THERMO_SLOT_NUMBER1 ? "левой":"правой");
            m_currentErrorMessage = QString("Перегрев " + rackPosition + " стойки! Температура = %1").arg(temperatureValue);
        }
        m_thermoprofileState = ThermoprofileProblemState;
    }
}

void SparkPlcConnection::checkUpsState()
{
    short state = m_bitsStatusUPS.value;
    if ((state & UpsAlarmState) || (((state & UpsBatteryState) != 0) & ((state & UpsLowBatteryState)!=0)))
    {
        if (m_upsBatteryTimer->isActive())
        {
            m_upsBatteryTimer->stop();
            m_logger->log("Battery mode timer is stoped");
        }
        m_upsState = UpsAlarmState;
        m_thermoprofileState = ThermoprofileProblemState;
        m_currentErrorMessage = QString("Ошибка ИБП! Состояние ИБП: %1").arg(state);
        m_logger->log(QStringLiteral("UPS AlarmState detected! UPS bitsStatus: %1").arg(state));
    }
    else if (state & UpsBatteryState)
    {
        m_upsState = UpsBatteryState;
        if (!m_upsBatteryTimer->isActive())
        {
            m_upsBatteryTimer->setInterval(m_upsBatteryTimeout * MS_INTO_MINUTE);
            m_upsBatteryTimer->setSingleShot(true);
            connect(m_upsBatteryTimer, &QTimer::timeout, [this](){m_upsState = UpsAlarmState; m_thermoprofileState = ThermoprofileProblemState;
                m_currentErrorMessage = QString("Превышено время работы от ИБП!");});
            m_upsBatteryTimer->start();
            m_logger->log(QStringLiteral("Starting battery mode timer with timeout = %1 ms").arg(m_upsBatteryTimeout));
            emit changeLeds();
        }
    }
    else if (state & UpsNormalState)
    {
        m_upsState = UpsNormalState;
        if (m_upsBatteryTimer->isActive())
        {
            m_upsBatteryTimer->stop();
            m_logger->log("Battery mode timer is stoped");
            emit changeLeds();
        }
    }
}
