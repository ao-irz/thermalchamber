#ifndef SPARKPLCCONNECTION_H
#define SPARKPLCCONNECTION_H

/*! РџСЂРѕС‚РѕРєРѕР»СЊРЅР°СЏ, СЃРµС‚РµРІР°СЏ С‡Р°СЃС‚СЊ РІР·Р°РёРјРѕРґРµР№СЃС‚РІРёСЏ СЃ РџР›Рљ !*/

#include <QObject>
#include <QPointer>
#include <QUdpSocket>
#include <QTimer>
#include <QNetworkDatagram>
#include <QQueue>
#include "ValueTypes.h"
#include "config.h"
#include "../../PXI-common/led-color.h"
#include "../../PXI-common/zummer.h"
#include "../../PXI-service/adapter/hardware/thermocouple.h"

class Logger;

class SparkPlcConnection : public QObject
{
    Q_OBJECT

public:
    SparkPlcConnection(const QPointer<Logger> &logger,
                       std::function<void(quint32)> pauseFunction,
                       quint32 nCameraSlot,
                       quint16 nPort,
                       QList<float> chamberTempSettings,
                       QString address = "",
                       QObject * parent = nullptr);

    struct AlarmData
    {
        AlarmData(ShortValue id, ShortValue instanceNumber, ShortValue errorCode, FloatValue param) :
            moduleId(id), instanceNumber(instanceNumber), errorCode(errorCode), parameter(param)
        {   }
        ShortValue moduleId;
        ShortValue instanceNumber;  // РµСЃС‚СЊ РїРѕРґРѕР·СЂРµРЅРёРµ, С‡С‚Рѕ Р±СѓРґРµС‚ РІСЃРµРіРґР° "0", РїРѕС‚РѕРјСѓ С‡С‚Рѕ РІСЃРµ РјРѕРґСѓР»РµР№ РїРѕ РѕРґРЅРѕР№ С€С‚СѓРєРµ
        ShortValue errorCode;
        FloatValue parameter;
    };

    enum ThermoprofileState { // РЅРµС‚ СЃРјС‹СЃР»Р° РІРІРѕРґРёС‚СЊ РЅРѕРІС‹Рµ define
        ThermoprofileNoState = NO_SLOT_STATE,
        ThermoprofileNormalState = NORMAL_SLOT_STATE,
        ThermoprofilePauseState = PAUSE_SLOT_STATE,
        ThermoprofileProblemState = ALARM_SLOT_STATE
    };

    enum UpsState {
        UpsNoState = 0,
        UpsNormalState = 1,
        UpsBatteryState = 2,
        UpsLowBatteryState = 64,
        UpsAlarmState = 8
    };

    bool sendStateRequest();                        // Р·Р°РїСЂРѕСЃ С‚РµРєСѓС‰РµРіРѕ СЃРѕСЃС‚РѕСЏРЅРёСЏ
    bool sendCommandRequest(qint16 cmd);
    bool sendParametersRequest();                   // СѓСЃС‚Р°РЅРѕРІРєР° РѕСЃРЅРѕРІРЅС‹С… РїР°СЂР°РјРµС‚СЂРѕРІ СЂР°Р±РѕС‚С‹ РєР°РјРµСЂС‹ РїРѕ СЃС‚Р°СЂС‚Сѓ
    bool sendParametersRequest(const float tempHH, const float tempLL, const quint16 autostop, const float tempAutostop,
                               const float rackSP, const float rackHH, const float rackH, const float rackL, const float rackLL, const float rackOverHeat, const int upsTimerTimeout); // СѓСЃС‚Р°РЅРѕРІРєР° РѕСЃРЅРѕРІРЅС‹С… РїР°СЂР°РјРµС‚СЂРѕРІ СЂР°Р±РѕС‚С‹ РєР°РјРµСЂС‹
    bool sendControlRequest(float temperature, float maxTemperatureChangeRate, int actionType);    // СѓРїСЂР°РІР»РµРЅРёРµ РёР·РјРµРЅСЏРµРјС‹РјРё РїР°СЂР°РјРµС‚СЂР°РјРё
    bool sendStandByRequest(bool enable);                      // РѕСЃС‚Р°РЅРѕРІРёС‚СЊ РєР°РјРµСЂСѓ С‡РµСЂРµР· РїРѕР»Рµ MODE // РїРѕ СЃСѓС‚Рё РЅР°РґРѕ Р±С‹ СѓРЅРµСЃС‚Рё РµРµ РІ private, РЅРѕ РµСЃС‚СЊ РѕРґРЅРѕ РёСЃРєР»СЋС‡РµРЅРёРµ РґР»СЏ PXISchedulePlc

    // Р’РµСЂРѕСЏС‚РЅРµРµ РІСЃРµРіРѕ Р·РґРµСЃСЊ РїРѕСЏРІСЏС‚СЃСЏ С„-РёРё РїСЂРµРґРЅР°Р·РЅР°С‡РµРЅРЅС‹Рµ РґР»СЏ РІРЅСѓС‚СЂРµРЅРЅРµРіРѕ РёСЃРїРѕР»СЊР·РѕРІР°РЅРёСЏ
    bool sendRGBRequest(LedColor color);
    bool sendBuzzerRequest(Zummer zummer);
    bool sendControlPackage(); // РєРѕСЃС‚С‹Р»СЊ РїРѕ РїСЂРѕСЃСЊР±Рµ Р®СЂС‹ - РјРµС‚РѕРґ Р±СѓРґРµС‚ РІС‹Р·С‹РІР°С‚СЊСЃСЏ РїРµСЂРёРѕРґРёС‡РµСЃРєРё Рё РїСЂРѕСЃС‚Рѕ РїРµСЂРµРґР°РІР°С‚СЊ РІ РџР›Рљ Р±СѓС„РµСЂ РІ С‚РµРєСѓС‰РµРј РµРіРѕ СЃРѕСЃС‚РѕСЏРЅРёРё
    //bool sendEmergencyStopRequest(); // С‡РµСЂРµР· РїРѕР»Рµ MODE

    bool sendAlarmReset(); // С‡РµСЂРµР· РїРѕР»Рµ RESET_ALARM

    void changeCheckInterval(qint64 newInterval);

    void pauseChamber();
    void resumeChamber();
    void resumeThermoprofileState();
    void beginThermoprofile();
    void endThermoprofile();
    bool isPaused() const {return m_isPauseOn;}
    void setPause(bool bPauseOn)
    {
      m_isPauseOn = bPauseOn;
    }

    const ShortValue & systemInfoId() const
    { return m_systemInfoId; }
    const ShortValue & systemInfoVersion() const
    { return m_systemInfoVersion; }
    const ShortValue & systemInfoErrorCode() const
    { return m_systemInfoErrorCode; }
    const ShortValue & systemInfoDeviceVersion() const
    { return m_systemInfoDeviceVersion; }
    const ShortValue & systemInfoHWVersion() const
    { return m_systemInfoHWVersion; }
    const ShortValue & systemInfoSWVersion() const
    { return m_systemInfoSWVersion; }
    const ShortValue & systemInfoSerialNumber() const
    { return m_systemInfoSerialNumber; }

    const ShortValue & thermalChamberId() const
    { return m_thermalChamberId; }
    const ShortValue & thermalChamberErrorCode() const
    { return m_thermalChamberErrorCode; }
    const FloatValue & mainTemperature() const
    { return m_thermalChamberTemperature; }
    const FloatValue & thermalChamberSlidedTemperatureSet() const
    { return m_thermalChamberSlidedTemperatureSet; }
    ShortValue thermalChamberMode() const
    { return m_thermalChamberMode; }
    const FloatValue & thermalChamberTemperatureSet() const
    { return m_thermalChamberTemperatureSet; }
    const FloatValue & thermalChamberTemperatureSpeed() const
    { return m_thermalChamberTemperatureSpeed; }

    const float & chamberParamTempHH() const
    { return m_tempHH; }
    const float & chamberParamTempLL() const
    { return m_tempLL; }
    bool chamberParamCoolAutostop() const
    { return ((bool)m_coolAutostopEnable); }
    const float & chamberParamTempAutostop() const
    { return m_tempCoolAutostop; }
    const float & chamberParamRackSP() const
    { return m_rackSP; }
    const float & chamberParamRackHH() const
    { return m_rackHH; }
    const float & chamberParamRackH() const
    { return m_rackH; }
    const float & chamberParamRackL() const
    { return m_rackL; }
    const float & chamberParamRackLL() const
    { return m_rackLL; }

    const float & chamberParamRackOverHeat() const
    { return m_rackOverHeat; }

    const int & chamberParamUpsTimeout() const
    { return m_upsBatteryTimeout; }


    const ShortValue & accessoriesChamberId() const
    { return m_accessoriesChamberId; }
    const ShortValue & accessoriesErrorCode() const
    { return m_accessoriesErrorCode; }

    const ShortValue & optionsChamberId() const
    { return m_optionsChamberId; }
    const ShortValue & optionsErrorCode() const
    { return m_optionsErrorCode; }

    const ShortValue & alarmChamberId() const
    { return m_alarmChamberId; }
    const ShortValue & alarmErrorCode() const
    { return m_alarmErrorCode; }

    const ShortValue & alarmErrorsNumber() const
    { return m_alarmErrorsNumber; }

    const QList<AlarmData> & alarmDataList() const
    { return m_alarmDataList; }

    const ShortValue & extDI() const
    { return m_extDI; }
    const ShortValue & extDO() const
    { return m_extDO; }

    const QList<QPair<FloatValue,ShortValue>> & peripheralTemperatures()
    { return m_sensorTemperatures; }

    const QList<QPair<FloatValue,ShortValue>> & slotTemperatures()
    { return m_sensorSlotTemperatures; }

    const QList<QPair<FloatValue,ShortValue>> & fanData()
    { return m_fanData; }

    ShortValue upsLink() const
    { return m_upsLink; }
    ShortValue cameraLink() const
    { return m_cameraLink; }

    const FloatValue & calculatedRackTemerature() const
    { return m_calculatedRackTemerature; }
    const FloatValue & fanFrequency() const
    { return m_fanData[0].first; }
    const ShortValue & fanErrorCode() const
    { return m_fanData[0].second; }

    const ShortValue & currentInputUPS() const
    { return m_currentInputUPS; }
    const ShortValue & currentOutputUPS() const
    { return m_currentOutputUPS; }
    const ShortValue & ratingPowerUPS() const
    { return m_ratingPowerUPS; }
    const ShortValue & outputVoltageUPS() const
    { return m_outputVoltageUPS; }
    const ShortValue & batteryVoltageUPS() const
    { return m_batteryVoltageUPS; }
    const ShortValue & loadPercentUPS() const
    { return m_loadPercentUPS; }
    const ShortValue & inputVoltageFrequencyUPS() const
    { return m_inputVoltageFrequencyUPS; }
    const ShortValue & outputVoltageFrequencyUPS() const
    { return m_outputVoltageFrequencyUPS; }
    const ShortValue & workingTimeUPS() const
    { return m_workingTimeUPS; }
    const ShortValue & chargeUPS() const
    { return m_chargeUPS; }
    const ShortValue & inputVoltageUPS() const
    { return m_inputVoltageUPS; }

    const ShortValue & bitsStatusUPS() const
    { return m_bitsStatusUPS; }


    //! СЌС‚Рё С„СѓРЅРєС†РёРё (РєР°Рє Рё РїРѕР»СЏ) РЅРµРїР»РѕС…Рѕ Р±С‹Р»Рѕ Р±С‹ РїРµСЂРµРЅРµСЃС‚Рё РІ СЃРїРµС†РёР°Р»СЊРЅРѕ СЃРѕР·РґР°РЅРЅС‹Р№ 9-С‹Р№ РѕР±СЉРµРєС‚ РѕС‚ PXISlot
    float averageMainTemperature();
    void setTemperatureToWindow(float preferedTemperature, float temperatureTolerance, bool doTemperatureCheck, bool append);
    void syncState(bool doTemperatureChack);
    ThermoprofileState thermoprofileState() const
    { return m_thermoprofileState; }

    bool linkState() const
    { return m_linkState; }

    quint16 plcPort() const
    { return m_nPort; }

    quint16 plcSlot() const
    { return m_nSlot; }

    UpsState upsState() const
    { return m_upsState; }

    bool centralRackOverheating() const
    { return m_centralRackOverheating; }

    void setCentralRackOverheating(bool val)
    {   if (val)
            m_currentErrorMessage = QString("Перегрев центральной стойки!");
        m_centralRackOverheating = val; }

    QString getErrorMessage()
    {  return m_currentErrorMessage; }

signals:
    void changeLeds(LedColor color = LedColor::NoColor);
    void saveChamberTempSettings(quint32 nSlot, float overHeatTemp, float overFreezeTemp, quint16 refrigeratorAutoStop, float refrigeratorStopTemp,
                                 float rackTemp, float rackTempHH, float  rackTempH, float rackTempL, float rackTempLL, float rackOverHeat, quint32 upsTimerTimeout);

private slots:
    void socketHandler();

private:
    void stateHandler(const QNetworkDatagram &datagram);

    std::function<void(quint32)> m_pauseFunction;

    void checkOverheating();

    void checkUpsState();

private:
    QPointer<Logger> m_logger;

    const QHostAddress m_hostAddress;
    QByteArray m_stateRequest;
    QByteArray m_controlRequest;
    QByteArray m_parametersRequest;
    QPointer<QUdpSocket> m_udpSocket;
    QPointer<QTimer> m_checkTimer;
    QPointer<QTimer> m_watchDogTimer;
    quint16 m_nPort;
    bool m_isPauseOn{false};
    quint32 m_nSlot;

    bool m_linkState{false};
    // SYSTEM_INFO
    ShortValue m_systemInfoId{0};                          //id модуля
    ShortValue m_systemInfoVersion{0};                     //версия модуля
    //IntValue m_systemAccessMode{0};                      // режим доступа
    ShortValue m_systemInfoErrorCode{0};                   // код аварии
    ShortValue m_systemInfoDeviceVersion{0};               //модель устройства
    //IntValue m_systemInfoProtocolVersion{0};             //версия modbus
    ShortValue m_systemInfoHWVersion{0};                   //версия аппаратной части
    ShortValue m_systemInfoSWVersion{0};                   //версия ПО
    ShortValue m_systemInfoSerialNumber{0};                //серийный номер устройства

    ShortValue m_thermalChamberId{0};                      //id модуля
    //IntValue m_thermalChamberVersion{0};                 //версия модуля
    //IntValue m_thermalChamberAccessMode{0};              // режим доступа
    ShortValue m_thermalChamberErrorCode{0};               // errorCode
    FloatValue m_thermalChamberTemperature{0.0};           // текущая температура в рабочем объеме камеры
    FloatValue m_thermalChamberSlidedTemperatureSet{0.0};  // cкользящая уставка температуры
    ShortValue m_thermalChamberMode{0};                    // режим камеры
    FloatValue m_thermalChamberTemperatureSet{0.0};        // текущая уставка температуры в рабочем объеме
    FloatValue m_thermalChamberTemperatureSpeed{0.0};      // скорость изменения температуры в минуту

    // СЌС‚Рё РїР°СЂР°РјРµС‚СЂС‹ РЅРµ РїРѕР»СѓС‡Р°СЋС‚СЃСЏ СЃРµСЂРІРёСЃРѕРј РёР· РїР°РєРµС‚Р° СЃРѕСЃС‚РѕСЏРЅРёСЏ, Р° СЃСѓС‰РµСЃС‚РІСѓСЋС‚ РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ Рё РІРІРѕРґСЏС‚СЃСЏ РїРѕР»СЊР·РѕРІР°С‚РµР»РµРј
    float m_tempHH{PLC_PARAMETER_TEMP_HH};
    float m_tempLL{PLC_PARAMETER_TEMP_LL};
    quint16 m_coolAutostopEnable{PLC_PARAMETER_COOL_AUTOSTOP_ENABLE};
    float m_tempCoolAutostop{PLC_PARAMETER_TEMP_COOL_AUTOSTOP};
    float m_rackSP{PLC_PARAMETER_RACK_SP};
    float m_rackHH{PLC_PARAMETER_RACK_HH};
    float m_rackH{PLC_PARAMETER_RACK_H};
    float m_rackL{PLC_PARAMETER_RACK_L};
    float m_rackLL{PLC_PARAMETER_RACK_LL};
    float m_rackOverHeat{RACK_OVERHEAT_TEMP};
    int m_upsBatteryTimeout{UPS_TIMER_TIMEOUT};

    ShortValue m_accessoriesChamberId{0};
    //IntValue m_accessoriesVersion;
    //IntValue m_accessoriesAccessMode;
    ShortValue m_accessoriesErrorCode{0};                  //! errorCode - РІРѕР·РјРѕР¶РЅРѕ РЅРµ Р°РєС‚РёРІРµРЅ

    ShortValue m_optionsChamberId{0};
    //IntValue m_optionsVersion;
    //IntValue m_optionsAccessMode;
    ShortValue m_optionsErrorCode{0};                      //! errorCode - РІРѕР·РјРѕР¶РЅРѕ РЅРµ Р°РєС‚РёРІРµРЅ

    ShortValue m_alarmChamberId{0};
    //IntValue m_alarmVersion;
    //IntValue m_alarmAccessMode;
    ShortValue m_alarmErrorCode{0};                        //! errorCode - РІРѕР·РјРѕР¶РЅРѕ РЅРµ Р°РєС‚РёРІРµРЅ

    ShortValue m_alarmErrorsNumber{0};                     // С‡РёСЃР»Рѕ Р°РєС‚РёРІРЅС‹С… Р°РІР°СЂРёР№
    QList<AlarmData> m_alarmDataList;                   // СЃРїРёСЃРѕРє РёР· 8 (РёР»Рё РјРµРЅСЊС€Рµ) СЌР»РµРјРµРЅС‚РѕРІ, С…Р°СЂР°РєС‚РµСЂРёР·СѓСЋС‰РёС… РѕС‚РєР°Р·С‹ РјРѕРґСѓР»РµР№

    ShortValue m_extDI{0};
    ShortValue m_extDO{0};

    QList<QPair<FloatValue,ShortValue>> m_sensorTemperatures {
        {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},
        {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()}
    };

    QList<QPair<FloatValue,ShortValue>> m_sensorSlotTemperatures
    {
      {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},
      {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},
      {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},
      {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()}
    };

    ShortValue m_upsLink{1};
    ShortValue m_cameraLink{1};

    FloatValue m_calculatedRackTemerature{0.0};              // СЂР°СЃС‡РёС‚Р°РЅРЅРѕРµ Р·РЅР°С‡РµРЅРёРµ С‚РµРјРїРµСЂР°С‚СѓСЂС‹ РІ СЃС‚РѕР№РєРµ, СѓСЃСЂРµРґРЅРµРЅРЅРѕРµ
    //IntValue m_calculatedRackTemeratureStatus;        // СЃС‚Р°С‚СѓСЃ СЃСЂРµРґРЅРµР№ С‚РµРјРїРµСЂР°С‚СѓСЂС‹ РІ СЃС‚РѕР№РєРµ (Р° РЅР°С„РёРі РѕРЅРѕ РЅСѓР¶РЅРѕ?)

    //FloatValue m_fanFrequencySet;                     // ?
    //FloatValue m_fanFrequency{0.0};                          // РўРµРєСѓС‰Р°СЏ С‡Р°СЃС‚РѕС‚Р° РІСЂР°С‰РµРЅРёСЏ РІРµРЅС‚РёР»СЏС‚РѕСЂР° РѕР±РѕСЂРѕС‚РѕРІ/РјРёРЅСѓС‚Сѓ
    //ShortValue m_fanErrorCode{0};                          //! РђРІР°СЂРёСЏ РІРµРЅС‚РёР»СЏС‚РѕСЂР° (1-РЅРµС‚/2-РґР°)
    QList<QPair<FloatValue,ShortValue>> m_fanData{
      {FloatValue(),ShortValue()},{FloatValue(),ShortValue()},{FloatValue(),ShortValue()} };

    ShortValue m_upperDoorState{0};
    ShortValue m_lowerDoorState{0};

    ShortValue m_currentInputUPS{0};
    ShortValue m_currentOutputUPS{0};
    ShortValue m_ratingPowerUPS{0};
    ShortValue m_outputVoltageUPS{0};
    ShortValue m_batteryVoltageUPS{0};
    ShortValue m_loadPercentUPS{0};
    ShortValue m_inputVoltageFrequencyUPS{0};
    ShortValue m_outputVoltageFrequencyUPS{0};
    ShortValue m_workingTimeUPS{0};
    ShortValue m_chargeUPS{0};
    ShortValue m_inputVoltageUPS{0};

    ShortValue m_bitsStatusUPS{0};

    ///-----------------------------------------------------------------------------------------------------------------

    QQueue<float> m_measuredTemperatures;
    float m_preferedTemperature;
    float m_temperatureTolerance;
    ThermoprofileState m_thermoprofileState{ThermoprofileNoState};

    bool m_centralRackOverheating{false};
    QString m_currentErrorMessage;
    UpsState m_upsState{UpsNoState};
    QPointer<QTimer> m_upsBatteryTimer;
};

#endif // SPARKPLCCONNECTION_H
