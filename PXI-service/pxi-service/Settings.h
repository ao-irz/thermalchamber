﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QUuid>

#define INI_FILE_NAME       QStringLiteral("settings.ini")
#define INI_GENERATORS_GROUP    QStringLiteral("Generators")
#define INI_PSS_GROUP    QStringLiteral("PowerSupplies")
#define INI_SLOTS_GROUP    QStringLiteral("PowerSlotChannelMap")
#define INI_PLC_GROUP    QStringLiteral("PLC")
#define INI_PLC_IP    QStringLiteral("ip")
#define INI_LEFT_CAMERA_PORT    QStringLiteral("leftcameraport")
#define INI_RIGHT_CAMERA_PORT    QStringLiteral("rightcameraport")
#define INI_DB_GROUP    QStringLiteral("DB")
#define INI_DB_ADDRESS    QStringLiteral("dbAddress")
#define INI_DB_PORT    QStringLiteral("dbPort")
#define INI_DB_DBNAME    QStringLiteral("dbName")
#define INI_DB_USER    QStringLiteral("dbUser")
#define INI_DB_PASSWORD    QStringLiteral("dbPassword")


#define INI_TCOUPLE_GROUP QStringLiteral("Thermocouples")
#define INI_SLOT_TCOUPLE_COUNT QStringLiteral("perslot")
#define INI_LEFTCAM_TCOUPLES QStringLiteral("leftcameraslots")
#define INI_RIGHTCAM_TCOUPLES QStringLiteral("rightcameraslots")

#define INI_UPS_GROUP QStringLiteral("UninterruptiblePowerSupply")
#define INI_UPS_BATTERY_STATE_TIMEOUT QStringLiteral("batteryStateTimeout")

#define INI_CHAMBER1_GROUP QStringLiteral("Chamber1Settings")
#define INI_CHAMBER2_GROUP QStringLiteral("Chamber2Settings")
#define INI_CHAMBER_OVERHEAT_TEMP QStringLiteral("chamberOverHeatTemp")
#define INI_CHAMBER_OVERFREEZE_TEMP QStringLiteral("chamberOverFreezeTemp")
#define INI_CHAMBER_REFRIGERATOR_AUTOSTOP QStringLiteral("chamberRefrigeratorAutoStop")
#define INI_CHAMBER_REFRIGERATOR_STOP_TEMP QStringLiteral("chamberRefrigeratorStopTemp")
#define INI_RACK1_GROUP QStringLiteral("Rack1Settings")
#define INI_RACK2_GROUP QStringLiteral("Rack2Settings")
#define INI_RACK_TEMP QStringLiteral("rackTemperature")
#define INI_RACK_TEMP_HH QStringLiteral("rackTemperatureHH")
#define INI_RACK_TEMP_H QStringLiteral("rackTemperatureH")
#define INI_RACK_TEMP_L QStringLiteral("rackTemperatureL")
#define INI_RACK_TEMP_LL QStringLiteral("rackTemperatureLL")
#define INI_RACK_OVERHEAT_TEMP QStringLiteral("rackOverHeatTemp")

class Settings : public QSettings
{
    Q_OBJECT

public:
    enum ChannelType
    {
        Stream,
        Alsa
    };

    struct ChannelSettings
    {
        quint32 number;
        QString name;
        QString url;
        QString scte35;
        ChannelType type;
    };

    Settings(const QString &selfPath, QObject *parent);
    virtual ~Settings();

    QString selfPath() const
    { return m_selfPath; }

    QString logPath() const
    { return value("LogPath", "").toString(); }

    qint32 numberOfChannels()
    { return childGroups().size(); }

    quint32 limitRecords() const
    { return value("limitRecords", 100).toUInt(); }

    bool processFeature() const
    { return value("processFeature", false).toBool(); }

    bool insertToDB() const
    { return value("insertToDb", false).toBool(); }

    QUuid instanceUUID() const
    { return value("instanceUuid", QUuid()).toUuid(); }

    qint32 timeZoneCorrection() const
    { return value("timeZoneCorrection", 0).toInt(); }

    QStringList channels() const
    { return childGroups(); }

    QStringList groupContents(QString groupName);
    QString groupValueContents(QString groupName, QString valueKey);

    ChannelSettings channel(const QString & identifier);
    ChannelSettings channel(const quint32 identifier);

    bool syncSettings();

    bool isSettingsOk() const
    { return m_settingsOk; }

private:
    void checkFileCorrectness();

    qint32 channelsNumber() const
    { return value("channelsNumber", 0).toInt(); }

    QString m_selfPath;
    quint16 m_checkSum{0};
    bool m_settingsOk{false};
};

#endif // SETTINGS_H
