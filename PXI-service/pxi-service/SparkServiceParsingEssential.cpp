#include <QJsonDocument>
#include <QJsonArray>
#include "config.h"
#include "SparkService.h"
#include "schedule-components/SparkSchedulePowerSupply.h"
#include "schedule-components/SparkScheduleGenerator.h"
#include "schedule-components/SparkScheduleThermocouple.h"

Bugs SparkService::parseCyclogram(quint32 nSlot, const QJsonObject & cyclogramObject)
{
    /* Пока сделаны следующие допущения и ограничения
     * 0. допущение №0 - элементы массива должны быть упорядоченны по возростанию времни. Без этого никак!
     * 1. duration - обязательное поле
     * 2. startTime - может отсутствовать, и это отсутствие будет означать для 0-го элемента "стартуй сейчас", для всех остальных "стартуй сразу после предыдущего"
     * 3. enable - может отсутствовать, если в предыдущем элементе сущестовал как true, а в случае =false присутствие обязательно
     * 4. все элементы, у которых enable  отсутствует или true все равно будут приводить к вызову on() (для PS и FG)
     * 5. обязательным является наличие в массивах последнего элемента, у которого enable = false, duration = 0, что бы выключать PS и FG
     * 6. При помощи пустых (off) блоков можно сделать прерывистое тестировани. Но off-блоки должны будут иметь не нулевой duration
     * 7. Настоятельно хочется отметить, что бы закрывающий блок с "duration=0,enable=false имел бы право появлсять только в конце массива. Во избежании
     *    перепутывания блоков с одинаковой датой. (Это вообще гворя нужно проверить - поведение QMap-овского итератора в этих случаях)
     * 8. Термопарам закрывающий блок не нужен. Зачем он им, вообще?
     */

    if (m_sparkSchedules.value(nSlot).size())
    {
        m_logger->log("Error: Trying to insert schedule to an uncompleted slot");
        return {};
    }
    Bugs errors;
    Sample *pSample = m_hardware->sample(nSlot);
    // проверка подключены ли источники питания
    if (cyclogramObject.value("powerSupplies").toArray().count() > 0)
        errors += checkPowerDevices(pSample, nSlot);
    // проверка подключены ли генераторы питания
    if (cyclogramObject.value("generator").toArray().count() > 0)
        errors += checkGenerators(pSample, nSlot);
    if (cyclogramObject.value("thermoProfile").toArray().count() > 0)
        errors += Bug("Циклограмма", nSlot + 1, Bug::Cyclogram, "Загружена термоциклограмма!");
    if (!errors.empty())
        return errors;


    //! TODO: Предусмотреть вариант, когда циклограмма на столько больная, что не возможно пускать ее в работу
    //! В этом случае уже созданный PXISlot должне быть уничтожен
    QSharedPointer<SparkSlot> currentPXISlot(new SparkSlot(m_logger, nSlot, cyclogramObject.value("chipsName").toString(), cyclogramObject.value("chipsBatch").toString()));
    m_slots.insert(nSlot, currentPXISlot);

    const QDateTime currentTime(QDateTime::currentDateTime());
    for (auto powerSupplyObject : cyclogramObject.value("powerSupplies").toArray())
    {
        quint32 nPowerSupply = powerSupplyObject.toObject().value("number").toInt();
        if (nPowerSupply >= POWER_SUPPLY_NUMBER)
        {
            logMessageString("Warning: (Power Supply) power supply number (" + QString::number(nPowerSupply) + ") out of range. That power supply will be skipped");
            continue;
        }
        QDateTime stopTime(currentTime);
        for (auto powerSupplySchedule : powerSupplyObject.toObject().value("schedule").toArray())
        {
            const QJsonObject & scheduleObject = powerSupplySchedule.toObject();

            qint64 duration = scheduleObject.value("duration").toString().toULongLong();
            if (scheduleObject.value("duration").isUndefined()
                    || scheduleObject.value("enable").isUndefined()
                    || ((duration == 0) && scheduleObject.value("enable").toBool())
                    )
            {
                logMessageString("Warning: (Power Supply) duration or enable attribute not defined or duration==0 not correspond to enable=false");
                continue;
            }

            QDateTime startTime;
            if (scheduleObject.value("startTime").isUndefined())  // для нулевого элемента это значит "начать прямо сейчас", для остальных "начать с конца предыдущего"
            {
                (startTime = stopTime);
            }
            else
            {
                (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));
            }
            stopTime = startTime.addMSecs(duration);
            if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            {
                m_sparkSchedules[nSlot].insert(
                            startTime,
                            QSharedPointer<SparkSchedulePowerSupply>(
                                new SparkSchedulePowerSupply(
                                    currentPXISlot,
                                    m_hardware,
                                    m_logger,
                                    scheduleObject.value("pollInterval").toInt(),
                                    startTime,
                                    stopTime,
                                    nPowerSupply,
                                    scheduleObject.value("voltage").toDouble(), scheduleObject.value("voltageTolerancePlus").toDouble(), scheduleObject.value("voltageToleranceMinus").toDouble(),
                                    scheduleObject.value("current").toDouble(), scheduleObject.value("currentTolerancePlus").toDouble(), scheduleObject.value("currentToleranceMinus").toDouble()
                                    )));
            }
            else
            {
                m_sparkSchedules[nSlot].insert(
                            startTime,
                            QSharedPointer<SparkSchedulePowerSupply>(
                                new SparkSchedulePowerSupply(currentPXISlot, m_hardware, m_logger, startTime, stopTime, nPowerSupply, SparkScheduleBase::ScheduleActionOff)));
            }
        }
    }

    QDateTime stopTime(currentTime);
    for (const auto & generatorObject : cyclogramObject.value("generator").toArray())
    {
        const QJsonObject & scheduleObject = generatorObject.toObject();

        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        if (scheduleObject.value("duration").isUndefined()
                || scheduleObject.value("enable").isUndefined()
                || ((duration == 0) && scheduleObject.value("enable").toBool()) ) {
            logMessageString("Warning: (Generator) duration or enable attribute not defined or duration==0 not correspond to enable=false");
            continue;
        }

        QDateTime startTime;

        (scheduleObject.value("startTime").isUndefined()) ?
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));

        stopTime = startTime.addMSecs(duration);
        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_sparkSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<SparkScheduleGenerator>(
                            new SparkScheduleGenerator(
                                currentPXISlot,
                                m_hardware,
                                m_logger,
                                scheduleObject.value("pollInterval").toInt(),
                                startTime,
                                stopTime,
                                scheduleObject.value("type").toString(),
                                scheduleObject.value("amplitude").toDouble(),
                                scheduleObject.value("offset").toDouble(),
                                scheduleObject.value("frequency").toDouble(),
                                scheduleObject.value("dutycycle").toDouble()
                                )));
        else
            m_sparkSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<SparkScheduleGenerator>(
                            new SparkScheduleGenerator(currentPXISlot, m_hardware, m_logger, startTime, stopTime, SparkScheduleBase::ScheduleActionOff)));
    }


    //++thermocouple parser
    stopTime = currentTime;
    for (const auto & thermocoupleObject : cyclogramObject.value("thermocouple").toArray())
    {
        const QJsonObject & scheduleObject = thermocoupleObject.toObject();
        qint64 duration = scheduleObject.value("duration").toString().toULongLong();
        if (scheduleObject.value("duration").isUndefined()
                || scheduleObject.value("enable").isUndefined()
                || ((duration == 0) && scheduleObject.value("enable").toBool()) ) {
            logMessageString("Warning: (Thermocouple) duration or enable attribute not defined or duration==0 not correspond to enable=false");
            continue;
        }
        QDateTime startTime;
        (scheduleObject.value("startTime").isUndefined()) ?
                    (startTime = stopTime) :
                    (startTime = QDateTime::fromString(scheduleObject.value("startTime").toString(), DB_DATETIME_FORMAT));
        stopTime = startTime.addMSecs(duration);
        m_plcConnection = nSlot < 10 ? m_plcConnectionList[0] :m_plcConnectionList[1];
        if (scheduleObject.value("enable").isUndefined() || scheduleObject.value("enable").toBool())
            m_sparkSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<SparkScheduleThermocouple>(
                            new SparkScheduleThermocouple(
                                currentPXISlot,
                                m_hardware,
                                m_plcConnection,
                                m_logger,
                                scheduleObject.value("pollInterval").toInt(),
                                startTime,
                                stopTime,
                                scheduleObject.value("temperatureMax").toDouble(),
                                scheduleObject.value("temperatureMin").toDouble()
                                )));
        else
            m_sparkSchedules[nSlot].insert(
                        startTime,
                        QSharedPointer<SparkScheduleThermocouple>(
                            new SparkScheduleThermocouple(currentPXISlot, m_hardware,m_plcConnection, m_logger, startTime, stopTime, SparkScheduleBase::ScheduleActionOff)));
    }

    // Установка длительности тестирования в данному cлоте
    const auto & current = m_sparkSchedules.constFind(nSlot);
    if (!current->isEmpty()) {
        QDateTime start = current->firstKey();
        QDateTime stop = current->lastKey().addMSecs( current->last()->duration() );
        currentPXISlot->setTestTimeFrame(start, stop);
    }

    //! TODO: Предусмотреть вариант, когда циклограмма на столько больная, что не возможно пускать ее в работу
    //! В этом случае уже созданный PXISlot должне быть уничтожен
    if (postParsing())
        logMessageString(QStringLiteral("Error: intersection"));

    return errors;
}

bool SparkService::postParsing()
{
    /*
     * Возможно, что этот метод понадобится, если придется в "ручном" режиме дорабатывать неточности циклограммы
     * Т.е. здесь можно будет дописать в график, допустим, закрывающий "off" для источника/генератора
     * Здесь можно будет дописать открывающие "on", если что-то поменяется в идеологии работы hardware
     * Здесь можно будет дописать закрывающий "off" для всех айтемов графика, после которых пустота в расписании
     */

    //!TODO: добавить проверку логических уровней hsdio для разных слотов в одной группе [0,1,2,3][4,5,6,7]

    for (auto slot = m_sparkSchedules.constBegin(); slot != m_sparkSchedules.constEnd(); ++slot) {
        auto schedule = slot.value();
        for (auto i = schedule.constBegin(); i != schedule.constEnd(); ++i) {
            QDateTime start = i.value()->startTime();
            QDateTime stop = i.value()->stopTime();

            for (auto j = schedule.constBegin(); j != schedule.constEnd(); ++j) {
                bool sameFlag = (i.value()->instanceNumber() == j.value()->instanceNumber()); // специально для того что бы не путать разные PS
                if (i.value() == j.value())
                    continue;
                if (((start > j.value()->startTime()) && (start < j.value()->stopTime()) && sameFlag)
                        || ((stop > j.value()->startTime()) && (stop < j.value()->stopTime()) && sameFlag)
                        || ((start <= j.value()->startTime()) && (stop >= j.value()->stopTime()) && sameFlag)
                        )
                    return false;
            }
        }
    }
    return false;
}

Bugs SparkService::checkPowerDevices(Sample* sample, quint32 slot)
{
    m_logger->log("Check power devices");
    Bugs errors;
    QList<Power*> samplePowerDevices = sample->powers();
    if (samplePowerDevices.count() != 0)
        for (Power * powerDevice: samplePowerDevices)
        {
            if (!powerDevice->isConnected())
            {
                Bugs res = powerDevice->reconnect(powerDevice->slotNumber(), powerDevice->getNumber() + 1);
                if (!res.empty())
                {
                    errors += res;
                    logBugs(res);
                    m_logger->dbgLog(res[0].message);
                    break;
                }
            }
            try
            {
                powerDevice->isOn();
            }
            catch (HWException & e)
            {
                m_logger->dbgLog(e.msg);
                errors += Bug(QString("Источник питания №%1").arg(powerDevice->getNumber() + 1), powerDevice->slotNumber() + 1, Bug::Power, e.msg);
                break;
            }
        }
    else
        errors += Bug("Источник питания", slot + 1, Bug::Power, " источник питания не инициализирован");
    return errors;
}

Bugs SparkService::checkGenerators(Sample* sample, quint32 slot)
{
    m_logger->log("Check generator");
    Bugs errors;
    FGen* pGenerator = sample->fgen;
    if (pGenerator != nullptr)
    {
        m_logger->log("here1");
        if (!pGenerator->isConnected())
        {
            Bugs res = pGenerator->reconnect(slot, pGenerator->getNumber() + 1);
            if (!res.empty())
            {
                m_logger->dbgLog(res[0].message);
                errors += res;
            }
        }
        else
        {
            try
            {
                pGenerator->isOn();
            }
            catch (HWException & e)
            {
                m_logger->dbgLog(e.msg);
                errors += Bug(QString("Генератор №%1").arg(pGenerator->getNumber() + 1), slot + 1, Bug::FGen, e.msg);
            }
        }
    }
    else
    {
        errors += Bug("Генератор", slot + 1, Bug::FGen, " генератор не инициализирован");
    }
    m_logger->log("here2");
    return errors;
}
