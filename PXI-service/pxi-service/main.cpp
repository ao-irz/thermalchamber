#include "SparkService.h"
#include <QDir>

int main(int argc, char *argv[])
{
    if (argc == 1) {
        qInfo() << "Spark-service, Copyright (c) 2022 AO 'IRZ' ";
        qInfo() << "Use administrator permissions for invoke";
        qInfo() << "Usage:";
        qInfo() << "PXIService -install          to install the service";
        qInfo() << "PXIService -start            to run the service";
        qInfo() << "PXIService -terminate        to stop the service";
        qInfo() << "PXIService -uninstall        to uninstall the service";
    }

    QFileInfo fileInfo(argv[0]);
    SparkService service(argc, argv, fileInfo.absolutePath());
    return service.exec();
}
