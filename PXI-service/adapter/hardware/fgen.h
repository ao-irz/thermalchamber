#ifndef FGEN_H
#define FGEN_H

#include <QString>
#include <QHash>
#include "scpi.h"
#include <QPointer>
#include <Logger.h>
#include <QThread>

namespace GenForm {
enum GenForm {
        sinus,
        square,
        triangle
    };
const QHash<GenForm, QString> items = {
    {GenForm::sinus, "SINE"},
    {GenForm::square, "SQUARE"},
    {GenForm::triangle, "RAMP"},
};
}

class Hardware;
class Sample;
class FGen;

class FGenDevice : public scpi { //внутренний класс для отдельного прибора генератора сигналов
    friend Hardware;
    friend FGen;
public:
    static const int internal_channel_count = 2;
    int getNumber() { return m_number; }
protected:
    FGenDevice(QString host_); //создаёт С++ объект
    FGenDevice(QString host_, const QPointer<Logger> &log, int number); //создаёт С++ объект
    Bugs init_c(); //инициализирует оборудование, возвращает список ошибок
    Bugs close(); //освобождает оборудование


    //logger
    QPointer<Logger> m_logger{nullptr};
    int m_number{-1};
};

class FGen { //генератор сигналов, один канал
    friend Hardware;
    friend Sample;
private:
    FGen(int affected_thermo_slot, FGenDevice * device, int internal_ch); //создаёт С++ объект
    FGenDevice * dev = nullptr;
    int slot = -1;
    int ch = -1;
    bool enabled = false;
    int cmdDelay = 30;
    Bugs close(); //закрывает канал
public:
    const QHash<GenForm::GenForm, QString> forms = {
        {GenForm::sinus, "SINE"},
        {GenForm::square, "SQUARE"},
        {GenForm::triangle, "RAMP"},
    };
    Bugs put(GenForm::GenForm type, double ampl, double dcoffset, double freq, double dutycycle); /*
        задаёт одну из базовых форм сигнала:
        type - тип формы сигнала
        ampl - амплитуда напряжения (peak-to-peak), В
        dcoffset - смещение напряжения, В
        freq - частота, Гц
        dutycycle - рабочий ход прямоугольного сигнала (Specify this attribute as a percentage
        of the time the square wave is high in a cycle), %
        */
    Bugs on(); //подключает выход канала
    bool isOn() const; //возвращает предполагаемое состояние выхода канала
		Bugs getOn(); // запрашивает с прибора состояние канала (для использования в pollHardware)
    Bugs off(); //отключает выход канала
    Bugs form(QList<int8_t> y_coordinates, double ampl, double dcoffset, double freq); /*
        задаёт произвольную форму сигнала:
        y_coordinates - вертикальные координаты одного периода сигнала
        ampl - амплитуда напряжения (peak-to-peak), В
        dcoffset - смещение напряжения, В
        freq - частота, Гц
        */
    static const GenForm::GenForm sinus = GenForm::sinus; //константа для метода put
    static const GenForm::GenForm square = GenForm::square; //константа для метода put
    static const GenForm::GenForm triangle = GenForm::triangle; //константа для метода put


    short getNumber() { return dev->getNumber(); }
    Bugs reconnect(int nSlot, int number){return dev->reconnectSocket(nSlot, number);}
    bool isConnected();

};

#endif // FGEN_H
