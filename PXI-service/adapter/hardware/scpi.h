#ifndef SCPI_H
#define SCPI_H


#include <config.h>
#include <QString>
//#include <winsock2.h>
#include <ws2tcpip.h>
#include "bugs.h"
#include <QTcpSocket>
#include <QPointer>
#include <Logger.h>

class Hardware;

class scpi{
    friend Hardware;
public:
protected:
    scpi(QString host_, int port_, int type_, QString prefix_, QString eol, unsigned long msleep_);
    scpi(QString host_, int port_, int type_, QString prefix_, QString eol, unsigned long msleep_, const QPointer<Logger> &logger);
    ~scpi();
    QString host;
    int port;
    int type;
    QString title;
    QList<int> slotes;
    QByteArray linesep;
    bool ready = false;
    int operation_timeout_ms = 1000;
    Bugs init_a(QSet<int> affected_slot_indices, int operation_timeout_ms_);
    static Bugs init_b(int connect_timeout_ms, QList<scpi*> list) noexcept;
    static Bugs wait(const QString msg, const int timeout_ms, QList<scpi*> src, bool after_write, QList<scpi*> & dst);
    static QString fmt(int ecode);
    static QString str(double src);
    Bugs to_double(QString src, double * dst);
    Bugs bugs(QString msg);
    void close();
    QString write(QString cmd);
    void writeBytes(QByteArray command);
    QString query(QString cmd);
    QString error();

    //QTCPSOCKET
    Bugs connectSocket(QSet<int> affected_slot_indices, int connectionTimeout, int operation_timeout);
    QString sendSCPICommand(QString strCommand, bool waitForResponce = false);
    Bugs reconnectSocket(int nslot = -1, int number = -1, int connectionTimeout = 3000);
    bool socketConnected(){return m_bConnected;}

private:
    void writeRaw(QByteArray cmd);
    SOCKET raw = INVALID_SOCKET;
    unsigned long msleep = 0;

    //QTCPSOCKET
    QTcpSocket* m_pSocket{nullptr};
    bool m_bConnected{false};
    void socketErrorSlot(QAbstractSocket::SocketError socketError);
    void socketChangeState(QAbstractSocket::SocketState state);
    bool m_bSockTimeoutDisabled{false};
    bool m_bForcefullyDisconnected{false};
    void socketDisconnectedSlot();
    bool writeToSocketSync(const QString& strCommand);
    void logBugs(const Bugs &val);

    //logger
    QPointer<Logger> m_logger;


};

#endif // SCPI_H
