#include "hardware.h"
#include <QtDebug>

Sample::Sample(const int power_count_, const int slot_, const QList<Power *> powers_, FGen *fgen_,const QList< Thermocouple*> thermocouples): fgen(fgen_) {
    power_count = power_count_;
    slot = slot_;
    power_chanels = powers_;
    thermocouples_list = thermocouples;
}

Sample::~Sample() {
}

Power * Sample::power(const int ch) {
    if ((ch < 0) || (ch >= power_chanels.count())) {
        qDebug() << "Sample::power: bad channel index";
        return nullptr;
    }
    return power_chanels[ch];
}

QList<Power *> Sample::powers() {
    return QList<Power *>(power_chanels);
}

QList<Thermocouple *> Sample::thermocouples()
{
    return QList<Thermocouple *>(thermocouples_list);
}

Bugs Sample::close() {
    Bugs rv;
    if (fgen) {
        rv += fgen->close();
    }
    for (auto p : power_chanels) {
        rv += p->close();
    }
    return rv;
}

int Sample::powerCount() {
    return power_count;
}

Bugs Sample::neutral() {
    Bugs rv;
    for(auto p : powers()) {
        if (!p->isConnected())
            rv += p->reconnect(slot, p->getNumber() + 1);
        else
            rv += p->off();
    }
    if (fgen) {
      if (!fgen->isConnected())
          rv += fgen->reconnect(slot, fgen->getNumber() + 1);
      else
          rv += fgen->off();
    }
    return rv;
}

Hardware::Hardware() {
}

Hardware::Hardware(const QPointer<Logger> &logger): m_logger(logger)
{
}

int Hardware::sampleCount() {
    int rv = samples.count();
    if (rv <= 0) {
        qInfo() << "Hardware::sampleCount() return" << rv;
    }
    return rv;
}

QString Hardware::configuration() {
    if (!power_config.count()) {
        return "Must be called after init(...)";
    }
    QStringList rv;
    rv += QString("power host count: %1").arg(power_config.count());
    rv += QString("fgen host count: %1").arg(fgen_config.count());
    return rv.join("; ");
}

//Bugs Hardware::init(const int slot_power_count_,
//                    const int connect_timeout_ms, const int operation_timeout_ms,
//                    const QStringList power_cfg, const QStringList fgen_cfg, QList<int> power_channel_map)
Bugs Hardware::init(const int slot_power_count_, const int connect_timeout_ms, const int operation_timeout_ms,
                    const QStringList power_cfg, const QStringList fgen_cfg, QList<int> power_channel_map,
                    const int tcouple_perslot)
{
    power_config = power_cfg;
    fgen_config = fgen_cfg;
    const int slot_power_count = std::min(slot_power_count_, MAX_SLOT_POWER_COUNT);
#if 1
    qDebug() << "Hardware::init";
    m_logger->dbgLog("Hardware::init");

    qDebug() << "slot_power_count_ ==" << slot_power_count;
    m_logger->dbgLog(QString("slot_power_count_ == %1").arg(slot_power_count));

    qDebug() << "connect_timeout_ms ==" << connect_timeout_ms;
    m_logger->dbgLog(QString("connect_timeout_ms == %1").arg(connect_timeout_ms));

    qDebug() << "operation_timeout_ms ==" << operation_timeout_ms;
    m_logger->dbgLog(QString("operation_timeout_ms == %1").arg(operation_timeout_ms));

    qDebug() << "power_cfg" << power_cfg.count() << "==" << power_cfg;
    m_logger->dbgLog(QString("power_cfg ") + QString::number(power_cfg.count()) + " == " + power_config.join(""));

    qDebug() << "fgen_cfg" << fgen_cfg.count() << "==" << fgen_cfg;
    m_logger->dbgLog(QString("fgen_cfg ") + QString::number(fgen_cfg.count()) + " == " + fgen_cfg.join(""));

    qDebug() << "power_channel_map" << power_channel_map.count();
    m_logger->dbgLog(QString("power_channel_map ") + QString::number(power_channel_map.count()));
#endif
    const int sample_count = calcSampleCount(slot_power_count);
    const int power_channel_total = power_cfg.count() * PowerDevice::channel_count;
    for (int i = (power_channel_map.count() - 1); i >= 0; --i) {
        if ((power_channel_map[i] < 0) || (power_channel_map[i] >= power_channel_total)) {
            power_channel_map.removeAt(i);
        }
    }
    Bugs rv;
    if ((power_channel_map.count() != power_channel_total) ||
            (QSet<int>::fromList(power_channel_map).count() != power_channel_total)) {
        qDebug() << "power_channel_map" << power_channel_map.count() << power_channel_total
                 << power_channel_map;
        auto msg = QString("Bad power_channel_map");
        msg += "power_channel_map:" + QString::number(power_channel_map.count())  + " power_channel_total " + QString::number(power_channel_total);
        for (int idx = 0; idx < sample_count; ++idx) {
            rv.append(Bug("Config", idx, Bug::Power, msg));
        }
        return rv;
    }
    QList<scpi*> devs;
    int used_pdev = power_cfg.count();
    qDebug() << "sample_count" << sample_count << "|" << "used_pdev" << used_pdev << "|"
             << "power_channel_map.count()" << power_channel_map.count();
    int extra_power_index = -1;
    for (int i = 0; i < used_pdev; ++i)
    {
        QSet<int> affected_slot_indices;
        auto cfg = power_cfg[i];
        //        auto pd = new PowerDevice(cfg);
        auto pd = new PowerDevice(cfg, m_logger, i);
        pdevs.append(pd);
        devs.append(pd);
        for (int j = 0; j < power_channel_map.count(); ++j) {
            if (i == (power_channel_map[j] / PowerDevice::channel_count)) {
                int slot_index = j / slot_power_count;
                //qDebug() << "sample_count" << sample_count << "slot_index" << slot_index;
                if (slot_index < sample_count) {
                    affected_slot_indices += slot_index;
                } else {
                    affected_slot_indices += extra_power_index;
                    --extra_power_index;
                }
            }
        }
        //qDebug() << "affected_slot_indices" << affected_slot_indices;
        //        rv += pd->init_a(affected_slot_indices, operation_timeout_ms);
        rv += pd->connectSocket(affected_slot_indices,connect_timeout_ms, operation_timeout_ms);
    }
    if (fgen_cfg.count()) {
        used_pdev = sample_count / FGenDevice::internal_channel_count +
                ((sample_count % FGenDevice::internal_channel_count) ? 1 : 0);
        for (int i = 0; i < used_pdev; ++i) {
            QSet<int> affected_slot_indices;
            auto host = fgen_cfg[i];
            int slot = FGenDevice::internal_channel_count * i;
//            auto gd = new FGenDevice(host);
            auto gd = new FGenDevice(host, m_logger, i);
            gdevs.append(gd);
            devs.append(gd);
            for (int idx = slot; idx < (slot + FGenDevice::internal_channel_count); ++idx) {
                if ((0 <= idx) && (idx < sample_count)) {
                    affected_slot_indices += idx;
                }
            }
//            rv += gd->init_a(affected_slot_indices, operation_timeout_ms);
            rv += gd->connectSocket(affected_slot_indices, connect_timeout_ms, operation_timeout_ms);
        }
    }
//    rv += scpi::init_b(connect_timeout_ms, devs);
    for (auto pd : pdevs) {
        rv += pd->init_c();
    }

    for (auto gd : gdevs) {
        rv += gd->init_c();
    }
		for (int slot = 0; slot < sample_count; ++slot)
		{
        QList<Power*> sample_power_list;
        for (int i = 0; i < slot_power_count; ++i) {
            if (power_channel_map.isEmpty()) {
                auto msg = QString("power_device_channel_index.isEmpty() -> init break");
                for (int idx = 0; idx < sample_count; ++idx) {
                    rv.append(Bug("?", idx, Bug::Power, msg));
                }
                return rv;
            }
            int linear = power_channel_map.takeFirst();
            auto pd = pdevs[linear / PowerDevice::channel_count];
            int internal_power_channel = linear % PowerDevice::channel_count;
            //qDebug() << "slot" << slot << "pd" << pd->title
            //         << "internal_power_channel" << internal_power_channel;
            sample_power_list.append(new Power(slot, pd, internal_power_channel));
        }
        FGen * fgen = nullptr;
        if (fgen_cfg.count()) {
            fgen = new FGen(slot, gdevs[slot / FGenDevice::internal_channel_count],
                    slot % FGenDevice::internal_channel_count);
        }
        //TODO: добавить термопары
        QList<Thermocouple*> sample_thermocouples;
        for (int i = 0; i < tcouple_perslot; i++)
        {
            Thermocouple *tcouple = new Thermocouple(slot, 0.0, 0.0);
            thermocouples.append(tcouple);
            sample_thermocouples.append(tcouple);
        }
        samples.append(new Sample(slot_power_count, slot, sample_power_list, fgen, sample_thermocouples));
    }
    while (!power_channel_map.isEmpty()) {
        int slot = -extra_powers.count() - 1;
        //qDebug() << "slot" << slot;
        int linear = power_channel_map.takeFirst();
        auto pd = pdevs[linear / PowerDevice::channel_count];
        int internal_power_channel = linear % PowerDevice::channel_count;
        auto p = new Power(slot, pd, internal_power_channel);
        extra_powers.append(p);
        internal_power_channel ++;
        internal_power_channel %= PowerDevice::channel_count;
    }

    qDebug() << "Hardware::init() finished with" << rv.count() << "errors;"
             << "Hardware::samples::count() ==" << samples.count();

    m_logger->dbgLog(QString("Hardware::init() finished with %1 errors;"
                             "Hardware::samples::count() == %2").arg(rv.count()).arg(samples.count()));
    return rv;
}

Sample * Hardware::sample(const int slot) {
    if ((slot < 0) || (slot >= samples.count())) {
        qInfo() << "Hardware::sample" << "bad sample slot" << slot;
        return nullptr;
    }
    return samples[slot];
}

Bugs Hardware::close() {
    Bugs rv;
    while (!samples.isEmpty()) {
        auto s = samples.takeLast();
        rv += s->close();
        if (s->fgen) {
            delete s->fgen;
        }
        delete s;
    }    
    while (!extra_powers.isEmpty()) {
        auto s = extra_powers.takeLast();
        rv += s->close();
    }
    while (!pdevs.isEmpty()) {
        auto pd = pdevs.takeLast();
        rv += pd->close();
        delete pd;
    }
    while (!gdevs.isEmpty()) {
        auto gd = gdevs.takeLast();
        rv += gd->close();
        delete gd;
    }
    return rv;
}

int Hardware::extraPowerCount() {
    return extra_powers.count();
}

Power *Hardware::extraPower(const int idx) {
    if ((idx < 0) || (idx >= extra_powers.count())) {
        qInfo() << "Hardware::extraPower" << "bad index" << idx;
        return nullptr;
    }
    return extra_powers[idx];
}

int Hardware::calcSampleCount(const int slot_power_count) {
    int rv = (power_config.count() * PowerDevice::channel_count) / slot_power_count;
    rv = std::min(rv, MAX_SLOT_COUNT);
    if (fgen_config.count()) {
        rv = std::min(rv, fgen_config.count() * FGenDevice::internal_channel_count);
    }
    //qDebug() << "sample_count" << rv;
    return rv;
}
