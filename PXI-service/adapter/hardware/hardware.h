#ifndef ITEMS_H
#define ITEMS_H

#include "bugs.h"
#include "power.h"
#include "fgen.h"
#include "thermocouple.h"
#include <Logger.h>
#include "../../PXI-common/common.h"
#include "hardwareparams.h"
#include <QStringList>
#include <QPointer>
#include <QObject>

class Hardware;

class Sample { // оборудование для тестирования определённого слота
    friend Hardware;
private:
    explicit Sample(const int power_count_, const int slot_, const QList<Power *> powers_, FGen * fgen_, QList<Thermocouple *> thermocouples); // создание объекта по индексу слота
    ~Sample();
    int power_count; // количество каналов питания в одном слоте
    int slot = -1; // индекс слота
    Bugs init(); // инициализация оборудования
    Bugs close(); // освобождение ресурсов оборудования
    QList<Power *> power_chanels;
    QList<Thermocouple *> thermocouples_list;
public:
    int powerCount(); // количество каналов питания в одном слоте
    Power * power(const int ch); // объект "питание" по индексу канала питания
    QList<Power *> powers(); // список всех объектов "питание" этого слота
    FGen * fgen = nullptr; // объект "генератор" этого слота
    QList<Thermocouple *> thermocouples();
    Bugs neutral();
};

class Hardware: public QObject {
    Q_OBJECT
public:
    Hardware(); // создание объекта
    Hardware(const QPointer<Logger> &logger);
    QString configuration(); // краткая информация о конфигурации
//    Bugs init(const int slot_power_count_,
//              const int connect_timeout_ms, const int operation_timeout_ms,
//              const QStringList power_cfg, const QStringList fgen_cfg,
//              QList<int> power_channel_map //номера каналов источников питания в порядке использования в слотах
//              ); // инициализация оборудования
    Bugs init(const int slot_power_count_,
                  const int connect_timeout_ms, const int operation_timeout_ms,
                  const QStringList power_cfg, const QStringList fgen_cfg,
                  QList<int> power_channel_map, //номера каналов источников питания в порядке использования в слотах
                  const int tcouple_perslot
                  ); // инициализация оборудования
    Bugs close(); // освобождение ресурсов оборудования
    int sampleCount(); // количество слотов
    Sample * sample(const int slot); // объект Sample по индексу слота или nullptr
    /*
    Bugs test();
            проверка всех слотов на экстраординарные неисправности (КЗ и т.п.),
            метод возможно ненужен, т.к. вероятно невозможно определить неисправность
            вне контекста циклограммы

    Ethernet имеет радикально большую задержку, нежели PXI, поэтому
    отдельный метод test() для опроса общего состояния очень сильно
    затормозит систему.

    Этот метод реализовывать нецелесообразно. Проверка на особенные ситуации можно сделать
    тогда же, когда проверяются иные режимы работы устройств.
    */
    int extraPowerCount(); // количество дополнительных, общих для стойки каналов питания,
                           // нулевое в некоторых конфигурациях
    Power * extraPower(const int idx); // дополнительный канал питания по его индексу или nullptr

    //logger
    QPointer<Logger> m_logger;
signals:
    void criticalErrorMessage(QString msg);


private:
    int calcSampleCount(const int slot_power_count);
    QList<PowerDevice *> pdevs;
    QList<FGenDevice *> gdevs;
    QList<Sample *> samples;
    QStringList power_config;
    QStringList fgen_config;
    QList<Power *> extra_powers;
    //термопары
    QList<Thermocouple *> thermocouples;
};

#endif // ITEMS_H
