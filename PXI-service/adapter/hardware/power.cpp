#include "power.h"
#include <QtDebug>

//PowerDevice::PowerDevice(QString host_) : scpi(host_, 80, Bug::Power, "Power", "", 1) {
//}

PowerDevice::PowerDevice(QString host_) : scpi(host_, 80, Bug::Power, "Power", "", 1) {
}
#ifdef PSU_EMULATOR
PowerDevice::PowerDevice(QString host_, const QPointer<Logger> &log, int number) : scpi(host_, 5025, Bug::Power, QString("Источник питания №%1").arg(number + 1), "", 1, log), m_logger(log), m_number(number) {
}
#else
PowerDevice::PowerDevice(QString host_, const QPointer<Logger> &log, int number) : scpi(host_, 80, Bug::Power, QString("Источник питания №%1").arg(number + 1), "", 1, log), m_logger(log), m_number(number) {
}
#endif

Bugs PowerDevice::init_c() {
    qDebug() << "PowerDevice::init_c" << title << "ready" << ready;
    m_logger->dbgLog(QString("PowerDevice::init_c %1 ready %2").arg(title).arg(ready));

    if (ready) {
        try {
            QString cmd = "*IDN?";
//            auto idn = query(cmd);
            QString idn = sendSCPICommand(cmd, true);
            if (!idn.contains("KIP,B5-120,")) {
                return bugs(QString("bad idn `%1`").arg(idn));
            }
        } catch (HWException hwe) {
            return bugs(hwe.msg + "HOST:" + host + ":" + QString::number(port));
        }
    }
    return {};
}

Bugs PowerDevice::close() {
    scpi::close();
    return {};
}

Power::Power(int affected_thermo_slot, PowerDevice * device, int internal_device_channel) {
    slot = affected_thermo_slot;
    dev = device;
    ch = internal_device_channel;
    //qDebug() << "Power::Power" << "slot" << slot << dev->title << "intch" << ch;
    if (!dev->slotes.contains(slot)) {
        qInfo() << "Power::Power slot" << slot << "not in dev->slotes" << dev->slotes;
    }
}

Bugs Power::on() {
    try {
#ifndef PSU_EMULATOR
        QString cmd = QString("SOUR%1:OUTP 1").arg(ch + 1);
//        check(dev->write(QString("SOUR%1:OUTP 1").arg(ch + 1)));
        //команда на включение для eez psu emulator
#else
        QString cmd = QString("OUTP ON, CH%1").arg(ch + 1);
#endif
    // команда для TDKLAMBDA
//        QString cmd = QString("OUTP ON");
        dev->sendSCPICommand(cmd);
        check(cmd);
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    enabled = true;
    return {};
}

bool Power::isOn() {
    // Возможно, более правильно использовать значение, прочитанное из прибора
    // команда для эмулятора, возможно на реальном устройстве она будет другой
#ifndef PSU_EMULATOR
    QString cmd = QString("SOUR%1:OUTP?").arg(ch +1);
#else
    QString cmd = QString("OUTP? CH%1").arg(ch +1);
#endif
    QString res = dev->sendSCPICommand(cmd, true);
//    this->check(cmd);
    if (res == "1")
        return true;
    else if (res == "0")
        return false;
    else
    {
        throw HWException(QString(dev->title + ":UNDEFINED RESPONSE:`%1` for `%2`").arg(res).arg(cmd));
        return false;
    }
}

Bugs Power::off() {
    try {
#ifndef PSU_EMULATOR
        QString cmd = QString("SOUR%1:OUTP 0").arg(ch + 1);
//        check(dev->write(QString("SOUR%1:OUTP 0").arg(ch + 1)));
        //команда на выключение для eez psu emulator
#else
        QString cmd = QString("OUTP OFF, CH%1").arg(ch + 1);
#endif
        dev->sendSCPICommand(cmd);
        check(cmd);
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    enabled = false;
    return {};
}

Bugs Power::put(double _u, double _i) {
    try {
        //        check(dev->write(QString("SOUR%1:CURR 0.5").arg(ch + 1)));
        //        check(dev->write(QString("SOUR%1:VOLT 0.5").arg(ch + 1)));
        //        check(dev->write(QString("SOUR%1:CURR %2").arg(ch + 1).arg(dev->str(_i))));
        //        check(dev->write(QString("SOUR%1:VOLT %2").arg(ch + 1).arg(dev->str(_u))));
        QString setCurr = "SOUR%1:CURR %2";
        QString setVolt = "SOUR%1:VOLT %2";
        dev->sendSCPICommand(setCurr.arg(ch + 1).arg(0.5));
        check(setCurr.arg(ch + 1).arg(0.5));

        dev->sendSCPICommand(setVolt.arg(ch + 1).arg(0.5));
        check(setVolt.arg(ch + 1).arg(0.5));

        dev->sendSCPICommand(setCurr.arg(ch + 1).arg(dev->str(_i)));
        check(setCurr.arg(ch + 1).arg(dev->str(_i)));

        dev->sendSCPICommand(setVolt.arg(ch + 1).arg(dev->str(_u)));
        check(setVolt.arg(ch + 1).arg(dev->str(_u)));
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    return {};
}

Bugs Power::voltage(double * result) {
    QString tmp;
    // понять как отлавливать отвал оборудования? нужна ли данная конструкция с реконнектом?
        try {
    //        tmp = dev->query(QString("SOUR%1:MEAS:VOLT?").arg(ch + 1));
#ifndef PSU_EMULATOR
            QString cmd = QString("SOUR%1:MEAS:VOLT?").arg(ch + 1);
            ////команда на проверку напряжения для eez psu emulator
#else
            QString cmd = QString("MEAS:VOLT? CH%1").arg(ch + 1);
#endif
            tmp = dev->sendSCPICommand(cmd, true);
            check(cmd);
        } catch (HWException hwe) {
            return {Bug(dev->title, slot, dev->type, hwe.msg)};
        }
        return dev->to_double(tmp.section(" ",0,0), result);
}

Bugs Power::current(double * result) {
    QString tmp;
    try {
//        tmp = dev->query(QString("SOUR%1:MEAS:CURR?").arg(ch + 1));
#ifndef PSU_EMULATOR
        QString cmd = QString("SOUR%1:MEAS:CURR?").arg(ch + 1);
//        команда на проверку тока для eez psu emulator
#else
        QString cmd = QString("MEAS:CURR? CH%1").arg(ch + 1);
#endif
        tmp = dev->sendSCPICommand(cmd, true);
        check(cmd);
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    return dev->to_double(tmp.section(" ",0,0), result);
}

Bugs Power::measure(double * voltage_, double * current_) {
    return voltage(voltage_) + current(current_);
    // or https://en.wikipedia.org/wiki/Standard_Commands_for_Programmable_Instruments#Concatenating_commands
}

bool Power::hasRemote() {
    return has_remote;
}

Bugs Power::getRemote(bool * value) {
    *value = false;
    return {};
}

Bugs Power::putRemote(bool value) {
    (void)value;
    return {Bug(dev->title, slot, dev->type, "Power::putRemote() not supported")};
}

QString Power::devChTitle() {
    return QString::number(ch + 1);
}

Bugs Power::close() {
    if (dev->ready) {
        return off() + put(0.5, 0.5);
    }
    return {};
}

void Power::check(QString cmd) {
    QString errorCmd = "SYST:ERR?";
//    auto rc = dev->query(errorCmd).trimmed();
    //TODO: проверить работу на реальном железе
    QString result = dev->sendSCPICommand(errorCmd, true);
    // rc:
    // 1 - ошибка синтаксиса
    // 2 - ошибка данных
    // 3 - выход за пределы
    // 4 - ошибка ответа от ячейки
#ifndef PSU_EMULATOR
    if (result[0] != "0") {
#else
    if (!result.contains("No error")) {
#endif
        throw HWException(QString("SYST:ERR `%1` for `%2`").arg(result).arg(cmd));
    }
}

double Power::limitI(double u) {
    return (u <= 7.5 ) ? 2.0 : (15.0 / u);
}

bool Power::isConnected()
{
    return dev->socketConnected();
}

/*
Bugs SMU::init_c() {
    Bugs bugs;
    if (bugs.count()) return bugs;
    if (hasRemote()) {
        bugs += putRemote(true);
    } else {
        bugs.append(Bug(dev->title, slot, Bug::Power, "Not a SMU"));
    }
    return bugs;
}
*/
