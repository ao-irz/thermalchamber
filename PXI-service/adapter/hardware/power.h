#ifndef POWER_H
#define POWER_H

#include "scpi.h"
#include <QPointer>
#include <Logger.h>

class Hardware;
class Sample;
class Power;

class PowerDevice : public scpi { //питание (одно устройство)
    friend Hardware;
    friend Power;
public:
#ifndef PSU_EMULATOR
    static const int channel_count = 20; //количество внутренних каналов в одном устройстве
#else
    static const int channel_count = 11;
#endif
    int getNumber() { return m_number ;}
protected:
    using scpi::scpi;
    PowerDevice(QString host_);
    PowerDevice(QString host_, const QPointer<Logger> &logger, int number);
    Bugs init_c(); //инициализирует оборудование, возвращает список ошибок
    Bugs close(); //освобождает оборудование

    //logger
    QPointer<Logger> m_logger{nullptr};
    int m_number;

};

/*
Примечания к системе команд "Б5-120"

1. Установка интернет адреса предусматривает установление трёх величин:
собственно адреса, маски подсети, шлюза по-умолчанию.
2. В каком состоянии находится блок питания сразу после его собственного включения?
3. Какая команда сбрасывает состояние блока питания и переводит его в состояние
блока питания сразу после его собственного включения?
*/

class Power { //питание (один канал)
    friend Hardware;
    friend Sample;
protected:
    PowerDevice * dev = nullptr;
    int slot = -1;
    int ch = -1;
    bool enabled = false;
    bool has_remote = false;
    Power(int affected_thermo_slot, PowerDevice * device, int internal_device_channel); //создаёт С++ объект
    Bugs close(); //закрывает канал
    void check(QString cmd);
public:
    Bugs on(); //подключает выход канала
    bool isOn(); //возвращает предполагаемое состояние выхода канала
    Bugs off(); //отключает выход канала
    Bugs put(double u, double i); //задаёт выходное напряжение и оганичение по току
    Bugs voltage(double * result); //возвращает список ошибок, напряжение по ссылке
    Bugs current(double * result); //возвращает список ошибок, ток по ссылке
    Bugs measure(double * voltage_, double * current_); //возвращает список ошибок; напряжение, ток по ссылке
    bool hasRemote(); //есть ли режим выносного измерения (SMU, Source Measure Unit)
    Bugs getRemote(bool * value); //возвращает по ссылке режим выносного измерения
    Bugs putRemote(bool value); //задаёт режим выносного измерения
    QString devChTitle(); //внутреннее название применяемого канала устройства (1..20)
    static double limitI(double u); //ограничение тока для напряжению (по документации)

    // возвращает состояние подключения сокета
    bool isConnected();
    int slotNumber(){return slot;}
    short getNumber() { return dev->getNumber(); }
    int getChannel(){return ch;}
    Bugs reconnect(int nSlot, int number){return dev->reconnectSocket(nSlot, number);}
};

class SMU : public Power { //канал питания, используемый как измеритель тока
public:
    using Power::Power;
    Bugs init_c(); //инициализирует оборудование, возвращает список ошибок
};

/*
Для получения измеренной силы тока SMU слота s следует делать:

double a;
bugs += pxi->sample(s)->smu->current(&a);

Никакие другие методы и свойства объекта smu использовать не следует.
*/
#endif // POWER_H
