#include <QtDebug>
#include "fgen.h"

FGenDevice::FGenDevice(QString host_) : scpi(host_, 5025, Bug::FGen, "FGen", "\n", 1) {
}

FGenDevice::FGenDevice(QString host_, const QPointer<Logger> &log, int number) : scpi(host_, 5025, Bug::FGen, QString("Генератор №%1").arg(number + 1), "\n", 1, log), m_logger(log), m_number(number){
}

Bugs FGenDevice::init_c() {
    qDebug() << "FGenDevice::init_c" << ready;
    m_logger->dbgLog(QString("FGenDevice::init_c %1").arg(ready));
    if (ready) {
        try {
            QString cmd = "*IDN?";
//            auto idn = query("*IDN?");
            QString idn = sendSCPICommand(cmd, true);
            if (!idn.contains("SDG2XCAX")) {
                return bugs(QString("bad idn `%1`").arg(idn));
            }
//            write("NBFM PNT,DOT");
//            write("NBFM SEPT,OFF");
            sendSCPICommand("NBFM PNT,DOT");
            sendSCPICommand("NBFM SEPT,OFF");
        } catch (HWException hwe) {
            return bugs(hwe.msg);
        }
    }
    return {};
}

Bugs FGenDevice::close() {
    Bugs rv;
    if (ready) {
        try {
//            write("*RST");
            sendSCPICommand("*RST");
        } catch (HWException hwe) {
            rv += bugs(hwe.msg);
        }
    }
    scpi::close();
    return rv;
}

FGen::FGen(int affected_thermo_slot, FGenDevice * device, int internal_ch) {
    slot = affected_thermo_slot;
    dev = device;
    ch = internal_ch;
    if (!dev->slotes.contains(slot)) {
        qInfo() << "FGen::FGen slot" << slot << "not in dev->slotes" << dev->slotes;
    }
}

Bugs FGen::close()
{
		if (dev->ready)
		{
            return off();
        }
    return {};
}

Bugs FGen::put(GenForm::GenForm type, double ampl, double dcoffset, double freq, double dutycycle) {
    QString f;
    if (GenForm::items.contains(type)) {
        f = GenForm::items[type];
    } else {
        return {Bug(dev->title, slot, dev->type, QString("Bad form %1").arg(type))};
    }
    try {
//        dev->write(QString("C%1:BaSic_WaVe WVTP,%2").arg(ch + 1).arg(f));
        dev->sendSCPICommand(QString("C%1:BaSic_WaVe WVTP,%2").arg(ch + 1).arg(f));
        QThread::msleep(cmdDelay);
//        dev->write(QString("C%1:BaSic_WaVe AMP,%2").arg(ch + 1).arg(dev->str(ampl)));
        dev->sendSCPICommand(QString("C%1:BaSic_WaVe AMP,%2").arg(ch + 1).arg(dev->str(ampl)));
        QThread::msleep(cmdDelay);
//        dev->write(QString("C%1:BaSic_WaVe OFST,%2").arg(ch + 1).arg(dev->str(dcoffset)));
        dev->sendSCPICommand(QString("C%1:BaSic_WaVe OFST,%2").arg(ch + 1).arg(dev->str(dcoffset)));
        QThread::msleep(cmdDelay);
//        dev->write(QString("C%1:BaSic_WaVe FRQ,%2").arg(ch + 1).arg(dev->str(freq)));
        dev->sendSCPICommand(QString("C%1:BaSic_WaVe FRQ,%2").arg(ch + 1).arg(dev->str(freq)));
        QThread::msleep(cmdDelay);
//        dev->write(QString("C%1:BaSic_WaVe DUTY,%2").arg(ch + 1).arg(dev->str(dutycycle)));
        dev->sendSCPICommand(QString("C%1:BaSic_WaVe DUTY,%2").arg(ch + 1).arg(dev->str(dutycycle)));
        QThread::msleep(cmdDelay);
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    return {};
}

Bugs FGen::on() {
    try {
//        dev->write(QString("C%1:OUTP ON").arg(ch + 1));
        QString cmd = QString("C%1:OUTP ON").arg(ch + 1);
        dev->sendSCPICommand(cmd);
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    enabled = true;
    return {};
}

bool FGen::isOn() const
{
    QString tmp;
    QString cmd = QString("C%1:OUTP?").arg(ch + 1);
    tmp = dev->sendSCPICommand(cmd, true);
//    enabled = tmp.toUpper().contains("OUTP ON");
    if (tmp.toUpper().contains("OUTP ON"))
        return true;
    else if (tmp.toUpper().contains("OUTP OFF"))
        return false;
    else
    {
        throw HWException(QString(dev->title+ ":UNDEFINED RESPONSE:`%1` for `%2`").arg(tmp).arg(cmd));
        return false;
    }
}

Bugs FGen::getOn()
{
	Bugs retVal;
	QString tmp;
	try
	{
//			tmp = dev->query(QString("C%1:OUTP?").arg(ch + 1));
      QString cmd = QString("C%1:OUTP?").arg(ch + 1);
      tmp = dev->sendSCPICommand(cmd, true);
      enabled = tmp.toUpper().contains("OUTP ON");
      if (tmp == "")
        throw HWException(QString("EMPTY RESPONSE:`%1` for `%2`").arg(tmp).arg(cmd));
	}
	catch (HWException hwe)
	{
		enabled = false;
		return {Bug(dev->title, slot, dev->type, hwe.msg)};
	}

	return {};
}

Bugs FGen::off()
{
    try {
//        dev->write(QString("C%1:OUTP OFF").arg(ch + 1));
        dev->sendSCPICommand(QString("C%1:OUTP OFF").arg(ch + 1));
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    enabled = false;
    return {};
}

Bugs FGen::form(QList<int8_t> y_coordinates, double ampl, double dcoffset, double freq) {
    auto name = QString("arb_c%1").arg(ch + 1);
    QByteArrayList qbl;
    qbl += QString("C%1:WVDT WVNM,%2,FREQ,%3,AMPL,%4,OFST,%5,PHASE,0.0,WAVEDATA,")
            .arg(ch + 1)
            .arg(name)
            .arg(dev->str(freq))
            .arg(dev->str(ampl))
            .arg(dev->str(dcoffset))
            .toUtf8();
    char cp[2];
    for (int16_t sp : y_coordinates) {
        uint16_t up = static_cast<uint16_t>(sp);
        cp[0] = static_cast<char>(uint8_t((up & 0xFFFF) >> 8));
        cp[1] = static_cast<char>(uint8_t(up & 0xFF));
        qbl += QByteArray(cp, 2);
    }
    try {
//        dev->writeBytes(qbl.join(""));
//        dev->write(QString("C%1:ARWV NAME,%2").arg(ch + 1).arg(name));
        dev->sendSCPICommand(qbl.join(""));
        dev->sendSCPICommand(QString("C%1:ARWV NAME,%2").arg(ch + 1).arg(name));
    } catch (HWException hwe) {
        return {Bug(dev->title, slot, dev->type, hwe.msg)};
    }
    return {};
}

bool FGen::isConnected()
{
    return dev->socketConnected();
}
