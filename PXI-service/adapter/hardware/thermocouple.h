#ifndef THERMOCOUPLE_H
#define THERMOCOUPLE_H

#define THERMOCOUPLE_NORMAL_STATUS 1

class Hardware;
class Sample;

class Thermocouple  {
    friend Hardware;
    friend Sample;
public:


    Thermocouple(int slot, float tempMax, float tempMin);

    void set_temperatureMax(float val){ m_temperatureMax = val;}
    void set_temperatureMin(float val){ m_temperatureMin = val;}
    void set_currentTemperature(float val){m_currentTemperature = val;}
    void set_thermocoupleState(short val){ m_status = val;}

    float temperatureMax(){return m_temperatureMax;}
    float temperatureMin(){return m_temperatureMin;}
    float currentTmperature(){return m_currentTemperature;}
    short getStatus(){return m_status;}
    int getSlot(){return m_slot;}



private:
    int m_slot {-1};
    short m_status {0};
    bool m_enabled {false};
    float m_temperatureMax {0.0};
    float m_temperatureMin {0.0};
    float m_currentTemperature {0.0};
};

#endif // THERMOCOUPLE_H
