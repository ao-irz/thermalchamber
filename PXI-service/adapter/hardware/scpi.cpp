#include "scpi.h"
#include <QThread>
#include <QElapsedTimer>
#include <QtDebug>
#include <algorithm>

//static bool wsa_inited = false;
//static WSADATA wsa_data;
static const int buff_size = 512;

scpi::scpi(QString host_, int port_, int type_, QString prefix_, QString eol, unsigned long msleep_) {
    host = host_;
    port = port_;
    type = type_;
    title = prefix_ + "_" + host_;
    linesep = eol.toUtf8();
    msleep = msleep_;
}

scpi::scpi(QString host_, int port_, int type_, QString prefix_, QString eol, unsigned long msleep_, const QPointer<Logger> &logger):
    m_logger(logger)
{
    host = host_;
    port = port_;
    type = type_;
//    title = prefix_ + "_" + host_;
    title = prefix_;
    linesep = eol.toUtf8();
    msleep = msleep_;
}


scpi::~scpi() {
}

void ms_to_timeval(int millisecond, timeval * out) {
    div_t tmp = div(millisecond, 1000);
    out->tv_sec = tmp.quot;
    out->tv_usec = tmp.rem * 1000;
}

Bugs scpi::init_a(QSet<int> affected_slot_indices, int operation_timeout_ms_)
{
    Q_UNUSED(affected_slot_indices)
    Q_UNUSED(operation_timeout_ms_)
    /*slotes = affected_slot_indices.toList();
    operation_timeout_ms = operation_timeout_ms_;
    std::sort(slotes.begin(), slotes.end());
    int rc;
    if (!wsa_inited) {
        rc = WSAStartup(MAKEWORD(2,2), &wsa_data);
        if (rc != 0) {
            return bugs(QString("WSAStartup() error %1").arg(rc));
        }
        wsa_inited = true;
        //qDebug() << "default SCPI port" << port;
    }
    struct addrinfo * result = nullptr, * ptr = nullptr, hints;

    ZeroMemory(&hints, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    auto t = host.toUtf8();
    rc = getaddrinfo(t.data(), QString::number(port).toUtf8(), &hints, &result);
    if (rc != 0) {
        return bugs(QString("getaddrinfo() error %1").arg(fmt(rc)));
    }
    ptr = result;
    raw = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (raw == INVALID_SOCKET) {
        rc = WSAGetLastError();
        freeaddrinfo(result);
        return bugs(QString("socket() error %1").arg(fmt(rc)));
    }

    u_long nblk = 1;
    if (ioctlsocket(raw, long(FIONBIO), &nblk) != 0) {
        rc = WSAGetLastError();
        freeaddrinfo(result);
        return bugs(QString("ioctlsocket(%1) error %2").arg(nblk).arg(fmt(rc)));
    }

    rc = connect(raw, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen));
    freeaddrinfo(result);
    if (raw == INVALID_SOCKET) {
        return bugs(QString("Unable to connect to server"));
    }
    qDebug() << "scpi::init_a" << title << "done" << slotes;
    return {};*/
    return{};
}

Bugs scpi::init_b(int connect_timeout_ms, QList<scpi *> list) noexcept {
    QList<scpi *> done;
    Bugs rv = scpi::wait("connect()", connect_timeout_ms, list, true, done);
    for (auto dev : done) {
        dev->ready = true;
        BOOL tcpnd = TRUE;
        int rc = setsockopt(dev->raw, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char*>(&tcpnd), sizeof tcpnd);
        if (rc != 0) {
            rc = WSAGetLastError();
            rv += dev->bugs(QString("setsockopt() TCP_NODELAY error %1").arg(fmt(rc)));
        }
    }
    qDebug() << "scpi::init_b" << "*" << "done";
    return rv;
}

Bugs scpi::wait(const QString msg, const int timeout_ms, QList<scpi *> src, bool after_write, QList<scpi *> & dst) {
    Bugs rv;
    if (src.size() > FD_SETSIZE) {
        auto e = QString("FD_SETSIZE (%1) error (%2)").arg(FD_SETSIZE).arg(src.size());
        for (auto dev : src) {
            rv += dev->bugs(e);
        }
        return rv;
    }
    QElapsedTimer et;
    timeval time_out;
    fd_set setT, setE;
    QList<scpi *> src_new;

    et.start();
    while (src.count()) {
        auto elapsed = et.elapsed();
        if (elapsed > timeout_ms) {
            break;
        }
        src_new.clear();
        ms_to_timeval(timeout_ms - static_cast<int>(elapsed), &time_out);
        FD_ZERO(&setT);
        FD_ZERO(&setE);
        for (auto s : src) {
            FD_SET(s->raw, &setT);
            FD_SET(s->raw, &setE);
        }
        int rc;
        if (after_write) {
            rc = select(0, nullptr, &setT, &setE, &time_out);
        } else {
            rc = select(0, &setT, nullptr, &setE, &time_out);
        }
        if (rc == SOCKET_ERROR) {
            auto err = fmt(WSAGetLastError());
            for (auto dev : src) {
                rv += dev->bugs("select() error " + err);
            }
        }
        for (auto dev : src) {
            if (FD_ISSET(dev->raw, &setT)) {
                dst.append(dev);
            } else if (FD_ISSET(dev->raw, &setE)) {
                QString err = dev->error();
                if (!err.size()) {
                    err = "`Timeout`";
                }
                rv += dev->bugs(QString("%1 error %2").arg(msg).arg(err));
            } else {
                src_new.append(dev);
            }
        }
        for (auto dev : src) {
            FD_CLR(dev->raw, &setT);
            FD_CLR(dev->raw, &setE);
        }
        src = src_new;
    }
    for(auto dev : src_new) {
        rv += dev->bugs(QString("%1 error `Timeout`").arg(msg));
    }
    return rv;
}

void scpi::close() {
//    closesocket(raw);
//    raw = INVALID_SOCKET;

    //QTCPSOCKET CLOSE
    m_bForcefullyDisconnected = true;

    if (m_pSocket && m_bConnected)
    {
        m_pSocket->disconnectFromHost();

        m_pSocket->disconnect();
        delete m_pSocket;
        m_pSocket = nullptr;
        m_bConnected = false;
    }
}

QString scpi::write(QString cmd) {
    writeRaw(cmd.toUtf8());
    QThread::currentThread()->msleep(msleep);
    return cmd;
}

void scpi::writeBytes(QByteArray command) {
    writeRaw(command);
    QThread::currentThread()->msleep(msleep);
}

void scpi::writeRaw(QByteArray cmd) {
    QByteArray data = cmd + linesep;
    qDebug() << QString("%1::write <- ").arg(title).toUtf8() + cmd;
    int rc;
    rc = send(raw, data.data(), data.size(), 0);
    if (rc == SOCKET_ERROR) {
        rc = WSAGetLastError();
        throw HWException(QString("write(`%1`)send error %2").arg(cmd.data()).arg(fmt(rc)));
    }
    QList<scpi*> done;
    auto bugs = scpi::wait("send()", 2000, {this}, true, done);
    if (bugs.count()) {
        throw HWException(bugs[0].message);
    }
}

QString scpi::query(QString cmd) {
    int rc;
    QList<scpi*> done;
    QByteArray buff(buff_size, 0);
    QByteArrayList rv;
    writeRaw(cmd.toUtf8());
    for (int i = 0; i < 2; ++i) {
        rc = recv(raw, buff.data(), buff_size, 0);
        //nonblocking: always rc != 0
        //qDebug() << cmd.toUtf8() << "recv" << rc << le;
        if (rc == SOCKET_ERROR) {
            int le = WSAGetLastError();
            if (le != WSAEWOULDBLOCK) {
                throw HWException(QString("query(`%1`)recv error %2").arg(cmd).arg(fmt(le)));
            }
        } else if (rc > buff_size) {
            throw HWException(QString("recv() rc {%1} > buff_size {%2}")
                              .arg(rc).arg(buff_size));
        }
        if (rc < 0) {
            auto bugs = scpi::wait(QString("query(`%1`)recv").arg(cmd), operation_timeout_ms,
                {this}, false, done);
            if (bugs.count()) {
                throw HWException(bugs[0].message + QString("buf data: %1, i: %2").arg(buff.data()).arg(i));
            }
            continue;
        }
        auto dst = buff.left(rc);
        //qDebug() << cmd.toUtf8() << "recv" << rc << dst.size() << dst;
        if (dst.size()) {
            rv.append(dst);
            break; // only one responce message?
        }
    }
    auto tmp = QString::fromUtf8(rv.join(""));
    qDebug() << QString("%1::query -> %2").arg(title).arg(tmp);
    QThread::currentThread()->msleep(msleep);
    return tmp;
}

QString scpi::fmt(int errcode) {
    DWORD src = DWORD(errcode);
    wchar_t * s = nullptr;
    DWORD rc = FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, src,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        reinterpret_cast<LPWSTR>(&s), 0, nullptr);
    QString rv;
    if (rc) {
        rv = QString("%1: %2").arg(errcode).arg(QString::fromWCharArray(s));
    } else {
        rv = QString("<WSA error %1>").arg(errcode);
    }
    LocalFree(s);
    rv = rv.simplified();
    while (rv.endsWith('.')) {
        rv = rv.chopped(1);
    }
    return QString("`%1`").arg(rv);
}

QString scpi::str(double src) {
    auto dst = QString::number(src, 'f', 6).left(12);
    if (dst.count() && (dst.back() == ".")) {
        dst = dst.left(dst.count() - 1);
    }
    return dst;
}

Bugs scpi::to_double(QString src, double * dst) {
    bool ok;
    *dst = src.toDouble(&ok);
    if (ok) return {};
    return bugs(QString("bad double `%1`").arg(src));
}

Bugs scpi::bugs(QString msg) {
    Bugs rv;
    for (int slot : slotes) {
        rv.append(Bug(title, slot, type, msg));
    }
    return rv;
}

QString scpi::error() {
    int rc;
    int opt;
    int optlen = sizeof opt;
    if (getsockopt(raw, SOL_SOCKET, SO_ERROR, reinterpret_cast<char*>(&opt), &optlen)) {
        rc = WSAGetLastError();
        return QString("scpi::error() %1 error on getsockopt(SO_ERROR)").arg(fmt(rc));
    }
    if (opt) {
        return fmt(opt);
    }
    return "";
}


//------QTCPSOCKET-----//
Bugs scpi::connectSocket(QSet<int> affected_slot_indices, int connectionTimeout, int operation_timeout)
{
    slotes = affected_slot_indices.toList();
    std::sort(slotes.begin(), slotes.end());
    operation_timeout_ms = operation_timeout;
    if (m_pSocket == nullptr)
        m_pSocket = new QTcpSocket();
    QObject::connect(m_pSocket,&QTcpSocket::disconnected,[=]{socketDisconnectedSlot();});
    QObject::connect(m_pSocket,
                     static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
                        [=](QAbstractSocket::SocketError socketError){socketErrorSlot(socketError);});
    QObject::connect(m_pSocket, &QTcpSocket::stateChanged, [=](QAbstractSocket::SocketState state){socketChangeState(state);});
    m_pSocket->connectToHost(host, port);
    //TODO: при кратковременном отключении кабеля не срабатывает сигнал дисконнекта
    if ( m_pSocket->waitForConnected(connectionTimeout) )
    {
        QThread::msleep(200);
        m_bConnected = true;
        ready = true;
    }
    else
    {
        return bugs(m_pSocket->errorString());
    }
    return {};
}

void scpi::socketChangeState(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::ConnectedState) {
        int fd = m_pSocket->socketDescriptor();
        DWORD  dwBytesRet = 0;

        struct tcp_keepalive   alive;    // your options for "keepalive" mode
        alive.onoff = TRUE;              // turn it on
        alive.keepalivetime = operation_timeout_ms;     // delay (ms) between requests, here is 30s, default is 2h (7200000)
        alive.keepaliveinterval = 10;  // delay between "emergency" ping requests, their number (6) is not configurable
        /* So with this config  socket will send keepalive requests every 30 seconds after last data transaction when everything is ok.
           If there is no reply (wire plugged out) it'll send 6 requests with 5s delay  between them and then close.
           As a result we will get disconnect after approximately 1 min timeout.
        */
        if (WSAIoctl(fd, SIO_KEEPALIVE_VALS, &alive, sizeof(alive), NULL, 0, &dwBytesRet, NULL, NULL) == SOCKET_ERROR) {
            qDebug() << "WSAIotcl(SIO_KEEPALIVE_VALS) failed with err#" <<  WSAGetLastError();
            return;
        }
    }
}

Bugs scpi::reconnectSocket(int nSlot, int number, int connectionTimeout)
{
    m_logger->log(QString("%1:Reconnecting IP:" + host).arg(title));
    if (m_pSocket == nullptr)
        m_pSocket = new QTcpSocket();
    {
        QObject::connect(m_pSocket,&QTcpSocket::disconnected,[=]{socketDisconnectedSlot();});
        QObject::connect(m_pSocket,
                         static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
                            [=](QAbstractSocket::SocketError socketError){socketErrorSlot(socketError);});
        QObject::connect(m_pSocket, &QTcpSocket::stateChanged, [=](QAbstractSocket::SocketState state){socketChangeState(state);});
    }
    m_pSocket->connectToHost(host, port);

    if ( m_pSocket->waitForConnected(connectionTimeout) )
    {
        QThread::msleep(200);
        m_bConnected = true;
        ready = true;
    }
    else
    {
        Bugs error;
//        QString deviceName;
//        switch (type) {
//        case Bug::Power:
//            deviceName = number == -1 ? "Источник питания": QString("Источник питания №%1").arg(number);
//            break;
//        case Bug::FGen:
//            deviceName = number == -1 ? "Генератор": QString("Генератор №%1").arg(number);
//            break;
//        default:
//            deviceName = "Устройство";
//            break;
//        }
        error.append(Bug(title, nSlot + 1, type, m_pSocket->errorString()));
        return error;
    }
    return {};
}

void scpi::logBugs(const Bugs &val)
{
    for (auto bug : val) {
        m_logger->log(bug.message + " type=" + QString(bug.type==0?"PS":"Gen") + " slot = " + QString::number(bug.slot));
    }
}

void scpi::socketDisconnectedSlot()
{
    if ( m_bForcefullyDisconnected )
        return;

    if (m_pSocket && m_bConnected)
    {
        QString strError = m_pSocket->errorString();

        m_logger->log(QString("%1:TcpSocket disconnected with error: %2").arg(title).arg(m_pSocket->errorString()));

        m_pSocket->abort();
        m_pSocket->disconnect();
        m_pSocket->deleteLater();
        m_pSocket = nullptr;
        m_bConnected = false;
    }
}

QString scpi::sendSCPICommand(QString strCommand, bool waitForResponce)
{
    QString result="";

    if ( writeToSocketSync(strCommand) && waitForResponce)
    {
        if (m_pSocket->waitForReadyRead(operation_timeout_ms))
        {
            m_bSockTimeoutDisabled = true;

            QByteArray responseData = m_pSocket->readAll();
            while (m_pSocket->waitForReadyRead(operation_timeout_ms))
                responseData += m_pSocket->readAll();

            m_bSockTimeoutDisabled = false;

            result = QString::fromLocal8Bit(responseData);
        }
    }
    if (result.length()>0)
        result = result.trimmed();

    return result;
}

bool scpi::writeToSocketSync(const QString& strCommand)
{
    bool retVal = false;

    if ( m_bConnected )
    {
        m_pSocket->write(strCommand.toLocal8Bit()+"\n");
        if ( !m_pSocket->waitForBytesWritten(operation_timeout_ms) )
        {
            m_logger->log(QString("%1:writeToSocketSync timeout").arg(title));
        }
        else
            retVal = true;
    }

    return retVal;
}

void scpi::socketErrorSlot(QAbstractSocket::SocketError socketError)
{
    if ( !(m_bSockTimeoutDisabled && socketError==QAbstractSocket::SocketError::SocketTimeoutError) )
    {
        if (!m_logger.isNull())
            m_logger->log(QString("%1:TcpSocket error: %2").arg(title).arg(m_pSocket->errorString()));
//            m_logger->log(QString("TcpSocket error: %1").arg(m_pSocket->errorString()));
    }
}

//------QTCPSOCKET-----//
