#ifndef BUGS_H
#define BUGS_H

#include <QException>
#include <QString>
#include <QList>


class HWException : public QException {
public:
    HWException(QString error);
    void raise() const override;
    HWException * clone() const override;
    QString msg = nullptr;
    virtual const char * what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override;
};

class Bug {
public:
    Bug(QString _device, int _slot, int _type, QString _message); /*
    _device - идентификатор устройства
    _slot - владелец ошибки
        - если владелец больше или равен нулю, то это индекс слота термокамеры
        - иначе, это инвертированный и смещённый на единицу индекс канала питания
        независимого от слота термокамеры (-1 -> [0]; -2 -> [1]; -3 -> [2])
    _type - тип (справочное значение)
    _message - текстовое описание ошибки
    */
    Bug(const Bug & o);
    QString device;
    int slot;
    int type;
    QString message;
    Bug & operator=(const Bug & o);
    // list of values for 'type' property:
    static const int Power = 0;
    static const int FGen = 1;
    static const int Thermocouple = 2;
    static const int Cyclogram = 3;
    static const int Thermocyclogram = 4;
};

typedef QList<Bug> Bugs;

#endif // BUGS_H
