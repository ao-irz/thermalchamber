#ifndef WIDGET_H
#define WIDGET_H

#include <QTimer>
#include <QWidget>
#include <QBoxLayout>
#include <QCloseEvent>
#include <QElapsedTimer>

#include "cfg/cfg.h"
#include "hardware.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(Cfg & cfg, QWidget * parent = nullptr);
    ~Widget();

public slots:
    void onBegin();
    void onPoll();
    void onNewValues();
    void onCheck();
    void onCheckTime();
    void onIpFromGui();

private:
    Ui::Widget * ui;
    QString logprefix;
    double load_r_from;
    double load_r_to;
    bool load_executed = false;
    QElapsedTimer et;
    QTimer * poll_timer = nullptr;
    int poll_count = 0;
    int check_idx = 0;
    QStringList check_list;
    Hardware * hw = nullptr;
    QSet<int> bad_slots;
    QList<int> work;
    QSet<int> bad_ep;
    QList<int> work_extra_power;
    void work_state_update(Bugs);
    void go(const Bugs & bugs, QString msg);
    void closeEvent(QCloseEvent * e);
    int fgen_idx = -1;
    GenForm::GenForm next_gen_form();
    int power_idx = -1;
    QHash<int, QList<double>> power_v;
    QHash<int, QList<double>> power_a;
    QString str(double v);
    Bugs onPollPower(int w, int ch, Power * p, QStringList & a);
    void enabledTo(QBoxLayout * layout, bool value);
};

#endif // WIDGET_H
