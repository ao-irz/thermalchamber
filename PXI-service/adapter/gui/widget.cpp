#include "local_cfg_values.h"
#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
#include <QStyle>
#include <QFile>
#include <QDesktopServices>

Widget::Widget(Cfg & cfg, QWidget * parent) : QWidget(parent), ui(new Ui::Widget) {
    ui->setupUi(this);
    enabledTo(ui->load_layout, false);
    hw = new Hardware();

    logprefix = cfg.logprefix;
    ui->json->setText(QString("json cfg: %1").arg(cfg.remote));
    if (power_ip_from_gui) {
        ui->ip->setText(power_cfg[0]);
        connect(ui->ip_button, SIGNAL(clicked()), this, SLOT(onIpFromGui()));
    } else {
        ui->ip->hide();
        ui->ip_button->hide();
        QTimer::singleShot(200, this, SLOT(onBegin()));
    }
    poll_timer = new QTimer(this);
    connect(poll_timer, SIGNAL(timeout()), this, SLOT(onPoll()));
    connect(ui->new_values_button, SIGNAL(clicked()), this, SLOT(onNewValues()));
    connect(ui->load_button, SIGNAL(clicked()), this, SLOT(onCheck()));
}

Widget::~Widget() {
    delete poll_timer;
    delete hw;
    delete ui;
}

void Widget::work_state_update(Bugs bugs) {
    for (auto bug : bugs) {
        if (bug.slot < 0) {
            bad_ep.insert(-bug.slot - 1);
        } else {
            bad_slots.insert(bug.slot);
        }
    }
    work.clear();
    work_extra_power.clear();
    for (int s = 0; s < hw->sampleCount(); ++s) {
        if (!bad_slots.contains(s)) {
            work.append(s);
        }
    }
    for (int s = 0; s < hw->extraPowerCount(); ++s) {
        if (!bad_ep.contains(s)) {
            work_extra_power.append(s);
        }
    }
    //qDebug() << "Widget::work_state_update" << "work" << work << ";" << "work_ep" << work_extra_power << bad_ep;
}

void Widget::go(const Bugs & bugs, QString msg) {
    work_state_update(bugs);
    if (!bugs.count()) return;
    ui->errors->addWidget(new QLabel(msg + ":"));
    for (auto bug : bugs) {
        auto m = bug.message.replace("\n\n", " ").replace("\n", " ");
        QString slot = (bug.slot < 0) ? QString("extra_power_%1").arg(-bug.slot - 1) :
                                        QString("slot_%1").arg(bug.slot);
        auto s = QString("%1:%2:hwtype_%3:%4").arg(bug.device).arg(slot).arg(bug.type).arg(m);
        auto l = new QLabel(s);
        l->setTextFormat(Qt::PlainText);
        l->setTextInteractionFlags(Qt::TextSelectableByMouse);
        ui->errors->addWidget(l);
        qDebug() << "Widget::go" << bug.message;
    }
    //ui->errors->addStretch(1);
}

void Widget::closeEvent(QCloseEvent * e) {
    poll_timer->stop();
    go(hw->close(), "after close");
    qDebug() << "hw object closed";
    e->accept();
}

void Widget::onBegin() {
    qDebug() << "Widget::onBegin";
    Bugs bugs;
    ui->subtitle->setText("инициализация...");
    qApp->processEvents();
    et.start();
    if (power_ip_from_gui) {
        bugs += hw->init(2, 25000, 105, {ui->ip->text().trimmed()}, {}, power_channel_map);
    } else {
        bugs += hw->init(3, 11000, 105,  power_cfg, fgen_cfg, power_channel_map);
    }
    ui->title->setText(QString("конфигурация: %1").arg(hw->configuration()));
    qDebug() << "hw object inited";
    auto t = QString("инициализация: %1 миллисекунд").arg(et.elapsed());
    ui->subtitle->setText(t);
    go(bugs, "after hw init");
    t = QString("%1 (%2 работающих слотов из %3)").arg(t).arg(work.count()).arg(hw->sampleCount());
    ui->subtitle->setText(t);
    bugs.clear();
    for (auto w : work) {
        bugs += hw->sample(w)->neutral();
    }
    go(bugs, "after neutral");
    onNewValues();
    poll_timer->start(1000);
    enabledTo(ui->load_layout, true);
}

Bugs Widget::onPollPower(int w, int ch, Power * p, QStringList & a) {
    Bugs bugs;
    QString group;
    double u_meas = -111, i_meas = -111;
    bugs += p->measure(&u_meas, &i_meas);
    double u = (power_v.contains(w) && (power_v[w].size() > ch)) ? power_v[w][ch] : -1;
    double u_from = u - (0.002 * abs(u) + 0.02);
    double u_to = u + (0.002 * abs(u) + 0.02);
    double i_from = 0, i_to = 0;
    if ((u > 0.010) && (load_r_from > 0) && (load_r_to > 0)) {
        i_from = u_from / load_r_to;
        i_to = u_to / load_r_from;
    }
    i_from -= (0.005 * abs(i_from) + 0.025);
    i_to += (0.005 * abs(i_to) + 0.025);
    group = (w < 0) ? "extra" : QString("slot_%1").arg(w, 2, 10, QLatin1Char('_'));
    a << QString("%1 power_%2 %3V ⇒ %4V Δ%5V %6A %7 канал_%8")
         .arg(group)
         .arg(ch, 2, 10, QLatin1Char('_'))
         .arg(str(u)).arg(str(u_meas)).arg(str(abs(u - u_meas)))
         .arg(str(i_meas)).arg(p->isOn() ? "вкл" : "выкл")
         .arg(p->devChTitle());
    if (load_executed && ((u_meas < u_from) || (u_meas > u_to) || (i_meas < i_from) || (i_meas > i_to))) {
        if (!check_list.count()) {
            check_list << QString("U_set, U_from, U_to, U_meas, ,I_from, I_to, I_meas, , , internal_channel\n");
        }
        check_list << QString("%1, %2, %3, %4, *, %5, %6, %7, *, %8_power_%9, ic_%10\n")
                      .arg(u).arg(u_from, 0, 'f', 4).arg(u_to, 0, 'f', 4)
                      .arg(u_meas, 0, 'f', 4)
                      .arg(i_from, 0, 'f', 4).arg(i_to, 0, 'f', 4)
                      .arg(i_meas, 0, 'f', 4)
                      .arg(group).arg(ch)
                      .arg(p->devChTitle());
    }
    return bugs;
}

void Widget::enabledTo(QBoxLayout *layout, bool value) {
    for (int i = 0; i < layout->count(); ++i) {
        auto w = layout->itemAt(i)->widget();
        if (!w) continue;
        w->setEnabled(value);
    }
}

void Widget::onPoll() {
    //qDebug() << " Widget::onPoll";
    Bugs bugs;
    poll_count = (poll_count + 1) % 11;
    QStringList a = {QString("Часто изменяемые значения%1").arg(QString(".").repeated(poll_count + 1))};
    et.start();
    for (auto w : work) {
        auto sample = hw->sample(w);
        for (int ch = 0; ch < sample->powers().count(); ++ch) {
            bugs += onPollPower(w, ch, sample->power(ch), a);
        }
    }
    for (auto w : work_extra_power) {
        bugs += onPollPower(-1, w, hw->extraPower(w), a);
    }
    a << QString("-> выполнено за %1 миллисекунд").arg(et.elapsed());
    go(bugs, "after poll");
    a << QString("-> %1 работающих слотов из %2").arg(work.count()).arg(hw->sampleCount());
    ui->value_a->setText(a.join("\n"));
}

GenForm::GenForm Widget::next_gen_form() {
    fgen_idx = (fgen_idx + 1) % GenForm::items.size();
    return GenForm::items.keys()[fgen_idx];
}

QString Widget::str(double v) {
    auto rv = QString("%1").arg(v, 0, 'f', 2);
    if (rv.size() && (rv[0] != '-')) {
        rv = ' ' + rv;
    }
    return rv;
}

void Widget::onNewValues() {
    qDebug() << "onNewValues" << "work" << work;
    ui->new_values_button->setEnabled(false);
    if (!poll_timer->isActive()) {
        poll_timer->start(1000);
    }
    Bugs bugs;
    QStringList b = {"Редко изменяемые значения:"};
    power_v.clear();
    power_idx = (power_idx + 1) % 2;
    double power_base = power_idx ? 10.5 : 3.2;
    b << QString("power_base == %1").arg(str(power_base));
    if (!work.count()) {
        b << GenForm::items[next_gen_form()];
    }
    et.start();
    for (auto w : work) {
        auto f = next_gen_form();
        auto g = hw->sample(w)->fgen;
        if (g) {
            double freq = 100 * (w + 1);
            double ampl = (power_base + 1.8) + 0.1 * w;
            double shift = 0.25 * ampl;
            double dc = 50;
            bugs += g->off();
            bugs += g->on();
            bugs += g->put(f, ampl, shift, freq, dc);
            b << QString("slot_%1 %2V %3V %4Hz %5% %6 %7")
                 .arg(w, 2, 10, QLatin1Char('_'))
                 .arg(str(ampl))
                 .arg(str(shift))
                 .arg(freq)
                 .arg(str(dc))
                 .arg(g->isOn() ? "вкл" : "выкл")
                 .arg(GenForm::items[f]);
        }
    }
#if 0
    // debug without ready slot
    hw->sample(0)->fgen->form({-128, -113, -97, -81, -65, -49, -33, -17, -1, 15, 31, 47, 63, 79,
                95, 111, 127, 127, 127, 127, 126, 124, 120, 112, 96, 64, 0},
                              4,
                              1,
                              2000
                              );
#endif
#if 0
    // debug without ready slot
    hw->sample(0)->fgen->put(FGen::triangle, 3, -1, 3000, 0);
#endif
    b << QString("-> выполнено (FGen) за %1 миллисекунд").arg(et.elapsed());
    et.start();
    for (auto w : work) {
        power_v[w] = {};
        auto s = hw->sample(w);
        for (int ch = 0; ch < s->powerCount(); ++ch) {
            auto p = s->power(ch);
            double dst = power_base + 0.5 * w + 0.1 * ch;
            bugs += p->off();
            bugs += p->put(dst, Power::limitI(dst));
            bugs += p->on();
            power_v[w].append(dst);
        }
    }
    power_v[-1] = {};
    for (auto w : work_extra_power) {
        auto p = hw->extraPower(w);
        //if (p->isBad()) continue;
        double dst = power_base + 0.7 * w;
        bugs += p->off();
        bugs += p->put(dst, Power::limitI(dst));
        bugs += p->on();
        power_v[-1].append(dst);
    }
    b << QString("-> выполнено (Power) за %1 миллисекунд").arg(et.elapsed());
    go(bugs, "after new values");
    b << QString("-> %1 работающих слотов из %2").arg(work.count()).arg(hw->sampleCount());
    ui->value_b->setText(b.join("\n"));
    ui->new_values_button->setEnabled(true);
}

static double get_double(QLineEdit * le) {
    double r_load;
    bool ok;
    auto tmp = le->text().trimmed();
    if (tmp.length()) {
        r_load = tmp.toDouble(&ok);
        if (!ok) {
            return -11;
        }
    } else {
        return -1;
    }
    return r_load;
}

void Widget::onCheck() {
    load_r_from = get_double(ui->load_from);
    load_r_to = get_double(ui->load_to);
    //qDebug() << r_load_from << r_load_to;
    if ((load_r_from < -10) || (load_r_to < -10) || ((load_r_from * load_r_to) < 0)
            || (load_r_from > load_r_to)) {
        return;
    }
    Bugs bugs;
    poll_timer->stop();
    enabledTo(ui->load_layout, false);
    ui->new_values_button->setEnabled(false);
    check_list.clear();
    check_idx = 1;
    load_executed = true;
    QTimer::singleShot(1000, this, SLOT(onCheckTime()));
}

void Widget::onCheckTime() {
    const int count = 20;
    double v = check_idx;
    double a = Power::limitI(v);
    bool overload = false;
    double u_to = v + (0.002 * abs(v) + 0.02);
    double i_max_meas = 0;
    if ((v > 0.010) && (load_r_from > 0) && (load_r_to > 0)) {
        i_max_meas = u_to / load_r_from;
        i_max_meas += (0.005 * abs(i_max_meas) + 0.025);
#if 0
        overload = i_max_meas > a;
#else
        overload = (v / load_r_from) > a;
#endif
    }
    if ((check_idx >= (count + 1)) || overload) {
        load_executed = false;
        auto csv = logprefix + ".csv";
        auto f = QFile(csv);
        if (f.open(QIODevice::WriteOnly)) {
            for (auto l : check_list) {
                f.write(l.toUtf8());
            }
            if (!check_list.count()) {
                f.write("Issues not found\n");
            }
            f.close();
            QDesktopServices::openUrl(QUrl::fromLocalFile(csv));
        }
        enabledTo(ui->load_layout, true);
        ui->new_values_button->setEnabled(true);
        return;
    }
    Bugs bugs;
    for (auto w : work) {
        power_v[w] = {};
        power_a[w] = {};
        auto s = hw->sample(w);
        for (int ch = 0; ch < s->powerCount(); ++ch) {
            auto p = s->power(ch);
            bugs += p->put(v, a);
            power_v[w].append(v);
            power_a[w].append(a);
        }
    }
    power_v[-1] = {};
    power_a[-1] = {};
    for (auto w : work_extra_power) {
        bugs += hw->extraPower(w)->put(v, a);
        power_v[-1].append(v);
        power_a[-1].append(a);
    }
    go(bugs, "after onCheckTime");
    check_idx++;
    QTimer::singleShot(3000, this, SLOT(onPoll()));
    QTimer::singleShot(3100, this, SLOT(onCheckTime()));
}

void Widget::onIpFromGui() {
    ui->ip->setEnabled(false);
    ui->ip_button->setEnabled(false);    
    QTimer::singleShot(20, this, SLOT(onBegin()));
}
