#include "widget.h"
#include <QApplication>
#include <QtDebug>

#include "cfg/cfg.h"
#include "log/log.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    auto l = log_init();
    auto cfg = Cfg("gui", l.prefix);
    qDebug() << cfg.valid << cfg.remote;
    Widget w{cfg};
    w.showMaximized();
    return a.exec();
}
