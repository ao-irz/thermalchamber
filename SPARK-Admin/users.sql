CREATE TABLE public.users (
  id        smallserial NOT NULL,
  username  text NOT NULL,
  pwd       text NOT NULL,
  fk_role   integer NOT NULL,
  /* Keys */
  CONSTRAINT users_pkey
    PRIMARY KEY (id), 
  CONSTRAINT users_unique
    UNIQUE (username),
  /* Foreign keys */
  CONSTRAINT fk_role_key
    FOREIGN KEY (fk_role)
    REFERENCES public.roles(id)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
);

INSERT INTO users (id, username, pwd, fk_role) VALUES
  (1, 'Admin', 'c543ac7df8f932bec295169607445344'/*gbnfy677*/, 0),
  (2, 'Пользователь', 'b5b73fae0d87d8b4e2573105f8fbe7bc'/*user1234*/, 1);
