﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

#define INI_FILE_NAME       QStringLiteral("settings.ini")

#define INI_DB_GROUP    QStringLiteral("DB")
#define INI_DB_ADDRESS    QStringLiteral("dbAddress")
#define INI_DB_PORT    QStringLiteral("dbPort")
#define INI_DB_DBNAME    QStringLiteral("dbName")
#define INI_DB_USER    QStringLiteral("dbUser")
#define INI_DB_PASSWORD    QStringLiteral("dbPassword")

#define INI_ERROR QStringLiteral("Файл settings.ini отсутствует или содержит неверную информацию")

class Settings : public QObject
{
    Q_OBJECT

public:
    Settings(const QString &selfPath, QObject *parent);
    virtual ~Settings();


    QString groupValueContents(QString valueKey);
    QString& checkGroupConsistent();

private:

    QString m_selfPath;
    QMap<QString,QString> m_dbSettings;
    QString m_resultValue;

    QSettings* m_pSettings;
    void readGroupContents();
};

#endif // SETTINGS_H
