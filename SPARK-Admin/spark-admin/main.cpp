#include "sparkadminmain.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  SparkAdminMain w(a.applicationDirPath());
  w.show();
  return a.exec();
}
