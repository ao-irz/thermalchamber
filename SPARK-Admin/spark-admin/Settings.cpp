﻿#include <QFile>
#include <QDebug>
#include "Settings.h"

Settings::Settings(const QString &selfPath, QObject * parent) :
    QObject(parent)
    ,m_selfPath(selfPath)
    ,m_resultValue("")
{
  if ( QFile::exists(selfPath + "/" + INI_FILE_NAME) )
  {
    m_pSettings = new QSettings(selfPath + "/" + INI_FILE_NAME, QSettings::IniFormat, this);
    readGroupContents();
  }
  else
    m_resultValue = INI_ERROR;
}

Settings::~Settings()
{
  delete m_pSettings;
}

void Settings::readGroupContents()
{

  if (m_pSettings->childGroups().contains(INI_DB_GROUP))
  {
    m_pSettings->beginGroup(INI_DB_GROUP);
    QStringList keys = m_pSettings->childKeys();
    for(auto & k : keys)
    {
      QVariant v = m_pSettings->value(k);
      if ( QString(v.typeName())=="QString" )
        m_dbSettings.insert(k,v.toString());
      else
        m_dbSettings.insert(k,"");
    }
    m_pSettings->endGroup();
  }
}

QString Settings::groupValueContents(QString valueKey)
{
  return m_dbSettings.value(valueKey,"");
}

QString& Settings::checkGroupConsistent()
{
  if ( m_resultValue.length()==0 )
  {
    if ( m_dbSettings.value(INI_DB_ADDRESS).length()==0 ||
         m_dbSettings.value(INI_DB_PORT).length()==0 ||
         m_dbSettings.value(INI_DB_DBNAME).length()==0 ||
         m_dbSettings.value(INI_DB_USER).length()==0 ||
         m_dbSettings.value(INI_DB_PASSWORD).length()==0
       )

    m_resultValue = INI_ERROR;
  }

  return m_resultValue;
}

