#ifndef SPARKADMINMAIN_H
#define SPARKADMINMAIN_H

#include <QDialog>
#include "Settings.h"
#include "SparkDb.h"
#include <QGridLayout>
#include <QComboBox>
#include <QListWidget>
#include <QPushButton>
#include <QTimer>

#include "createuserdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SparkAdminMain; }
QT_END_NAMESPACE

class SparkAdminMain : public QDialog
{
    Q_OBJECT

  public:
    SparkAdminMain(QString appPath, QWidget *parent = nullptr);
    ~SparkAdminMain();

  public slots:
    void errorFieldTextChangedSlot();
    void userCreateSlot(QString userName, QString userPwd, int userRole);
    void userPwdUpdatedSlot(QString userName, QString userPwd);

  private:
    Ui::SparkAdminMain *ui;
    QString m_appPath;
    QString m_settingsPath;
    Settings* m_settings{nullptr};
    SparkDb* m_db{nullptr};
    QString m_strCurrentUser;
    int m_userRole;
    QString m_userPwd;
    QTimer m_clearErrorTextTimer;

    QGridLayout* m_pGrid;
    QComboBox* m_pRoleBox;
    QListWidget* m_usersList;
    QPushButton* m_pSaveButton;
    QPushButton* m_pChangePwdButton;
    QPushButton* m_pCreateNewButton;
    QPushButton* m_pDeleteButton;

    CreateUserDialog* m_pCreateUserDialog;

    void setupUi();
    void fillContents();

  private slots:
    void userChangedSlot(QListWidgetItem *current, QListWidgetItem *previous);
    void userRoleChangedSlot(int index);

    void changeUserRoleClickedSlot(bool checked);
    void deleteUserClickedSlot(bool checked);
    void changePwdButtonClickedSlot(bool checked);
    void createUserButtonClickedSlot(bool checked);

};
#endif // SPARKADMINMAIN_H
