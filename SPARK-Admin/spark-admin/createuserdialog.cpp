#include "createuserdialog.h"
#include "ui_createuserdialog.h"
#include <QMessageBox>

CreateUserDialog::CreateUserDialog(QString strTitle, QWidget *parent, QString strUserName, QString strOldPass) :
  QDialog(parent),
  ui(new Ui::CreateUserDialog)
  ,m_strOldPass(strOldPass)
{
  ui->setupUi(this);
  setWindowTitle(strTitle);
  if ( strUserName!="" )
  {
    m_openMode=UPDATE_PWD_MODE;
    ui->m_userName->setText(strUserName);
    ui->m_userName->setEnabled(false);
    ui->m_pUserRole->setEnabled(false);
  }
  else
  {
    m_openMode=CREATE_USER_MODE;
    ui->m_pUserRole->addItem("Администратор",0);
    ui->m_pUserRole->addItem("Пользователь",1);
    ui->m_pUserRole->setCurrentIndex(1);
  }
  connect(ui->m_saveButton,&QAbstractButton::clicked,
          this,&CreateUserDialog::acceptedSlot);
  connect(ui->m_cancelButton,&QAbstractButton::clicked,
          this,&CreateUserDialog::rejectedSlot);
  connect(ui->m_showPassCheck,&QCheckBox::stateChanged,
          [=](int newState){ui->m_pwdEdit->setEchoMode(newState==Qt::Checked?QLineEdit::Normal:QLineEdit::Password);});
  connect(ui->m_showConfirmedCheck,&QCheckBox::stateChanged,
          [=](int newState){ui->m_pwdConfirm->setEchoMode(newState==Qt::Checked?QLineEdit::Normal:QLineEdit::Password);});
}

CreateUserDialog::~CreateUserDialog()
{
  delete ui;
}

void CreateUserDialog::acceptedSlot()
{
  if ( ui->m_userName->text().length()==0 )
  {
    QMessageBox::critical(this,"Ошибка","Не указано имя пользователя");
    return;
  }

  if ( ui->m_pwdEdit->text().length()<8 )
  {
    QMessageBox::critical(this,"Ошибка","Не допускается пароль короче 8-ми символов");
    return;
  }

  if ( ui->m_pwdEdit->text()!=ui->m_pwdConfirm->text() )
  {
    QMessageBox::critical(this,"Ошибка","Пароли в 2-х полях не совпадают");
    return;
  }

  if ( ui->m_pwdEdit->text()==m_strOldPass )
  {
    QMessageBox::critical(this,"Ошибка","Пароль совпадает с предыдущим");
    return;
  }

  switch(m_openMode)
  {
    case CREATE_USER_MODE:
    {
      emit userCreateSignal(ui->m_userName->text(),ui->m_pwdEdit->text(), ui->m_pUserRole->currentData().toInt());
      break;
    }
    case UPDATE_PWD_MODE:
    {
      emit userPwdUpdatedSignal(ui->m_userName->text(),ui->m_pwdEdit->text());
    }
  }

  emit accept();
}

void CreateUserDialog::rejectedSlot()
{
  emit reject();
}
