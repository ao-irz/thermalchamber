#include "sparkadminmain.h"
#include "ui_sparkadminmain.h"
#include <QMessageBox>
#include <QSpacerItem>
#include <QDebug>

#define SETTINGS_PATH "/home/romanyn/Projects/thermalchamber"

SparkAdminMain::SparkAdminMain(QString appPath, QWidget *parent)
  : QDialog(parent)
  , ui(new Ui::SparkAdminMain)
  , m_appPath(appPath)

{

#ifdef SETTINGS_PATH
  m_settingsPath = SETTINGS_PATH;
#else
  m_settingsPath = m_appPath;
#endif

  ui->setupUi(this);
  setupUi();
  ui->m_errorField->viewport()->setAutoFillBackground(false);

  m_clearErrorTextTimer.setSingleShot(true);
  m_clearErrorTextTimer.setInterval(7000);
  connect(&m_clearErrorTextTimer,&QTimer::timeout,
          [=](){ui->m_errorField->clear();});
  connect(ui->m_errorField,&QPlainTextEdit::textChanged,
          this,&SparkAdminMain::errorFieldTextChangedSlot);

  m_settings = new Settings(m_settingsPath,this);
  if ( m_settings->checkGroupConsistent()!="")
  {
    ui->m_errorField->setPlainText( m_settings->checkGroupConsistent() );
    return;
  }

  m_db = new SparkDb(m_settings,this);
  ui->m_errorField->setPlainText(m_db->getError());

  if ( m_db->isOpened() )
    fillContents();

}

SparkAdminMain::~SparkAdminMain()
{
  if ( m_db )
    delete m_db;

  if ( m_settings )
    delete m_settings;

  delete ui;
}

void SparkAdminMain::errorFieldTextChangedSlot()
{
  if ( ui->m_errorField->toPlainText().length()>0 )
    m_clearErrorTextTimer.start();
}

void SparkAdminMain::userCreateSlot(QString userName, QString userPwd, int userRole)
{
  QString retVal = m_db->createNewUser(userName, userPwd, userRole);
  if ( retVal.length()==0 )
  {
    retVal = "Пользователь '"+userName+"' успешно создан";
    fillContents();
    QList<QListWidgetItem *> itemsList = m_usersList->findItems(userName,Qt::MatchExactly);
    if ( itemsList.count()>0 )
      m_usersList->setCurrentItem(itemsList[0]);
  }

  ui->m_errorField->setPlainText(retVal);
}

void SparkAdminMain::userPwdUpdatedSlot(QString userName, QString userPwd)
{
  QString retVal = m_db->updateUserPwd(userName, userPwd);
  if ( retVal.length()==0 )
    retVal = "Пароль пользователя '"+userName+"' успешно изменен";

  ui->m_errorField->setPlainText(retVal);
}

void SparkAdminMain::setupUi()
{
  m_usersList = ui->m_usersList;
  connect(m_usersList,&QListWidget::currentItemChanged,
          this,&SparkAdminMain::userChangedSlot);

  m_pGrid = ui->gridLayout;
  m_pGrid->addWidget(new QLabel("Роль:",this),0,0,Qt::AlignLeft);
  m_pRoleBox = new QComboBox(this);
  m_pRoleBox->addItem("Администратор",0);
  m_pRoleBox->addItem("Пользователь",1);
  m_pGrid->addWidget(m_pRoleBox,0,1);
  connect(m_pRoleBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,&SparkAdminMain::userRoleChangedSlot);

  m_pGrid->addItem(new QSpacerItem(1,1,QSizePolicy::MinimumExpanding,QSizePolicy::Fixed),0,2);
  m_pGrid->addItem(new QSpacerItem(1,1,QSizePolicy::Fixed,QSizePolicy::MinimumExpanding),1,0);

  m_pSaveButton = new QPushButton("Сохранить роль",this);
  m_pGrid->addWidget(m_pSaveButton,2,0);
  m_pChangePwdButton = new QPushButton("Сменить пароль",this);
  m_pGrid->addWidget(m_pChangePwdButton,2,1);
  m_pCreateNewButton = new QPushButton("Новый пользователь",this);
  m_pGrid->addWidget(m_pCreateNewButton,2,2);
  m_pDeleteButton = new QPushButton("Удалить пользователя",this);
  m_pGrid->addWidget(m_pDeleteButton,2,3);

  connect(m_pSaveButton,&QAbstractButton::clicked,
          this,&SparkAdminMain::changeUserRoleClickedSlot);
  connect(m_pChangePwdButton,&QAbstractButton::clicked,
          this,&SparkAdminMain::changePwdButtonClickedSlot);
  connect(m_pCreateNewButton,&QAbstractButton::clicked,
          this,&SparkAdminMain::createUserButtonClickedSlot);
  connect(m_pDeleteButton,&QAbstractButton::clicked,
          this,&SparkAdminMain::deleteUserClickedSlot);
}

void SparkAdminMain::fillContents()
{
  m_usersList->clear();
  QStringList usersList = m_db->readUsersList();
  m_usersList->addItems(usersList);
  m_usersList->setCurrentRow(0);
}

void SparkAdminMain::userChangedSlot(QListWidgetItem *current, QListWidgetItem *previous)
{
  //qDebug() << "userChangedSlot";
  if ( !current )
    return;

  Q_UNUSED(previous)

  m_strCurrentUser = current->text();
  QPair<int,QString> userData = m_db->getUserCredentials(m_strCurrentUser);
  m_userRole = userData.first;
  m_userPwd = userData.second;

  m_pChangePwdButton->setEnabled(m_strCurrentUser.toLower()!="admin");
  m_pDeleteButton->setEnabled(m_strCurrentUser.toLower()!="admin");
  m_pRoleBox->setEnabled(m_strCurrentUser.toLower()!="admin");
  m_pSaveButton->setEnabled( m_pRoleBox->currentData().toInt()!=m_userRole &&
                             m_strCurrentUser.toLower()!="admin" );

  int userRoleIdx = m_pRoleBox->findData(m_userRole);
  if ( userRoleIdx!=-1 )
    m_pRoleBox->setCurrentIndex(userRoleIdx);

}

void SparkAdminMain::userRoleChangedSlot(int index)
{
  m_pSaveButton->setEnabled( m_pRoleBox->itemData(index).toInt()!=m_userRole &&
                             m_strCurrentUser.toLower()!="admin" );

}

void SparkAdminMain::changeUserRoleClickedSlot(bool checked)
{
  Q_UNUSED(checked)

  int newRole = m_pRoleBox->currentData().toInt();

  m_db->changeUserRole(m_strCurrentUser, newRole);

  userChangedSlot(m_usersList->currentItem(),nullptr);

  ui->m_errorField->setPlainText("Роль пользователя изменена");

}

void SparkAdminMain::deleteUserClickedSlot(bool checked)
{
  Q_UNUSED(checked)

  if (QMessageBox::question(this,"Удаление пользователя",
       "Вся информация о пользователе '"+m_strCurrentUser+"' будет удалена")==QMessageBox::StandardButton::No )
    return;

  QString retVal = m_db->deleteUser(m_strCurrentUser);
  if ( retVal.length()==0 )
  {
    fillContents();
    m_usersList->setCurrentRow(0);
  }

  ui->m_errorField->setPlainText(retVal);
}

void SparkAdminMain::changePwdButtonClickedSlot(bool checked)
{
  Q_UNUSED(checked)

  m_pCreateUserDialog = new CreateUserDialog("Сменить пароль",this,m_strCurrentUser,m_userPwd);
  connect(m_pCreateUserDialog,&CreateUserDialog::userPwdUpdatedSignal,
          this,&SparkAdminMain::userPwdUpdatedSlot);
  m_pCreateUserDialog->exec();
  m_pCreateUserDialog->deleteLater();
}

void SparkAdminMain::createUserButtonClickedSlot(bool checked)
{
  Q_UNUSED(checked)

  m_pCreateUserDialog = new CreateUserDialog("Создать пользователя",this);
  connect(m_pCreateUserDialog,&CreateUserDialog::userCreateSignal,
          this,&SparkAdminMain::userCreateSlot);
  m_pCreateUserDialog->exec();
  m_pCreateUserDialog->deleteLater();

}

