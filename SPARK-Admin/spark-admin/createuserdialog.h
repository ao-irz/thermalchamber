#ifndef CREATEUSERDIALOG_H
#define CREATEUSERDIALOG_H

#include <QDialog>

namespace Ui {
class CreateUserDialog;
}

class CreateUserDialog : public QDialog
{
    Q_OBJECT

  public:
    explicit CreateUserDialog(QString strTitle, QWidget *parent = nullptr,
                              QString strUserName="", QString strOldPass="");
    ~CreateUserDialog();

  signals:
    void userCreateSignal(QString userName, QString userPwd, int userRole);
    void userPwdUpdatedSignal(QString userName, QString userPwd);

  private:
    Ui::CreateUserDialog *ui;
    QString m_strOldPass;
    enum openMode{CREATE_USER_MODE, UPDATE_PWD_MODE};
    openMode m_openMode;

  private slots:
    void acceptedSlot();
    void rejectedSlot();

};

#endif // CREATEUSERDIALOG_H
