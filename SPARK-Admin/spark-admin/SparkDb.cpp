#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>
#include "SparkDb.h"
#include <QDebug>
#include <QCryptographicHash>

SparkDb::SparkDb(Settings* settings, QObject *parent) :
    QObject(parent),
    m_settings(settings)
{
    dbConnect();
}

SparkDb::~SparkDb()
{
		if (m_db.isOpen())
      m_db.close();
}

QString &SparkDb::getError()
{
  return m_strErrorMsg;
}

QStringList &SparkDb::readUsersList()
{
  QSqlQuery query(m_db);
  m_strUsersList.clear();

  QString strSql = "select username from public.users order by 1";
  query.exec(strSql);

  QString userName;

  while (query.next())
  {
    userName = query.value("username").toString();
    m_strUsersList.append(userName);
  }
  query.finish();

  return m_strUsersList;

}

QPair<int, QString> SparkDb::getUserCredentials(QString &userName)
{
  QPair<int, QString> retVal(-1,"");

  QSqlQuery query(m_db);

  QString strSql = "select u.pwd, u.fk_role from public.users u where userName='"+userName+"'";
  query.exec(strSql);

  if (query.next())
  {
    retVal.first = query.value("fk_role").toInt();
    retVal.second = query.value("pwd").toString();
  }
  query.finish();

  return retVal;

}

void SparkDb::changeUserRole(QString &userName, int &newRole)
{
  QSqlQuery query(m_db);

  QString strSql = "update public.users set fk_role="+QString::number(newRole)+" where username='"+userName+"'";
  query.exec(strSql);
}

QString SparkDb::createNewUser(QString &userName, QString &userPwd, int &userRole)
{
  QString retVal="";
  QSqlQuery query(m_db);
  QByteArray encryptedPwd = QCryptographicHash::hash(QByteArray(userPwd.toLatin1()),QCryptographicHash::Md5).toHex();

  QString strSql = "insert into public.users(username,pwd,fk_role) values('"+userName+"','"+
                   QString(encryptedPwd)+"',"+QString::number(userRole)+")";
  if (!query.exec(strSql))
    retVal = query.lastError().text();

  return retVal;
}

QString SparkDb::updateUserPwd(QString &userName, QString &userPwd)
{
  QString retVal="";
  QSqlQuery query(m_db);
  QByteArray encryptedPwd = QCryptographicHash::hash(QByteArray(userPwd.toUtf8()),QCryptographicHash::Md5).toHex();

  QString strSql = "update public.users set pwd='"+QString(encryptedPwd)+"' where username='"+userName+"'";
  if (!query.exec(strSql))
    retVal = query.lastError().text();

  return retVal;
}

QString SparkDb::deleteUser(QString &userName)
{
  QString retVal="";
  QSqlQuery query(m_db);

  QString strSql = "delete from public.users where username='"+userName+"'";
  if (!query.exec(strSql))
    retVal = query.lastError().text();

  return retVal;
}

bool SparkDb::dbConnect()
{
		m_db = QSqlDatabase::addDatabase("QPSQL");
    m_db.setHostName(m_settings->groupValueContents(INI_DB_ADDRESS));
    m_db.setPort(m_settings->groupValueContents(INI_DB_PORT).toInt());
    m_db.setDatabaseName(m_settings->groupValueContents(INI_DB_DBNAME));
    m_db.setUserName(m_settings->groupValueContents(INI_DB_USER));
    m_db.setPassword(m_settings->groupValueContents(INI_DB_PASSWORD));

    if (!m_db.open())
		{
      m_strErrorMsg = m_db.lastError().text();
      return false;
    }
    else
      m_strErrorMsg = "Соединение с БД установлено";

    return true;
}

