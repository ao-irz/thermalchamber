#ifndef DB_PostgresH
#define DB_PostgresH

#include <QObject>
#include <QSqlDatabase>
#include <QDateTime>
#include "Settings.h"
#include <QPointer>

class SparkDb : public QObject
{
    Q_OBJECT

public:
    SparkDb(Settings* settings, QObject *parent);

    virtual ~SparkDb();
    QString& getError();

    bool isOpened()
    {
      return m_db.isOpen();
    }

    QStringList& readUsersList();
    QPair<int,QString> getUserCredentials(QString& userName);

    void changeUserRole(QString& userName, int& newRole);
    QString createNewUser(QString& userName, QString& userPwd, int& userRole);
    QString updateUserPwd(QString& userName, QString& userPwd);
    QString deleteUser(QString& userName);

private:

    Settings* m_settings;

    QSqlDatabase m_db;
    bool m_bDbOk;

    bool dbConnect();
    QString m_strErrorMsg;
    QStringList m_strUsersList;
};

#endif // DB_PostgresH
