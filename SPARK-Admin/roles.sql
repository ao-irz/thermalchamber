CREATE TABLE public.roles (
  id      integer NOT NULL PRIMARY KEY,
  "role"  text NOT NULL,
  /* Keys */
  CONSTRAINT roles_pkey
    PRIMARY KEY (id)
);

INSERT INTO roles (id, "role") VALUES
  (0, 'Administrator'),
  (1, 'User');